import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatTableModule } from '@angular/material/table';
import { MatSortModule } from '@angular/material/sort';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatTooltipModule } from '@angular/material/tooltip';

export const MATERIAL_MODULES = [
	MatCardModule,
	MatButtonModule,
	MatIconModule,
	MatInputModule,
	MatTableModule,
	MatSortModule,
	MatPaginatorModule,
	MatTooltipModule,
];
