import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './app/app.module';
import { environment } from '@environment/environment';
import { environmentLoader as environmentLoaderPromise } from './environments/environmentLoader';
import { take } from 'rxjs/operators';
import { NgxLoggerLevel } from 'ngx-logger';

environmentLoaderPromise()
	.pipe(take(1))
	.subscribe((env: any) => {
		if (env.properties.param.environment === 'prod') {
			console.log('============ Prod environment ==============');
			enableProdMode();
		}
		environment.service = env.properties.service;
		environment.param = env.properties.param;
		environment.log.level =
			NgxLoggerLevel[env.properties.log.level as keyof typeof NgxLoggerLevel];
		environment.log.serverLogLevel =
			NgxLoggerLevel[env.properties.log.serverLogLevel as keyof typeof NgxLoggerLevel];

		platformBrowserDynamic()
			.bootstrapModule(AppModule)
			.catch((err) => console.error(err));
	});

platformBrowserDynamic()
	.bootstrapModule(AppModule)
	.then((ref) => {
		// Ensure Angular destroys itself on hot reloads.
		if (window['ngRef']) {
			window['ngRef'].destroy();
		}
		window['ngRef'] = ref;

		// Otherwise, log the boot error
	})
	.catch((err) => console.error(err));
