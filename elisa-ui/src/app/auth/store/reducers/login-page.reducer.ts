import { createReducer, on } from '@ngrx/store';
import { authApiActions, loginPageActions } from '../actions';

export const loginPageFeatureKey = 'loginPage';

export interface State {
	readonly error: string;
	readonly pending: boolean;
}

export const initialState: State = {
	error: null,
	pending: false,
};

export const reducer = createReducer(
	initialState,
	on(loginPageActions.login, (state) => ({
		...state,
		error: null,
		pending: true,
	})),

	on(authApiActions.loginSuccess, (state) => ({
		...state,
		error: null,
		pending: false,
	})),
	on(authApiActions.loginFailure, (state, { error }) => ({
		...state,
		error: JSON.stringify(error),
		pending: false,
	}))
);

export const getError = (state: State) => state.error;
export const getPending = (state: State) => state.pending;
