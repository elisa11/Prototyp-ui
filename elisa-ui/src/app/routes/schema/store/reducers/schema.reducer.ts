import { createReducer, on } from '@ngrx/store';
import { createEntityAdapter, EntityAdapter, EntityState } from '@ngrx/entity';
import { Version } from '@elisa11/elisa-api-angular-client';
import {
	searchParentSchemasSuccess,
	searchSchemasSuccess,
} from '../actions/schema-api.actions';
import { loadSchemasDialogSuccess } from '@core/store/actions/schema.actions';
import { deleteSchemaDone } from '../actions/new-schema.actions';

export const schemaFeatureKey = 'schema';

export interface State extends EntityState<Version> {
	readonly count: number;
}

export const adapter: EntityAdapter<Version> = createEntityAdapter<Version>({
	selectId: (version: Version) => version.id,
	sortComparer: false,
});

export const initialState: State = adapter.getInitialState({
	count: 0,
});

export const reducer = createReducer(
	initialState,
	on(searchSchemasSuccess, (state, { versions }) =>
		adapter.upsertMany(versions.results, {
			...state,
			count: versions.count,
		})
	),
	on(searchParentSchemasSuccess, (state, { versions }) =>
		adapter.upsertMany(versions, state)
	),
	on(loadSchemasDialogSuccess, (state, { data }) =>
		adapter.addOne(data, { ...state, selectedVersionId: data.id })
	),
	on(deleteSchemaDone, (state, { id }) => adapter.removeOne(id, state))
);

export const getCount = (state: State) => state.count;
