import { Component, EventEmitter, Output } from '@angular/core';
import { Observable } from 'rxjs';
import { CommonText } from '@data/common-text';

@Component({ template: '' })
export abstract class AbstractSelectedPageComponent<T> {
	data$: Observable<T>;
	pagination = CommonText.pagination; // todo: settings selector
	@Output()
	private onBack = new EventEmitter<string>();

	back(path: string) {
		this.onBack.emit(path);
	}
}
