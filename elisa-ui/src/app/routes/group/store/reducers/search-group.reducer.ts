import { createReducer, on } from '@ngrx/store';
import { PaginatorFilter } from '@data/filter/paginator.filter';
import { groupApiActions } from '../actions';

export const searchGroupFeatureKey = 'searchGroup';

export interface State {
	readonly ids: number[];
	readonly loading: boolean;
	readonly error: string;
	readonly query: PaginatorFilter;
}

export const initialState: State = {
	ids: [],
	loading: false,
	error: '',
	query: {} as PaginatorFilter,
};

export const reducer = createReducer(
	initialState,
	on(groupApiActions.searchGroup, (state, { query }) => {
		return query === null
			? {
					ids: [],
					loading: false,
					error: '',
					query,
			  }
			: {
					...state,
					loading: true,
					error: '',
					query,
			  };
	}),
	on(groupApiActions.searchGroupSuccess, (state, { data }) => ({
		ids: data.results.map((user) => user.id),
		loading: false,
		error: '',
		query: state.query,
	})),
	on(groupApiActions.searchGroupFailure, (state, { error }) => ({
		...state,
		loading: false,
		error: error,
	}))
);

export const getIds = (state: State) => state.ids;

export const getQuery = (state: State) => state.query;

export const getLoading = (state: State) => state.loading;

export const getError = (state: State) => state.error;
