import { createReducer, on } from '@ngrx/store';
import { PaginatorFilter } from '@data/filter/paginator.filter';
import { requirementApiActions } from '../actions';

export const searchRequirementFeatureKey = 'searchRequirement';

export interface State {
	readonly ids: number[];
	readonly loading: boolean;
	readonly error: string;
	readonly query: PaginatorFilter;
}

export const initialState: State = {
	ids: [],
	loading: false,
	error: '',
	query: {} as PaginatorFilter,
};

export const reducer = createReducer(
	initialState,
	on(requirementApiActions.searchRequirement, (state, { query }) => ({
		...state,
		loading: true,
		error: '',
		query,
	})),
	on(requirementApiActions.searchRequirementSuccess, (state, { data }) => ({
		ids: data.results.map((model) => model.id),
		loading: false,
		error: '',
		query: state.query,
	})),
	on(requirementApiActions.searchRequirementFailure, (state, { error }) => ({
		...state,
		loading: false,
		error: error,
	}))
);

export const getIds = (state: State) => state.ids;

export const getQuery = (state: State) => state.query;

export const getLoading = (state: State) => state.loading;

export const getError = (state: State) => state.error;
