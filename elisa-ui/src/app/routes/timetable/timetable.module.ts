import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TimetableRoutingModule } from './timetable-routing.module';
import { TimetableComponent } from './components/timetable/timetable.component';
import { SharedModule } from '@shared/shared.module';
import { TranslateModule } from '@ngx-translate/core';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { MATERIAL_MODULES } from './index';
import { StoreModule } from '@ngrx/store';
import * as fromTimetable from './store/reducers';
import { EffectsModule } from '@ngrx/effects';
import {
	TimetablesCollisionsEffects,
	TimetablesEffects,
	DepartmentEffects,
	TimetablesEventsEffects,
	GroupsEffects,
	RequirementEffects,
	RoomEffects,
} from './store/effects';

import { CollectionPageComponent } from './containers/collection-page.component';
import { MatListModule, MatSelectionList } from '@angular/material/list';
import { ReactiveFormsModule } from '@angular/forms';
import { TimetableFilterItemComponent } from './components/timetable/timetable-filter/timetable-filter-item/timetable-filter-item.component';
import { TimetableFilterComponent } from './components/timetable/timetable-filter/timetable-filter.component';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatTableModule } from '@angular/material/table';
import { CdkColumnDef } from '@angular/cdk/table';
import { TimetableCellComponent } from './components/timetable/timetable-cell/timetable-cell.component';
import { TimetableEventItemComponent } from './components/timetable/timetable-event-item/timetable-event-item.component';
import { TimetableVersionComponent } from './components/timetable/timetable-version/timetable-version.component';
import { TimetableCollisionDialogComponent } from './components/timetable/timetable-event-item/timetable-collision-dialog/timetable-collision-dialog.component';
import { TimetableEventOptionsDialogComponent } from './components/timetable/timetable-event-item/timetable-event-options-dialog/timetable-event-options-dialog.component';
import { MatInputModule } from '@angular/material/input';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { EventTypePipe } from './pipes/event-type.pipe';

@NgModule({
	declarations: [
		TimetableComponent,
		CollectionPageComponent,
		TimetableFilterItemComponent,
		TimetableFilterComponent,
		TimetableCellComponent,
		TimetableEventItemComponent,
		TimetableVersionComponent,
		TimetableCollisionDialogComponent,
		TimetableEventOptionsDialogComponent,
		EventTypePipe,
	],
	providers: [CdkColumnDef],
	imports: [
		CommonModule,
		SharedModule,
		TranslateModule.forChild(),
		TimetableRoutingModule,
		DragDropModule,
		MatInputModule,
		MatProgressSpinnerModule,
		MatAutocompleteModule,
		MatTableModule,
		ReactiveFormsModule,
		MatGridListModule,
		MatListModule,
		...MATERIAL_MODULES,
		StoreModule.forFeature(fromTimetable.timetablesFeatureKey, fromTimetable.reducers),
		EffectsModule.forFeature([
			GroupsEffects,
			DepartmentEffects,
			TimetablesEffects,
			TimetablesEventsEffects,
			TimetablesCollisionsEffects,
			RequirementEffects,
			RoomEffects,
		]),
	],
})
export class TimetableModule {}
