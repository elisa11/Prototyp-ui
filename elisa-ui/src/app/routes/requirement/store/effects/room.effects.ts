import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { SnackEffects } from '@core/generics/snack.effects';
import { RoomsCacheService } from '../../../room/services/rooms-cache.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { NGXLogger } from 'ngx-logger';
import { requirementApiActions, roomApiActions } from '../actions';
import { asyncScheduler, of, zip } from 'rxjs';
import {
	catchError,
	debounceTime,
	map,
	skip,
	switchMap,
	switchMapTo,
	takeUntil,
	tap,
	withLatestFrom,
} from 'rxjs/operators';
import { schemaSelectors } from '@core/store/selectors';
import { Store } from '@ngrx/store';
import { PaginatorFilter } from '@data/filter/paginator.filter';
import { PaginatedRoomList } from '@elisa11/elisa-api-angular-client';
import { resetCachedData } from '@core/store/actions/settings.actions';

const tag = 'RoomEffects';

@Injectable()
export class RoomEffects extends SnackEffects {
	resetData$ = createEffect(
		() =>
			this.actions$.pipe(
				ofType(resetCachedData),
				tap(() => this.service.clearCache())
			),
		{ dispatch: false }
	);

	searchRoom$ = createEffect(
		() =>
			({ debounce = 300, scheduler = asyncScheduler } = {}) =>
				this.actions$.pipe(
					ofType(roomApiActions.loadRooms),
					debounceTime(debounce, scheduler),
					withLatestFrom(this.store.select(schemaSelectors.selectScheme)),
					map((value) => {
						return {
							search: value[0].search,
							roomFilter: value[0].roomFilter,
							version: value[1].name,
						};
					}),
					switchMap(({ search, version, roomFilter }) => {
						const nextSearch$ = this.actions$.pipe(
							ofType(roomApiActions.loadRooms),
							skip(1)
						);

						const limit = 25;
						return this.service
							.listRooms(version, roomFilter, { search, limit } as PaginatorFilter)
							.pipe(
								takeUntil(nextSearch$),
								switchMap((value) => {
									const count = value.count;
									if (count > limit) {
										let req = Math.floor(count / limit) - 1;
										req = req + (count % limit > 0 ? 1 : 0);
										return zip(
											of(value),
											...[...Array(req).keys()].map((index) =>
												this.service.listRooms(version, roomFilter, {
													search,
													offset: (1 + index) * limit,
													limit,
												} as PaginatorFilter)
											)
										).pipe(
											map((data) => {
												return {
													count: data[0].count,
													results: data.flatMap((list) => list.results),
												} as PaginatedRoomList;
											})
										);
									} else {
										return of(value);
									}
								}),
								map((data) => roomApiActions.loadRoomsSuccess({ data })),
								catchError((err) => {
									this.logger.error(tag, 'searchRoom$', err);
									return of(roomApiActions.loadRoomsFailure({ error: err.message }));
								})
							);
					}),
					catchError((err) => {
						this.logger.error(tag, 'searchRoom$', err);
						return of(roomApiActions.loadRoomsFailure({ error: err.message }));
					})
				)
	);

	getRoomsByRequirement$ = createEffect(() =>
		this.actions$.pipe(
			ofType(requirementApiActions.searchRequirementSuccess),
			withLatestFrom(this.store.select(schemaSelectors.selectScheme)),
			map((value) => {
				return {
					ids: [
						...new Set(
							value[0].data.results.flatMap((r) =>
								r.requirement_parts.map((part) => part.room)
							)
						),
					],
					version: value[1].name,
				};
			}),
			switchMap(({ ids, version }) => {
			  let filteredIds = ids.filter(id => id != null );
				return zip(...filteredIds.map((id) => this.service.getRoom(version, id))).pipe(
					map((data) => roomApiActions.loadRoomsSuccess({ data: { results: data } })),
					catchError((err) => {
						this.logger.error(tag, 'getRoomsByRequirement$', err);
						return of(roomApiActions.loadRoomsFailure({ error: err.message }));
					})
				);
			}),
			catchError((err) => {
				this.logger.error(tag, 'getRoomsByRequirement$', err);
				return of(roomApiActions.loadRoomsFailure({ error: err.message }));
			})
		)
	);

	constructor(
		private actions$: Actions,
		private store: Store,
		private logger: NGXLogger,
		private service: RoomsCacheService,
		snack: MatSnackBar
	) {
		super(snack);
	}
}
