import { createAction, props } from '@ngrx/store';
import { Timetable } from '@elisa11/elisa-api-angular-client';

export const loadTimetables = createAction('[Timetable - Timetable] Load Timetables');
export const loadTimetablesSuccess = createAction(
	'[Timetable - Timetable] Load Timetables Success',
	props<{ timetables: Timetable[] }>()
);

export const loadTimetablesFailure = createAction(
	'[Timetable - Timetable] Load Timetables Failure',
	props<{ error: any }>()
);

export const createTimetable = createAction(
	'[Timetable - Timetable] Create Timetable',
	props<{ name: string }>()
);

export const branchTimetable = createAction(
	'[Timetable - Timetable] Branch Timetable',
	props<{ name: string }>()
);

export const createTimetableSuccess = createAction(
	'[Timetable - Timetable] Create Timetable Success',
	props<{ timetable: Timetable }>()
);

export const createTimetableFailure = createAction(
	'[Timetable - Timetable] Create Timetable Failure',
	props<{ error: any }>()
);

export const deleteTimetable = createAction(
	'[Timetable - Timetable] Delete Timetable',
	props<{ id: number }>()
);
export const deleteTimetableSuccess = createAction(
	'[Timetable - Timetable] Delete Timetable Success',
	props<{ id: number }>()
);

export const deleteTimetableFailure = createAction(
	'[Timetable - Timetable] Delete Timetable Failure',
	props<{ error: any }>()
);

export const timetableSelected = createAction(
	'[Timetable - Timetable] Timetable Selected',
	props<{ timetable: Timetable }>()
);
