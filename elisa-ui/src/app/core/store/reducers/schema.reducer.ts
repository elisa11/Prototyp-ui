import { createReducer, on } from '@ngrx/store';
import { marker } from '@biesbjerg/ngx-translate-extract-marker';
import { Version } from '@elisa11/elisa-api-angular-client';
import { schemaActions } from '@core/store/actions';

export const schemaFeatureKey = 'selected-schema';
const defaultScheme = { name: marker('schema.toolbar.label') } as Version;

export interface State {
	readonly scheme: Version;
	readonly importing: boolean;
}

export const initialState: State = {
	scheme: defaultScheme,
	importing: false,
};

export const reducer = createReducer(
	initialState,
	on(schemaActions.loadSchemasImportStart, (state) => ({ ...state, importing: true })),
	on(schemaActions.loadSchemasImportDone, (state) => ({
		...state,

		importing: false,
	})),
	on(schemaActions.loadSchemasDialogSuccess, (state, payload) => ({
		...state,
		scheme: payload.data,
	})),
	on(schemaActions.loadSchemasDialogRedirect, (state) => ({
		...state,
		scheme: defaultScheme,
	}))
);

export const getScheme = (state: State) => state.scheme;
export const getImporting = (state: State) => state.importing;
