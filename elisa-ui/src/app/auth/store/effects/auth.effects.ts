import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { combineLatest, of } from 'rxjs';
import { catchError, debounceTime, exhaustMap, map, take, tap } from 'rxjs/operators';
import { authActions, authApiActions, loginPageActions } from '../actions';
import { AuthService } from '../../services/auth.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { HttpErrorResponse } from '@angular/common/http';
import { marker } from '@biesbjerg/ngx-translate-extract-marker';
import { NGXLogger } from 'ngx-logger';
import { LocaleService } from '@core/service/locale/locale.service';
import { Store } from '@ngrx/store';
import { selectLoggedIn, selectPayload, selectToken } from '../selectors/auth.selectors';
import { selectQueryParams } from '@core/store/selectors/router.selectors';
import { AppState } from '@core/store/reducers';
import { refreshToken } from '../actions/auth-api.actions';
import { authInit, logout } from '../actions/auth.actions';
import { SnackEffects } from '@core/generics/snack.effects';

const tag = 'AuthEffects';

@Injectable()
export class AuthEffects extends SnackEffects {
	login$ = createEffect(() =>
		this.actions$.pipe(
			ofType(loginPageActions.login),
			map((value) => value.credentials),
			exhaustMap((credentials) =>
				this.authService.logIn(credentials).pipe(
					map((data) => {
						return data.token && data.payload
							? authApiActions.loginSuccess({ ...data })
							: authApiActions.loginFailure({ error: 'Http client error' });
					}),
					catchError((error) => {
						this.logger.error(tag, 'login$', error);
						return of(authApiActions.loginFailure({ error }));
					})
				)
			)
		)
	);

	loginSuccess$ = createEffect(() =>
		this.actions$.pipe(
			ofType(authApiActions.loginSuccess),
			map((action) => authActions.authRedirect({ url: '/' }))
		)
	);

	authInit$ = createEffect(() =>
		this.actions$.pipe(
			ofType(authInit),
			map((action) =>
				authActions.authInitSuccess({
					token: this.authService.token,
					payload: this.authService.currentTokenPayload,
				})
			),
			catchError((error) => {
				this.logger.error(tag, 'authInit$', error);
				return of(logout({}));
			})
		)
	);

	loginFail$ = createEffect(
		() =>
			this.actions$.pipe(
				ofType(authApiActions.loginFailure),
				tap((action) => {
					const error = action.error;
					this.logger.debug(tag, action);
					if (error.status === 401) {
						this.openSnackbar(
							this.localeService.translate(marker('login.error.unauthorized'))
						);
					} else if (error.status >= 500 || error.status === 0) {
						this.openSnackbar(this.localeService.translate(marker('login.error.server')));
					} else {
						this.openSnackbar(
							this.localeService.translate(marker('login.error.unknown'))
						);
					}
				})
			),
		{ dispatch: false }
	);

	authRedirect$ = createEffect(
		() =>
			this.actions$.pipe(
				ofType(authActions.authRedirect),
				debounceTime(750),
				tap((payload) => {
					let query = null;
					if (payload) {
						if (payload.returnUrl) {
							query = { queryParams: { returnUrl: payload.returnUrl } };
						}
						if (payload.url) {
							query
								? this.router.navigate([payload.url], query)
								: this.router.navigate([payload.url]);
						} else {
							this.store
								.select(selectQueryParams)
								.pipe(take(1))
								.subscribe((routerQueries) => {
									const redirect = routerQueries ? routerQueries.returnUrl ?? '/' : '/';
									query
										? this.router.navigate([redirect], query)
										: this.router.navigate([redirect]);
								});
						}
					}
				})
			),
		{ dispatch: false }
	);

	checkToken$ = createEffect(() =>
		this.actions$.pipe(
			ofType(authActions.refreshTokenValidity),
			exhaustMap((action) => {
				return this.authService.isUserAuthenticated().pipe(
					map((data) => authActions.validToken({ valid: data })),
					catchError((error: HttpErrorResponse) => {
						this.logger.error(tag, 'checkToken$', error);
						return of(authApiActions.loginFailure({ error }));
					})
				);
			})
		)
	);

	refreshToken$ = createEffect(() =>
		this.actions$.pipe(
			ofType(refreshToken),
			exhaustMap((action) => {
				return this.authService.refreshToken().pipe(
					exhaustMap((refreshed) => {
						return combineLatest([
							this.store.select(selectPayload),
							this.store.select(selectToken),
						]).pipe(
							map(([payload, token]) => {
								return { payload, token };
							}),
							map((data) => {
								return authApiActions.loginSuccess({ ...data });
							})
						);
					})
				);
			})
		)
	);

	logout$ = createEffect(
		() =>
			this.actions$.pipe(
				ofType(authActions.logout),
				exhaustMap((action) => {
					this.logger.debug(tag, action.type);
					return this.store.select(selectLoggedIn).pipe(
						take(1),
						map((value) => {
							console.log('LOGOUT EFFECT');
							// value ? this.authService.logout() : this.authService.deleteData();
							this.authService.logout();
							return authActions.authRedirect({
								url: 'auth/login',
								returnUrl: action.returnUrl,
							});
						})
					);
				})
			)
		// { dispatch: false }
	);

	constructor(
		private actions$: Actions,
		private authService: AuthService,
		private router: Router,
		private logger: NGXLogger,
		private localeService: LocaleService,
		private store: Store<AppState>,
		snackbar: MatSnackBar
	) {
		super(snackbar);
	}
}
