import { createFeatureSelector, createSelector } from '@ngrx/store';
import { groupsFeatureKey, GroupState } from '../reducers';
import * as fromGroup from '../reducers/group.reducer';
import { Group } from '@elisa11/elisa-api-angular-client';

export const selectGroupState = createFeatureSelector<GroupState>(groupsFeatureKey);

export const selectGroupEntitiesState = createSelector(
	selectGroupState,
	(state) => state[fromGroup.groupFeatureKey]
);

export const selectSelectedGroupId = createSelector(
	selectGroupEntitiesState,
	fromGroup.getSelectId
);

export const selectTotalCount = createSelector(
	selectGroupEntitiesState,
	fromGroup.getCount
);

export const { selectIds, selectEntities } = fromGroup.adapter.getSelectors(
	selectGroupEntitiesState
);

export const selectSelectedGroup = createSelector(
	selectEntities,
	selectSelectedGroupId,
	(entities, selectedId) => {
		return selectedId && entities ? entities[selectedId] : ({} as Group);
	}
);
