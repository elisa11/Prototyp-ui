import { Component } from '@angular/core';
import { Department } from '@elisa11/elisa-api-angular-client';
import { AbstractTableComponent } from '@core/generics/abstract-table.component';

@Component({
	selector: 'app-department-list',
	templateUrl: './department-list.component.html',
})
export class DepartmentListComponent extends AbstractTableComponent<Department> {
	displayedColumns = ['id', 'abbr', 'name'];

	constructor() {
		super();
	}
}
