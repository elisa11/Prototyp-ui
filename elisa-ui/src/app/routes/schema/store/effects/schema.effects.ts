import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { resetCachedData } from '@core/store/actions/settings.actions';
import {
	catchError,
	debounceTime,
	map,
	skip,
	switchMap,
	takeUntil,
	tap,
} from 'rxjs/operators';
import { VersionCacheService } from '@core/service/cache/version-cache.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { asyncScheduler, EMPTY, of, zip } from 'rxjs';
import { schemaApiActions } from '../actions';
import { Store } from '@ngrx/store';
import { isNotNullOrUndefined } from 'codelyzer/util/isNotNullOrUndefined';
import { SnackEffects } from '@core/generics/snack.effects';
import { NGXLogger } from 'ngx-logger';
import { marker } from '@biesbjerg/ngx-translate-extract-marker';
import { LocaleService } from '@core/service/locale/locale.service';

const tag = 'SchemaEffects';

@Injectable()
export class SchemaEffects extends SnackEffects {
	resetData$ = createEffect(
		() =>
			this.actions$.pipe(
				ofType(resetCachedData),
				tap(() => this.service.clearCache())
			),
		{ dispatch: false }
	);

	search$ = createEffect(
		() =>
			({ debounce = 300, scheduler = asyncScheduler } = {}) =>
				this.actions$.pipe(
					ofType(schemaApiActions.searchSchemas),
					debounceTime(debounce, scheduler),
					switchMap(({ query }) => {
						if (query === null) {
							return EMPTY;
						}

						const nextSearch$ = this.actions$.pipe(
							ofType(schemaApiActions.searchSchemas),
							skip(1)
						);

						return this.service.getVersions(query).pipe(
							map((versions) => schemaApiActions.searchSchemasSuccess({ versions })),
							catchError((err) => {
								this.logger.error(tag, 'search$', err);
								return of(schemaApiActions.searchSchemasFailure({ error: err.message }));
							}),
							takeUntil(nextSearch$)
						);
					})
				)
	);
	searchSchemasSuccess$ = createEffect(() =>
		this.actions$.pipe(
			ofType(schemaApiActions.searchSchemasSuccess),
			map(({ versions }) =>
				schemaApiActions.searchParentSchemas({
					ids: versions.results
						.map((v) => v.parent)
						.filter((id) => isNotNullOrUndefined(id)),
				})
			)
		)
	);
	searchFailed$ = createEffect(
		() =>
			this.actions$.pipe(
				ofType(schemaApiActions.searchSchemasFailure),
				tap((err) =>
					this.openSnackbar(this.localeService.translate(marker('schema.search.error')))
				)
			),
		{ dispatch: false }
	);

	searchParentSchemas$ = createEffect(() =>
		this.actions$.pipe(
			ofType(schemaApiActions.searchParentSchemas),
			map((action) => action.ids),
			switchMap((ids) =>
				zip(ids.map((id) => this.service.getVersion(id))).pipe(
					map((versions) => schemaApiActions.searchParentSchemasSuccess({ versions })),
					catchError((err) => {
						this.logger.error(tag, 'searchParentSchemas$', err);
						return of(schemaApiActions.searchSchemasFailure({ error: err.message }));
					})
				)
			)
		)
	);
	searchParentFailed$ = createEffect(
		() =>
			this.actions$.pipe(
				ofType(schemaApiActions.searchParentSchemasFailure),
				tap((err) =>
					this.openSnackbar(this.localeService.translate(marker('schema.parent.error')))
				)
			),
		{ dispatch: false }
	);

	constructor(
		private actions$: Actions,
		private service: VersionCacheService,
		snack: MatSnackBar,
		private store: Store,
		private localeService: LocaleService,
		private logger: NGXLogger
	) {
		super(snack);
	}
}
