import { ChangeDetectionStrategy, Component, EventEmitter, Output } from '@angular/core';
import { AbstractCollectionPageComponent } from '@core/generics/abstract-collection-page.component';
import { RequirementModel } from '../../model/requirement.model';
import { PaginatorFilter } from '@data/filter/paginator.filter';
import { Store } from '@ngrx/store';
import { requirementApiActions } from '../../store/actions';
import { RequirementFilter } from '../../model/requirement-filter.model';
import { combineLatest } from 'rxjs';
import {
	requirementSelectors,
	roomSelectors,
	searchRequirementSelectors,
	userSelectors,
} from '../../store/selectors';
import { map } from 'rxjs/operators';
import { Router } from '@angular/router';
import {
	activityCategorySelectors,
	userSubjectRoleSelectors,
} from '@shared/store/selectors';
import { SubjectTeachers, User } from '@elisa11/elisa-api-angular-client';
import { SubjectTeacherModel } from '../../model/subject-teacher.model';
import { isNotNullOrUndefined } from 'codelyzer/util/isNotNullOrUndefined';

@Component({
	template: `
		<app-requirement-list
			[data]="data$ | async"
			[resultsLength]="totalCount$ | async"
			[pagination]="pagination"
			[isLoading]="loading$ | async"
			(onFilterChange)="handleFilterChange($event)"
			(onNewRequirement)="handleNewRequirement($event)"
		></app-requirement-list>
	`,
	changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CollectionPageComponent extends AbstractCollectionPageComponent<RequirementModel> {
	private requirementFilter: RequirementFilter;

	constructor(private store: Store, private router: Router) {
		super();
		this.store.dispatch(
			requirementApiActions.searchRequirement({
				query: {
					limit: this.pagination[0],
					offset: 0,
				} as PaginatorFilter,
				filter: this.requirementFilter,
			})
		);
		this.loading$ = store.select(searchRequirementSelectors.selectSearchLoading);
		this.totalCount$ = store.select(requirementSelectors.selectTotalCount);
		// todo ,groups,rooms
		this.data$ = combineLatest([
			store.select(searchRequirementSelectors.selectSearchResults),
			store.select(userSelectors.selectSelected),
			store.select(activityCategorySelectors.selectAll),
			store.select(userSubjectRoleSelectors.selectAll),
			store.select(roomSelectors.selectSearchResults),
		]).pipe(
			map((data) => {
				return {
					requirement: data[0],
					users: data[1],
					category: data[2],
					roles: data[3],
					rooms: data[4],
				};
			}),
			map(({ requirement, users, category, roles, rooms }) =>
				requirement.map((r) => {
					// parse teachers + roles
					const parsedTeacherData = r.requirement_teachers.map((d) => {
						return {
							user: users.find((u) => u.id === d.teacher),
							roles: r.requirement_teachers
								.filter((fd) => fd.teacher === d.teacher)
								.map((fd) => roles.find((role) => role.id === fd.role)),
						} as SubjectTeacherModel;
					});
					const teacherData: SubjectTeacherModel[] = [];
					parsedTeacherData.forEach((d) => {
						if (
							!isNotNullOrUndefined(
								teacherData.find((nd) => nd.user && d.user && nd.user.id === d.user.id)
							)
						) {
							teacherData.push(d);
						}
					});

					return new RequirementModel(r, users, category, teacherData, rooms);
				})
			)
		);
	}

	handleFilterChange(filterChange: PaginatorFilter) {
		this.store.dispatch(
			requirementApiActions.searchRequirement({
				query: filterChange,
				filter: this.requirementFilter,
			})
		);
	}

	handleNewRequirement($event: RequirementModel | undefined) {
	  console.log($event);
		this.router.navigate(['requirement', 'form'], {state: $event});
	}
}
