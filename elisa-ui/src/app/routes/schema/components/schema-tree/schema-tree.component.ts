import { Component, OnInit } from '@angular/core';
import { Version, VersionStatusEnum } from '@elisa11/elisa-api-angular-client';
import { FlatTreeControl } from '@angular/cdk/tree';
import { MatTreeFlatDataSource, MatTreeFlattener } from '@angular/material/tree';
import { SelectionModel } from '@angular/cdk/collections';

/**
 * Node for version
 */
export class VersionNode implements Version {
	children: VersionNode[];
	readonly id: number;
	name: string;
	parent: number | null;
	period: number | null;
	status: VersionStatusEnum;
	readonly updated_at: string;
}

/** Flat to-do item node with expandable and level information */
export class VersionFlatNode implements Version {
	level: number;
	expandable: boolean;
	readonly id: number;
	name: string;
	parent: number | null;
	period: number | null;
	status: VersionStatusEnum;
	readonly updated_at: string;
}

@Component({
	selector: 'app-schema-tree',
	templateUrl: './schema-tree.component.html',
})
export class SchemaTreeComponent implements OnInit {
	constructor() {
		this.treeFlattener = new MatTreeFlattener(
			this.transformer,
			this.getLevel,
			this.isExpandable,
			this.getChildren
		);

		this.dataSource = new MatTreeFlatDataSource(this.treeControl, this.treeFlattener);

		this.treeControl = new FlatTreeControl<VersionFlatNode>(
			this.getLevel,
			this.isExpandable
		);
	}

	ngOnInit(): void {
		this.treeFlattener = new MatTreeFlattener(
			this.transformer,
			this.getLevel,
			this.isExpandable,
			this.getChildren
		);

		this.dataSource = new MatTreeFlatDataSource(this.treeControl, this.treeFlattener);

		this.treeControl = new FlatTreeControl<VersionFlatNode>(
			this.getLevel,
			this.isExpandable
		);
	}

	/** Map from flat node to nested node. This helps us finding the nested node to be modified */
	flatNodeMap = new Map<VersionFlatNode, VersionNode>();

	/** Map from nested node to flattened node. This helps us to keep the same object for selection */
	nestedNodeMap = new Map<VersionNode, VersionFlatNode>();

	/** A selected parent node to be inserted */
	selectedParent: VersionFlatNode | null = null;

	/** The new item's name */
	newItemName = '';

	treeControl: FlatTreeControl<VersionFlatNode>;

	treeFlattener: MatTreeFlattener<VersionNode, VersionFlatNode>;

	dataSource: MatTreeFlatDataSource<VersionNode, VersionFlatNode>;

	/** The selection for checklist */
	checklistSelection = new SelectionModel<VersionFlatNode>(true /* multiple */);

	getLevel = (node: VersionFlatNode) => node.level;

	isExpandable = (node: VersionFlatNode) => node.expandable;

	getChildren = (node: VersionNode): VersionNode[] => node.children;

	hasChild = (_: number, _nodeData: VersionFlatNode) => _nodeData.expandable;

	hasNoContent = (_: number, _nodeData: VersionFlatNode) => _nodeData.name === '';

	/**
	 * Transformer to convert nested node to flat node. Record the nodes in maps for later use.
	 */
	transformer = (node: VersionNode, level: number) => {
		const existingNode = this.nestedNodeMap.get(node);
		const flatNode =
			existingNode && existingNode.id === node.id ? existingNode : new VersionFlatNode();
		flatNode.name = node.name;
		flatNode.level = level;
		flatNode.expandable = !!node.children?.length;
		this.flatNodeMap.set(flatNode, node);
		this.nestedNodeMap.set(node, flatNode);
		return flatNode;
	};

	/** Whether all the descendants of the node are selected. */
	descendantsAllSelected(node: VersionFlatNode): boolean {
		const descendants = this.treeControl.getDescendants(node);
		const descAllSelected =
			descendants.length > 0 &&
			descendants.every((child) => {
				return this.checklistSelection.isSelected(child);
			});
		return descAllSelected;
	}

	/** Whether part of the descendants are selected */
	descendantsPartiallySelected(node: VersionFlatNode): boolean {
		const descendants = this.treeControl.getDescendants(node);
		const result = descendants.some((child) => this.checklistSelection.isSelected(child));
		return result && !this.descendantsAllSelected(node);
	}

	/** Toggle the to-do item selection. Select/deselect all the descendants node */
	todoItemSelectionToggle(node: VersionFlatNode): void {
		this.checklistSelection.toggle(node);
		const descendants = this.treeControl.getDescendants(node);
		this.checklistSelection.isSelected(node)
			? this.checklistSelection.select(...descendants)
			: this.checklistSelection.deselect(...descendants);

		// Force update for the parent
		descendants.forEach((child) => this.checklistSelection.isSelected(child));
		this.checkAllParentsSelection(node);
	}

	/** Toggle a leaf to-do item selection. Check all the parents to see if they changed */
	todoLeafItemSelectionToggle(node: VersionFlatNode): void {
		this.checklistSelection.toggle(node);
		this.checkAllParentsSelection(node);
	}

	/* Checks all the parents when a leaf node is selected/unselected */
	checkAllParentsSelection(node: VersionFlatNode): void {
		let parent: VersionFlatNode | null = this.getParentNode(node);
		while (parent) {
			this.checkRootNodeSelection(parent);
			parent = this.getParentNode(parent);
		}
	}

	/** Check root node checked state and change it accordingly */
	checkRootNodeSelection(node: VersionFlatNode): void {
		const nodeSelected = this.checklistSelection.isSelected(node);
		const descendants = this.treeControl.getDescendants(node);
		const descAllSelected =
			descendants.length > 0 &&
			descendants.every((child) => {
				return this.checklistSelection.isSelected(child);
			});
		if (nodeSelected && !descAllSelected) {
			this.checklistSelection.deselect(node);
		} else if (!nodeSelected && descAllSelected) {
			this.checklistSelection.select(node);
		}
	}

	/* Get the parent node of a node */
	getParentNode(node: VersionFlatNode): VersionFlatNode | null {
		const currentLevel = this.getLevel(node);

		if (currentLevel < 1) {
			return null;
		}

		const startIndex = this.treeControl.dataNodes.indexOf(node) - 1;

		for (let i = startIndex; i >= 0; i--) {
			const currentNode = this.treeControl.dataNodes[i];

			if (this.getLevel(currentNode) < currentLevel) {
				return currentNode;
			}
		}
		return null;
	}

	/** Select the category so we can insert the new item. */
	addNewItem(node: VersionFlatNode) {
		const parentNode = this.flatNodeMap.get(node);
		// this._database.insertItem(parentNode!, '');
		this.treeControl.expand(node);
	}

	/** Save the node to database */
	saveNode(node: VersionFlatNode, itemValue: string) {
		const nestedNode = this.flatNodeMap.get(node);
		// this._database.updateItem(nestedNode!, itemValue);
	}
}
