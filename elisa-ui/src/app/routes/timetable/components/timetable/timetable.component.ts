import {
	ChangeDetectionStrategy,
	ChangeDetectorRef,
	Component,
	OnInit,
	Renderer2,
	ViewChild,
} from '@angular/core';
import { ActionsSubject, Store } from '@ngrx/store';
import { MatDialog } from '@angular/material/dialog';
import { debounceTime, Observable } from 'rxjs';
import {
	Requirement,
	SuitabilityEnum,
	Timetable,
} from '@elisa11/elisa-api-angular-client';
import { FormBuilder, FormGroup } from '@angular/forms';
import { CdkDragDrop } from '@angular/cdk/drag-drop';
// import { Activity, Collision } from '../../model/activity.model';
import { searchRequirementSelectors } from 'src/app/routes/requirement/store/selectors';
import { requirementApiActions } from 'src/app/routes/requirement/store/actions';
import { PaginatorFilter } from '@data/filter/paginator.filter';
import { RequirementFilter } from 'src/app/routes/requirement/model/requirement-filter.model';
import { DayEnum } from '@elisa11/elisa-api-angular-client';
import { TeacherType } from '@data/model/teacher-type.model';
import {
	selectAllTimetableEvents,
	selectTimetableEventsLoading,
} from '../../store/selectors/timetables-events.selectors';
import { Event, Collision } from '@elisa11/elisa-api-angular-client';
import {
	loadTimetableEvents,
	postTimetableEvent,
	putTimetableEvent,
} from '../../store/actions/timetables-events.actions';
import { selectAllTimetableCollisions } from '../../store/selectors/timetables-collisions.selectors';
import { loadTimetableCollisions } from '../../store/actions/timetables-collisions.actions';
import { SnackEffects } from '@core/generics/snack.effects';
import { MatSnackBar } from '@angular/material/snack-bar';
import { EventWrapper } from '../../model/activity.model';
import { requirementsSelector } from '../../store/selectors';
import { requirementActions } from '../../store/actions';
import { selectTimetable } from '../../store/selectors/timetables.selectors';
import { CollisionService } from '../../services/collision.service';
import { selectRequirement } from '../../store/selectors/requirement.selectors';
import { TimetableEventItemComponent } from './timetable-event-item/timetable-event-item.component';

type Hour = 8 | 9 | 10 | 11 | 12 | 13 | 14 | 15 | 16 | 17 | 18;

// type Day = 'Po' | 'Ut' | 'St' | 'Št' | 'Pi';
type Day = 'MONDAY' | 'TUESDAY' | 'WEDNESDAY' | 'THURSDAY' | 'FRIDAY';

type Data = {
	[day in Day]: {
		[hour in Hour]: {
			items: Array<EventWrapper>;
			classes?: string;
		};
	};
};

@Component({
	selector: 'app-timetable',
	templateUrl: './timetable.component.html',
	styleUrls: ['./timetable.component.scss'],
	changeDetection: ChangeDetectionStrategy.Default,
})
export class TimetableComponent extends SnackEffects implements OnInit {
	events$: Observable<Event[]> = this.store.select(selectAllTimetableEvents);
	events: EventWrapper[] = [];
	@ViewChild('eventItem') eventItem: TimetableEventItemComponent;

	hours = [8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18];
	days = [
		DayEnum.Monday,
		DayEnum.Tuesday,
		DayEnum.Wednesday,
		DayEnum.Thursday,
		DayEnum.Friday,
	];
	currentlyDraggedEvent: EventWrapper;
	currentRequirement$: Observable<Requirement> = this.store.select(selectRequirement);
	currentRequirement: Requirement;
	isLoadingEvents: Observable<boolean> = this.store.select(selectTimetableEventsLoading);

	data: Data = {
		MONDAY: {
			8: { items: [], classes: 'cell ' },
			9: { items: [], classes: 'cell ' },
			10: { items: [], classes: 'cell ' },
			11: { items: [], classes: 'cell ' },
			12: { items: [], classes: 'cell ' },
			13: { items: [], classes: 'cell ' },
			14: { items: [], classes: 'cell ' },
			15: { items: [], classes: 'cell ' },
			16: { items: [], classes: 'cell ' },
			17: { items: [], classes: 'cell ' },
			18: { items: [], classes: 'cell ' },
		},
		TUESDAY: {
			8: { items: [], classes: 'cell ' },
			9: { items: [], classes: 'cell ' },
			10: { items: [], classes: 'cell ' },
			11: { items: [], classes: 'cell ' },
			12: { items: [], classes: 'cell ' },
			13: { items: [], classes: 'cell ' },
			14: { items: [], classes: 'cell ' },
			15: { items: [], classes: 'cell ' },
			16: { items: [], classes: 'cell ' },
			17: { items: [], classes: 'cell ' },
			18: { items: [], classes: 'cell ' },
		},
		WEDNESDAY: {
			8: { items: [], classes: 'cell ' },
			9: { items: [], classes: 'cell ' },
			10: { items: [], classes: 'cell ' },
			11: { items: [], classes: 'cell ' },
			12: { items: [], classes: 'cell ' },
			13: { items: [], classes: 'cell ' },
			14: { items: [], classes: 'cell ' },
			15: { items: [], classes: 'cell ' },
			16: { items: [], classes: 'cell ' },
			17: { items: [], classes: 'cell ' },
			18: { items: [], classes: 'cell ' },
		},
		THURSDAY: {
			8: { items: [], classes: 'cell ' },
			9: { items: [], classes: 'cell ' },
			10: { items: [], classes: 'cell ' },
			11: { items: [], classes: 'cell ' },
			12: { items: [], classes: 'cell ' },
			13: { items: [], classes: 'cell ' },
			14: { items: [], classes: 'cell ' },
			15: { items: [], classes: 'cell ' },
			16: { items: [], classes: 'cell ' },
			17: { items: [], classes: 'cell ' },
			18: { items: [], classes: 'cell ' },
		},
		FRIDAY: {
			8: { items: [], classes: 'cell ' },
			9: { items: [], classes: 'cell ' },
			10: { items: [], classes: 'cell ' },
			11: { items: [], classes: 'cell ' },
			12: { items: [], classes: 'cell ' },
			13: { items: [], classes: 'cell ' },
			14: { items: [], classes: 'cell ' },
			15: { items: [], classes: 'cell ' },
			16: { items: [], classes: 'cell ' },
			17: { items: [], classes: 'cell ' },
			18: { items: [], classes: 'cell ' },
		},
	};

	requirements: Requirement[];

	advancedFilter: FormGroup;
	isDragging = false;

	constructor(
		private renderer: Renderer2,
		private store: Store,
		private dialog: MatDialog,
		private actionSubject$: ActionsSubject,
		private fb: FormBuilder,
		snack: MatSnackBar,
		private cdr: ChangeDetectorRef
	) {
		super(snack);
	}

	ngOnInit(): void {
		this.advancedFilter = this.fb.group({
			faculties: [[]],
			studyPlans: [[]],
			studyYears: [[]],
			specializations: [[]],
			timetableEvents: [[]],
			room: null,
		});

		this.store.dispatch(requirementActions.loadRequirements());
		this.store
			.select(requirementsSelector.selectAllRequirements)
			.pipe()
			.subscribe((reqs) => {
				this.requirements = reqs;
			});

		this.store.dispatch(loadTimetableEvents({}));
		this.store.dispatch(loadTimetableCollisions());

		this.advancedFilter.valueChanges.subscribe((val) => {
			console.log(val);
			this.loadEvents();
		});

		this.events$.subscribe((events) => {
			this.distributeEvents(events);
		});

		this.currentRequirement$.subscribe((req) => {
			this.currentRequirement = req;
		});
	}

	loadEvents() {
		this.store.dispatch(
			loadTimetableEvents({
				roomId: this.f.room.value,
				activityCategoryIdIn:
					this.f.timetableEvents.value.length > 0 ? this.f.timetableEvents.value : null,
				activityCourseDepartmentIdIn:
					this.f.faculties.value.length > 0 ? this.f.faculties.value : null,
				groupsIdIn: this.f.studyPlans.value
					.concat(this.f.studyYears.value)
					.concat(this.f.specializations.value),
			})
		);
	}

	timetableSelected(timetable: Timetable) {
		this.loadEvents();
	}

	reformatTime(time: string): number {
		return Number(time.split(':')[0]);
	}

	sortByEventDayAndTime(first: Event, second: Event): number {
		if (!first.time) return -1;
		if (!second.time) return 1;
		return this.reformatTime(first.time) - this.reformatTime(second.time);
	}

	onDropEvent(event) {
		if (!event.event.event.room) {
			event.openEventOptionsDialog();
		}
	}

	distributeEvents(events: Event[]) {
		this.resetTimetableData();
		let currentEvents = [];
		events
			.sort((a, b) => this.sortByEventDayAndTime(a, b))
			.forEach((event) => {
				if (event.day && event.time) {
					let time = this.reformatTime(event.time);
					let indexOfEmpty = this.data[event.day][time].items.indexOf(
						this.data[event.day][time].items.find((item) => item.empty)
					);

					for (let i = 0; i <= 18 - time; i++) {
						if (indexOfEmpty > -1) {
							this.data[event.day][time + i].items[indexOfEmpty] = new EventWrapper(
								event,
								i > 0 ? false : true,
								i < event.activity.duration ? false : true
							);
						} else {
							this.data[event.day][time + i].items.push(
								new EventWrapper(
									event,
									i > 0 ? false : true,
									i < event.activity.duration ? false : true
								)
							);
						}
					}
				} else {
					currentEvents.push(new EventWrapper(event));
				}
			});
		this.events = currentEvents as EventWrapper[];
		this.store.dispatch(loadTimetableCollisions());
		this.cdr.detectChanges();
	}

	resetTimetableData() {
		Object.keys(this.data).forEach((day) => {
			Object.keys(this.data[day]).forEach((hour) => {
				this.data[day][hour].items = [];
			});
		});
	}

	get f() {
		return this.advancedFilter.controls;
	}

	onDrop(event: CdkDragDrop<string[]>, day: DayEnum, hour: number) {
		if (hour + this.currentlyDraggedEvent.event.activity.duration > 19) {
			this.openSnackbar('Trvanie udalosti presahuje 19. hodinu.');
			return;
		}
		let newEvent = {
			id: this.currentlyDraggedEvent.event.id,
		} as Event;
		newEvent.day = day === null ? null : day;
		newEvent.time = hour === null ? null : hour + ':00:00';

		this.store.dispatch(
			putTimetableEvent({
				event: newEvent,
			})
		);
		this.isDragging = false;
	}

	onDragOver(event: any) {
		// console.log(event);
		event.preventDefault();
	}

	onDragStart(event: EventWrapper, day: string, hour: number) {
		this.store.dispatch(
			requirementActions.setSelectedRequirementByCourse({
				courseId: event.event.activity.course,
			})
		);
		this.isDragging = true;
		this.currentlyDraggedEvent = event;
		this.setClasses(event);
	}

	setClasses(event: EventWrapper) {
		//sets the classes for the cells by availability from requirements
		this.resetClasses();

		this.currentRequirement$.subscribe((req) => {
			if (req === null || req === undefined) return;

			req.requirement_parts
				.filter(
					(part) =>
						(part.category === event.event.activity.category &&
							part.suitability === SuitabilityEnum.Unavailable) ||
						part.suitability === SuitabilityEnum.Unsuitable
				)
				.forEach((part) => {
					let time = part.time.split(':')[0];
					let timeNumber = Number(time.startsWith('0') ? time.slice(1) : time);
					try {
						this.data[part.day][timeNumber].classes +=
							part.suitability === SuitabilityEnum.Unavailable
								? 'unavailable'
								: 'unsuitable';
					} catch (e) {
						console.log(e);
						console.log(part);
					}
				});
		});
	}

	resetClasses() {
		Object.keys(this.data).forEach((day) => {
			Object.keys(this.data[day]).forEach((hour) => {
				this.data[day][hour].classes = 'cell ';
			});
		});
	}
}
