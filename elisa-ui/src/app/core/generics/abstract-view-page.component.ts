import { Component, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';

@Component({ template: '' })
export abstract class AbstractViewPageComponent implements OnDestroy {
	protected actionsSubscription: Subscription;

	protected constructor(private router: Router) {}

	ngOnDestroy() {
		this.actionsSubscription.unsubscribe();
	}

	back(path: string): void {
		this.router.navigate([path]);
	}
}
