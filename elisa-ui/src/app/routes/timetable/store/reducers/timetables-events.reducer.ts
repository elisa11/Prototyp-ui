import { createReducer, on } from '@ngrx/store';
import { Event } from '@elisa11/elisa-api-angular-client';
import { timetableEventsActions } from '../actions';
import { createEntityAdapter, EntityAdapter, EntityState } from '@ngrx/entity';

export const timetablesFeatureKey = 'timetables-events';

export interface State extends EntityState<Event> {
	readonly selectedIds: number[];
	readonly count: number;
	readonly loading: boolean;
}

export const adapter: EntityAdapter<Event> = createEntityAdapter<Event>({
	selectId: (model) => model.id,
	sortComparer: false,
});
export const initialState = adapter.getInitialState({
	selectedIds: [],
	count: 0,
	loading: false,
});

export const reducer = createReducer(
	initialState,
	on(timetableEventsActions.loadTimetableEvents, (state) => ({
		...state,
		loading: true,
	})),
	on(timetableEventsActions.loadTimetableEventsSuccess, (state, { events }) =>
		adapter.upsertMany(events, {
			...state,
			selectedIds: events.map((x) => x.id),
			count: events.length,
			loading: false,
		})
	),
	on(timetableEventsActions.loadTimetableEventsFailure, (state, { error }) => ({
		...state,
		loading: false,
		error: error,
	})),
	on(timetableEventsActions.postTimetableEventSuccess, (state, { event }) =>
		adapter.upsertOne(event, {
			...state,
			loading: false,
		})
	),
	on(timetableEventsActions.putTimetableEventSuccess, (state, { event }) =>
		adapter.upsertOne(event, {
			...state,
			loading: false,
		})
	),
	on(timetableEventsActions.deleteTimetableEventSuccess, (state, { event }) =>
		adapter.removeOne(event.id, state)
	)
);

export const getSelectedIds = (state: State) => state.selectedIds;
export const getLoading = (state: State) => state.loading;
export const getCount = (state: State) => state.count;
