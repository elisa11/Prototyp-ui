import { Provider } from '@angular/core';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { JwtInterceptor } from '@core/interceptor/jwt-interceptor.service';

export const jwtInterceptorProvider: Provider = {
	provide: HTTP_INTERCEPTORS,
	useClass: JwtInterceptor,
	multi: true,
};
