import { createReducer, on } from '@ngrx/store';
import { PaginatorFilter } from '@data/filter/paginator.filter';
import {
	searchSchemas,
	searchSchemasFailure,
	searchSchemasSuccess,
} from '../actions/schema-api.actions';
import { HttpErrorResponse } from '@angular/common/http';

export const searchSchemaFeatureKey = 'searchSchema';

export interface State {
	readonly ids: number[];
	readonly loading: boolean;
	readonly error: string | HttpErrorResponse;
	readonly query: PaginatorFilter;
}

export const initialState: State = {
	ids: [],
	loading: false,
	error: '',
	query: {} as PaginatorFilter,
};

export const reducer = createReducer(
	initialState,
	on(searchSchemas, (state, { query }) => {
		return query === null
			? {
					ids: [],
					loading: false,
					error: '',
					query,
			  }
			: {
					...state,
					loading: true,
					error: '',
					query,
			  };
	}),
	on(searchSchemasSuccess, (state, { versions }) => ({
		ids: versions.results.map((version) => version.id),
		loading: false,
		error: '',
		query: state.query,
	})),
	on(searchSchemasFailure, (state, { error }) => ({
		...state,
		loading: false,
		error,
	}))
);

export const getIds = (state: State) => state.ids;

export const getQuery = (state: State) => state.query;

export const getLoading = (state: State) => state.loading;

export const getError = (state: State) => state.error;
