import * as roomEquipmentSelectors from './room-equipment.selectors';
import * as roomSelectors from './room.selectors';
import * as searchRoomSelectors from './search-room.selectors';
import * as departmentSelectors from './department.selectors';
import * as typeSelectors from './type.selectors';
import * as equipmentSelectors from './equipment.selectors';

export {
  equipmentSelectors,
	roomEquipmentSelectors,
	roomSelectors,
	departmentSelectors,
	typeSelectors,
	searchRoomSelectors,
};
