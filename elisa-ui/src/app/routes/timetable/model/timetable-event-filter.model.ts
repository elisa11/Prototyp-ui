import { Activity, User } from '@elisa11/elisa-api-angular-client';

export interface TimetableEventFilterModel {
  activity: Activity;
  user: User[];
}
