import { createReducer, on } from '@ngrx/store';
import { createEntityAdapter, EntityAdapter, EntityState } from '@ngrx/entity';
import { Timetable } from '@elisa11/elisa-api-angular-client';
import { timetablesActions } from '../actions';
import { state } from '@angular/animations';

export const timetablesFeatureKey = 'timetables';

export interface State extends EntityState<Timetable> {
	readonly selectedIds: number[];
	readonly count: number;
	readonly loading: boolean;
	readonly timetable: Timetable;
}

export const adapter: EntityAdapter<Timetable> = createEntityAdapter<Timetable>({
	selectId: (model: Timetable) => model.id,
	sortComparer: false,
});

export const initialState = adapter.getInitialState({
	selectedIds: [],
	count: 0,
	loading: false,
	timetable: null,
});

export const reducer = createReducer(
	initialState,
	on(timetablesActions.loadTimetables, (state) => ({
		...state,
		loading: true,
	})),
	on(timetablesActions.loadTimetablesSuccess, (state, { timetables }) =>
		adapter.upsertMany(timetables, {
			...state,
			selectedIds: timetables.map((timetable) => timetable.id),
			count: timetables.length,
			loading: false,
			timetable: state.timetable ? state.timetable : timetables[0],
		})
	),
	on(timetablesActions.loadTimetablesFailure, (state, { error }) => ({
		...state,
		loading: false,
		error: error,
	})),
	on(timetablesActions.createTimetable, (state) => ({
		...state,
		loading: true,
	})),
	on(timetablesActions.createTimetableSuccess, (state, { timetable }) =>
		adapter.upsertOne(timetable, {
			...state,
			selectedIds: [...state.selectedIds, timetable.id],
			loading: false,
		})
	),
	on(timetablesActions.loadTimetablesFailure, (state, { error }) => ({
		...state,
		loading: false,
		error: error,
	})),
	on(timetablesActions.deleteTimetableSuccess, (state, { id }) =>
		adapter.removeOne(id, state)
	),
	on(timetablesActions.timetableSelected, (state, { timetable }) => ({
		...state,
		timetable: timetable,
	}))
);

export const getSelectedIds = (state: State) => state.selectedIds;
export const getLoading = (state: State) => state.loading;
export const getCount = (state: State) => state.count;
export const getTimetable = (state: State) => state.timetable;
