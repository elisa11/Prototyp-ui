import { Pipe, PipeTransform } from '@angular/core';
import { ActivityCategory } from '@elisa11/elisa-api-angular-client';
import { LocaleService } from '@core/service/locale/locale.service';

@Pipe({
	name: 'categoryTranslate',
	pure: false,
})
export class CategoryTranslatePipe implements PipeTransform {
	constructor(private localeService: LocaleService) {}

	transform(value: ActivityCategory): unknown {
		return this.localeService.currentLocale === 'sk' ? value.name : value.name_en;
	}
}
