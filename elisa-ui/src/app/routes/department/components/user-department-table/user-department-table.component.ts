import { Component } from '@angular/core';
import { AbstractTableComponent } from '@core/generics/abstract-table.component';
import { UserDepartmentModel } from '@data/model/user-department.model';

@Component({
	selector: 'app-user-department-table',
	templateUrl: './user-department-table.component.html',
})
export class UserDepartmentTableComponent extends AbstractTableComponent<UserDepartmentModel> {
	displayedColumns = ['username', 'name', 'type'];

	constructor() {
		super();
	}
}
