import * as fromForm from '../reducers/form.reducer';
import { Action, combineReducers } from '@ngrx/store';
import * as fromRoot from '@core/store/reducers';
import * as fromRoomTypes from './room-category.reducer';
import * as fromActivityCategory from './activity-category.reducer';
import * as fromStudyForm from './study-form.reducer';
import * as fromUserSubjectRole from './user-subject-role.reducer';

export const sharedFeatureKey = 'shared';

export interface SharedState extends fromRoot.AppState {
	[fromForm.formFeatureKey]: fromForm.FormState;
	[fromRoomTypes.roomTypesFeatureKey]: fromRoomTypes.State;
	[fromActivityCategory.activityCategoryFeatureKey]: fromActivityCategory.State;
	[fromStudyForm.studyFormFeatureKey]: fromStudyForm.State;
	[fromUserSubjectRole.userSubjectRoleFeatureKey]: fromUserSubjectRole.State;
}

export function reducers(state: SharedState | undefined, action: Action) {
	return combineReducers({
		[fromForm.formFeatureKey]: fromForm.reducer,
		[fromRoomTypes.roomTypesFeatureKey]: fromRoomTypes.reducer,
		[fromActivityCategory.activityCategoryFeatureKey]: fromActivityCategory.reducer,
		[fromStudyForm.studyFormFeatureKey]: fromStudyForm.reducer,
		[fromUserSubjectRole.userSubjectRoleFeatureKey]: fromUserSubjectRole.reducer,
	})(state, action);
}
