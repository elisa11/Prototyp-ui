import { createReducer, on } from '@ngrx/store';
import { createEntityAdapter, EntityAdapter, EntityState } from '@ngrx/entity';
import { courseActions, courseApiActions } from '../actions';
import { CourseSerializerFull } from '@elisa11/elisa-api-angular-client';

export const courseFeatureKey = 'course';

export interface State extends EntityState<CourseSerializerFull> {
	readonly selectedId: number;
	readonly count: number;
}

export const adapter: EntityAdapter<CourseSerializerFull> =
	createEntityAdapter<CourseSerializerFull>({
		selectId: (model: CourseSerializerFull) => model.id,
		sortComparer: false,
	});

export const initialState: State = adapter.getInitialState({
	selectedId: null,
	count: 0,
});

export const reducer = createReducer(
	initialState,
	on(courseApiActions.searchCourseSuccess, (state: State, { data }) =>
		adapter.upsertMany(data.results, {
			...state,
			count: data.count,
		})
	),
	on(courseActions.loadCourse, (state, { course }) => adapter.upsertOne(course, state)),
	on(courseActions.selectCourse, (state, { id }) => ({ ...state, selectedId: id })),
  on(courseApiActions.deleteCourseDone, (state, { id }) =>
    adapter.removeOne(id, state))
);

export const getSelectId = (state: State) => state.selectedId;
export const getCount = (state: State) => state.count;
