import { Pipe, PipeTransform } from '@angular/core';
import { RoomCategory } from '@elisa11/elisa-api-angular-client';

@Pipe({
	name: 'roomType',
})
export class RoomTypePipe implements PipeTransform {
	transform(value: RoomCategory): unknown {
		return value ? value.name ?? '' : '';
	}
}
