import { Component, OnInit } from '@angular/core';
import { CommonText } from '@data/common-text';
import { MockService } from '@core/service/mock.service';
import { Store } from '@ngrx/store';
import { loadSchemasDialogSuccess } from '@core/store/actions/schema.actions';

@Component({
	selector: 'app-home',
	templateUrl: './home.component.html',
	styleUrls: ['./home.component.css'],
})
export class HomeComponent implements OnInit {
	data = ['data 1', 'data 2', 'data 3'];

	constructor(private mock: MockService, private store: Store) {}

	get req(): any {
		return CommonText.StatusValues.map((status) => {
			const item: {
				color: string;
				name: string;
				icon: string;
				count?: number;
			} = Object.assign({}, status);

			// item.count = this.mock.requirements.length;
			item.count = this.mock.requirements.filter(
				(value) => value.state === item.name
			).length;
			return item;
		});
	}

	get newReq(): number {
		return this.mock.requirements.filter((value) => value.state === 'Created').length;
	}

	get editedReq(): number {
		return this.mock.requirements.filter((value) => value.state === 'Edited').length;
	}

	get rejectedReq(): number {
		return this.mock.requirements.filter((value) => value.state === 'Rejected').length;
	}

	get approvedReq(): number {
		return this.mock.requirements.filter((value) => value.state === 'Approved').length;
	}

	ngOnInit(): void {
		if (localStorage.getItem('schema') !== null) {
			this.store.dispatch(
				loadSchemasDialogSuccess({
					data: JSON.parse(localStorage.getItem('schema')),
					old: null,
				})
			);
		}
	}
}
