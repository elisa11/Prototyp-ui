import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import {
	DepartmentsService,
	PaginatedUserDepartmentList,
	UserDepartmentService,
} from '@elisa11/elisa-api-angular-client';
import { AbstractCacheService } from './abstract-cache.service';
import { PaginatorFilter } from '@data/filter/paginator.filter';
import { BasicUtils } from '@shared/utils/basic.utils';

const tag = 'UserDepartmentsCacheService';

@Injectable({
	providedIn: 'root',
})
export class UserDepartmentsCacheService extends AbstractCacheService {
	private getUserDepartmentsCache = new Map();

	constructor(
		private service: DepartmentsService,
		private userService: UserDepartmentService
	) {
		super();
	}

	clearCache(): void {
		this.getUserDepartmentsCache.clear();
	}

	getUserDepartments(
		uid: number,
		did: number,
		filter?: PaginatorFilter
	): Observable<PaginatedUserDepartmentList> {
		const obs = this.getUserDepartmentsCache.get({ uid, did, filter });
		if (obs) {
			return obs;
		}

		const response = this.userService.userDepartmentList(
			did,
			null,
			filter ? filter.limit : undefined,
			filter ? filter.offset : undefined,
			BasicUtils.getSortBy(filter),
			filter ? filter.search : undefined,
			uid
		);

		this.getUserDepartmentsCache.set({ uid, did, filter }, response);
		return response;
	}
}
