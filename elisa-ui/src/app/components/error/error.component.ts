import { AfterViewChecked, Component, OnDestroy } from '@angular/core';
import { Subscription, timer } from 'rxjs';
import { Router } from '@angular/router';

@Component({
	selector: 'app-error',
	templateUrl: './error.component.html',
	styleUrls: ['./error.component.css'],
})
export class ErrorComponent implements AfterViewChecked, OnDestroy {
	sub$: Subscription;

	constructor(private router: Router) {}

	ngAfterViewChecked(): void {
		const source = timer(4500);
		this.sub$ = source.subscribe((val) => this.router.navigate(['']));
	}

	ngOnDestroy(): void {
		if (this.sub$) {
			this.sub$.unsubscribe();
		}
	}
}
