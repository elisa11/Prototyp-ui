import { Component, Input, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import {
	ActivityCategory,
	Department,
	Group,
	Room,
} from '@elisa11/elisa-api-angular-client';
import { select, Store } from '@ngrx/store';
import { activityCategoryActions } from '@shared/store/actions';
import { activityCategorySelectors } from '@shared/store/selectors';
import { Observable, map, switchMap, debounceTime } from 'rxjs';
import { AdvancedFilter } from '../../../model/advanced-filter.model';
import { filterActions, roomActions } from '../../../store/actions';
import { filterSelector, roomSelectors } from '../../../store/selectors';

@Component({
	selector: 'app-timetable-filter',
	templateUrl: './timetable-filter.component.html',
	styleUrls: ['./timetable-filter.component.scss'],
})
export class TimetableFilterComponent implements OnInit {
	@Input() formGroup: FormGroup;

	faculties$: Observable<Department[]>;
	studyPlans$: Observable<Group[]>;
	studyYears$: Observable<Group[]>;
	specializations$: Observable<Group[]>;
	timetableEvents$: Observable<ActivityCategory[]>;
	filter$: Observable<AdvancedFilter>;

	isLoading$ = this.store.pipe(select(filterSelector.selectFilterOptionsLoading));
	isLoadingCategories$ = this.store.pipe(select(activityCategorySelectors.selectLoading));

	//Room Filter
	room: FormControl = new FormControl();
	filteredRooms: Room[];
	selectedRoom: Room;
	filteredRooms$: Observable<Room[]> = this.store.select(
		roomSelectors.selectSearchResults
	);
	loading$: Observable<boolean> = this.store.select(roomSelectors.selectSearchLoading);

	previousFormValue: any;
	constructor(private store: Store) {}

	ngOnInit(): void {
		this.getFilter();

		this.formGroup.valueChanges.subscribe((value) => {
			console.log(value);
			if (this.formGroup.get('room').dirty) {
				this.room.reset();
				this.selectedRoom = null;
				this.formGroup.get('room').reset();
			}
		});

		this.room.valueChanges.pipe(debounceTime(250)).subscribe((room) => {
			this.handleRoomFilterChange({ room });
		});
	}

	roomSelected(room) {
		if (room === null) {
			this.formGroup.setValue(this.previousFormValue);
		} else {
			this.resetForm();
			this.formGroup.get('room').setValue(room.id, { emitEvent: true });
			this.formGroup.get('room').markAsDirty();
			this.selectedRoom = room;
		}
	}

	resetForm() {
		this.previousFormValue = JSON.parse(JSON.stringify(this.formGroup.value));
		Object.keys(this.formGroup.controls).forEach((key) => {
			this.formGroup.get(key).setValue([], { emitEvent: false });
		});
	}

	handleRoomFilterChange(data: { room: string }) {
		this.store.dispatch(
			roomActions.loadRooms({
				search: typeof data.room === 'string' ? data.room : undefined,
			})
		);
	}

	getFilter() {
		this.store.select(filterSelector.selectFilterOptionsEmpty).subscribe((isEmpty) => {
			if (isEmpty) {
				this.store.dispatch(filterActions.getFilterOptions());
			}
		});
		const filter$ = this.store.pipe(select(filterSelector.selectFilterOptions));
		this.faculties$ = filter$.pipe(map((data) => data.faculties));
		this.studyPlans$ = filter$.pipe(map((data) => data.studyPlans));
		this.studyYears$ = filter$.pipe(map((data) => data.years));
		this.specializations$ = filter$.pipe(map((data) => data.specializations));
		this.timetableEvents$ = this.store
			.select(activityCategorySelectors.selectIsEmpty)
			.pipe(
				switchMap((isEmpty) => {
					console.log(isEmpty);
					if (isEmpty) {
						this.store.dispatch(activityCategoryActions.loadActivityCategories());
					}
					return this.store.select(activityCategorySelectors.selectAll);
				})
			);
	}
}
