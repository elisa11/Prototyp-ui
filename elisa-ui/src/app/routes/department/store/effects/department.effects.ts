import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { SnackEffects } from '@core/generics/snack.effects';
import { MatSnackBar } from '@angular/material/snack-bar';
import {
	catchError,
	debounceTime,
	map,
	skip,
	switchMap,
	takeUntil,
	tap,
	withLatestFrom,
} from 'rxjs/operators';
import { departmentApiActions } from '../actions';
import { resetCachedData } from '@core/store/actions/settings.actions';
import { DepartmentsCacheService } from '@core/service/cache/departments-cache.service';
import { asyncScheduler, EMPTY, of } from 'rxjs';
import { PaginatorFilter } from '@data/filter/paginator.filter';
import { Store } from '@ngrx/store';
import { schemaSelectors } from '@core/store/selectors';

@Injectable()
export class DepartmentEffects extends SnackEffects {
	resetData$ = createEffect(
		() =>
			this.actions$.pipe(
				ofType(resetCachedData),
				tap(() => this.service.clearCache())
			),
		{ dispatch: false }
	);

	search$ = createEffect(
		() =>
			({ debounce = 300, scheduler = asyncScheduler } = {}) =>
				this.actions$.pipe(
					ofType(departmentApiActions.searchDepartment),
					debounceTime(debounce, scheduler),
					withLatestFrom(this.store.select(schemaSelectors.selectScheme)),
					map((value) => {
						return { query: value[0].query, version: value[1].name };
					}),
					switchMap(({ query, version }) => {
						if (query === null) {
							return EMPTY;
						}

						const nextSearch$ = this.actions$.pipe(
							ofType(departmentApiActions.searchDepartment),
							skip(1)
						);

						return this.service.listDepartments(query, version).pipe(
							takeUntil(nextSearch$),
							map((data) => departmentApiActions.searchDepartmentSuccess({ data })),
							catchError((err) =>
								of(departmentApiActions.searchDepartmentFailure({ error: err.message }))
							)
						);
					})
				)
	);

	searchFailed$ = createEffect(
		() =>
			this.actions$.pipe(
				ofType(departmentApiActions.searchDepartmentFailure),
				tap((err) => this.openSnackbar(err.error))
			),
		{ dispatch: false }
	);

	constructor(
		private actions$: Actions,
		private service: DepartmentsCacheService,
		snack: MatSnackBar,
		private store: Store
	) {
		super(snack);
	}
}
