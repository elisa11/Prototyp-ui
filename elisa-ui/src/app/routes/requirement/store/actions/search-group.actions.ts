import { createAction, props } from '@ngrx/store';
import { PaginatedGroupList } from '@elisa11/elisa-api-angular-client';

export const searchGroups = createAction(
	'[Requirement - Group] Search Groups',
	props<{ search: string }>()
);

export const searchGroupsSuccess = createAction(
	'[Requirement - Group] Search Groups Success',
	props<{ data: PaginatedGroupList }>()
);

export const searchGroupsFailure = createAction(
	'[Requirement - Group] Search Groups Failure',
	props<{ error: any }>()
);
