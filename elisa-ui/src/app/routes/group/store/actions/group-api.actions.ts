import { createAction, props } from '@ngrx/store';
import { PaginatedGroupList } from '@elisa11/elisa-api-angular-client';
import { PaginatorFilter } from '@data/filter/paginator.filter';

export const searchGroup = createAction(
	'[Group - Group/Api] Search Group',
	props<{ query: PaginatorFilter; filters: { studyPlan: number; parent: number } }>()
);

export const searchGroupSuccess = createAction(
	'[Group - Group/Api] Search Group Success',
	props<{ data: PaginatedGroupList }>()
);

export const searchGroupFailure = createAction(
	'[Group - Group/Api] Search Group Failure',
	props<{ error: any }>()
);
