import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { debounceTime, takeUntil } from 'rxjs/operators';
import { User, UserSubjectRole } from '@elisa11/elisa-api-angular-client';
import { FormBuilder, FormGroupDirective, Validators } from '@angular/forms';
import { isNotNullOrUndefined } from 'codelyzer/util/isNotNullOrUndefined';
import { Subject, Subscription } from 'rxjs';
import { FullnamePipe } from '@shared/pipes/user/fullname.pipe';
import { SubjectTeacherModel } from '../../model/subject-teacher.model';
import { Paths } from '@shared/store/reducers/form.reducer';
import { Store } from '@ngrx/store';
import { formActions } from '@shared/store/actions';

@Component({
	selector: 'app-teacher-select',
	templateUrl: './teacher-select.component.html',
})
export class TeacherSelectComponent implements OnInit, OnDestroy {
	@Input()
	readonly = false;
	selectedTeachers: SubjectTeacherModel[] = [];
	formGroup = this.fb.group({
		teacher: [undefined, Validators.required],
		teacherType: [[], Validators.required],
	});
	controls = {
		teacher: this.formGroup.controls.teacher,
		teacherType: this.formGroup.controls.teacherType,
	};
	@Input()
	teacherTypes: UserSubjectRole[] = [];
	@Input() filteredTeachers: User[] = [];
	private unsubscribe$ = new Subject();
	private filteredTeachersSub: Subscription;
	@Output()
	private onTeacherChange = new EventEmitter<string>();
	@Output()
	private onDataChange = new EventEmitter<SubjectTeacherModel[]>();

	constructor(
		private fb: FormBuilder,
		private fullnamePipe: FullnamePipe,
		private store: Store
	) {}

	@Input()
	set data(input: SubjectTeacherModel[]) {
		if (input) {
			this.selectedTeachers = input;
			this.onDataChange.emit(this.selectedTeachers);
		}
	}

	displayFn = (user: User) => {
		return this.fullnamePipe.transform(user);
	};
	path = Paths.teacher;
	@Input()
	loading: boolean = false;

	ngOnInit(): void {
		this.selectedTeachers = [];
		this.filteredTeachersSub = this.formGroup
			.get('teacher')
			.valueChanges.pipe(debounceTime(250), takeUntil(this.unsubscribe$))
			.subscribe((teacher) => {
				this.onTeacherChange.emit(teacher);
			});
	}

	ngOnDestroy(): void {
		this.unsubscribe$.next(this);
		this.unsubscribe$.complete();
	}

	addTeacher(): void {
		this.selectedTeachers.push({
			user: this.formGroup.controls.teacher.value,
			roles: this.formGroup.controls.teacherType.value
				.map((value) => this.teacherTypes.find((type) => type.id === value.id))
				.filter((type) => isNotNullOrUndefined(type)),
		});
		this.store.dispatch(formActions.submitFormSuccess({ path: this.path }));
		this.onDataChange.emit(this.selectedTeachers);
	}

	deleteTeacher(id: number): void {
		this.selectedTeachers = this.selectedTeachers.filter((value) => value.user.id !== id);
		this.onDataChange.emit(this.selectedTeachers);
	}

	resetForm(form: FormGroupDirective) {
		form.resetForm();
	}

	calculateHeight(selectedTeachers: SubjectTeacherModel[]) {
		return `${50 + (selectedTeachers.length % 5) * 50}px`;
	}
}
