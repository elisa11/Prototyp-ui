import { Component, Inject } from '@angular/core';
import { marker } from '@biesbjerg/ngx-translate-extract-marker';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { ConfirmDialogModel } from './confirm-dialog.model';

@Component({
	template: `
		<h1 mat-dialog-title>{{ title | translate }}</h1>
		<div mat-dialog-content>
			<p>{{ question | translate }}</p>
		</div>
		<div mat-dialog-actions>
			<button [mat-dialog-close]="false" mat-button>
				<mat-icon class="decline-color">cancel</mat-icon>
				{{ noLabel | translate }}
			</button>
			<button [mat-dialog-close]="true" mat-button>
				<mat-icon class="confirm-color">done</mat-icon>
				{{ yesLabel | translate }}
			</button>
		</div>
	`,
})
export class ConfirmDialogComponent {
	title: string = marker('dialog.confirm.default.title');
	question: string = marker('dialog.confirm.default.question');
	yesLabel: string = marker('dialog.confirm.default.yes-label');
	noLabel: string = marker('dialog.confirm.default.no-label');

	constructor(
		public dialogRef: MatDialogRef<ConfirmDialogComponent>,
		@Inject(MAT_DIALOG_DATA) private data: ConfirmDialogModel
	) {
		if (data) {
			if (data.title) {
				this.title = data.title;
			}
			if (data.question) {
				this.question = data.question;
			}
			if (data.yesLabel) {
				this.yesLabel = data.yesLabel;
			}
			if (data.noLabel) {
				this.noLabel = data.noLabel;
			}
		}
	}
}
