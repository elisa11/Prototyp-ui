import { ChangeDetectionStrategy, Component } from '@angular/core';
import { AbstractCollectionPageComponent } from '@core/generics/abstract-collection-page.component';
import { Group } from '@elisa11/elisa-api-angular-client';
import { PaginatorFilter } from '@data/filter/paginator.filter';
import { groupApiActions } from '../../store/actions';
import { Store } from '@ngrx/store';
import { searchGroup } from '../../store/actions/group-api.actions';
import { groupSelectors, searchGroupSelectors } from '../../store/selectors';

@Component({
	template: `
		<app-group-list
			[data]="data$ | async"
			[isLoading]="loading$ | async"
			[pagination]="pagination"
			[resultsLength]="totalCount$ | async"
			(onFilterChange)="handleFilterChange($event)"
		></app-group-list>
	`,
	changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CollectionPageComponent extends AbstractCollectionPageComponent<Group> {
	constructor(private store: Store) {
		super();
		store.dispatch(
			searchGroup({
				query: { limit: this.pagination[0], offset: 0 } as PaginatorFilter,
				filters: { studyPlan: undefined, parent: undefined },
			})
		);
		this.loading$ = store.select(searchGroupSelectors.selectSearchLoading);
		this.data$ = store.select(searchGroupSelectors.selectSearchResults);
		this.totalCount$ = store.select(groupSelectors.selectTotalCount);
	}

	handleFilterChange(filterChange: PaginatorFilter) {
		this.store.dispatch(
			groupApiActions.searchGroup({
				query: filterChange,
				filters: { studyPlan: undefined, parent: undefined },
			})
		);
	}
}
