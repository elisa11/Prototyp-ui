import { createAction, props } from '@ngrx/store';
import { Room } from '@elisa11/elisa-api-angular-client';
export const cancelRoom = createAction('[Room] Cancel Room');

export const loadRoom = createAction('[Room - Room] Load Room', props<{ data: Room }>());

export const selectRoom = createAction(
	'[Room - View Room page] Select Room',
	props<{ id: number }>()
);
