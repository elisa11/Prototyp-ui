import { createAction, props } from '@ngrx/store';
import { Collision, Timetable } from '@elisa11/elisa-api-angular-client';

// GET - Timetables/{parent_lookup_timetable}/collisions
export const loadTimetableCollisions = createAction(
	'[Timetable - Timetable Collisions] Load Timetable Collisions'
);

export const loadTimetableCollisionsSuccess = createAction(
	'[Timetable - Timetable Collisions] Load Timetable Collisions Success',
	props<{ collisions: Collision[] }>()
);

export const loadTimetableCollisionsFailure = createAction(
	'[Timetable - Timetable Collisions] Load Timetable Collisions Failure',
	props<{ error: any }>()
);

// GET - Timetables/{parent_lookup_timetable}/collisions
export const loadTimetableCollision = createAction(
	'[Timetable - Timetable Collisions] Load Timetable Collision',
	props<{ collision: number }>()
);

export const loadTimetableCollisionSuccess = createAction(
	'[Timetable - Timetable Collisions] Load Timetable Collision Success',
	props<{ collision: Collision }>()
);

export const loadTimetableCollisionFailure = createAction(
	'[Timetable - Timetable Collisions] Load Timetable Collision Failure',
	props<{ error: any }>()
);

export const updateCollision = createAction(
	'[Timetable - Timetable Collision] Put Timetable Collision',
	props<{ collision: Collision }>()
);

export const updateCollisionSuccess = createAction(
	'[Timetable - Timetable Collisions] Update Collision Success',
	props<{ collision: Collision }>()
);

export const updateCollisionFailure = createAction(
	'[Timetable - Timetable Collisions] Update Collision Failure',
	props<{ error: any }>()
);
