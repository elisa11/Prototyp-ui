import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { CommentModel } from '@data/model/comment.model';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
	selector: 'app-comment-list',
	templateUrl: './comment-list.component.html',
})
export class CommentListComponent implements OnInit {
	commentForm: FormGroup;
	editing: number = undefined;
	@Input()
	loggedUserId: number;
	@Output()
	private onDeleteComment = new EventEmitter<number>();
	@Output()
	private onEditComment = new EventEmitter<CommentModel>();
	private commentData: CommentModel[] = [];

	constructor(private fb: FormBuilder) {}

	@Input()
	get comments(): CommentModel[] {
		return this.commentData;
	}

	set comments(input) {
		this.commentData = input;
	}

	ngOnInit(): void {
		this.commentForm = this.fb.group({
			message: ['', Validators.required],
		});
	}

	deleteComment(id: number): void {
		this.onDeleteComment.emit(id);
	}

	editComment(model: CommentModel): void {
		this.editing = model.id;
	}

	saveEditComment(data: CommentModel): void {
		this.editing = undefined;
		this.onEditComment.emit({ ...data, text: this.commentForm.controls.message.value });
	}

	cancelEditing(): void {
		this.editing = undefined;
		this.commentForm.reset();
	}

	heightByComments(comments: CommentModel[]) {
		return `${(comments.length % 5) * 100}px`;
	}
}
