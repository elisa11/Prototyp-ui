import { Injectable } from '@angular/core';
import {
  Comments,
  CourseSerializerFull, DayEnum,
  Department,
  DepartmentSerializerShort,
  Equipment,
  Group,
  Requirement,
  Room,
  RoomEquipment,
  User,
  UserDepartment,
} from '@elisa11/elisa-api-angular-client';
import { Reply } from '@data/model/reply.model';
import { isNotNullOrUndefined } from 'codelyzer/util/isNotNullOrUndefined';
import moment from 'moment';
import { CommentModel } from '@data/model/comment.model';
import { CommonText } from '@data/common-text';
import { RequirementData } from '@data/model/requirement-data';
import { EventData } from '@data/model/event.model';
import { Collision } from '@data/model/collision.model';
import { CourseSerializerShort } from '@elisa11/elisa-api-angular-client';
import { TimeDataUnit } from '../../routes/requirement/model/time-data-unit.model';

@Injectable({
	providedIn: 'root',
})
export class MockService {
	events = [];
	departments: Array<Department> = [
		{ id: 1, abbr: 'ÚAMT', name: 'Ústav automobilovej mechatroniky' },
		{
			id: 2,
			abbr: 'ÚEAE',
			name: 'Ústav elektroenergetiky a aplikovanej elektrotechniky',
		},
		{ id: 3, name: 'Ústav elektroniky a fotoniky', abbr: 'ÚEF' },
		{ id: 4, abbr: 'ÚE', name: 'Ústav elektrotechniky' },
		{ id: 5, abbr: 'ÚIM', name: 'Ústav informatiky a matematiky' },
		{ id: 6, abbr: 'ÚJFI', name: 'Ústav jadrového a fyzikálneho inžinierstva' },
		{ id: 7, abbr: 'ÚRK', name: 'Ústav robotiky a kybernetiky' },
		{
			id: 8,
			abbr: 'ÚMIKT',
			name: 'Ústav multimediálnych informačných a komunikačných technológií',
		},
		{ id: 9, abbr: 'IKAL', name: 'Inštitút komunikácie a aplikovanej lingvistiky' },
		{ id: 10, abbr: 'TIŠ', name: 'Technologický inštitút športu' },
	];
	equipment150: Array<RoomEquipment> = [
		{
			equipment: { name: CommonText.roomItem[1], id: 1 } as Equipment,
			count: 1,
		} as RoomEquipment,
		// {equipment: {name: CommonText.roomItem[2], id: 2} as Equipment, count: 1} as RoomEquipment
	];
	equipment300: Array<RoomEquipment> = [
		{
			equipment: { name: CommonText.roomItem[1], id: 1 } as Equipment,
			count: 1,
		} as RoomEquipment,
		{
			equipment: { name: CommonText.roomItem[2], id: 2 } as Equipment,
			count: 2,
		} as RoomEquipment,
	];
	groups: Array<Group> = [
		{ id: 1, abbr: 'I-SK1', name: 'I-Skupina 1' },
		{ id: 2, abbr: 'I-SK2', name: 'I-Skupina 2' },
		{ id: 3, abbr: 'I-SK3', name: 'I-Skupina 3' },
		{ id: 4, abbr: 'I-SK4', name: 'I-Skupina 4' },
		{ id: 5, abbr: 'I-SK5', name: 'I-Skupina 5' },
	];
	users: Array<User> = [
		{
			id: 1,
			username: 'imate',
			title_after: 'Phd.',
			last_name: 'Matelko',
			first_name: 'Igor',
			title_before: 'Ing.',
			groups: [this.groups[0].name, this.groups[1].name],
		} as User,
		{
			id: 2,
			username: 'xbencot',
			title_after: '',
			last_name: 'Benco',
			first_name: 'Tomas',
			title_before: 'Bc.',
			groups: [this.groups[2].name, this.groups[3].name, this.groups[4].name],
		} as User,
	];
	userDepartment: Array<UserDepartment> = [
		{ employment: 'interny', department: 5, user: 1 } as UserDepartment,
		{ employment: 'externy', department: 5, user: 2 } as UserDepartment,
	];
	courses: Array<CourseSerializerFull> = [
		{
			id: 1,
			code: 'I-ASOS',
			name: 'Architektúra softvérových systémov ',
			department: this.departmentSerialize(5),
			teacher: this.teacherSerialize(1),
			completion: 's',
		},
		{
			id: 2,
			code: 'I-DP1-AI',
			name: 'Diplomový projekt 1 ',
			department: this.departmentSerialize(5),
			teacher: this.teacherSerialize(1),
			completion: 'kz',
		},
		{
			id: 3,
			code: 'I-MOBV',
			name: 'Mobilné výpočty ',
			department: this.departmentSerialize(5),
			teacher: this.teacherSerialize(1),
			completion: 's',
		},
		{
			id: 4,
			code: 'I-MSUS',
			name: 'Modelovanie a simulácia udalostných systémov',
			teacher: this.teacherSerialize(1),
			department: this.departmentSerialize(5),
			completion: 's',
		},
		{
			id: 5,
			code: 'I-MTMP',
			name: 'Multimédiá a telematika pre mobilné platformy ',
			teacher: this.teacherSerialize(1),
			department: this.departmentSerialize(2),
			completion: 's',
		},
		{
			id: 6,
			code: 'I-OP-AI',
			name: 'Odborná prax ',
			department: this.departmentSerialize(5),
			teacher: this.teacherSerialize(1),
			completion: 'z',
		},
	];
	loggedUser: User = this.users[1];
	admin = this.users[1];
	requirements = [
		{
			id: 1,
			course: this.courses[1],
			created_at: moment().toISOString(true),
			created_by: this.users[0],
			department: this.departments[0],
			state: 'Created',
			students_count: 20,
			exercise_after_lecture: false,
			exercise_odd_week: true,
			groups_count: 10,
			// max_parallel_courses: 3,
			max_serial_courses: 3,
			requirement_comments: [],
			requirement_parts: [],
			requirement_teachers: [],
			students_count_extra: 3,
		} as RequirementData,
		{
			id: 2,
			course: this.courses[1],
			created_at: moment().toISOString(true),
			created_by: this.users[0],
			department: this.departments[0],
			state: 'Edited',
			students_count: 20,
			exercise_after_lecture: false,
			exercise_odd_week: true,
			groups_count: 10,
			// max_parallel_courses: 3,
			max_serial_courses: 3,
			requirement_comments: [],
			requirement_parts: [],
			requirement_teachers: [],
			students_count_extra: 3,
		} as RequirementData,
	];
	replies = [];
	private rooms: Array<Room> = [
		{
			id: 1,
			capacity: 150,
			name: 'AB150',
			department: 6,
		},
		{
			id: 2,
			capacity: 300,
			name: 'AB300',
			department: 6,
		},
		{
			id: 3,
			capacity: 150,
			name: 'BC150',
			department: 5,
		},
		{
			id: 4,
			capacity: 300,
			name: 'BC300',
			department: 5,
		},
		{
			id: 5,
			capacity: 150,
			name: 'DE150',
			department: 3,
		},
		{
			id: 6,
			capacity: 300,
			name: 'DE300',
			department: 3,
		},
	];
	private practices: Array<CourseSerializerFull> = [
		{
			id: 7,
			code: 'I-ASOS',
			name: 'Architektúra softvérových systémov ',
			teacher: this.teacherSerialize(1),
			department: this.departmentSerialize(5),
		},
		{
			id: 8,
			code: 'I-DP1-AI',
			name: 'Diplomový projekt 1 ',
			teacher: this.teacherSerialize(1),
			department: this.departmentSerialize(5),
		},
		{
			id: 9,
			code: 'I-MOBV',
			name: 'Mobilné výpočty ',
			teacher: this.teacherSerialize(1),
			department: this.departmentSerialize(5),
		},
		{
			id: 10,
			code: 'I-MSUS',
			name: 'Modelovanie a simulácia udalostných systémov',
			teacher: this.teacherSerialize(1),
			department: this.departmentSerialize(5),
		},
		{
			id: 11,
			code: 'I-MTMP',
			name: 'Multimédiá a telematika pre mobilné platformy ',
			teacher: this.teacherSerialize(1),
			department: this.departmentSerialize(1),
		},
		{
			id: 12,
			code: 'I-OP-AI',
			name: 'Odborná prax ',
			teacher: this.teacherSerialize(1),
			department: this.departmentSerialize(5),
		},
	];

	private coursesShort: CourseSerializerShort =
    {
     id: 1,
     name: "Kurz matematiky"
    }

  private coursesArray: {course: CourseSerializerShort, time:string}[] =[{
	  course: this.coursesShort,
    time: "10:00"
  }]

  private roomsArray: {room: Room, time: string} ={
	  room: this.rooms.find((room) => room.id == 1),
    time: "13:00",
  }

  private collisions: Array<Collision> = [{
    courses : this.coursesArray,
    rooms: this.roomsArray
  }]
  private timeUnit: TimeDataUnit = {
	  day: 'FRIDAY',
    suitability:'Suitable',
    time: "20:00"
  }
  private departmentShort: DepartmentSerializerShort = {
    id:1,
    name: "školský ústav"
}

  eventsData: EventData = {
    id: 1,
    type: 1,
    teacher: this.teacherSerialize(1),
    room: this.rooms.find((room) => room.id == 1),
    // group: this.groups.find((group) => group.id == 1),
    day: DayEnum["MONDAY"],
    // startTime: moment().toISOString(true).toString(),
    // endTime: moment().toISOString(true),
    collisions: this.collisions,
    // requestedTime: this.timeUnit,
    name: "EventProblem",
    timetable: 1,
    department: this.departmentShort
  };

	getEventData(): EventData{
	  return this.eventsData;
  }

	getCourses(): Array<CourseSerializerFull> {
		return this.courses;
	}

	getPractices(): Array<CourseSerializerFull> {
		return this.practices;
	}

	getRooms(): Array<Room> {
		return this.rooms;
	}

	getDepartments(): Array<Department> {
		return this.departments;
	}

	departmentSerialize(id: number): DepartmentSerializerShort {
		const dep = this.departments.find((value) => value.id === id);
		return { id: dep.id, name: dep.name } as DepartmentSerializerShort;
	}

	getAllReply(commentId: number): Reply[] {
		const rep = this.replies.filter((value) => value.commentId === commentId);
		console.log('getAllReply ', commentId, 'result ', rep);
		return rep;
	}
	addComment(requirementId: number, item: CommentModel): void {
		if (requirementId && requirementId < this.requirements.length) {
			console.log('addComment', requirementId, item);
			const requirement = this.requirements.find((value) => requirementId === value.id);
			if (requirement) {
				requirement.requirement_comments.push(item);
			}
		}
	}

	deleteComment(id: string, postId: number): Array<CommentModel> {
		this.requirements
			.find((value) => value.id === postId)
			.requirement_comments.push(
				...this.requirements
					.find((value) => value.id === postId)
					.requirement_comments.filter((value) =>
						moment(value.created_at).isSame(moment(id))
					)
			);
		return this.requirements
			.find((value) => value.id === postId)
			.requirement_comments.map((value) => {
				return { ...value } as CommentModel;
			});
	}

	getComments(id: number): Array<Comments> {
		const post = this.requirements.find((value) => value.id === id);
		if (post) {
			return post.requirement_comments;
		} else {
			return [];
		}
	}

	addReply(submittedVal: Reply): Reply[] {
		console.log('addReply ', submittedVal);
		submittedVal.id = this.replies.length + 1;
		this.replies.push(submittedVal);
		return this.getAllReply(submittedVal.commentId);
	}

	deleteReply(id: number, commentId: number): Reply[] {
		console.log('delete replies: ', this.replies, ' id: ', id, 'commentId: ', commentId);
		this.replies = this.replies.filter((value) => value.id !== id);
		return this.replies;
	}

	findRoom(roomName: string): Room {
		return this.rooms.find((value) => value.name === roomName);
	}
	addRequirement(item: RequirementData): RequirementData[] {
		if (isNotNullOrUndefined(item.id)) {
			this.requirements = this.requirements.filter((value) => value.id !== item.id);
			this.requirements.push(item);
		} else {
			this.requirements.push(item);
		}
		return this.requirements;
	}

	login(username: string): boolean {
		const loginAttempt = this.users.find((value) => value.username === username);
		if (isNotNullOrUndefined(loginAttempt)) {
			this.loggedUser = loginAttempt;
			return true;
		} else {
			return false;
		}
	}

	findGroup(inputGroup: string): Group {
		return this.groups.find((value) => value.name === inputGroup);
	}

	teacherSerialize(id: number): User {
		return this.users.find((value) => value.id === id);
	}
}
