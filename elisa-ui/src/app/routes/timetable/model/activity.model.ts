import { TeacherType } from '@data/model/teacher-type.model';
import { DayEnum, Room, Event, Collision } from '@elisa11/elisa-api-angular-client';

export interface Activity {
	id: number;
	name: string;
	type: number;
	color: string;
	day?: string;
	hour?: number;
	courseId?: number;
	duration?: number;
	teachers?: TeacherType[];
	collision: boolean;
}

// export class Collision {
// 	activity1: number | string;
// 	activity2: number | string;
// 	teachers?: number[] | string[];
// 	hour: number;
// 	day: string;
// }

export class EventWrapper {
	event: Event;
	visible?: boolean = true;
	empty: boolean;

	constructor(event: Event, visible = true, empty: boolean = true) {
		this.event = event;
		this.visible = visible;
		this.empty = empty;
	}
}
