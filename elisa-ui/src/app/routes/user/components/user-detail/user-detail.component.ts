import { Component, Input } from '@angular/core';
import { CommonText } from '@data/common-text';
import { User } from '@elisa11/elisa-api-angular-client';
import { DepartmentData } from '../../model/department-data.model';
import { AbstractDetailComponent } from '@core/generics/abstract-detail.component';

@Component({
	selector: 'app-user-detail',
	templateUrl: './user-detail.component.html',
})
export class UserDetailComponent extends AbstractDetailComponent<User> {
	@Input()
	departmentData!: DepartmentData[];
	@Input()
	departmentDataError!: string;
	@Input()
	departmentDataLoading!: boolean;

	moduleId = 'user';
	moduleTitle = CommonText.navLinks.find((value) => value.link === this.moduleId).label;
}
