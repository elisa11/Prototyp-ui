import { DepartmentSerializerShort, Period, User } from '@elisa11/elisa-api-angular-client';

export interface CourseFormModel {
  id: number;
  code: string;
  name: string;
  completion: string;
  credits: number;
  period: Period;
  department: DepartmentSerializerShort;
  teacher: User;
}
