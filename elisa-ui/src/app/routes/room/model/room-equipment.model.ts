import { Equipment, RoomEquipment } from '@elisa11/elisa-api-angular-client';

export class RoomEquipmentModel{
  readonly id: number;
  countEquipment: number;
  facilityEquipment: Equipment;

  constructor(unit: RoomEquipment){
    Object.assign(this,unit);
  }
}

