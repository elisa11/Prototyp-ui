# Elisa-Ui Prototype

[![pipeline status](https://gitlab.com/elisa11/Prototyp-ui/badges/master/pipeline.svg)](https://gitlab.com/elisa11/Prototyp-ui/-/commits/master)
[![coverage report](https://gitlab.com/elisa11/Prototyp-ui/badges/master/coverage.svg)](https://gitlab.com/elisa11/Prototyp-ui/-/commits/master)

This project uses [Angular](https://github.com/angular/angular-cli) version 13.3.9.

## Development server

Run `npm run start` for a dev server. Navigate to `http://localhost:4202/`. The app will automatically reload if you change any of the source files.

## Initial project setup

Commands to run before first run.

### Project setup

- Add gitlab npm registry on your local machine - [tutorial](https://docs.gitlab.com/ee/user/packages/npm_registry/#install-a-package)
- Install / update elisa api client in project: `npm i @elisa11/elisa-api-angular-client@latest`
- Install / update other dependencies: `npm i`
- In case of dependency conflicts add `--legacy-peer-deps` flag
- Project needs Spring Boot server running, found in `elisa-ui-api/elisa-ui-api-endpoint` folder

## Elisa11 package

Elisa11 package is generated via Gitlab Pipeline, that runs each time new Backend version is pushed. It contains all services, which are generated based on models and endpoints from BE.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

## Diagrams

### Elisa-UI component diagram

![elisa-ui component diagram](modules_ui-Elisa_UI___Component_Diagram.png)

## Running Elisa via Ansible scripts

If you are deploying Elisa on a local virtual system using ansible scripts, it's recommended that you set
one network adapter to a bridged adapter on the virtual system. During working on Elisa system it's high recommended to use
VPN to school network and command:

`ssh -o StrictHostKeyChecking=no -o ServerAliveInterval=60 -i elisa_priv.pem -fNT -L 12345:zzz.is.stuba.sk:1521 ubuntu@147.175.xxx.yyy`

When the virtual system environment and backend (tutorial for how to set backend is in the documentation of backend)
are ready, it is necessary in the directory
`elisa-ui-api/elisa-ui-api-endpoint/src/main/resources` in the file
**application-dev.yml** (for development) change url for service elisa for the current Bridged Adapter IP address
in the form `https://192.168.xxx.xxx:8000`. Replace X with IP address on the Bridged Adapter.
The port may remain unchanged.

After this modification, you must use the command `mvn clean package`
in the elisa-ui-api-endpoint directory to run the build maven project. Then you can run command
`java -Dspring.profiles.active=dev -jar config-server.jar` in the newly created target directory, which runs
Spring Boot in developer mode.

In addition, you also need to edit file `proxy.conf.js` in the `elisa-ui/src/` directory, where a target with an IP address
and port of `http://192.168.xxx.xxx:8080/` is set. Replace X with IP address on the Bridged Adapter.
If you change port from 8080 in **application-dev.yml** to another, port in the proxy file must also change
with this new port.

It's recommended that you install the angular client for your virtual system to run the command
`ng serve --proxy-config src/proxy.conf.js --host 0.0.0.0` and start Angular CLI for Elisa.
Then you can set IP address with port 4200 on the browser and Elisa will show.

If you can't sign in to your account, you must go on the site: `https://192.168.xxx.xxx:8000/auth/login/` and when
you see warning: **Your connection is not private**, you only click on the button **Advanced** and proceed to page (unsafe).
Then you shall sign in to your account on the Angular port 4200.

All of these settings were tested by us (they are working well), but you can set them according to you.
