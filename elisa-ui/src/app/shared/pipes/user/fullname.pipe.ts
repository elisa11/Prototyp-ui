import { Pipe, PipeTransform } from '@angular/core';
import { User } from '@elisa11/elisa-api-angular-client';

@Pipe({
	name: 'fullname',
})
export class FullnamePipe implements PipeTransform {
	transform(user: User, title: boolean = true): string {
		if (user === null || user === undefined) {
			return '';
		}
		if (title) {
			return `${user.title_before} ${user.first_name} ${user.last_name} ${user.title_after}`;
		} else {
			return `${user.first_name} ${user.last_name}`;
		}
	}
}
