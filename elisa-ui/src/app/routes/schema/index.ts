import { MatTableModule } from '@angular/material/table';
import { MatIconModule } from '@angular/material/icon';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatTreeModule } from '@angular/material/tree';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatSortModule } from '@angular/material/sort';
import { MatCardModule } from '@angular/material/card';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { BooleanIconPipe } from './pipes/boolean-icon.pipe';
import { BooleanIconClassPipe } from './pipes/boolean-icon-color.pipe';
import { PeriodNamePipe } from './pipes/period-name.pipe';

export const MATERIAL_MODULES = [
	MatCheckboxModule,
	MatTreeModule,
	MatButtonModule,
	MatFormFieldModule,
	MatIconModule,
	MatTableModule,
	MatDatepickerModule,
	MatInputModule,
	MatPaginatorModule,
	MatProgressSpinnerModule,
	MatTooltipModule,
	MatCardModule,
	MatAutocompleteModule,
	MatSortModule,
];

export const PIPES = [BooleanIconPipe, BooleanIconClassPipe, PeriodNamePipe];
