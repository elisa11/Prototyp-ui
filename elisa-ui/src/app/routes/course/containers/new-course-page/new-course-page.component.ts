import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { courseActions, courseApiActions } from '../../store/actions';
import { CourseModel } from '../../model/course.model';

@Component({
  template: `<app-course-form
    [courseExistsData]="courseExistsData$"
    (onSubmit)="handleSubmit($event)"
    (onPut)="handlePut($event)"
    (onCancel)="handleCancelled()"
    (onBack)="handleBack($event)">
  </app-course-form>
	`,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NewCoursePageComponent implements OnInit {
  courseExistsData$: CourseModel | undefined = this.router.getCurrentNavigation().extras.state as CourseModel;

  ngOnInit(): void {
  }
  constructor(private router: Router, private store: Store) {
  }

  handleCancelled() {
    this.store.dispatch(courseActions.cancelCourse());
  }

  handleBack(path: string) {
    return this.router.navigate([path]);
  }

  handleSubmit(data: CourseModel) {
    this.store.dispatch(courseApiActions.postCourse({ data }));
  }

  handlePut(data: CourseModel) {
    this.store.dispatch(courseApiActions.putCourse({ data }));
  }
}
