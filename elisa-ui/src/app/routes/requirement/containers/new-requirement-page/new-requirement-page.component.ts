import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import {
	activityActions,
	courseApiActions,
	groupApiActions,
	requirementActions,
	requirementApiActions,
	roomApiActions,
	searchGroupActions,
	searchUserActions,
	studyPlansApiActions,
	subjectTeachersApiActions,
} from '../../store/actions';
import {
	activitySelectors,
	courseSelectors,
	groupSelectors,
	roomSelectors,
	searchGroupSelectors,
	searchUserSelectors,
	subjectTeachersSelectors,
} from '../../store/selectors';
import { DepartmentCourse } from '@data/model/department-course.model';
import { combineLatest, Observable } from 'rxjs';
import { Router } from '@angular/router';
import {
	activityCategorySelectors,
	roomCategorySelectors,
	studyFormSelectors,
	userSubjectRoleSelectors,
} from '@shared/store/selectors';
import {
	ActivityCategory,
	CourseSerializerFull,
	Group,
	Requirement,
	Room,
	RoomCategory,
	StudyForm,
	User,
	UserSubjectRole,
} from '@elisa11/elisa-api-angular-client';
import { map } from 'rxjs/operators';
import { ActivityModel } from '../../model/activity.model';
import { authSelectors } from '../../../../auth/store/selectors';
import { SubjectTeacherModel } from '../../model/subject-teacher.model';
import { RoomFilterModel } from '../../../room/model/room-filter.model';
import { RequirementModel } from '../../model/requirement.model';
import { RequirementPart } from '@elisa11/elisa-api-angular-client';

@Component({
	template: `
		<app-requirement-form
			[courseGroupOptions]="courseGroupOptions$ | async"
			[loadingCourses]="loadingCourses$ | async"
			[activityData]="activityData$ | async"
			[groupsData]="groupsData$ | async"
			[user]="user$ | async"
			[roomData]="roomData$ | async"
			[teacherData]="subjectTeachersData$ | async"
      [requirementExistsData]="requirementModelUpdate$"
			(onCourseChanged)="handleCourseChanged($event)"
			(onCourseSelected)="handleCourseSelected($event)"
			(onCourseReset)="handleCourseReset()"
			(onTeacherChanged)="handleTeacherChanged($event)"
			(onGroupChanged)="handleGroupChanged($event)"
			(onRoomFilterChange)="handleRoomFilterChange($event)"
			(onSubmit)="handleSubmit($event)"
			(onPut)="handlePut($event)"
			(onCancel)="handleCancelled()"
			(onBack)="handleBack($event)"
		></app-requirement-form>
	`,
	changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NewRequirementPageComponent implements OnInit {
	courseGroupOptions$: Observable<DepartmentCourse[]>;
	categories$: Observable<ActivityCategory[]>;
	activities$: Observable<ActivityModel[]>;
	activityData$: Observable<{
		data: ActivityModel[];
		categories: ActivityCategory[];
		studyForms: StudyForm[];
	}>;
	studyForms$: Observable<StudyForm[]>;
	user$: Observable<User>;
	subjectTeachersData$: Observable<{
		filtered: User[];
		types: UserSubjectRole[];
		data: SubjectTeacherModel[];
		loading: boolean;
	}>;
	loadingCourses$: Observable<boolean>;
	groupsData$: Observable<{
		filtered: Group[];
		data: Group[];
		loading: boolean;
	}>;
	roomData$: Observable<{ types: RoomCategory[]; filtered: Room[]; loading: boolean }>;
  requirementModelUpdate$: RequirementModel | undefined = this.router.getCurrentNavigation().extras.state as RequirementModel;

	constructor(private router: Router, private store: Store) {
		this.roomData$ = combineLatest([
			store.select(roomSelectors.selectSearchResults),
			store.select(roomCategorySelectors.selectAll),
			store.select(roomSelectors.selectSearchLoading),
		]).pipe(
			map((value) => {
				return { types: value[1], filtered: value[0], loading: value[2] };
			})
		);
		this.subjectTeachersData$ = combineLatest([
			store.select(subjectTeachersSelectors.selectSearchResults),
			store.select(userSubjectRoleSelectors.selectAll),
			store.select(searchUserSelectors.selectSearchResults),
			store.select(searchUserSelectors.selectSearchLoading),
		]).pipe(
			map((value) => {
				return { data: value[0], types: value[1], filtered: value[2], loading: value[3] };
			})
		);
		this.groupsData$ = combineLatest([
			store.select(groupSelectors.selectSelectedResults),
			store.select(searchGroupSelectors.selectSearchResults),
			store.select(groupSelectors.selectSearchLoading),
		]).pipe(
			map((value) => {
				return { data: value[0], filtered: value[1], loading: value[2] };
			})
		);
		this.user$ = store.select(authSelectors.selectLoggedUser);
		this.courseGroupOptions$ = store.select(courseSelectors.selectGroupedData);
		this.loadingCourses$ = store.select(courseSelectors.selectSearchLoading);
		this.categories$ = store.select(activityCategorySelectors.selectAll);
		this.studyForms$ = store.select(studyFormSelectors.selectAll);
		this.activities$ = combineLatest([
			store.select(activitySelectors.selectCourseResults),
			store.select(activityCategorySelectors.selectAll),
			store.select(studyFormSelectors.selectAll),
		]).pipe(
			map((value) => {
				return { activities: value[0], categories: value[1], forms: value[2] };
			}),
			map((data) =>
				data.activities.map(
					(a) =>
						new ActivityModel(
							a,
							data.categories.find((c) => c.id === a.category),
							data.forms.find((f) => f.id === a.form)
						)
				)
			)
		);
		this.activityData$ = combineLatest([
			this.activities$,
			this.categories$,
			this.studyForms$,
		]).pipe(
			map((value) => {
				return { data: value[0], categories: value[1], studyForms: value[2] };
			})
		);
	}

	ngOnInit(): void {
		this.handleCourseSelected({} as CourseSerializerFull);
	}

	handleCourseChanged(search: string | CourseSerializerFull): void {
		if (typeof search === 'string') {
			this.store.dispatch(courseApiActions.loadCourses({ search }));
		}
	}

	handleGroupChanged(search: string) {
		if (typeof search === 'string') {
			this.store.dispatch(searchGroupActions.searchGroups({ search }));
		}
	}

	handleCancelled() {
		this.store.dispatch(requirementActions.cancelRequirement());
	}

	handleBack(path: string) {
		return this.router.navigate([path]);
	}

	handleCourseSelected(data: CourseSerializerFull) {
		this.store.dispatch(activityActions.loadActivities({ cid: data.id }));
		this.store.dispatch(
			subjectTeachersApiActions.loadUserSubjectsByCourse({ cid: data.id })
		);
		this.store.dispatch(studyPlansApiActions.loadStudyPlan({ cid: data.id }));
	}

	handleTeacherChanged(search: string) {
		if (typeof search === 'string') {
			this.store.dispatch(searchUserActions.searchUsers({ search }));
		}
	}

	handleCourseReset() {
		this.store.dispatch(activityActions.resetActivities());
		this.store.dispatch(subjectTeachersApiActions.resetUserSubjectsByCourse());
		this.store.dispatch(groupApiActions.resetGroupBySP());
	}

	handleRoomFilterChange(data: {
		room: string;
		capacity: { min: number; max: number };
		type: RoomCategory[];
	}) {
		this.store.dispatch(
			roomApiActions.loadRooms({
				search: typeof data.room === 'string' ? data.room : undefined,
				roomFilter: {
					capacity: data.capacity,
					type: data.type.map((t) => t.id),
				} as RoomFilterModel,
			})
		);
	}

	handleSubmit(data: RequirementModel) {
    console.log("postRequirement");
		this.store.dispatch(requirementApiActions.postRequirement({ data }));
	}

  handlePut(data: RequirementModel) {
    console.log("putRequirement");
    this.store.dispatch(requirementApiActions.putRequirement({ data }));
  }
}
