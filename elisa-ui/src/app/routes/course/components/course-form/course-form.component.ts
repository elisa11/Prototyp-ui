import {
  Component,
  EventEmitter, Input,
  NgZone,
  OnDestroy,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import {
  CourseSerializerFull, DepartmentSerializerShort, User,
} from '@elisa11/elisa-api-angular-client';
import { Subject, Subscription } from 'rxjs';
import { CdkTextareaAutosize } from '@angular/cdk/text-field';
import { Paths } from '@shared/store/reducers/form.reducer';
import { take } from 'rxjs/operators';
import { CommonText } from '@data/common-text';
import { AbstractControl, FormBuilder, FormGroup, FormGroupDirective, Validators } from '@angular/forms';
import { NGXLogger } from 'ngx-logger';
import { Store } from '@ngrx/store';
import { CourseModel } from '../../model/course.model';

const tag = 'CourseFormComponent';

@Component({
  selector: 'app-course-form',
  templateUrl: './course-form.component.html',
})
export class CourseFormComponent implements OnInit, OnDestroy {
  @ViewChild('autosize') autosize: CdkTextareaAutosize;
  courseForm: FormGroup;
  controls: { [p: string]: AbstractControl };
  path: Paths.course;
  private moduleId = 'course';
  moduleTitle = CommonText.navLinks.find((value) => value.link === this.moduleId).label;
  private unsubscribe$ = new Subject();
  @Input()
  courseExistsData!: CourseModel | undefined;

  @Output() private onSubmit = new EventEmitter<CourseModel>();
  @Output() private onPut = new EventEmitter<CourseModel>();
  @Output() private onCancel = new EventEmitter<string>();
  @Output() private onBack = new EventEmitter<string>();
  private errorSub: Subscription;

  constructor(
    private ngZone: NgZone,
    private fb: FormBuilder,
    private logger: NGXLogger,
    private store: Store
  ) {}

  get formData(): any {
    return this.courseForm.value;
  }


  ngOnDestroy(): void {
    this.unsubscribe$.next(this);
    this.unsubscribe$.complete();
  }

  triggerResize(): void {
    this.ngZone.onStable
      .pipe(take(1))
      .subscribe(() => this.autosize.resizeToFitContent(true));
  }

  ngOnInit(): void {
    const numberValidation = Validators.compose([Validators.required, Validators.min(0)]);
    if (this.courseExistsData !== undefined && this.courseExistsData){
      this.courseForm = this.fb.group({
        courseName: [this.courseExistsData.name],
        guarantor: [this.courseExistsData.teacher.first_name+' '+this.courseExistsData.teacher.last_name],
        departmentForCourse: [this.courseExistsData.department.name],
        codeForCourse: [this.courseExistsData.code],
        completionForCourse: [this.courseExistsData.completion],
        creditsForCourse: [this.courseExistsData.credits, numberValidation],
        periodOfCourse: [this.courseExistsData.period, numberValidation],
      });
    }
    else {
      this.courseForm = this.fb.group({
        courseName: [''],
        guarantor: [''],
        departmentForCourse: [''],
        codeForCourse: [''],
        completionForCourse: [''],
        creditsForCourse: ['', numberValidation],
        periodOfCourse: ['', numberValidation],
      });
    }
    this.controls = this.courseForm.controls;
    this.errorSub = this.courseForm.valueChanges.subscribe((value) => {
      const errors = [];
      Object.keys(this.courseForm.controls).forEach((key) => {
        // Get errors of every form control
        errors.push({ key, err: this.courseForm.get(key).errors });
      });
      console.log(errors);
    })
  }

  back(): void {
    return this.onBack.emit(this.moduleId);
  }

  onCourseCancel(): void {
    this.logger.debug(tag, 'onCourseCancel');
    this.onCancel.emit(this.moduleId);
    this.back();
  }

  submit(): void {
    //TODO: vytvoriť logiku, ako nasetovať údaje pre učiteľa a department
    // po zadaní vo formulári pre post a put
    let department: DepartmentSerializerShort= {
      id: 0, name: this.formData.departmentForCourse
    }
    let teacher: User = {
      first_name: '',
      groups: undefined,
      id: 0,
      last_name: '',
      title_after: undefined,
      title_before: undefined,
      username: this.formData.guarantor
    }
    let item: CourseModel = {
      code: this.formData.codeForCourse,
      completion: this.formData.completionForCourse,
      credits: this.formData.creditsForCourse,
      department: department,
      id: 0,
      name: this.formData.courseName,
      period: this.formData.periodOfCourse,
      teacher: teacher
    }

    this.logger.debug(tag, 'onSubmit');
    if (this.courseExistsData !== undefined){
      item.id = this.courseExistsData.id;
      this.onPut.emit(item);
    }
    else{
      this.onSubmit.emit(item);
    }
    this.back();
  }

  resetForm(form: FormGroupDirective) {
    form.resetForm();
  }
}
