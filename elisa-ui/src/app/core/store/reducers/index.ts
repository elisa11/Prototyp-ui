import { Action, ActionReducer, ActionReducerMap, MetaReducer } from '@ngrx/store';
import { environment } from '@environment/environment';
import * as fromRouter from '@ngrx/router-store';
import * as fromLayout from './layout.reducer';
import * as fromLanguage from './language.reducer';
import * as fromSchema from './schema.reducer';
import { InjectionToken } from '@angular/core';

export interface AppState {
	[fromLayout.layoutFeatureKey]: fromLayout.LayoutState;
	[fromLanguage.languageFeatureKey]: fromLanguage.LanguageState;
	[fromSchema.schemaFeatureKey]: fromSchema.State;
	router: fromRouter.RouterReducerState<any>;
}

/**
 * Our state is composed of a map of action reducer functions.
 * These reducer functions are called with each dispatched action
 * and the current or initial state and return a new immutable state.
 */
export const ROOT_REDUCERS = new InjectionToken<ActionReducerMap<AppState, Action>>(
	'Root reducers token',
	{
		factory: () => ({
			[fromLayout.layoutFeatureKey]: fromLayout.reducer,
			[fromLanguage.languageFeatureKey]: fromLanguage.reducer,
			[fromSchema.schemaFeatureKey]: fromSchema.reducer,
			router: fromRouter.routerReducer,
		}),
	}
);

// console.log all actions
export function logger(reducer: ActionReducer<AppState>): ActionReducer<AppState> {
	return (state, action) => {
		const result = reducer(state, action);
		console.groupCollapsed(action.type);
		console.log('prev state', state);
		console.log('action', action);
		console.log('next state', result);
		console.groupEnd();

		return result;
	};
}

/**
 * By default, @ngrx/store uses combineReducers with the reducer map to compose
 * the root meta-reducer. To add more meta-reducers, provide an array of meta-reducers
 * that will be composed to form the root meta-reducer.
 */
export const metaReducers: MetaReducer<AppState>[] =
	environment.param.environment !== 'prod' ? [logger] : [];
