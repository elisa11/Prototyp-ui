import { MatSnackBar } from '@angular/material/snack-bar';

export abstract class SnackEffects {
	protected constructor(private snack: MatSnackBar) {}

	protected openSnackbar(msg: string) {
		return this.snack.open(msg, 'X', {
			horizontalPosition: 'end',
			verticalPosition: 'bottom',
		});
	}
}
