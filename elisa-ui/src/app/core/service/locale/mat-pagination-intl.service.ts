import { Injectable } from '@angular/core';
import { MatPaginatorIntl } from '@angular/material/paginator';
import { marker } from '@biesbjerg/ngx-translate-extract-marker';
import { LocaleService } from '@core/service/locale/locale.service';

@Injectable({
	providedIn: 'root',
})
export class MatPaginationIntlService extends MatPaginatorIntl {
	constructor(private translateService: LocaleService) {
		super();
		// React whenever the language is changed
		this.translateService.onLangChange.subscribe((_event) => {
			this.translateLabels();
		});

		// Initialize the translations once at construction time
		this.translateLabels();
	}

	getRangeLabel = (page: number, pageSize: number, length: number): string => {
		const of = this.translateService
			? this.translateService.translate(marker('material.paginator.of'))
			: 'of';
		if (length === 0 || pageSize === 0) {
			return '0 ' + of + ' ' + length;
		}
		length = Math.max(length, 0);
		const startIndex =
			page * pageSize > length
				? (Math.ceil(length / pageSize) - 1) * pageSize
				: page * pageSize;

		const endIndex = Math.min(startIndex + pageSize, length);
		return startIndex + 1 + ' - ' + endIndex + ' ' + of + ' ' + length;
	};

	translateLabels(): void {
		this.firstPageLabel = this.translateService.translate(
			marker('material.paginator.first_page')
		);
		this.itemsPerPageLabel = this.translateService.translate(
			marker('material.paginator.items_per_page')
		);
		this.lastPageLabel = this.translateService.translate(
			marker('material.paginator.last_page')
		);
		this.nextPageLabel = this.translateService.translate(
			marker('material.paginator.next_page')
		);
		this.previousPageLabel = this.translateService.translate(
			marker('material.paginator.previous_page')
		);
		this.changes.next(); // Fire a change event to make sure that the labels are refreshed
	}
}
