import { LOCALE_ID, Provider } from '@angular/core';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import {
	MAT_MOMENT_DATE_ADAPTER_OPTIONS,
	MAT_MOMENT_DATE_FORMATS,
	MomentDateAdapter,
} from '@angular/material-moment-adapter';
import { LocaleService } from '@core/service/locale/locale.service';

export class LocaleId extends String {
	constructor(private localeService: LocaleService) {
		super();
	}

	toString(): string {
		return this.localeService.currentLocale;
	}

	valueOf(): string {
		return this.toString();
	}
}

export const localeProvider: Provider = {
	provide: LOCALE_ID,
	useClass: LocaleId,
	deps: [LocaleService],
} as Provider;

export const dateLocaleProvider: Provider = { provide: MAT_DATE_LOCALE, useValue: 'sk' };

export const dateAdapterProvider: Provider = {
	provide: DateAdapter,
	useClass: MomentDateAdapter,
	deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS],
};

export const dateFormatsProvider: Provider = {
	provide: MAT_DATE_FORMATS,
	useValue: MAT_MOMENT_DATE_FORMATS,
};
