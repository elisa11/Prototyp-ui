import { Component, EventEmitter, Output } from '@angular/core';
import { RequirementModel } from '../../model/requirement.model';
import { AbstractTableComponent } from '@core/generics/abstract-table.component';
import { Store } from '@ngrx/store';
import { deleteRequirement } from '../../store/actions/requirement-api.actions';
import { MatDialog } from '@angular/material/dialog';
import { ConfirmDialogModel } from '../../../../dialogs/confirm-dialog/confirm-dialog.model';
import { ConfirmDialogComponent } from '../../../../dialogs/confirm-dialog/confirm-dialog.component';
import { map, Observable } from 'rxjs';

@Component({
	selector: 'app-requirement-list',
	templateUrl: './requirement-list.component.html',
  styleUrls: ['./requirement-list.component.scss'],

})
export class RequirementListComponent extends AbstractTableComponent<RequirementModel> {
	displayedColumns = ['code', 'course', 'status', 'createdBy', 'guarantor', 'teacher', 'btn'];
	@Output()
  private onNewRequirement: EventEmitter<RequirementModel|undefined> =
    new EventEmitter<RequirementModel | undefined>();

	constructor(
	  private store: Store,
    private dialog: MatDialog) {
		super();
	}

	newRequirement(): void {
		this.onNewRequirement.emit();
	}

  deleteItem($event:MouseEvent,row: RequirementModel): void {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      data: {
        title: "Vymazanie požiadavky",
        // title: marker('dialog.import.title'),
        question: "Chcete vymazať požiadavku?",
        // question: marker('dialog.import.question'),
        noLabel: "Nie",
        // noLabel: marker('dialog.import.no-label'),
        yesLabel: "Áno",
        // yesLabel: marker('dialog.import.yes-label'),
      } as ConfirmDialogModel,
      disableClose: true,
    });
    dialogRef.afterClosed().subscribe(dialogRes =>{
      if (dialogRes){
        this.store.dispatch(deleteRequirement({id:row.id}));
      }
    })
    $event.stopImmediatePropagation();
  }

  //TODO: načítavanie súborov pre requirements
  handle($event) {
    console.log('loading file', $event.target.files);
  }

  //TODO: editovanie requirementu spraviť
  editRequirement($event: MouseEvent, row: any) {
    this.onNewRequirement.emit(row);
    $event.stopImmediatePropagation();
  }
}
