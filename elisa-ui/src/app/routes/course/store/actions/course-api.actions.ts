import { createAction, props } from '@ngrx/store';
import { Course, PaginatedCourseSerializerFullList } from '@elisa11/elisa-api-angular-client';
import { PaginatorFilter } from '@data/filter/paginator.filter';
import { CourseModel } from '../../model/course.model';

export const searchCourse = createAction(
	'[Course - Course/Api] Search Course/Api',
	props<{ query: PaginatorFilter }>()
);

export const searchCourseSuccess = createAction(
	'[Course - Course/Api] Search Course/Api Success',
	props<{ data: PaginatedCourseSerializerFullList }>()
);

export const searchCourseFailure = createAction(
	'[Course - Course/Api] Search Course/Api Failure',
	props<{ error: string }>()
);

export const postCourse = createAction(
  '[Course - Course/Api Post Course',
  props<{ data: CourseModel }>()
);

export const postCourseSuccess = createAction(
  '[Course - Course/Api Create (Post) Course Success',
  props<{ data: Course }>()
);

export const postCourseFailure = createAction(
  '[Course - Course/Api Create (Post) Course Failure',
  props<{ error: any }>()
);

export const putCourse = createAction(
  '[Course - Course/Api Put Course',
  props<{ data: CourseModel }>()
);

export const putCourseSuccess = createAction(
  '[Course - Course/Api Put Course Success',
  props<{ data: Course }>()
);

export const putCourseFailure = createAction(
  '[Course - Course/Api Put Course Failure',
  props<{ error: any }>()
);

export const deleteCourse = createAction(
  '[Course - Course/Api] Delete Course',
  props<{ id: number }>()
);

export const deleteCourseDone = createAction(
  '[Course - Course/Api] Delete Course Done',
  props<{ id: number }>()
);

export const deleteCourseFailed = createAction(
  '[Course - Course/Api] Delete Course Failed',
  props<{ error: any }>()
);

