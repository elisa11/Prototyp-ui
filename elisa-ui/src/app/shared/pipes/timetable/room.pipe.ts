import { Pipe, PipeTransform } from '@angular/core';
import { EventData } from '@data/model/event.model';

@Pipe({
	name: 'room',
})
export class RoomPipe implements PipeTransform {
	transform(data: EventData, ...args: unknown[]): unknown {
		if (data && data.room) {
			return data.room.name;
		}
		return '';
	}
}
