import { createAction, props } from '@ngrx/store';
import { User } from '@elisa11/elisa-api-angular-client';

export const loadLoggedUser = createAction(
	'[Auth - User] Load LoggedUser',
	props<{ uid: number }>()
);

export const loadLoggedUserSuccess = createAction(
	'[Auth - User] Load LoggedUser Success',
	props<{ data: User }>()
);

export const loadLoggedUserFailure = createAction(
	'[Auth - User] Load LoggedUser Failure',
	props<{ uid: number; error: any }>()
);
