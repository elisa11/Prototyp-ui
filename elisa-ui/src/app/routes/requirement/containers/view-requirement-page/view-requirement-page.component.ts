import { ChangeDetectionStrategy, Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { map } from 'rxjs/operators';
import { AbstractViewPageComponent } from '@core/generics/abstract-view-page.component';
import { requirementActions } from '../../store/actions';

@Component({
	template: `
		<app-selected-requirement-page
			(onBack)="back($event)"
		></app-selected-requirement-page>
	`,
	changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ViewRequirementPageComponent extends AbstractViewPageComponent {
	constructor(router: Router, store: Store, route: ActivatedRoute) {
		super(router);
		this.actionsSubscription = route.params
			.pipe(map((params) => requirementActions.selectRequirement({ id: params.id })))
			.subscribe((action) => store.dispatch(action));
	}
}
