import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
	name: 'booleanIconClass',
})
export class BooleanIconClassPipe implements PipeTransform {
	transform(value: boolean): unknown {
		return value ? 'confirm-color' : 'decline-color';
	}
}
