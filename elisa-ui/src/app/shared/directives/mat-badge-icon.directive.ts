import {
	AfterViewInit,
	Directive,
	ElementRef,
	EventEmitter,
	HostListener,
	Input,
	OnDestroy,
	OnInit,
	Output,
	Renderer2,
	SecurityContext,
} from '@angular/core';
import { BadgeIconData } from '@core/model/badge-icon.model';
import { DomSanitizer } from '@angular/platform-browser';
import { isNotNullOrUndefined } from 'codelyzer/util/isNotNullOrUndefined';

/**
 * Material Badge modification, mat badge directive is needed
 */
@Directive({
	selector: '[appMatBadgeIcon]',
})
export class MatBadgeIconDirective implements OnInit, AfterViewInit, OnDestroy {
	@Input() matBadgeIcon: BadgeIconData;
	@Output() badgeClick = new EventEmitter<void>();
	private badge: HTMLElement;
	private badgeIconId: string = undefined;
	private clickListener: () => void;

	constructor(
		private elementRef: ElementRef,
		private renderer: Renderer2,
		private sanitizer: DomSanitizer
	) {}

	ngAfterViewInit(): void {
		if (this.badgeIconId) {
			this.clickListener = this.renderer.listen(this.badge, 'click', (event) => {
				console.log('icon clicked', event);
			});
			this.badge.click();
		}
	}

	ngOnInit(): void {
		if (this.matBadgeIcon && isNotNullOrUndefined(this.matBadgeIcon.name)) {
			this.badge = this.elementRef.nativeElement.querySelector('.mat-badge-content');
			this.renderer.setStyle(this.badge, 'display', 'flex');
			this.renderer.setStyle(this.badge, 'alignItems', 'center');
			this.renderer.setStyle(this.badge, 'justifyContent', 'center');

			this.badgeIconId = this.badge.id + '-icon';
			const icon = this.renderer.createElement('i');
			this.renderer.addClass(icon, 'material-icons');
			this.renderer.setProperty(icon, 'id', this.badgeIconId);
			this.renderer.setStyle(icon, 'z-index', this.badgeIconId);
			const iconText = this.renderer.createText(
				this.sanitizer.sanitize(SecurityContext.HTML, this.matBadgeIcon.name)
			);

			this.renderer.appendChild(icon, iconText);
			this.renderer.appendChild(this.badge, icon);
		}
	}

	@HostListener('document:click', ['$event.target'])
	public onClick(target): void {
		const clickedInside = this.elementRef.nativeElement.contains(target);
		if (clickedInside) {
			this.badgeClick.emit();
		}
	}

	ngOnDestroy(): void {
		if (this.clickListener) {
			this.clickListener();
		}
	}
}
