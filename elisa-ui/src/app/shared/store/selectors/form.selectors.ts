import { createSelector } from '@ngrx/store';
import {
	formFeatureKey,
	getComment,
	getNewVersion,
	getTeacher,
} from '@shared/store/reducers/form.reducer';
import { selectSharedState } from '@shared/store/selectors/shared.selectors';

export const selectFormState = createSelector(
	selectSharedState,
	(state) => state[formFeatureKey]
);

export const selectNewSchemaForm = createSelector(selectFormState, getNewVersion);
export const selectCommentForm = createSelector(selectFormState, getComment);
export const selectTeacherForm = createSelector(selectFormState, getTeacher);
