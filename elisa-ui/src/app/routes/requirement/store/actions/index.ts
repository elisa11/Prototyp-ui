import * as requirementApiActions from './requirement-api.actions';
import * as requirementActions from './requirement.actions';
import * as activityActions from './activity.actions';
import * as departmentApiActions from './department-api.actions';
import * as courseApiActions from './course-api.actions';
import * as groupApiActions from './group-api.actions';
import * as roomApiActions from './room-api.actions';
import * as commentsApiActions from './comments-api.actions';
import * as userApiActions from './user-api.actions';
import * as searchUserActions from './search-user.actions';
import * as subjectTeachersApiActions from './subject-teachers-api.actions';
import * as studyPlansApiActions from './study-plans-api.actions';
import * as searchGroupActions from './search-group.actions';

export {
	requirementActions,
	requirementApiActions,
	courseApiActions,
	departmentApiActions,
	groupApiActions,
	roomApiActions,
	commentsApiActions,
	userApiActions,
	subjectTeachersApiActions,
	activityActions,
	searchUserActions,
	studyPlansApiActions,
	searchGroupActions,
};
