import { createReducer, on } from '@ngrx/store';
import { createEntityAdapter, EntityAdapter, EntityState } from '@ngrx/entity';
import { Department, Group } from '@elisa11/elisa-api-angular-client';
import { filterActions } from '../actions';
import { AdvancedFilter } from '../../model/advanced-filter.model';

export const filterFeatureKey = 'departments';

export interface State extends EntityState<Department> {
	readonly selectedIds: number[];
	readonly count: number;
	readonly loading: boolean;
	readonly faculties: Department[];
	readonly studyPlans: Group[];
	readonly years: Group[];
	readonly specializations: Group[];
}

export const adapter: EntityAdapter<Department> = createEntityAdapter<Department>({
	selectId: (model: Department) => model.id,
	sortComparer: false,
});

export const initialState = adapter.getInitialState({
	selectedIds: [],
	count: 0,
	loading: false,
	faculties: [],
	studyPlans: [],
	years: [],
	specializations: [],
});

export const reducer = createReducer(
	initialState,
	on(filterActions.getFilterOptions, (state) => ({
		...state,
		loading: true,
	})),
	on(filterActions.getFacultiesSuccess, (state: State, { data }) =>
		adapter.upsertMany(data.results, {
			...state,
			count: data.count,
			faculties: data.results,
		})
	),
	on(filterActions.getFilterOptionsSuccess, (state: State, { data }) =>
		adapter.upsertMany([], {
			...state,
			faculties: data.faculties.results,
			studyPlans: data.studyPlans.results,
			years: data.years.results,
			specializations: data.specializations.results,
			loading: false,
		})
	),
	on(filterActions.getFilterOptionsFailure, (state, { error }) => ({
		...state,
		loading: false,
		error: error,
	}))
);

export const getSelectedIds = (state: State) => state.selectedIds;
export const isLoading = (state: State) => state.loading;
export const getCount = (state: State) => state.count;
export const getFaculties = (state: State) => state.faculties;
export const getFilterOptions = (state: State) => {
	return {
		faculties: state.faculties,
		studyPlans: state.studyPlans,
		years: state.years,
		specializations: state.specializations,
	} as AdvancedFilter;
};
