import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { SnackEffects } from '@core/generics/snack.effects';
import { UserCacheService } from '@core/service/cache/user-cache.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { resetCachedData } from '@core/store/actions/settings.actions';
import {
	catchError,
	debounceTime,
	map,
	skip,
	switchMap,
	takeUntil,
	tap,
	withLatestFrom,
} from 'rxjs/operators';
import {
	requirementActions,
	requirementApiActions,
	searchUserActions,
	userApiActions,
} from '../actions';
import { marker } from '@biesbjerg/ngx-translate-extract-marker';
import { LocaleService } from '@core/service/locale/locale.service';
import { asyncScheduler, of, zip } from 'rxjs';
import { NGXLogger } from 'ngx-logger';
import { Store } from '@ngrx/store';
import { requirementSelectors } from '../selectors';
import { PaginatorFilter } from '@data/filter/paginator.filter';
import { PaginatedUserList } from '@elisa11/elisa-api-angular-client';

const tag = 'UserEffects';

@Injectable()
export class UserEffects extends SnackEffects {
	resetData$ = createEffect(
		() =>
			this.actions$.pipe(
				ofType(resetCachedData),
				tap(() => this.service.clearCache())
			),
		{ dispatch: false }
	);

	selectReq$ = createEffect(() =>
		this.actions$.pipe(
			ofType(requirementActions.selectRequirement),
			withLatestFrom(this.store.select(requirementSelectors.selectEntities)),
			map((value) => value[1][value[0].id]),
			map((data) =>
				userApiActions.loadUsers({
					ids: [...data.requirement_comments.map((c) => c.created_by),...data.requirement_teachers.map(t=>t.teacher)],
				})
			)
		)
	);
	loadUsers$ = createEffect(() =>
		this.actions$.pipe(
			ofType(userApiActions.loadUsers),
			map((value) => value.ids),
			switchMap((ids) => {
				if (ids === null || ids.length == 0) {
					return of(userApiActions.loadUsersSuccess({ data: [] }));
				}

				return zip(...ids.map((id) => this.service.getUser(id))).pipe(
					map((data) => userApiActions.loadUsersSuccess({ data })),
					catchError((error) => {
						this.logger.error(tag, 'loadUsers$', error);
						return of(userApiActions.loadUsersFailure({ error }));
					})
				);
			})
		)
	);

	loadUsersFailed$ = createEffect(
		() =>
			this.actions$.pipe(
				ofType(userApiActions.loadUsersFailure),
				tap((err) =>
					this.openSnackbar(
						this.localeService.translate(marker('requirement.users.error'))
					)
				)
			),
		{ dispatch: false }
	);

	searchUsers$ = createEffect(
		() =>
			({ debounce = 300, scheduler = asyncScheduler } = {}) =>
				this.actions$.pipe(
					ofType(searchUserActions.searchUsers),
					debounceTime(debounce, scheduler),
					switchMap(({ search }) => {
						if (search === null || search === undefined || search.length === 0) {
							return of(
								searchUserActions.searchUsersSuccess({ data: { count: 0, results: [] } })
							);
						}

						const nextSearch$ = this.actions$.pipe(
							ofType(searchUserActions.searchUsers),
							skip(1)
						);
						const limit = 25;
						return this.service.listUsers({ search, limit } as PaginatorFilter).pipe(
							takeUntil(nextSearch$),
							switchMap((value) => {
								const count = value.count;
								if (count > limit) {
									let req = Math.floor(count / limit) - 1;
									req = req + (count % limit > 0 ? 1 : 0);
									return zip(
										of(value),
										...[...Array(req).keys()].map((index) =>
											this.service.listUsers({
												search,
												offset: (1 + index) * limit,
												limit,
											} as PaginatorFilter)
										)
									).pipe(
										map((data) => {
											return {
												count: data[0].count,
												results: data.flatMap((list) => list.results),
											} as PaginatedUserList;
										})
									);
								} else {
									return of(value);
								}
							}),
							map((data) => searchUserActions.searchUsersSuccess({ data })),
							catchError((err) => {
								this.logger.error(tag, 'searchUsers$', err);
								return of(searchUserActions.searchUsersFailure({ error: err.message }));
							})
						);
					})
				)
	);
	searchFailed$ = createEffect(
		() =>
			this.actions$.pipe(
				ofType(searchUserActions.searchUsersFailure),
				tap((err) =>
					this.openSnackbar(
						this.localeService.translate(marker('requirement.users.error'))
					)
				)
			),
		{ dispatch: false }
	);

	loadByRequirement$ = createEffect(() =>
		this.actions$.pipe(
			ofType(requirementApiActions.searchRequirementSuccess),
			map(({ data }) => {
				const teachers = data.results.flatMap((d) =>
					d.requirement_teachers.map((t) => t.teacher)
				);
				const users = data.results.flatMap((d) =>
					d.requirement_comments.map((t) => t.created_by)
				);
				return userApiActions.loadUsers({ ids: [...teachers, ...users] });
			}),
			catchError((err) => {
				this.logger.error(tag, 'loadByRequirement$', err);
				return of(userApiActions.loadUsersFailure({ error: err.message }));
			})
		)
	);

	constructor(
		private actions$: Actions,
		private service: UserCacheService,
		private localeService: LocaleService,
		private logger: NGXLogger,
		private store: Store,
		snack: MatSnackBar
	) {
		super(snack);
	}
}
