import { MatTableDataSource } from '@angular/material/table';
import {
	AfterViewInit,
	Component,
	EventEmitter,
	Input,
	OnDestroy,
	Output,
	ViewChild,
} from '@angular/core';
import { PaginatorFilter } from '@data/filter/paginator.filter';
import { MatSort } from '@angular/material/sort';
import { merge, Subscription } from 'rxjs';
import { PageEvent } from '@angular/material/paginator';

@Component({ template: '' })
export abstract class AbstractTableComponent<T> implements AfterViewInit, OnDestroy {
	dataSource: MatTableDataSource<T>;
	displayedColumns = [];
	searchText = '';
	pageIndex = 0;
	@Input()
	pagination: number[] = [5];
	pageSize = this.pagination[0];
	@Input()
	resultsLength = 0;
	@Input()
	isLoading!: boolean;
	@Output()
	onFilterChange: EventEmitter<PaginatorFilter> = new EventEmitter<PaginatorFilter>();

	private searchChange: EventEmitter<void> = new EventEmitter<void>();
	private sort: MatSort;
	private data$: Subscription;

	protected constructor() {
		this.dataSource = new MatTableDataSource<T>([]);
	}

	@Input()
	set filter(filter: PaginatorFilter) {
		this.pageSize = filter.limit;
		this.pageIndex = filter.offset ? filter.offset / filter.limit : 0;
		this.searchText = filter.search;
	}

	@Input()
	set data(data: T[]) {
		this.dataSource.data = data;
	}

	@ViewChild(MatSort, { static: false }) set matSort(ms: MatSort) {
		this.sort = ms;
	}

	ngOnInit(): void {
		this.dataSource.sort = this.sort;
	}

	ngAfterViewInit(): void {
		this.data$ = merge(this.searchChange, this.sort.sortChange).subscribe(() => {
			const filter = {
				offset: this.pageIndex * this.pageSize,
				search: this.searchText.length > 0 ? this.searchText : undefined,
				limit: this.pageSize,
				sortBy: this.sort.active,
			} as PaginatorFilter;

			if (this.sort.direction !== '') {
				filter.asc = this.sort.direction === 'asc';
			}
			this.onFilterChange.emit(filter);
		});
	}

	ngOnDestroy(): void {
		if (this.data$) {
			this.data$.unsubscribe();
			this.data$ = null;
		}
	}

	loadPage(event: PageEvent) {
		if (event) {
			this.pageIndex = event.pageIndex;
			this.pageSize = event.pageSize;
		} else {
			this.pageIndex = 0;
		}
		this.searchChange.emit();
	}

	refresh() {
		this.searchChange.emit();
	}

	applyFilter(filterValue: string): void {
		this.searchText = filterValue;
		this.searchChange.emit();
	}
}
