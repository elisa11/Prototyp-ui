package sk.stuba.fei.elisa.elisauiapi.rest;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import sk.stuba.fei.elisa.elisauiapi.model.Config;
import sk.stuba.fei.elisa.elisauiapi.service.ConfigService;

@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/config")
@Tag(name = "config", description = "Service for exposure of certain config parameters")
public class ConfigEndpoint {

    private final ConfigService service;

    @Operation(summary = "Return config urls and parameters.", method = "getConfig")
    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "If getting of config was succesful"),
            @ApiResponse(responseCode = "500", description = "Unexpected error")
    })
    public Config getConfig() {
        return service.getConfig();
    }
}
