import { Injectable } from '@angular/core';
import {
	ActivatedRouteSnapshot,
	CanActivate,
	Router,
	RouterStateSnapshot,
	UrlTree,
} from '@angular/router';
import { Observable, of } from 'rxjs';
import { Store } from '@ngrx/store';
import { NGXLogger } from 'ngx-logger';
import { CoursesCacheService } from '../services/courses-cache.service';
import { courseSelector, searchCourseSelector } from '../store/selectors';
import {
	catchError,
	exhaustMap,
	filter,
	map,
	switchMap,
	take,
	tap,
} from 'rxjs/operators';
import { courseActions } from '../store/actions';
import { schemaSelectors } from '@core/store/selectors';

const tag = 'CourseExistsGuard';

@Injectable({
	providedIn: 'root',
})
export class CourseExistsGuard implements CanActivate {
	constructor(
		private store: Store,
		private service: CoursesCacheService,
		private logger: NGXLogger,
		private router: Router
	) {}

	canActivate(
		route: ActivatedRouteSnapshot,
		state: RouterStateSnapshot
	): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
		return this.waitForCollectionToLoad().pipe(
			switchMap(() => this.hasCourse(route.params.id))
		);
	}

	private waitForCollectionToLoad(): Observable<boolean> {
		return this.store.select(searchCourseSelector.selectSearchLoading).pipe(
			filter((loading) => !loading),
			take(1)
		);
	}

	private hasCourseInStore(id: number): Observable<boolean> {
		return this.store.select(courseSelector.selectCourseEntities).pipe(
			map((entities) => !!entities[id]),
			take(1)
		);
	}

	private hasCourseInApi(id: number): Observable<boolean> {
		return this.store.select(schemaSelectors.selectScheme).pipe(
			exhaustMap((version) =>
				this.service.getCourse(id, version.name).pipe(
					map((entity) => courseActions.loadCourse({ course: entity })),
					tap((action) => this.store.dispatch(action)),
					map((user) => !!user),
					catchError(() => {
						this.router.navigate(['/404']);
						return of(false);
					})
				)
			)
		);
	}

	private hasCourse(id: number): Observable<boolean> {
		return this.hasCourseInStore(id).pipe(
			switchMap((inStore) => {
				if (inStore) {
					this.logger.debug(tag, 'hasCourse', 'in store');
					return of(inStore);
				}
				this.logger.debug(tag, 'hasCourse try API');
				return this.hasCourseInApi(id);
			})
		);
	}
}
