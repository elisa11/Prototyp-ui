import * as groupSelector from './groups.selectors';
import * as timetableEventsSelector from './timetables-events.selectors';
import * as timetablesSelector from './timetables.selectors';
import * as activitySelector from './activity.selectors';
import * as filterSelector from './filter.selectors';
import * as timetableCollisionsSelector from './timetables-collisions.selectors';
import * as requirementsSelector from './requirement.selectors';
import * as roomSelectors from './room.selectors';

export {
	groupSelector,
	timetableEventsSelector,
	timetablesSelector,
	activitySelector,
	filterSelector,
	timetableCollisionsSelector,
	requirementsSelector,
	roomSelectors,
};
