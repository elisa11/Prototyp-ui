'use strict';

const HttpsProxyAgent = require('https-proxy-agent');
const proxyConfig = [
	{
		context: '/api-gateway',
		pathRewrite: { '^/api-gateway': '' },
		target: 'http://localhost:8080/',
		changeOrigin: true,
		secure: true,
		logLevel: 'debug',
	},
];

function setupForCorporateProxy(config) {
	const proxyServer = process.env.http_proxy || process.env.HTTP_PROXY;
	if (proxyServer) {
		const agent = new HttpsProxyAgent(proxyServer);
		console.log('Using corporate proxy server: ' + proxyServer);
		config.forEach(function (entry) {
			entry.agent = agent;
		});
	}

	console.log('Starting proxy');
	return config;
}

module.exports = setupForCorporateProxy(proxyConfig);
