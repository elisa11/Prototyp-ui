import { DepartmentSerializerShort, RoomCategory } from '@elisa11/elisa-api-angular-client';

export interface RoomFormModel {
  id: number;
  name: string;
  capacity: number;
  room_type: RoomCategory;
  department: DepartmentSerializerShort;
}
