import {
	Department,
	Room,
	RoomCategory,
	RoomEquipment,
} from '@elisa11/elisa-api-angular-client';

export class RoomModel {
	capacity: number | null;
	department: Department;
	id: number;
	name: string;
	type: RoomCategory;
	equipment: RoomEquipment[];

	constructor(
		room: Room,
		category: RoomCategory,
		equipment: RoomEquipment[],
		dep: Department
	) {
		Object.assign(this, room);
		this.type = category;
		this.equipment = equipment;
		this.department = dep;
	}
}
