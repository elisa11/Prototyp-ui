import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { SnackEffects } from '@core/generics/snack.effects';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivitiesService } from '@elisa11/elisa-api-angular-client';
import { activityActions } from '../actions';
import { LocaleService } from '@core/service/locale/locale.service';
import { catchError, map, switchMap, tap, withLatestFrom } from 'rxjs/operators';
import { marker } from '@biesbjerg/ngx-translate-extract-marker';
import { Store } from '@ngrx/store';
import { schemaSelectors } from '@core/store/selectors';
import { of } from 'rxjs';
import { NGXLogger } from 'ngx-logger';

const tag = 'ActivityEffects';

@Injectable()
export class ActivityEffects extends SnackEffects {
	loadActivities$ = createEffect(() =>
		this.actions$.pipe(
			ofType(activityActions.loadActivities),
			withLatestFrom(this.store.select(schemaSelectors.selectScheme)),
			map((value) => {
				return { scheme: value[1].name, cid: value[0].cid };
			}),
			switchMap((values) => {
				return this.service
					.activitiesList(values.scheme, undefined, values.cid, 3, undefined, 3, 0)
					.pipe(
						switchMap((res) =>
							this.service
								.activitiesList(
									values.scheme,
									undefined,
									values.cid,
									3,
									undefined,
									res.count,
									0
								)
								.pipe(
									map((data) => activityActions.loadActivitiesSuccess({ data })),
									catchError((err) => {
										this.logger.error(tag, 'loadActivities$', err);
										return of(activityActions.loadActivitiesFailure({ error: err }));
									})
								)
						),
						catchError((err) => {
							this.logger.error(tag, 'loadActivities$', err);
							return of(activityActions.loadActivitiesFailure({ error: err }));
						})
					);
			})
		)
	);
	loadActivitiesFailure$ = createEffect(
		() =>
			this.actions$.pipe(
				ofType(activityActions.loadActivitiesFailure),
				tap((err) =>
					this.openSnackbar(
						this.localeService.translate(marker('requirement.activities.error'))
					)
				)
			),
		{ dispatch: false }
	);

	constructor(
		private actions$: Actions,
		private store: Store,
		private service: ActivitiesService,
		private logger: NGXLogger,
		private localeService: LocaleService,
		snack: MatSnackBar
	) {
		super(snack);
	}
}
