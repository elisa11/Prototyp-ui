import { Injectable } from '@angular/core';
import { AbstractCacheService } from '@core/service/cache/abstract-cache.service';
import { RoomCategory, RoomTypesService } from '@elisa11/elisa-api-angular-client';
import { shareReplay } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Injectable({
	providedIn: 'root',
})
export class TypeCacheService extends AbstractCacheService {
	private getTypeCache = new Map();

	constructor(private service: RoomTypesService) {
		super();
	}

	clearCache(): void {
		this.getTypeCache.clear();
	}

	getType(schema: string, id: number): Observable<RoomCategory> {
		const key = { schema, id };
		const obs = this.getTypeCache.get(key);
		if (obs) {
			return obs;
		}

		const response = this.service
			.roomTypesRetrieve(schema, id)
			.pipe(shareReplay({ bufferSize: 1, refCount: true }));
		this.getTypeCache.set(key, response);
		return response;
	}
}
