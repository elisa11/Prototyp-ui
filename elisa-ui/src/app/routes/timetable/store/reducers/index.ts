import { Action, combineReducers } from '@ngrx/store';
import * as fromRoot from '@core/store/reducers';
import * as fromGroups from './groups.reducer';
import * as fromTimetablesEvents from './timetables-events.reducer';
import * as fromTimetables from './timetables.reducer';
import * as fromActivities from './activity.reducer';
import * as fromFilter from './filter.reducer';
import * as fromTimetablesCollisions from './timetables-collisions.reducer';
import * as fromRequirements from './requirement.reducer';
import * as fromRoom from './room.reducer';

export const timetablesFeatureKey = 'timetables';

export interface TimetablesState extends fromRoot.AppState {
	[fromTimetablesEvents.timetablesFeatureKey]: fromTimetablesEvents.State;
	[fromGroups.groupsFeatureKey]: fromGroups.State;
	[fromTimetables.timetablesFeatureKey]: fromTimetables.State;
	[fromActivities.activityFeatureKey]: fromActivities.State;
	[fromFilter.filterFeatureKey]: fromFilter.State;
	[fromTimetablesCollisions.timetablesCollisionsFeatureKey]: fromTimetablesCollisions.State;
	[fromRequirements.requirementsFeatureKey]: fromRequirements.State;
	[fromRoom.roomFeatureKey]: fromRoom.State;
}

export function reducers(state: TimetablesState | undefined, action: Action) {
	return combineReducers({
		[fromTimetablesEvents.timetablesFeatureKey]: fromTimetablesEvents.reducer,
		[fromGroups.groupsFeatureKey]: fromGroups.reducer,
		[fromTimetables.timetablesFeatureKey]: fromTimetables.reducer,
		[fromActivities.activityFeatureKey]: fromActivities.reducer,
		[fromFilter.filterFeatureKey]: fromFilter.reducer,
		[fromRequirements.requirementsFeatureKey]: fromRequirements.reducer,
		[fromRoom.roomFeatureKey]: fromRoom.reducer,
		[fromTimetablesCollisions.timetablesCollisionsFeatureKey]:
			fromTimetablesCollisions.reducer,
	})(state, action);
}
