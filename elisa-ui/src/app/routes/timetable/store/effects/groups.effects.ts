import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { SnackEffects } from '@core/generics/snack.effects';
import { Store } from '@ngrx/store';
import { MatSnackBar } from '@angular/material/snack-bar';
import {
  catchError,
  map,
  switchMap,
  withLatestFrom,
} from 'rxjs/operators';
import { groupsActions } from '../actions';
import { TimetableDataService } from '../../services/timetable-data.service';
import { loadGroupsFailure, loadGroupsSuccess } from '../actions/groups.actions';
import { from, of } from 'rxjs';
import { schemaSelectors } from '@core/store/selectors';

const tag = 'GroupEffects';

@Injectable()
export class GroupsEffects extends SnackEffects {
  loadGroups$ = createEffect(()=>
    this.actions$.pipe(
      ofType(groupsActions.loadGroups),
      withLatestFrom(this.store.select(schemaSelectors.selectScheme)),
      map((value)=> {
        return {scheme: value[1].name}
        }
      ),
      switchMap((scheme)=>
        from(this.timetableService.listGroups(scheme.scheme).pipe(
          map((groupList) => loadGroupsSuccess({
            groups: groupList.results
          })),
          catchError((error)=>of(loadGroupsFailure({error})))
        ))
      )
    )
  );

  constructor(
    private actions$: Actions,
    private store: Store,
    private timetableService: TimetableDataService,
    snack: MatSnackBar
  ) {
    super(snack);
  }
}
