import { Pipe, PipeTransform } from '@angular/core';
import { EventData } from '@data/model/event.model';

@Pipe({
	name: 'collisionIcon',
})
export class CollisionIconPipe implements PipeTransform {
	transform(event: EventData): string {
		const n = event.collisions ? event.collisions.length : 0;
		return n === 0 ? 'done' : 'error';
	}
}
