import { createSelector } from '@ngrx/store';
import * as fromSubjectTeachers from '../reducers/subject-teachers.reducer';
import { selectRequirementsState } from './requirement.selectors';

export const selectSTState = createSelector(
	selectRequirementsState,
	(s) => s[fromSubjectTeachers.userSubjectFeatureKey]
);

export const selectTotalCount = createSelector(
	selectSTState,
	fromSubjectTeachers.getCount
);
export const selectSearchId = createSelector(
	selectSTState,
	fromSubjectTeachers.getSelectedId
);
export const selectSearchLoading = createSelector(
	selectSTState,
	fromSubjectTeachers.getLoading
);

export const { selectAll } = fromSubjectTeachers.adapter.getSelectors(selectSTState);

export const selectSearchResults = createSelector(
	selectAll,
	selectSearchId,
	(data, searchIds) => searchIds.flatMap((id) => data.filter((d) => d.user.id === id))
);
