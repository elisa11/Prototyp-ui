import { Component, Inject, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import {
	Collision,
	CollisionStatusEnum,
	Requirement,
	Room,
} from '@elisa11/elisa-api-angular-client';
import { Store } from '@ngrx/store';
import { debounceTime, map, Observable, shareReplay, takeUntil } from 'rxjs';
import { RoomFilterModel } from 'src/app/routes/room/model/room-filter.model';
import { selectRoom } from 'src/app/routes/room/store/actions/room.actions';
import { EventWrapper } from 'src/app/routes/timetable/model/activity.model';
import { roomActions } from 'src/app/routes/timetable/store/actions';
import { roomSelectors } from 'src/app/routes/timetable/store/selectors';
import { selectRequirement } from 'src/app/routes/timetable/store/selectors/requirement.selectors';
import { TimetableCollisionDialogComponent } from '../timetable-collision-dialog/timetable-collision-dialog.component';

@Component({
	selector: 'app-timetable-event-options-dialog',
	templateUrl: './timetable-event-options-dialog.component.html',
	styleUrls: ['./timetable-event-options-dialog.component.scss'],
})
export class TimetableEventOptionsDialogComponent implements OnInit {
	requirement$: Observable<Requirement>;
	event: EventWrapper;
	room: FormControl = new FormControl();
	loading: boolean = false;
	filteredRooms: Room[];

	selectedRoom: Room;

	filteredRooms$: Observable<Room[]> = this.store.select(
		roomSelectors.selectSearchResults
	);
	loading$: Observable<boolean> = this.store.select(roomSelectors.selectSearchLoading);
	roomSub: any;

	requestedRooms: String[] = [];

	constructor(
		private store: Store,
		public dialogRef: MatDialogRef<TimetableCollisionDialogComponent>,
		@Inject(MAT_DIALOG_DATA) public data
	) {}

	ngOnInit(): void {
		this.requirement$ = this.store.select(selectRequirement);
		this.event = this.data.event;
		this.roomSub = this.room.valueChanges.pipe(debounceTime(250)).subscribe((room) => {
			this.handleRoomFilterChange({ room });
		});

		this.requirement$
			.pipe(shareReplay({ refCount: true, bufferSize: 1 }))
			.subscribe((requirement: Requirement) => {
				if (requirement === null || requirement === undefined) return;

				requirement.requirement_activities.forEach((activity) => {
					this.requestedRooms.push(
						requirement.requirement_parts.find(
							(part) => part.category === activity.category
						).room_name
					);
				});
			});
	}

	roomSelected(room) {
		this.selectedRoom = room;
	}

	assignRoom() {
		this.dialogRef.close(this.selectedRoom);
	}

	handleRoomFilterChange(data: { room: string }) {
		this.store.dispatch(
			roomActions.loadRooms({
				search: typeof data.room === 'string' ? data.room : undefined,
			})
		);
	}

	close() {
		this.dialogRef.close();
	}

	accept(collision: Collision) {
		collision.status = CollisionStatusEnum.Warning;
		// this.dialogRef.close(true);
		//TODO: request to accept collision
	}
	unaccept(collision: Collision) {
		collision.status = CollisionStatusEnum.Error;
		// this.dialogRef.close(true);
		//TODO: request to unaccept collision
	}
}
