import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { UserCacheService } from '@core/service/cache/user-cache.service';
import { resetCachedData } from '@core/store/actions/settings.actions';
import {
  catchError,
  debounceTime,
  map,
  skip,
  switchMap,
  takeUntil,
  tap,
} from 'rxjs/operators';
import { asyncScheduler, EMPTY, of } from 'rxjs';
import { UserApiActions } from '../actions';
import { MatSnackBar } from '@angular/material/snack-bar';
import { SnackEffects } from '@core/generics/snack.effects';
import { PaginatorFilter } from '@data/filter/paginator.filter';
import { Store } from '@ngrx/store';
import { postUserFailure, postUserSuccess } from '../actions/user-api.actions';

@Injectable()
export class UserEffects extends SnackEffects {
	resetData$ = createEffect(
		() =>
			this.actions$.pipe(
				ofType(resetCachedData),
				tap(() => this.service.clearCache())
			),
		{ dispatch: false }
	);

	search$ = createEffect(
		() =>
			({ debounce = 300, scheduler = asyncScheduler } = {}) =>
				this.actions$.pipe(
					ofType(UserApiActions.searchUsers),
					debounceTime(debounce, scheduler),
					switchMap(({ query }) => {
						if (query === null) {
							return EMPTY;
						}

						const nextSearch$ = this.actions$.pipe(
							ofType(UserApiActions.searchUsers),
							skip(1)
						);

						return this.service.listUsers(query).pipe(
							takeUntil(nextSearch$),
							map((users) => UserApiActions.searchUsersSuccess({ users })),
							catchError((err) =>
								of(UserApiActions.searchUsersFailure({ errorMsg: err.message }))
							)
						);
					})
				)
	);

	searchFailed$ = createEffect(
		() =>
			this.actions$.pipe(
				ofType(UserApiActions.searchUsersFailure),
				tap((err) => this.openSnackbar(err.errorMsg))
			),
		{ dispatch: false }
	);

  postUser$ = createEffect(()=>
    this.actions$.pipe(
      ofType(UserApiActions.postUser),
      map((value)=> {
          return {userModel: value.data}
        }
      ),
      switchMap((values)=> {
        return this.service.createUser(values.userModel).pipe(
          map((user)=> UserApiActions.postUserSuccess({data:user})),
          catchError((err) =>
            of (UserApiActions.postUserFailure({error:err})))
        );
      })
    )
  );

  postUserSuccess = createEffect(() =>
    this.actions$.pipe(
      ofType(UserApiActions.postUserSuccess),
      map(() => {
        this.openSnackbar("Osoba bola pridaná do zoznamu.");
        // this.openSnackbar(this.localeService.translate(marker('user.post.msg')))
        return UserApiActions.searchUsers({
          query: {} as PaginatorFilter});
      })
    )
  );

  postUserFailure$ = createEffect(() =>
      this.actions$.pipe(
        ofType(UserApiActions.postUserFailure),
        map(() =>
            this.openSnackbar("Nastala chyba pri vytváraní osoby.")
          // this.openSnackbar(this.localeService.translate(marker('user.post.error')))
        )
      ),
    { dispatch: false }
  );

  putUser$ = createEffect(()=>
      this.actions$.pipe(
        ofType(UserApiActions.putUser),
        map((value)=> {
            return {userModel: value.data}
          }
        ),
        switchMap((values)=>{
          return this.service.putUser(values.userModel).pipe(
              map((user)=> UserApiActions.putUserSuccess({data:user})),
              catchError ((err) =>
                of(UserApiActions.putUserFailure({error: err}))
              )
          )
        }))
  );

  putUserSuccess$ = createEffect(() =>
    this.actions$.pipe(
      ofType(UserApiActions.putUserSuccess),
      map(() => {
        this.openSnackbar("Osoba bola zmenená úspešne.");
        // this.openSnackbar(this.localeService.translate(marker('user.put.msg')))
        return UserApiActions.searchUsers({
          query: {} as PaginatorFilter});
      })
    )
  );

  putUserFailure$ = createEffect(() =>
      this.actions$.pipe(
        ofType(UserApiActions.putUserFailure),
        map(() =>
            this.openSnackbar("Nastala chyba pri editovaní osoby.")
          // this.openSnackbar(this.localeService.translate(marker('user.put.error')))
        )
      ),
    { dispatch: false }
  );

  deleteUser = createEffect(() =>
    this.actions$.pipe(
      ofType(UserApiActions.deleteUser),
      map((data) => {
        return { id: data.id};
      }),
      switchMap((data) => {
        return this.service.deleteUser( data.id).pipe(
          map(()=> UserApiActions.deleteUserDone({id:data.id})),
          catchError ((err) =>
            of(UserApiActions.deleteUserFailed({error: err}))
          )
        );
      })
    ),
  );

  deleteUserDone$ = createEffect(() =>
    this.actions$.pipe(
      ofType(UserApiActions.deleteUserDone),
      map(() => {
        this.openSnackbar("Osoba bola vymazaná.");
        // this.openSnackbar(this.localeService.translate(marker('user.delete.msg')))
        return UserApiActions.searchUsers({
          query: {} as PaginatorFilter});
      })
    )
  );
  deleteUserFailed$ = createEffect(() =>
      this.actions$.pipe(
        ofType(UserApiActions.deleteUserFailed),
        map((err) =>
            this.openSnackbar("Nastala chyba pri vymazávaní osoby.")
          // this.openSnackbar(this.localeService.translate(marker('user.delete.error')))
        )
      ),
    { dispatch: false }
  );

  setRoleUser$ = createEffect(()=>
    this.actions$.pipe(
      ofType(UserApiActions.setRoleUser),
      map((value)=> {
          return {uid: value.id, role: value.role}
        }
      ),
      switchMap((values)=> {
        return this.service.setRoleForUser(values.uid, values.role).pipe(
          map(()=> UserApiActions.setRoleUserDone()),
          catchError((err) =>
            of (UserApiActions.setRoleUserFailed({error:err})))
        );
      })
    )
  );

  setRoleUserSuccess = createEffect(() =>
    this.actions$.pipe(
      ofType(UserApiActions.setRoleUserDone),
      map(() => {
        this.openSnackbar("Osobe bola úspešne priradená rola.");
        // this.openSnackbar(this.localeService.translate(marker('user.setRole.msg')))
      })
    ),
    {dispatch:false}
  );

  setRoleUserFailure$ = createEffect(() =>
      this.actions$.pipe(
        ofType(UserApiActions.setRoleUserFailed),
        map(() =>
            this.openSnackbar("Nastala chyba pri priraďovaní role pre osobu.")
          // this.openSnackbar(this.localeService.translate(marker('user.setRole.error')))
        )
      ),
    { dispatch: false }
  );

  removeRoleUser$ = createEffect(()=>
    this.actions$.pipe(
      ofType(UserApiActions.removeRoleUser),
      map((value)=> {
          return {uid: value.id, role: value.role}
        }
      ),
      switchMap((values)=> {
        return this.service.removeRoleForUser(values.uid, values.role).pipe(
          map(()=> UserApiActions.removeRoleUserDone()),
          catchError((err) =>
            of (UserApiActions.removeRoleUserFailed({error:err})))
        );
      })
    )
  );

  removeRoleUserSuccess = createEffect(() =>
    this.actions$.pipe(
      ofType(UserApiActions.removeRoleUserDone),
      map(() => {
        this.openSnackbar("Osobe bola úspešne odstránená rola.");
      })),
    {dispatch:false}
  );

  removeRoleUserFailure$ = createEffect(() =>
      this.actions$.pipe(
        ofType(UserApiActions.removeRoleUserFailed),
        map(() =>
            this.openSnackbar("Nastala chyba pri odstraňovaní role pre osobu.")
          // this.openSnackbar(this.localeService.translate(marker('user.setRole.error')))
        )
      ),
    { dispatch: false }
  );

	constructor(
		private actions$: Actions,
		private service: UserCacheService,
		private store: Store,
		snack: MatSnackBar
	) {
		super(snack);
	}
}
