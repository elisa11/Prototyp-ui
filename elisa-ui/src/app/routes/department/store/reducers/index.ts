import * as fromRoot from '@core/store/reducers';
import * as fromSearchDepartment from './search-department.reducer';
import * as fromDepartment from './department.reducer';
import { Action, combineReducers } from '@ngrx/store';
import * as fromUserDepartment from './user-department.reducer';
import * as fromUser from './user.reducer';

export const departmentsFeatureKey = 'departments';

export interface DepartmentsState extends fromRoot.AppState {
	[fromSearchDepartment.searchDepartmentFeatureKey]: fromSearchDepartment.State;
	[fromDepartment.departmentFeatureKey]: fromDepartment.State;
	[fromUserDepartment.userDepartmentFeatureKey]: fromUserDepartment.State;
	[fromUser.userFeatureKey]: fromUser.State;
}

export function reducers(state: DepartmentsState | undefined, action: Action) {
	return combineReducers({
		[fromSearchDepartment.searchDepartmentFeatureKey]: fromSearchDepartment.reducer,
		[fromDepartment.departmentFeatureKey]: fromDepartment.reducer,
		[fromUserDepartment.userDepartmentFeatureKey]: fromUserDepartment.reducer,
		[fromUser.userFeatureKey]: fromUser.reducer,
	})(state, action);
}
