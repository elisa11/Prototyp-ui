import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { CommonText } from '@data/common-text';
import { TimeDataUnit } from '../../model/time-data-unit.model';
import moment from 'moment/moment';
import { ActivityCategory } from '@elisa11/elisa-api-angular-client';

@Component({
	selector: 'app-time-picker',
	templateUrl: './time-picker.component.html',
	styleUrls: ['./time-picker.component.scss'],
})
export class TimePickerComponent implements OnInit {
	@Input()
	readonly = false;
	@Input() label: ActivityCategory;
	days = CommonText.daysValues;
	hours: string[];
	eventTypes = CommonText.eventStatusValues;
	@Output()
	onDataChange: EventEmitter<TimeDataUnit[][]> = new EventEmitter();
	private data: TimeDataUnit[][] = [];

	get time() {
		return this.data;
	}

	@Input() set time(time: TimeDataUnit[][]) {
		if (this.readonly && time && time.length > 0) {
			this.data = time;
		}
	}

	ngOnInit(): void {
		// todo: variabilne podla nastaveni z layout editoru, ktory treba urobit
		this.hours = [...Array(15).keys()].map((index) =>
			moment()
				.hour(index + 7)
				.minute(0)
				.toISOString()
		);
		if (this.data.length === 0) {
			this.data = this.hours.map((h) => {
				return this.days.map((d) => {
					return {
						day: d.enum,
						time: h,
						suitability: 'Suitable',
					} as TimeDataUnit;
				});
			});
		}
	}

	getColor(data: TimeDataUnit): string {
		const type = this.eventTypes.find((value) => value.enum === data.suitability);
		return type ? type.color : 'white';
	}

	changeAvailability(i: number, j: number): void {
		if (!this.readonly) {
			const eventType = this.eventTypes.find(
				(value) => value.enum === this.data[i][j].suitability
			);
			this.data[i][j].suitability =
				this.eventTypes[(eventType.id + 1) % this.eventTypes.length].enum;
			this.onDataChange.emit(this.data);
		}
	}

	changeRowAvailability(rowId: number): void {
		for (let j = 0; j < this.days.length; j++) {
			this.changeAvailability(rowId, j);
		}
	}

	changeColAvailability(colId: number): void {
		for (let j = 0; j < this.hours.length; j++) {
			this.changeAvailability(j, colId);
		}
	}
}
