import { createAction, props } from '@ngrx/store';
import { PaginatedCommentsList } from '@elisa11/elisa-api-angular-client';
import { CommentModel } from '@data/model/comment.model';

export const loadComments = createAction(
	'[Requirement - Comment/Api] Load Comments',
	props<{ rid: number }>()
);
export const loadCommentsSuccess = createAction(
	'[Requirement - Comment/Api] Load Comments Success',
	props<{ rid: number; data: PaginatedCommentsList }>()
);
export const loadCommentsFailure = createAction(
	'[Requirement - Comment/Api] Load Comments Failure',
	props<{ error: any }>()
);

export const deleteComment = createAction(
	'[Requirement - Comment/Api] delete Comment',
	props<{ cid: number }>()
);
export const deleteCommentFailure = createAction(
	'[Requirement - Comment/Api] delete Comment Failure',
	props<{ error: any }>()
);

export const editComment = createAction(
	'[Requirement - Comment/Api] Edit Comment',
	props<{ data: CommentModel }>()
);
export const editCommentFailure = createAction(
	'[Requirement - Comment/Api] Edit Comment Failure',
	props<{ error: any }>()
);

export const addComment = createAction(
	'[Requirement - Comment/Api] Add Comment',
	props<{ data: CommentModel }>()
);
export const addCommentFailure = createAction(
	'[Requirement - Comment/Api] Add Comment Failure',
	props<{ error: any }>()
);
