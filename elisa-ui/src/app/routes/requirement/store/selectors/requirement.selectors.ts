import { createFeatureSelector, createSelector } from '@ngrx/store';
import * as fromReq from '../reducers/requirement.reducer';
import { Requirement } from '@elisa11/elisa-api-angular-client';
import { requirementFeatureKey, RequirementsState } from '../reducers';

export const selectRequirementsState =
	createFeatureSelector<RequirementsState>(requirementFeatureKey);

export const selectEntityState = createSelector(
	selectRequirementsState,
	(s) => s[fromReq.requirementFeatureKey]
);

export const selectSelectedId = createSelector(selectEntityState, fromReq.getSelectId);

export const selectTotalCount = createSelector(selectEntityState, fromReq.getCount);

export const { selectIds, selectEntities } =
	fromReq.adapter.getSelectors(selectEntityState);

export const selectSelectedRequirement = createSelector(
	selectEntities,
	selectSelectedId,
	(entities, selectedId) => {
		return selectedId && entities ? entities[selectedId] : ({} as Requirement);
	}
);
