import { Injectable } from '@angular/core';
import {
	CourseSerializerFull,
	CoursesService,
	PaginatedCourseSerializerFullList,
	PaginatedSubjectTeachersList,
} from '@elisa11/elisa-api-angular-client';
import { shareReplay } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { AbstractCacheService } from '@core/service/cache/abstract-cache.service';
import { PaginatorFilter } from '@data/filter/paginator.filter';
import { BasicUtils } from '@shared/utils/basic.utils';
import { CourseModel } from '../model/course.model';

@Injectable({
	providedIn: 'root',
})
export class CoursesCacheService extends AbstractCacheService {
	private listCoursesCache = new Map();
	private getCourseCache = new Map();
	private listTeachersCache = new Map();

	constructor(private service: CoursesService) {
		super();
	}

	listCourses(
		filter: PaginatorFilter,
		schema: string
	): Observable<PaginatedCourseSerializerFullList> {
		const obs = this.listCoursesCache.get(filter);
		if (obs) {
			return obs;
		}

		const response = this.service
			.coursesList(
				schema,
				undefined,
				filter.limit,
				filter.offset,
				BasicUtils.getSortBy(filter),
				filter.search
			)
			.pipe(shareReplay({ bufferSize: 1, refCount: true }));
		this.listCoursesCache.set(filter, response);
		return response;
	}

	getCourse(courseId: number, schema: string): Observable<CourseSerializerFull> {
		const obs = this.listCoursesCache.get(courseId);
		if (obs) {
			return obs;
		}
		const response = this.service
			.coursesRetrieve(schema, courseId)
			.pipe(shareReplay({ bufferSize: 1, refCount: true }));
		this.getCourseCache.set(courseId, response);
		return response;
	}

	listTeachers(
		courseId: number,
		schema: string,
		filter: PaginatorFilter
	): Observable<PaginatedSubjectTeachersList> {
		const obs = this.listTeachersCache.get(courseId);
		if (obs) {
			return obs;
		}
		const response = this.service
			.coursesTeachersList(schema, courseId, filter.limit, filter.offset)
			.pipe(shareReplay({ bufferSize: 1, refCount: true }));
		this.listTeachersCache.set(courseId, response);
		return response;
	}

  putCourse(schema: string, course: CourseModel): Observable<any> {
    let courseFull: CourseSerializerFull = {
      code: course.code,
      completion: course.completion,
      credits: course.credits,
      department: course.department,
      id: course.id,
      name: course.name,
      period: course.period,
      teacher: course.teacher
    }
    return this.service.coursesUpdate(schema,course.id, courseFull);
  }


  createCourse(schema: string, course: CourseModel): Observable<any> {
	  let courseFull: CourseSerializerFull = {
      code: course.code,
      completion: course.completion,
      credits: course.credits,
      department: course.department,
      id: undefined,
      name: course.name,
      period: course.period,
      teacher: course.teacher
    }
    return this.service.coursesCreate(schema, courseFull);
  }

  deleteCourse(schema: string, cid: number): Observable<any> {
    return this.service.coursesDestroy(schema, cid);
  }

  clearCache(): void {
		this.getCourseCache.clear();
		this.listCoursesCache.clear();
		this.listTeachersCache.clear();
	}
}
