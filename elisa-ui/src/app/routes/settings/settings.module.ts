import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SettingsRoutingModule } from './settings-routing.module';
import { SettingsComponent } from './settings.component';
import { SharedModule } from '@shared/shared.module';
import { TranslateModule } from '@ngx-translate/core';
import { MATERIAL_MODULES } from './index';
import { EffectsModule } from '@ngrx/effects';
import { SettingsEffects } from './store/effects/settings.effects';

@NgModule({
	declarations: [SettingsComponent],
	imports: [
		CommonModule,
		SharedModule,
		TranslateModule.forChild(),
		SettingsRoutingModule,
		...MATERIAL_MODULES,
		EffectsModule.forFeature([SettingsEffects]),
	],
})
export class SettingsModule {}
