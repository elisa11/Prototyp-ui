import { createAction, props } from '@ngrx/store';

export const loadStatisticss = createAction(
  '[Statistics] Load Statisticss'
);

export const loadStatisticssSuccess = createAction(
  '[Statistics] Load Statisticss Success',
  props<{ data: any }>()
);

export const loadStatisticssFailure = createAction(
  '[Statistics] Load Statisticss Failure',
  props<{ error: any }>()
);
