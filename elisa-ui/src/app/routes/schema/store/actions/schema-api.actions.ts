import { createAction, props } from '@ngrx/store';
import { PaginatorFilter } from '@data/filter/paginator.filter';
import { PaginatedVersionList, Version } from '@elisa11/elisa-api-angular-client';

export const searchSchemas = createAction(
	'[Schema - Schema/Api] Search Schemas',
	props<{ query: PaginatorFilter }>()
);

export const searchSchemasSuccess = createAction(
	'[Schema - Schema/Api] Search Schemas Success',
	props<{ versions: PaginatedVersionList }>()
);

export const searchSchemasFailure = createAction(
	'[Schema - Schema/Api] Search Schemas Failure',
	props<{ error: any }>()
);

export const searchParentSchemas = createAction(
	'[Schema - Schema/Api] Search Parent Schemas',
	props<{ ids: number[] }>()
);

export const searchParentSchemasSuccess = createAction(
	'[Schema - Schema/Api] Search Parent Schemas Success',
	props<{ versions: Version[] }>()
);

export const searchParentSchemasFailure = createAction(
	'[Schema - Schema/Api] Search Parent Schemas Failure',
	props<{ error: any }>()
);
