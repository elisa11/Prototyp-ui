import { Pipe, PipeTransform } from '@angular/core';
import { CommonText } from '@data/common-text';
import { StateEnum } from '@elisa11/elisa-api-angular-client';

@Pipe({
	name: 'statusIconColor',
})
export class StatusIconColorPipe implements PipeTransform {
	transform(status: StateEnum, ...args: unknown[]): unknown {
		return (
			CommonText.StatusValues.find((value) => {
				return value.value === status;
			}).color ?? 'red'
		);
	}
}
