import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
	name: 'flag',
})
export class FlagPipe implements PipeTransform {
	transform(value: string): string {
		switch (value) {
			case 'cs':
				return 'cz';
			case 'en':
				return 'gb';
			default:
				return value;
		}
	}
}
