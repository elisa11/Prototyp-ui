import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
	name: 'loggedUser',
})
export class LoggedUserPipe implements PipeTransform {
	transform(value: number, logged: number): boolean {
		return value === logged;
	}
}
