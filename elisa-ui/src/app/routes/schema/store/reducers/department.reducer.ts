import { createReducer, on } from '@ngrx/store';
import { Department } from '@elisa11/elisa-api-angular-client';
import { createEntityAdapter, EntityAdapter, EntityState } from '@ngrx/entity';
import { loadDepartmentsSuccess } from '../actions/department.actions';

export const departmentFeatureKey = 'department';

export interface State extends EntityState<Department> {}

export const adapter: EntityAdapter<Department> = createEntityAdapter<Department>({
	selectId: (entity: Department) => entity.id,
	sortComparer: false,
});

export const initialState: State = adapter.getInitialState({ count: 0 });

export const reducer = createReducer(
	initialState,
	on(loadDepartmentsSuccess, (state: State, { data }) => adapter.upsertMany(data, state))
);
