import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { ROUTER_NAVIGATION } from '@ngrx/router-store';
import { tap } from 'rxjs/operators';
import { refreshTokenValidity } from '../../../auth/store/actions/auth.actions';

@Injectable()
export class RouterEffects {
	// refresh stavu tokenu pred kazdou zmenou url
	urlChange$ = createEffect(
		() =>
			this.actions$.pipe(
				ofType(ROUTER_NAVIGATION),
				tap(() => {
					this.store.dispatch(refreshTokenValidity());
				})
			),
		{ dispatch: false }
	);

	constructor(private actions$: Actions, private store: Store) {}
}
