import { createReducer, on } from '@ngrx/store';
import { UserDepartment } from '@elisa11/elisa-api-angular-client';
import { createEntityAdapter, EntityAdapter, EntityState } from '@ngrx/entity';
import { departmentActions, userApiActions, userDepartmentApiActions } from '../actions';

export const userDepartmentFeatureKey = 'userDepartment';

export type UserDepartments = { id: number; data: UserDepartment[] };

export interface State extends EntityState<UserDepartments> {
	readonly selectedId: number | null;
	readonly total: number;
	readonly loading: boolean;
}

export const adapter: EntityAdapter<UserDepartments> =
	createEntityAdapter<UserDepartments>({
		selectId: (ud: UserDepartments) => ud.id,
		sortComparer: false,
	});

export const initialState: State = adapter.getInitialState({
	selectedId: null,
	total: 0,
	loading: false,
});

export const reducer = createReducer(
	initialState,
	on(departmentActions.selectDepartment, (state: State, { id }) => ({
		...state,
		selectedId: id,
	})),
	on(userDepartmentApiActions.loadUserDepartmentsSuccess, (state) => ({
		...state,
		loading: true,
	})),
	on(userApiActions.loadUsersSuccess, (state) => ({ ...state, loading: false })),
	on(userApiActions.loadUsersFailure, (state) => ({ ...state, loading: false })),
	on(
		userDepartmentApiActions.loadUserDepartmentsSuccess,
		(state: State, { data, query }) =>
			adapter.upsertOne({ data: data.results, id: query.id } as UserDepartments, {
				...state,
				total: data.count,
			})
	)
);

export const getSelectedId = (state: State) => state.selectedId;
export const getTotal = (state: State) => state.total;
export const getLoading = (state: State) => state.loading;
