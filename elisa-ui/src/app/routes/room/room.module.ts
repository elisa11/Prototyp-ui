import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RoomRoutingModule } from './room-routing.module';
import { RoomDetailComponent } from './components/room-detail/room-detail.component';
import { RoomListComponent } from './components/room-list/room-list.component';
import { SharedModule } from '@shared/shared.module';
import { TranslateModule } from '@ngx-translate/core';
import { MATERIAL_MODULES } from './index';
import { EffectsModule } from '@ngrx/effects';
import {
	DepartmentEffects,
	RoomEquipmentEffects,
  EquipmentEffects,
	RoomEffects,
	TypeEffects,
} from './store/effects';
import { CollectionPageComponent } from './containers/collection-page/collection-page.component';
import { ViewRoomPageComponent } from './containers/view-room-page/view-room-page.component';
import { SelectedRoomPageComponent } from './containers/selected-room-page/selected-room-page.component';
import { NewRoomPageComponent } from './containers/new-room-page/new-room-page.component';
import { RoomFormComponent } from './components/room-form/room-form.component';
import { StoreModule } from '@ngrx/store';
import * as fromRoom from './store/reducers';
import { RoomTypePipe } from './pipes/room-type.pipe';
import { CapacityPipe } from './pipes/capacity.pipe';
import { ReactiveFormsModule } from '@angular/forms';
import { EquipmentSelectionComponent } from './components/equipment-selection/equipment-selection.component';

@NgModule({
	declarations: [
		RoomListComponent,
		RoomDetailComponent,
		CollectionPageComponent,
		ViewRoomPageComponent,
		SelectedRoomPageComponent,
    NewRoomPageComponent,
    RoomFormComponent,
    RoomTypePipe,
    CapacityPipe,
    EquipmentSelectionComponent,
	],
	imports: [
		CommonModule,
		SharedModule,
		TranslateModule.forChild(),
		RoomRoutingModule,
    ReactiveFormsModule,
		...MATERIAL_MODULES,
		StoreModule.forFeature(fromRoom.roomsFeatureKey, fromRoom.reducers),
		EffectsModule.forFeature([
			RoomEffects,
			TypeEffects,
			RoomEquipmentEffects,
			DepartmentEffects,
      EquipmentEffects
		]),
	],
})
export class RoomModule {}
