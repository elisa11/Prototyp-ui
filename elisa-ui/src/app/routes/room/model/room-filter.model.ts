import { Capacity } from './capacity.model';

export interface RoomFilterModel {
	departments: number[];
	type: number[];
	capacity: Capacity;
}
