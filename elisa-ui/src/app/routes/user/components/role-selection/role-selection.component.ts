import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, FormGroupDirective, Validators } from '@angular/forms';
import { Paths } from '@shared/store/reducers/form.reducer';
import { Store } from '@ngrx/store';
import { formActions } from '@shared/store/actions';
import { RoleEnum } from '@elisa11/elisa-api-angular-client';

@Component({
  selector: 'app-role-selection',
  templateUrl: './role-selection.component.html'
})
export class RoleSelectionComponent implements OnInit {
  @Input() readOnly: boolean = false;
  formGroup: FormGroup;
  path = Paths.split;
  selectedRoles: string[] = [];
  roleEnum = Object.keys(RoleEnum);
  private firstImport = true;
  @Output()
  private onChange = new EventEmitter<string[]>();
  @Output()
  private onSetRole = new EventEmitter<string>();
  @Output()
  private onRemoveRole = new EventEmitter<string>();
  @Output()
  private onSelectedRoles = new EventEmitter<string[]>();
  private existingData: boolean = false;

  constructor(private fb: FormBuilder, private store: Store) {
    this.formGroup = fb.group({
      rToAdd: ["", Validators.required ],
    });
  }

  get roles(){
    return this.roleEnum;
  }

  @Input() set roles(data: string[]) {
    if (this.firstImport) {
      this.roleEnum = data;
      const selectedRole = new Set<string>();
      this.selectedRoles = this.selectedRoles.filter((x) => !selectedRole.has(x));
      this.onChange.emit(this.roleEnum);
    }
  }

  @Input() set selectedRolesData(data: string[]) {
    if (this.firstImport) {
      this.existingData = true;
      let roles: string[] = [];
      if (data !== undefined && data.length > 0) {
        data.forEach((g) => {
          Object.keys(RoleEnum).forEach((r) =>{
            if (RoleEnum[r] == g){
              roles.push(r);
            }
          })
        })
      }
      this.selectedRoles = roles;
      this.roles = this.roles.filter((x) => !this.selectedRoles.includes(x));
    }
  }

  ngOnInit(): void {
    this.firstImport = true;
  }

  remove(data: string): void {
    this.selectedRoles = this.selectedRoles.filter((d)=> d !== data)
    this.roles.push(data);
    this.onChange.emit(this.roleEnum);
    if (this.existingData){
      this.onRemoveRole.emit(data);
    }
    else{
      this.onSelectedRoles.emit(this.selectedRoles);
    }
    this.formGroup.reset();
  }

  resetForm(form: FormGroupDirective) {
    form.resetForm();
  }

  add() {
    const formData = this.formGroup.value;
    const roleToAdd = formData.rToAdd;
    this.selectedRoles.push(roleToAdd);
    this.roles = this.roles.filter((x) => x !== roleToAdd)
    if (this.existingData){
      this.onSetRole.emit(roleToAdd);
    }
    else{
      this.onSelectedRoles.emit(this.selectedRoles);
    }
    this.store.dispatch(formActions.submitFormSuccess({ path: this.path }))
  }

}
