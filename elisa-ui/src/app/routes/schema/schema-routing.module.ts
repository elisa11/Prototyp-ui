import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from '../../auth/services/auth.guard';
import { SchemaViewPageComponent } from './containers/schema-view-page/schema-view-page.component';

const routes: Routes = [
	{ path: '', component: SchemaViewPageComponent, canActivate: [AuthGuard] },
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule],
})
export class SchemaRoutingModule {}
