import { createAction, props } from '@ngrx/store';
import { PaginatedDepartmentList } from '@elisa11/elisa-api-angular-client';

export const loadDepartmentScrolls = createAction(
	'[Room - DepartmentScroll] Load DepartmentScrolls'
);

export const loadDepartmentScrollsSuccess = createAction(
	'[Room - DepartmentScroll] Load DepartmentScrolls Success',
	props<{ data: PaginatedDepartmentList }>()
);

export const loadDepartmentScrollsFailure = createAction(
	'[Room - DepartmentScroll] Load DepartmentScrolls Failure',
	props<{ error: any }>()
);
