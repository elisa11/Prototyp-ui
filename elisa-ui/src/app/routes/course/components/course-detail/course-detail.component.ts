import { Component, Input } from '@angular/core';
import { CommonText } from '@data/common-text';
import {
	CourseSerializerFull,
	DepartmentSerializerShort,
	User,
} from '@elisa11/elisa-api-angular-client';
import { AbstractDetailComponent } from '@core/generics/abstract-detail.component';

@Component({
	selector: 'app-course-detail',
	templateUrl: './course-detail.component.html',
})
export class CourseDetailComponent extends AbstractDetailComponent<CourseSerializerFull> {
	@Input()
	department!: DepartmentSerializerShort;
	@Input()
	teacher!: User;
	moduleId = 'course';
	moduleTitle = CommonText.navLinks.find((value) => value.link === this.moduleId).label;

	constructor() {
		super();
	}
}
