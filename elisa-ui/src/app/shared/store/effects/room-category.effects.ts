import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { RoomTypesService } from '@elisa11/elisa-api-angular-client';
import { roomCategoryActions } from '@shared/store/actions';
import { catchError, map, switchMap, withLatestFrom } from 'rxjs/operators';
import { Store } from '@ngrx/store';
import { schemaSelectors } from '@core/store/selectors';
import { NGXLogger } from 'ngx-logger';
import { of } from 'rxjs';
import { schemaActions } from '@core/store/actions';

const tag = 'RoomCategoryEffects';

@Injectable()
export class RoomCategoryEffects {
	loadSchemasDialogSuccess$ = createEffect(() =>
		this.actions$.pipe(
			ofType(schemaActions.loadSchemasImportDone),
			map(() => roomCategoryActions.loadRoomCategories())
		)
	);

	loadRoomCategories$ = createEffect(() =>
		this.actions$.pipe(
			ofType(roomCategoryActions.loadRoomCategories),
			withLatestFrom(this.store.select(schemaSelectors.selectScheme)),
			map((data) => data[1].name),
			switchMap((schema) => {
				return this.service.roomTypesList(schema, 1, 0).pipe(
					switchMap((res) => {
						return this.service.roomTypesList(schema, res.count, 0).pipe(
							map((data) => roomCategoryActions.loadRoomCategoriesSuccess({ data })),
							catchError((err) => {
								this.logger.error(tag, 'loadRoomCategories$', err);
								return of(roomCategoryActions.loadRoomCategoriesFailure({ error: err }));
							})
						);
					}),
					catchError((err) => {
						this.logger.error(tag, 'loadRoomCategories$', err);
						return of(roomCategoryActions.loadRoomCategoriesFailure({ error: err }));
					})
				);
			})
		)
	);

	constructor(
		private actions$: Actions,
		private service: RoomTypesService,
		private logger: NGXLogger,
		private store: Store
	) {}
}
