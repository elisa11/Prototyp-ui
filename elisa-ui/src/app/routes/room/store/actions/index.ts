import * as roomActions from './room.actions';
import * as roomApiActions from './room-api.actions';
import * as departmentApiActions from './department-api.actions';
import * as equipmentApiActions from './equipment-api.actions';
import * as roomEquipmentApiActions from './room-equipment-api.actions';
import * as typeApiActions from './type-api.actions';
import * as scrollApiActions from './department-scroll.actions';

export {
	roomActions,
	roomApiActions,
	departmentApiActions,
  equipmentApiActions,
  roomEquipmentApiActions,
	typeApiActions,
	scrollApiActions,
};
