import {
	ChangeDetectorRef,
	Component,
	EventEmitter,
	Input,
	OnInit,
	Output,
} from '@angular/core';
import { Activity, EventWrapper } from '../../../model/activity.model';

@Component({
	selector: 'app-timetable-cell',
	templateUrl: './timetable-cell.component.html',
	styleUrls: ['./timetable-cell.component.scss'],
})
export class TimetableCellComponent implements OnInit {
	@Input() events?: EventWrapper[];
	@Input() hour?: number;
	@Output() onAssignedActivityStart = new EventEmitter<EventWrapper>();

	constructor(private cdr: ChangeDetectorRef) {}

	offset: EventWrapper[];
	displayEvents: EventWrapper[] = [];

	ngOnInit(): void {}

	onDragStart(activity: EventWrapper) {
		this.onAssignedActivityStart.emit(activity);
	}
}
