import { Injectable } from '@angular/core';
import { AbstractCacheService } from '@core/service/cache/abstract-cache.service';
import {
  EquipmentsService,
  PaginatedEquipmentList,
} from '@elisa11/elisa-api-angular-client';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class EquipmentsCacheService extends AbstractCacheService {
  private listEquipmentCache = new Map();

  constructor(private equipmentService: EquipmentsService) {
    super();
  }

  listEquipment(
    schema:string
  ): Observable<PaginatedEquipmentList>{
    return this.equipmentService.equipmentsList(schema);
  }

  clearCache(): void {
    this.listEquipmentCache.clear();
  }

}
