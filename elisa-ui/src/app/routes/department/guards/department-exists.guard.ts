import { Injectable } from '@angular/core';
import {
	ActivatedRouteSnapshot,
	CanActivate,
	Router,
	RouterStateSnapshot,
	UrlTree,
} from '@angular/router';
import { Observable, of } from 'rxjs';
import { Store } from '@ngrx/store';
import { NGXLogger } from 'ngx-logger';
import { DepartmentsCacheService } from '@core/service/cache/departments-cache.service';
import { catchError, filter, map, switchMap, take, tap } from 'rxjs/operators';
import { departmentActions, userDepartmentApiActions } from '../store/actions';
import { PaginatorFilter } from '@data/filter/paginator.filter';
import { departmentSelectors, searchDepartmentSelectors } from '../store/selectors';

const tag = 'DepartmentExistsGuard';

@Injectable({
	providedIn: 'root',
})
export class DepartmentExistsGuard implements CanActivate {
	constructor(
		private store: Store,
		private service: DepartmentsCacheService,
		private logger: NGXLogger,
		private router: Router
	) {}

	canActivate(
		route: ActivatedRouteSnapshot,
		state: RouterStateSnapshot
	): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
		return this.waitForCollectionToLoad().pipe(
			switchMap(() => this.hasDepartment(route.params.id))
		);
	}

	private waitForCollectionToLoad(): Observable<boolean> {
		return this.store.select(searchDepartmentSelectors.selectSearchLoading).pipe(
			filter((loading) => !loading),
			take(1)
		);
	}

	private hasDepartmentInStore(id: number): Observable<boolean> {
		return this.store.select(departmentSelectors.selectEntities).pipe(
			map((entities) => !!entities[id]),
			take(1)
		);
	}

	private hasDepartmentInApi(id: number): Observable<boolean> {
		return this.service.getDepartment('', id).pipe(
			map((dep) => departmentActions.loadDepartment({ department: dep })),
			tap((action) => this.store.dispatch(action)),
			map((entity) => !!entity),
			catchError(() => {
				this.router.navigate(['/404']);
				return of(false);
			})
		);
	}

	private hasDepartment(id: number): Observable<boolean> {
		return this.hasDepartmentInStore(id)
			.pipe(
				switchMap((inStore) => {
					if (inStore) {
						this.logger.debug(tag, 'hasDepartment', 'in store');
						return of(inStore);
					}
					this.logger.debug(tag, 'hasDepartment try API');
					return this.hasDepartmentInApi(id);
				})
			)
			.pipe(
				tap((dispatch) => {
					this.logger.debug(tag, 'hasDepartment', dispatch);
					if (dispatch) {
						this.store.dispatch(
							userDepartmentApiActions.loadUserDepartments({
								query: {
									id,
									offset: 0,
									limit: 5,
								} as PaginatorFilter,
							})
						);
					}
				})
			);
	}
}
