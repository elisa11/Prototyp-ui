import { Component, EventEmitter, Input, Output } from '@angular/core';
import { CommonText } from '@data/common-text';
import { AbstractDetailComponent } from '@core/generics/abstract-detail.component';
import { PaginatorFilter } from '@data/filter/paginator.filter';
import { UserDepartmentModel } from '@data/model/user-department.model';
import { Department } from '@elisa11/elisa-api-angular-client';

@Component({
	selector: 'app-department-detail',
	templateUrl: './department-detail.component.html',
})
export class DepartmentDetailComponent extends AbstractDetailComponent<Department> {
	moduleId = 'department';
	moduleTitle = CommonText.navLinks.find((value) => value.link === this.moduleId).label;
	@Input()
	pagination: number[] = [5];
	@Input()
	usersLength!: number;
	@Input()
	userData!: UserDepartmentModel[];
	@Input()
	userLoading!: boolean;
	@Output()
	private onUserFilterChange = new EventEmitter<PaginatorFilter>();

	constructor() {
		super();
	}

	userFilterChange(filter: PaginatorFilter) {
		this.onUserFilterChange.emit(filter);
	}
}
