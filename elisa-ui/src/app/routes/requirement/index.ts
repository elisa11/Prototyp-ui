import { StatusIconColorPipe } from './pipes/status-icon-color.pipe';
import { DetailTitlePipe } from './pipes/detail-title.pipe';
import { StatusIconPipe } from './pipes/status-icon.pipe';
import { StatusPipe } from './pipes/status.pipe';
import { TeacherTypeListPipe } from './pipes/teacher-type-list.pipe';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatTableModule } from '@angular/material/table';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatChipsModule } from '@angular/material/chips';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatStepperModule } from '@angular/material/stepper';
import { MatListModule } from '@angular/material/list';
import { MatCardModule } from '@angular/material/card';
import { MatIconModule } from '@angular/material/icon';
import { MatSortModule } from '@angular/material/sort';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatMenuModule } from '@angular/material/menu';
import { MatSelectModule } from '@angular/material/select';
import { MatTabsModule } from '@angular/material/tabs';
import { CourseNamePipe } from './pipes/course-name.pipe';
import { LoggedUserPipe } from './pipes/logged-user.pipe';
import { CategoryTranslatePipe } from './pipes/category-translate.pipe';
import { HasPracticePipe } from './pipes/has-practice.pipe';
import { SelectedActivityPipe } from './pipes/selected-activity.pipe';

export const MATERIAL_MODULES = [
	MatFormFieldModule,
	MatInputModule,
	MatButtonModule,
	MatTooltipModule,
	MatAutocompleteModule,
	MatChipsModule,
	MatCheckboxModule,
	MatStepperModule,
	MatPaginatorModule,
	MatListModule,
	MatCardModule,
	MatIconModule,
	MatTableModule,
	MatSortModule,
	MatProgressSpinnerModule,
	MatMenuModule,
	MatSelectModule,
	MatTabsModule,
];

export const REQ_PIPES = [
	StatusIconColorPipe,
	StatusIconPipe,
	StatusPipe,
	DetailTitlePipe,
	TeacherTypeListPipe,
	CourseNamePipe,
	LoggedUserPipe,
	CategoryTranslatePipe,
	HasPracticePipe,
	SelectedActivityPipe,
];
