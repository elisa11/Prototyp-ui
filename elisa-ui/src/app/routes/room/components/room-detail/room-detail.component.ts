import { Component, EventEmitter, Input, Output } from '@angular/core';
import { CommonText } from '@data/common-text';
import { AbstractDetailComponent } from '@core/generics/abstract-detail.component';
import { UserDepartmentModel } from '@data/model/user-department.model';
import { PaginatorFilter } from '@data/filter/paginator.filter';
import { RoomModel } from '../../model/room.model';

@Component({
	selector: 'app-room-detail',
	templateUrl: './room-detail.component.html',
})
export class RoomDetailComponent extends AbstractDetailComponent<RoomModel> {
	moduleId = 'room';
	moduleTitle = CommonText.navLinks.find((value) => value.link === this.moduleId).label;
	@Input()
	pagination: number[] = [5];
	@Input()
	equipmentLength!: number;
	@Input()
	equipmentData!: UserDepartmentModel[];
	@Input()
	equipmentLoading!: boolean;
	@Output()
	private onEquipmentFilterChange = new EventEmitter<PaginatorFilter>();

	constructor() {
		super();
	}

	equipmentFilterChange(filter: PaginatorFilter) {
		this.onEquipmentFilterChange.emit(filter);
	}
}
