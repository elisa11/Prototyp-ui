import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Room } from '@elisa11/elisa-api-angular-client';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { roomSelectors } from 'src/app/routes/timetable/store/selectors';

@Component({
	selector: 'app-room-autocomplete',
	templateUrl: './room-autocomplete.component.html',
	styleUrls: ['./room-autocomplete.component.scss'],
})
export class RoomAutocompleteComponent implements OnInit {
	@Input() room: FormControl = new FormControl();
	@Input() filteredRooms$: Observable<Room[]>;
	@Input() loading$: Observable<boolean>;
	@Output() roomSelected: EventEmitter<Room> = new EventEmitter<Room>();

	constructor() {}

	ngOnInit(): void {}

	selectRoom($event) {
		this.room.setValue($event.option.value.name, { emitEvent: false });
		this.roomSelected.emit($event.option.value);
	}

	reset() {
		this.room.reset();
		this.roomSelected.emit(null);
	}
}
