import * as departmentSelectors from './department.selectors';
import * as userSelectors from './user.selectors';
import * as userDepartmentSelectors from './user-department.selectors';
import * as searchDepartmentSelectors from './search-department.selectors';

export {
	departmentSelectors,
	userDepartmentSelectors,
	searchDepartmentSelectors,
	userSelectors,
};
