import { createAction, props } from '@ngrx/store';
import { Departments } from '../reducers/department.reducer';

export const loadDepartmentsSuccess = createAction(
	'[User - Department/Api] Load Department Success',
	props<{ departments: Departments }>()
);

export const loadDepartmentsFailure = createAction(
	'[User - Department/Api] Load Department Failure',
	props<{ error: any }>()
);
