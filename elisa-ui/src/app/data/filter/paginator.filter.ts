export interface PaginatorFilter {
	asc: boolean;
	sortBy: string;
	search?: string;
	limit: number;
	offset: number;
	id?: number; // pomocne id, ak zobrazujeme detail
}
