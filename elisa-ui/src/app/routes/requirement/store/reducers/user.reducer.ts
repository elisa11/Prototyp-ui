import { createReducer, on } from '@ngrx/store';
import { createEntityAdapter, EntityAdapter, EntityState } from '@ngrx/entity';
import { User } from '@elisa11/elisa-api-angular-client';
import { searchUserActions, userApiActions } from '../actions';

export const userFeatureKey = 'user';

export interface State extends EntityState<User> {
	readonly selectedIds: number[];
	readonly loading: boolean;
}

export const adapter: EntityAdapter<User> = createEntityAdapter<User>({
	selectId: (model: User) => model.id,
	sortComparer: false,
});

export const initialState = adapter.getInitialState({
	selectedIds: [],
	loading: false,
});

export const reducer = createReducer(
	initialState,
	on(searchUserActions.searchUsersSuccess, (state, { data }) =>
		adapter.upsertMany(data.results, state)
	),
	on(userApiActions.loadUsers, (state: State, { ids }) => ({
		...state,
		selectedIds: ids,
		loading: true,
	})),
	on(userApiActions.loadUsersSuccess, (state: State, { data }) =>
		adapter.upsertMany(data, { ...state, loading: false })
	),
	on(userApiActions.loadUsersFailure, (state: State) => ({ ...state, loading: false }))
);

export const getSelectedIds = (state: State) => state.selectedIds;
export const getLoading = (state: State) => state.loading;
