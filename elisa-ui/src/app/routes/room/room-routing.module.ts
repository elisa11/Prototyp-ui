import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from '../../auth/services/auth.guard';
import { CollectionPageComponent } from './containers/collection-page/collection-page.component';
import { ViewRoomPageComponent } from './containers/view-room-page/view-room-page.component';
import { NewRoomPageComponent } from './containers/new-room-page/new-room-page.component';
import { RoomExistsGuard } from './guards';

const routes: Routes = [
	{
		path: '',
		component: CollectionPageComponent,
		canActivate: [AuthGuard],
	},
  {
    path: 'form',
    component: NewRoomPageComponent,
    canActivate: [AuthGuard],
  },
	{
		path: ':id',
		component: ViewRoomPageComponent,
		canActivate: [AuthGuard, RoomExistsGuard],
	},
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule],
})
export class RoomRoutingModule {}
