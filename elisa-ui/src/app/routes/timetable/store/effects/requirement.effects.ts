import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { SnackEffects } from '@core/generics/snack.effects';
import { MatSnackBar } from '@angular/material/snack-bar';
import { catchError, map, skip, switchMap, takeUntil } from 'rxjs/operators';
import { filterActions, requirementActions } from '../actions';
import { of, withLatestFrom } from 'rxjs';
import { Store } from '@ngrx/store';
import { schemaSelectors } from '@core/store/selectors';
import { RequirementsCacheService } from '@core/service/cache/requirements-cache.service';
import { NGXLogger } from 'ngx-logger';
const tag = 'RequirementEffects';

@Injectable()
export class RequirementEffects extends SnackEffects {
	loadRequirements$ = createEffect(() =>
		this.actions$.pipe(
			ofType(requirementActions.loadRequirements),
			withLatestFrom(this.store.select(schemaSelectors.selectScheme)),
			map((value) => {
				return {
					version: value[1].name,
				};
			}),
			switchMap(({ version }) => {
				return this.requirementsService.listRequirements(null, version).pipe(
					map((data) => requirementActions.loadRequirementsSuccess({ data })),
					catchError((err) => {
						this.logger.error(tag, 'search$', err);
						return of(requirementActions.loadRequirementsFailure({ error: err.message }));
					})
				);
			})
		)
	);
	constructor(
		private actions$: Actions,
		private requirementsService: RequirementsCacheService,
		private logger: NGXLogger,
		private store: Store,
		snack: MatSnackBar
	) {
		super(snack);
	}
}
