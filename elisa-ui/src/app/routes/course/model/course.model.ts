import {
	CourseSerializerFull,
	DepartmentSerializerShort,
	User,
} from '@elisa11/elisa-api-angular-client';

export class CourseModel implements CourseSerializerFull {
	id: number;
	name: string;
	code?: string;
	department: DepartmentSerializerShort;
	teacher: User;
	completion?: string;
	period?: number;
	credits?: number;

	public constructor(init?: CourseSerializerFull) {
		Object.assign(this, init);
	}
}
