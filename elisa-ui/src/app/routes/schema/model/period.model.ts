import { Department, Period } from '@elisa11/elisa-api-angular-client';

export class PeriodModel implements Period {
	academic_sequence: number | null;
	active: boolean;
	department: number;
	departmentData?: Department;
	end_date: string;
	readonly id: number;
	name: string;
	next_period: number | null;
	previous_period: number | null;
	start_date: string;
	university_period: number | null;

	constructor(arg: Period, dep: Department) {
		Object.assign(this, arg);
		this.departmentData = dep;
	}
}
