import { createAction, props } from '@ngrx/store';
import { CourseSerializerFull } from '@elisa11/elisa-api-angular-client';

export const cancelCourse = createAction('[Course] Cancel Course');

export const loadCourse = createAction(
	'[Course - Course] Load Course',
	props<{ course: CourseSerializerFull }>()
);

export const selectCourse = createAction(
	'[Course - View Course Page] Select Course',
	props<{ id: number }>()
);
