import { createAction, props } from '@ngrx/store';
import { Requirement } from '@elisa11/elisa-api-angular-client';

export const cancelRequirement = createAction('[Requirement] Cancel Requirement');

export const loadRequirement = createAction(
	'[Requirement - Requirement] Load Requirement',
	props<{ data: Requirement }>()
);

export const selectRequirement = createAction(
	'[Requirement - Requirement] Select Requirement',
	props<{ id: number }>()
);
