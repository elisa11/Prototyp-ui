import { Provider } from '@angular/core';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { HttpErrorInterceptor } from '@core/interceptor/http-error-interceptor.service';

export const errorInterceptorProvider: Provider = {
	provide: HTTP_INTERCEPTORS,
	useClass: HttpErrorInterceptor,
	multi: true,
};
