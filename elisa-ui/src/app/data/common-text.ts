import { MenuItem } from '@core/model/menu-item.model';
import { TeacherType } from './model/teacher-type.model';
import {
	DayEnum,
	Equipment,
	StateEnum,
	SuitabilityEnum,
} from '@elisa11/elisa-api-angular-client';
import { TextData } from './text-data';
import { marker } from '@biesbjerg/ngx-translate-extract-marker';
import { RoleManager } from '../auth/store/model/Roles.model';

export enum EventType {
	blank,
	lecture,
	practice,
	seminar,
}

export class CommonText {
	public static navLinks: MenuItem[] = [
		{
			label: marker('timetable.link'),
			link: 'timetable',
			showOnLarge: true,
			showOnMedium: true,
			icon: 'timeline',
			version: true,
			forRoles: [...RoleManager.CREATORS_ID],
		},
		{
			link: 'department',
			label: marker('department.link'),
			showOnLarge: true,
			icon: 'apartment',
			forRoles: [...RoleManager.CREATORS_ID],
		},
		{
			link: 'course',
			label: marker('course.link'),
			showOnLarge: true,
			showOnMedium: true,
			version: true,
			icon: 'subject',
			forRoles: [...RoleManager.CREATORS_ID],
		},
		{
			link: 'room',
			label: marker('room.link'),
			showOnLarge: true,
			icon: 'room',
			version: true,
			forRoles: [...RoleManager.CREATORS_ID],
		},
		{
			link: 'group',
			label: marker('group.link'),
			showOnLarge: true,
			icon: 'group',
			version: true,
			forRoles: [...RoleManager.CREATORS_ID],
		},
		{
			link: 'user',
			label: marker('user.link'),
			showOnLarge: true,
			icon: 'person',
			forRoles: [...RoleManager.ID_ALL],
		},
		{
			link: 'requirement',
			label: marker('requirement.link'),
			showOnLarge: true,
			showOnMedium: true,
			version: true,
			icon: 'sms',
			forRoles: [...RoleManager.EXCEPT_STUDENT_ID],
		},
	];

	public static StatusValues = [
		{
			name: marker('requirement.status.created.plural'),
			// name: 'Created',
			icon: 'create',
			color: 'darkseagreen',
			value: StateEnum.Created,
		},
		{
			value: StateEnum.Edited,
			name: marker('requirement.status.edited.plural'),
			// name: 'Edited',
			icon: 'edit',
			color: 'dodgerblue',
		},
		{
			value: StateEnum.Rejected,
			name: marker('requirement.status.rejected.plural'),
			icon: 'clear',
			color: 'orangered',
		},
		{
			value: StateEnum.Approved,
			name: marker('requirement.status.approved.plural'),
			icon: 'check',
			color: 'chartreuse',
		},
	];
	public static roomItem = {
		1: marker('room.item.table'),
		2: marker('room.item.projector'),
		3: marker('room.item.computer'),
	};
	public static daysValues = [
		{
			id: 1,
			enum: DayEnum.Monday,
			value: marker('day.monday.value'),
			short: marker('day.monday.short'),
		},
		{
			id: 2,
			enum: DayEnum.Tuesday,
			value: marker('day.tuesday.value'),
			short: marker('day.tuesday.short'),
		},
		{
			id: 3,
			enum: DayEnum.Wednesday,
			value: marker('day.wednesday.value'),
			short: marker('day.wednesday.short'),
		},
		{
			id: 4,
			enum: DayEnum.Thursday,
			value: marker('day.thursday.value'),
			short: marker('day.thursday.short'),
		},
		{
			id: 5,
			enum: DayEnum.Friday,
			value: marker('day.friday.value'),
			short: marker('day.friday.short'),
		},
	];

	public static eventTypes: TextData[] = [
		{ id: 0, value: 'empty' },
		{ id: 1, value: 'lecture' },
		{ id: 2, value: 'practice' },
		{ id: 3, value: 'seminar' },
	];

	public static eventStatusValues = [
		{
			id: 0,
			enum: SuitabilityEnum.Suitable,
			value: marker('event.type.suitable'),
			color: 'green',
			icon: 'done',
		},
		{
			id: 1,
			enum: SuitabilityEnum.Unsuitable,
			value: marker('event.type.unsuitable'),
			color: 'yellow',
			icon: 'warning',
		},
		{
			id: 2,
			enum: SuitabilityEnum.Unavailable,
			value: marker('event.type.unavailable'),
			color: 'red',
			icon: 'error',
		},
	];

	public static requirementTypesValues = [
		{
			value: 1,
			name: 'Lecture',
			icon: 'insert_chart_outlined',
			color: 'deepskyblue',
		},
		{ value: 2, name: 'Seminar', icon: 'insert_chart_outlined', color: 'limegreen' },
	];

	public static roomPriorityIcons = [
		{ id: 0, icon: 'trending_up', color: 'green' },
		{ id: 1, icon: 'trending_flat', color: 'yellow' },
		{ id: 2, icon: 'trending_down', color: 'orange' },
	];

	public static userType = {
		1: marker('user.type.student'),
		2: marker('user.type.garant'),
		3: marker('user.type.prednasajuci'),
		4: marker('user.type.cviciaci'),
		5: marker('user.type.skusajuci'),
		6: marker('user.type.administrator'),
		7: marker('user.type.tutor'),
	};

	public static teacherTypeValues: TeacherType[] = [
		{ id: 2, value: CommonText.userType[2] },
		{ id: 3, value: CommonText.userType[3] },
		{ id: 4, value: CommonText.userType[4] },
		{ id: 5, value: CommonText.userType[5] },
		{ id: 6, value: CommonText.userType[6] },
		{ id: 7, value: CommonText.userType[7] },
	];

	public static pagination = [5, 10, 25, 100];
	public static roomItemValue: Equipment[] = [
		{ id: 1, name: CommonText.roomItem[1] } as Equipment,
		{ id: 2, name: CommonText.roomItem[2] } as Equipment,
		{ id: 3, name: CommonText.roomItem[3] } as Equipment,
	];
	public static languages = [
		{ value: 'en', name: 'English version' },
		{ value: 'sk', name: 'Slovenská verzia' },
	];
	// premenna pre ngx-translate-extract pri conditionals ?:
	private static translateMarks = [
		marker('list.data.empty'),
		marker('list.data.filter'),
		marker('button.edit'),
		marker('button.save'),
	];
}
