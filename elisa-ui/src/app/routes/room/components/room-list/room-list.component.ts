import { Component, EventEmitter, Input, Output } from '@angular/core';
import { MatSelectChange } from '@angular/material/select';
import { Department, RoomCategory } from '@elisa11/elisa-api-angular-client';
import { AbstractTableComponent } from '@core/generics/abstract-table.component';
import { RoomModel } from '../../model/room.model';
import { RoomFilterModel } from '../../model/room-filter.model';
import { ConfirmDialogComponent } from '../../../../dialogs/confirm-dialog/confirm-dialog.component';
import { ConfirmDialogModel } from '../../../../dialogs/confirm-dialog/confirm-dialog.model';
import { Store } from '@ngrx/store';
import { MatDialog } from '@angular/material/dialog';
import { deleteRoom } from '../../store/actions/room-api.actions';

@Component({
	selector: 'app-room-list',
	templateUrl: './room-list.component.html',
})
export class RoomListComponent extends AbstractTableComponent<RoomModel> {
	capacity: number;
	department: Department;
	displayedColumns = ['name', 'capacity', 'department', 'type', 'btn'];
	@Output()
	roomFilterChange: EventEmitter<RoomFilterModel> = new EventEmitter<RoomFilterModel>();
  @Output()
  private onNewRoom = new EventEmitter();
	roomFilter = {} as RoomFilterModel;

	constructor(private store: Store,
              private dialog: MatDialog) {
		super();
	}

	capacities: Array<{ min: number; max: number }> = [
		{ max: 20, min: 0 },
		{ max: 50, min: 20 },
		{ max: 150, min: 50 },
		{ max: 300, min: 150 },
		{ max: undefined, min: 300 },
	];
	@Input()
	roomTypes: RoomCategory[];
	@Output()
	onSelection = new EventEmitter<RoomCategory[]>();

	applyCapacityFilter(change: MatSelectChange): void {
		console.log('applyCapacityFilter', change);
		this.roomFilter = { ...this.roomFilter, capacity: change.value } as RoomFilterModel;
		this.roomFilterChange.emit(this.roomFilter);
	}

	valueChange(data: RoomCategory[]) {
		console.log('valueChange', data);
		this.roomFilter = {
			...this.roomFilter,
			type: data.map((d) => d.id),
		} as RoomFilterModel;
		this.roomFilterChange.emit(this.roomFilter);
	}

  newRoom() {
    console.log('New Room click');
    this.onNewRoom.emit();
  }

  deleteItem($event: MouseEvent, row: RoomModel) {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      data: {
        title: "Vymazanie miestnosti",
        // title: marker('dialog.room.title'),
        question: "Chcete skutočne vymazať miestnosť?",
        // question: marker('dialog.room.question'),
        noLabel: "Nie",
        // noLabel: marker('dialog.room.no-label'),
        yesLabel: "Áno",
        // yesLabel: marker('dialog.room.yes-label'),
      } as ConfirmDialogModel,
      disableClose: true,
    });
    dialogRef.afterClosed().subscribe(dialogRes =>{
      if (dialogRes){
        console.log("deleteRoom");
        this.store.dispatch(deleteRoom({id:row.id}));
      }
    })
    $event.stopImmediatePropagation();
  }

  editRoom($event: MouseEvent, row: RoomModel) {
    this.onNewRoom.emit(row);
    $event.stopImmediatePropagation();
  }
}
