import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { LocaleService } from '@core/service/locale/locale.service';
import { loadLanguage } from '@core/store/actions/language.actions';
import { tap } from 'rxjs/operators';

@Injectable()
export class LanguageEffects {
	langChange$ = createEffect(
		() =>
			this.actions$.pipe(
				ofType(loadLanguage),
				tap((action) => this.languageService.use(action.lang))
			),
		{ dispatch: false }
	);

	constructor(private actions$: Actions, private languageService: LocaleService) {}
}
