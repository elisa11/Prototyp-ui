import { createFeatureSelector, createSelector } from '@ngrx/store';
import * as fromTimetables from '../reducers/timetables.reducer';
import { Timetable } from '@elisa11/elisa-api-angular-client';
import { timetablesFeatureKey, TimetablesState } from '../reducers';

export const selectTimetablesState =
	createFeatureSelector<TimetablesState>(timetablesFeatureKey);

export const selectEntityState = createSelector(
	selectTimetablesState,
	(s) => s[fromTimetables.timetablesFeatureKey]
);

export const selectSelectedIds = createSelector(
	selectEntityState,
	fromTimetables.getSelectedIds
);

export const selectTotalCount = createSelector(
	selectEntityState,
	fromTimetables.getCount
);
export const selectTimetable = createSelector(
	selectEntityState,
	fromTimetables.getTimetable
);

export const { selectIds, selectEntities } =
	fromTimetables.adapter.getSelectors(selectEntityState);

export const selectSelectedTimetable = createSelector(
	selectEntities,
	selectSelectedIds,
	(entities, selectedId) => {
		return selectedId.map((id) => entities[id]).filter((d): d is Timetable => d != null);
	}
);

export const selectAllTimetables = createSelector(
	selectEntities,
	selectSelectedIds,
	(data, ids) => {
		return ids.map((id) => data[id]);
	}
);
