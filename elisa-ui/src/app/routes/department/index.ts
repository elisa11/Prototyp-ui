import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatTableModule } from '@angular/material/table';
import { MatSortModule } from '@angular/material/sort';
import { MatIconModule } from '@angular/material/icon';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatCardModule } from '@angular/material/card';

export const MATERIAL_MODULES = [
	MatInputModule,
	MatButtonModule,
	MatTooltipModule,
	MatTableModule,
	MatSortModule,
	MatIconModule,
	MatProgressSpinnerModule,
	MatPaginatorModule,
	MatCardModule,
];
