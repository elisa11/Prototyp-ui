import { createReducer, on } from '@ngrx/store';
import { createEntityAdapter, EntityAdapter, EntityState, Update } from '@ngrx/entity';
import { Requirement } from '@elisa11/elisa-api-angular-client';
import {
	commentsApiActions,
	requirementActions,
	requirementApiActions,
} from '../actions';
import { deleteSchemaDone } from '../../../schema/store/actions/new-schema.actions';

export const requirementFeatureKey = 'requirement';

export interface State extends EntityState<Requirement> {
	readonly selectedId: number;
	readonly count: number;
}

export const adapter: EntityAdapter<Requirement> = createEntityAdapter<Requirement>({
	selectId: (model: Requirement) => model.id,
	sortComparer: false,
});

export const initialState = adapter.getInitialState({
	selectedId: null,
	count: 0,
});

export const reducer = createReducer(
	initialState,
	on(requirementApiActions.searchRequirementSuccess, (state: State, { data }) =>
		adapter.upsertMany(data.results, {
			...state,
			count: data.count,
		})
	),
	on(requirementActions.loadRequirement, (state, { data }) =>
		adapter.upsertOne(data, state)
	),
	on(requirementActions.selectRequirement, (state, { id }) => ({
		...state,
		selectedId: id,
	})),
	on(commentsApiActions.loadCommentsSuccess, (state: State, { data, rid }) => {
		const update = {
			id: rid,
			changes: { requirement_comments: data.results },
		} as Update<Requirement>;
		return adapter.updateOne(update, state);
	}),
	// on(requirementApiActions.postRequirementSuccess, (state: State, { data }) => {
	// 	adapter.upsertOne(data);
	// 	return adapter.updateOne(update, state);
	// }),
	on(requirementApiActions.deleteRequirementDone, (state, { id }) =>
		adapter.removeOne(id, state)
	)
);

export const getSelectId = (state: State) => state.selectedId;
export const getCount = (state: State) => state.count;
