import { createAction, props } from '@ngrx/store';
import { Department } from '@elisa11/elisa-api-angular-client';

export const loadDepartment = createAction(
	'[Department - Department] Load Department',
	props<{ department: Department }>()
);

export const selectDepartment = createAction(
	'[Department - View Department page] Select Department',
	props<{ id: number }>()
);
