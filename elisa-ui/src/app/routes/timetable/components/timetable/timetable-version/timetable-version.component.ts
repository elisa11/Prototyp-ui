import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Timetable } from '@elisa11/elisa-api-angular-client';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { timetableEventsActions, timetablesActions } from '../../../store/actions';
import { loadTimetableEvents } from '../../../store/actions/timetables-events.actions';
import { loadTimetables } from '../../../store/actions/timetables.actions';
import {
	selectAllTimetables,
	selectTimetable,
} from '../../../store/selectors/timetables.selectors';

@Component({
	selector: 'app-timetable-version',
	templateUrl: './timetable-version.component.html',
	styleUrls: ['./timetable-version.component.scss'],
})
export class TimetableVersionComponent implements OnInit {
	@Output() selected: EventEmitter<Timetable> = new EventEmitter();

	timetablesList$: Observable<Timetable[]> = this.store.select(selectAllTimetables);
	selectedTimetable: Timetable = null;
	showCreateTC: boolean = false;
	nameOfNewTimetable: string = '';
	constructor(private store: Store) {}

	ngOnInit(): void {
		this.store.dispatch(loadTimetables());
		this.store.select(selectTimetable).subscribe((timetable) => {
			this.selectedTimetable = timetable;
		});
	}

	timetableModelChange($event: Timetable) {
		if ($event != null) {
			this.store.dispatch(timetablesActions.timetableSelected({ timetable: $event }));
			this.selected.emit($event);
		}
	}

	addTimetable() {
		this.store.dispatch(
			timetablesActions.createTimetable({ name: this.nameOfNewTimetable })
		);
		this.nameOfNewTimetable = '';
	}

	addTimetableFrom() {
		this.store.dispatch(
			timetablesActions.branchTimetable({ name: this.nameOfNewTimetable })
		);
		this.nameOfNewTimetable = '';
	}

	importTimetableEvents() {
		console.log('importTimetableEvents');
		this.store.dispatch(
			timetableEventsActions.importTimetableEvents({ timetable: this.selectedTimetable })
		);
	}
}
