import * as UserActions from './user.actions';
import * as UserApiActions from './user-api.actions';
import * as DepartmentApiActions from './department-api.actions';
import * as UserDepartmentApiActions from './user-department-api.actions';

export { UserActions, UserDepartmentApiActions, UserApiActions, DepartmentApiActions };
