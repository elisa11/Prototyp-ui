import { createSelector } from '@ngrx/store';
import * as fromSearch from '../reducers/search-requirement.reducer';
import { Requirement } from '@elisa11/elisa-api-angular-client';
import { selectEntities, selectRequirementsState } from './requirement.selectors';

export const selectSearchState = createSelector(
	selectRequirementsState,
	(s) => s[fromSearch.searchRequirementFeatureKey]
);

export const selectSearchIds = createSelector(selectSearchState, fromSearch.getIds);
export const selectSearchQuery = createSelector(selectSearchState, fromSearch.getQuery);
export const selectSearchLoading = createSelector(
	selectSearchState,
	fromSearch.getLoading
);
export const selectSearchError = createSelector(selectSearchState, fromSearch.getError);

export const selectSearchResults = createSelector(
	selectEntities,
	selectSearchIds,
	(data, searchIds) => {
		return searchIds.map((id) => data[id]).filter((d): d is Requirement => d != null);
	}
);
