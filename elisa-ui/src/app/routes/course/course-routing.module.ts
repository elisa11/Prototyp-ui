import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from '../../auth/services/auth.guard';
import { CollectionPageComponent } from './containers/collection-page/collection-page.component';
import { NewCoursePageComponent } from './containers/new-course-page/new-course-page.component';
import { ViewCoursePageComponent } from './containers/view-course-page/view-course-page.component';
import { CourseExistsGuard } from './guards';

const routes: Routes = [
	{
		path: '',
		component: CollectionPageComponent,
		canActivate: [AuthGuard],
	},
  {
    path: 'form',
    component: NewCoursePageComponent,
    canActivate: [AuthGuard],
  },
	{
		path: ':id',
		component: ViewCoursePageComponent,
		canActivate: [AuthGuard, CourseExistsGuard],
	},
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule],
})
export class CourseRoutingModule {}
