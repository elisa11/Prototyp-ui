import { Component, OnInit } from '@angular/core';
import { Settings } from '@data/settings.data';
import { Observable } from 'rxjs';
import { SettingsService } from '@core/service/settings.service';

@Component({
	selector: 'app-settings',
	templateUrl: './settings.component.html',
	styleUrls: ['./settings.component.css'],
})
export class SettingsComponent implements OnInit {
	settings$: Observable<Settings>;

	constructor(private settingsService: SettingsService) {}

	ngOnInit(): void {
		this.settings$ = this.settingsService.settings;
	}
}
