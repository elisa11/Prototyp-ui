import { MatBadgeModule } from '@angular/material/badge';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { MatCardModule } from '@angular/material/card';
import { MatInputModule } from '@angular/material/input';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatSortModule } from '@angular/material/sort';
import { MatTabsModule } from '@angular/material/tabs';

export const MATERIAL_MODULES = [
	MatBadgeModule,
	MatButtonModule,
	MatIconModule,
	MatCheckboxModule,
	MatTableModule,
	MatPaginatorModule,
	MatFormFieldModule,
	MatCardModule,
	MatSelectModule,
  MatFormFieldModule,
  MatInputModule,
  MatTooltipModule,
  MatSortModule,
  MatTabsModule
];
