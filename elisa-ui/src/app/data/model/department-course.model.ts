import {
	CourseSerializerFull,
	DepartmentSerializerShort,
} from '@elisa11/elisa-api-angular-client';

export class DepartmentCourse implements DepartmentSerializerShort {
	courses: CourseSerializerFull[];
	readonly id: number;
	readonly name: string;

	constructor(department) {
		Object.assign(this, department);
		this.courses = [];
	}
}
