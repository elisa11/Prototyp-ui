import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { resetCachedData } from '@core/store/actions/settings.actions';
import {
	catchError,
	debounceTime,
	map,
	skip,
	switchMap,
	takeUntil,
	tap,
} from 'rxjs/operators';
import { PeriodCacheService } from '../../services/period-cache.service';
import { asyncScheduler, EMPTY, of, zip } from 'rxjs';
import { Store } from '@ngrx/store';
import { MatSnackBar } from '@angular/material/snack-bar';
import { NGXLogger } from 'ngx-logger';
import { SnackEffects } from '@core/generics/snack.effects';
import { periodActions, periodApiActions, schemaApiActions } from '../actions';
import { LocaleService } from '@core/service/locale/locale.service';
import { marker } from '@biesbjerg/ngx-translate-extract-marker';

const tag = 'PeriodEffects';

@Injectable()
export class PeriodEffects extends SnackEffects {
	resetData$ = createEffect(
		() =>
			this.actions$.pipe(
				ofType(resetCachedData),
				tap(() => this.service.clearCache())
			),
		{ dispatch: false }
	);

	searchPeriods$ = createEffect(
		() =>
			({ debounce = 300, scheduler = asyncScheduler } = {}) =>
				this.actions$.pipe(
					ofType(periodApiActions.searchPeriods),
					debounceTime(debounce, scheduler),
					switchMap(({ query }) => {
						if (query === null) {
							return EMPTY;
						}

						const nextSearch$ = this.actions$.pipe(
							ofType(periodApiActions.searchPeriods),
							skip(1)
						);

						return this.service.listPeriods(query).pipe(
							takeUntil(nextSearch$),
							map((periods) => periodApiActions.searchPeriodsSuccess({ periods })),
							catchError((err) => {
								this.logger.error(tag, 'searchPeriods$', err);
								return of(periodApiActions.searchPeriodsFailure({ error: err.message }));
							})
						);
					})
				)
	);
	searchPeriodsFailure$ = createEffect(
		() =>
			this.actions$.pipe(
				ofType(periodApiActions.searchPeriodsFailure),
				tap((err) =>
					this.openSnackbar(this.localeService.translate(marker('period.search.error')))
				)
			),
		{ dispatch: false }
	);

	loadPeriod$ = createEffect(() =>
		this.actions$.pipe(
			ofType(schemaApiActions.searchSchemasSuccess),
			map((action) => action.versions.results),
			map((x) => x.map((v) => v.period)),
			map((x) => [...new Set(x)]),
			switchMap((ids) => {
				this.logger.debug(tag, 'loadPeriod$', 'parallel');
				return zip(
					...ids.map((id) =>
						this.service.getPeriod(id).pipe(
							catchError((error) => {
								this.logger.error(tag, 'loadPeriods - getPeriod', error);
								return of(null);
							})
						)
					)
				).pipe(
					map((data) => {
						this.logger.debug(tag, 'loadPeriod$', data);
						return periodActions.loadPeriodsSuccess({
							data: data.filter((value) => value != null),
						});
					}),
					catchError((error) => {
						this.logger.error(tag, 'loadPeriods', error);
						return of(periodActions.loadPeriodsFailure({ error }));
					})
				);
			})
		)
	);
	loadPeriodsFailure$ = createEffect(
		() =>
			this.actions$.pipe(
				ofType(periodActions.loadPeriodsFailure),
				tap((err) =>
					this.openSnackbar(this.localeService.translate(marker('period.schema.error')))
				)
			),
		{ dispatch: false }
	);

	searchParentSchemasSuccess$ = createEffect(() =>
		this.actions$.pipe(
			ofType(schemaApiActions.searchParentSchemasSuccess),
			map(({ versions }) => versions.map((value) => value.period)),
			map((ids) => periodActions.loadParentPeriods({ ids }))
		)
	);
	loadParentPeriod$ = createEffect(() =>
		this.actions$.pipe(
			ofType(periodActions.loadParentPeriods),
			map((action) => action.ids),
			map((ids) => zip(ids.map((id) => this.service.getPeriod(id)))),
			switchMap((observable) =>
				observable.pipe(
					map((data) => periodActions.loadParentPeriodsSuccess({ data })),
					catchError((error) => {
						this.logger.error(tag, 'loadParentPeriod$', error);
						return of(periodActions.loadPeriodsFailure({ error }));
					})
				)
			)
		)
	);
	loadParentPeriodsFailure$ = createEffect(
		() =>
			this.actions$.pipe(
				ofType(periodActions.loadParentPeriodsFailure),
				tap((err) =>
					this.openSnackbar(this.localeService.translate(marker('period.parent.error')))
				)
			),
		{ dispatch: false }
	);

	constructor(
		private actions$: Actions,
		private store: Store,
		private service: PeriodCacheService,
		private localeService: LocaleService,
		snack: MatSnackBar,
		private logger: NGXLogger
	) {
		super(snack);
	}
}
