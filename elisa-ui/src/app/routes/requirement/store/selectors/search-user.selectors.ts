import { createSelector } from '@ngrx/store';
import * as fromSearchUser from '../reducers/search-user.reducer';
import { User } from '@elisa11/elisa-api-angular-client';
import { selectRequirementsState } from './requirement.selectors';
import { selectEntities } from './user.selectors';

export const selectUserState = createSelector(
	selectRequirementsState,
	(s) => s[fromSearchUser.searchUserFeatureKey]
);

export const selectSearchIds = createSelector(selectUserState, fromSearchUser.getIds);
export const selectSearchLoading = createSelector(
	selectUserState,
	fromSearchUser.getLoading
);

export const selectSearchResults = createSelector(
	selectEntities,
	selectSearchIds,
	(data, searchIds) => {
		return searchIds.map((id) => data[id]).filter((d): d is User => d != null);
	}
);
