import { createAction, props } from '@ngrx/store';
import { Version } from '@elisa11/elisa-api-angular-client';

export const loadSchemasDialog = createAction('[Core - Schema] Load Schema Dialog');

export const loadSchemasDialogSuccess = createAction(
	'[Core - Schema] Load Schemas Dialog Success',
	props<{ data: Version; old: Version }>()
);

export const loadSchemasDialogRedirect = createAction(
	'[Core - Schema] Load Schemas Dialog Redirect'
);
export const loadSchemasDialogCancel = createAction(
	'[Core - Schema] Load Schemas Dialog Cancel'
);

export const loadSchemasImportStart = createAction(
	'[Core - Schema] Load Schemas Start Import'
);

export const loadSchemasImportDone = createAction(
	'[Core - Schema] Load Schemas Import Done'
);
export const loadSchemasImportFailed = createAction(
	'[Core - Schema] Load Schemas Import Failed'
);
