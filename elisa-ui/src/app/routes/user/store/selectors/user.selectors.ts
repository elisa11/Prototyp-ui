import { createFeatureSelector, createSelector } from '@ngrx/store';
import * as fromUser from '../reducers/user.reducer';
import { usersFeatureKey, UsersState } from '../reducers';

export const selectUsersState = createFeatureSelector<UsersState>(usersFeatureKey);

export const selectUserEntitiesState = createSelector(
	selectUsersState,
	(state) => state[fromUser.userFeatureKey]
);

export const selectSelectedUserId = createSelector(
	selectUserEntitiesState,
	fromUser.getSelectId
);

export const selectUserCount = createSelector(selectUserEntitiesState, fromUser.getCount);

export const {
	selectIds: selectUserIds,
	selectEntities: selectUserEntities,
	selectAll: selectAllUsers,
	selectTotal: selectTotalUsers,
} = fromUser.adapter.getSelectors(selectUserEntitiesState);

export const selectSelectedUser = createSelector(
	selectUserEntities,
	selectSelectedUserId,
	(entities, selectedId) => {
		return selectedId && entities[selectedId];
	}
);
