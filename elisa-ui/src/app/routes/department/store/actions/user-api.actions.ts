import { createAction, props } from '@ngrx/store';
import { Users } from '../reducers/user.reducer';

export const loadUsers = createAction('[Department - User/Api] Load Users');

export const loadUsersSuccess = createAction(
	'[Department - User/Api] Load Users Success',
	props<{ data: Users }>()
);

export const loadUsersFailure = createAction(
	'[Department - User/Api] Load Users Failure',
	props<{ error: string }>()
);
