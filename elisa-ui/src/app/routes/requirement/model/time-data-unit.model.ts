// todo bude nutne zmenit pri custom layoute, len hruby nastrel
import { DayEnum } from '@elisa11/elisa-api-angular-client';
import { SuitabilityEnum } from '@elisa11/elisa-api-angular-client';
import { RequirementPart } from '@elisa11/elisa-api-angular-client';
import moment from 'moment/moment';

export class TimeDataUnit {
	day: DayEnum;
	suitability: SuitabilityEnum;
	time: string;

	constructor(unit: RequirementPart) {
		Object.assign(this, unit);
		this.time = moment(unit.time, 'HH:mm:ss').toISOString();
	}
}
