import { createAction, props } from '@ngrx/store';
import { Department } from '@elisa11/elisa-api-angular-client';

export const loadDepartmentSuccess = createAction(
	'[Room - Department/API] Load Department Success',
	props<{ data: Department }>()
);

export const loadDepartmentFailure = createAction(
	'[Room - Department/API] Load Department Failure',
	props<{ error: any }>()
);

export const loadDepartmentListSuccess = createAction(
	'[Room - Department/API] Load Department List Success',
	props<{ data: Department[] }>()
);

export const loadDepartmentListFailure = createAction(
	'[Room - Department/API] Load Department List Failure',
	props<{ error: any }>()
);
