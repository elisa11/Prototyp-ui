import { createAction, props } from '@ngrx/store';
import { Period } from '@elisa11/elisa-api-angular-client';

export const loadPeriods = createAction('[Period] Load Periods');

export const loadPeriodsSuccess = createAction(
	'[Schema - Period] Load Periods Success',
	props<{ data: Period[] }>()
);

export const loadPeriodsFailure = createAction(
	'[Schema - Period] Load Periods Failure',
	props<{ error: any }>()
);

export const loadParentPeriods = createAction(
	'[Schema - Period] Load Parent Periods',
	props<{ ids: number[] }>()
);
export const loadParentPeriodsSuccess = createAction(
	'[Schema - Period] Load Parent Periods Success',
	props<{ data: Period[] }>()
);

export const loadParentPeriodsFailure = createAction(
	'[Schema - Period] Load Parent Periods Failure',
	props<{ error: any }>()
);
