import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Login } from '@elisa11/elisa-api-angular-client';
import { authActions, loginPageActions } from '../../store/actions';
import { Store } from '@ngrx/store';
import { first, map } from 'rxjs/operators';
import { combineLatest } from 'rxjs';
import {
	selectLoggedIn,
	selectLoginPageError,
	selectLoginPagePending,
} from '../../store/selectors/auth.selectors';
import { selectQueryParams } from '@core/store/selectors/router.selectors';

@Component({
	selector: 'app-login',
	templateUrl: './login.component.html',
})
export class LoginComponent implements OnInit {
	loginForm: FormGroup;
	isLoading = false;
	data$ = combineLatest([
		this.store.select(selectLoginPagePending),
		this.store.select(selectLoginPageError),
	]).pipe(
		map(([pending, error]) => {
			return {
				pending,
				error: JSON.parse(error),
			};
		})
	);

	constructor(private fb: FormBuilder, private store: Store) {}

	ngOnInit(): void {
		this.store.dispatch(authActions.refreshTokenValidity());
		this.store
			.select(selectLoggedIn)
			.pipe(first())
			.subscribe((data) => {
				if (data) {
					this.store
						.select(selectQueryParams)
						.pipe(first())
						.subscribe((params) =>
							this.store.dispatch(authActions.authRedirect({ url: params.returnUrl }))
						);
				}
			});

		this.loginForm = this.fb.group({
			login: ['', Validators.required],
			pass: ['', Validators.required],
		});
	}

	onSubmit(): void {
		this.store.dispatch(
			loginPageActions.login({
				credentials: {
					username: this.loginForm.value.login,
					password: this.loginForm.value.pass,
				} as Login,
			})
		);
	}
}
