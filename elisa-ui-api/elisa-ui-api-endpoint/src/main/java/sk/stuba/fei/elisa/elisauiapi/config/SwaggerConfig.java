package sk.stuba.fei.elisa.elisauiapi.config;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;
import org.springdoc.core.GroupedOpenApi;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
public class SwaggerConfig {

    @Value("${spring.application.version}")
    private String version;

    @Bean
    @Profile("!prod")
    public GroupedOpenApi actuatorApi() {
        return GroupedOpenApi.builder().group("Actuator")
                .pathsToMatch("/actuator/**")
                .pathsToExclude("/actuator/health/*")
                .build();
    }

    @Bean
    public GroupedOpenApi configApi() {
        return GroupedOpenApi.builder().group("Config")
                .pathsToMatch("/config/**")
                .build();
    }

    @Bean
    public OpenAPI swaggerSpringMvcPlugin() {
        return new OpenAPI()
                .info(getApiInfo());
    }

    private Info getApiInfo() {
        return new Info()
                .title("Elisa UI Api")
                .description("REST interface for Elisa UI Api")
                .version(version)
                .contact(new Contact().name("Tomas Benco").email("xbencot@stuba.sk"));
    }
}
