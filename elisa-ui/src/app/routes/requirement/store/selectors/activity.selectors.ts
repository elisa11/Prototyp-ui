import { createSelector } from '@ngrx/store';
import * as fromCourse from '../reducers/activity.reducer';
import { selectRequirementsState } from './requirement.selectors';

export const selectActivityState = createSelector(
	selectRequirementsState,
	(s) => s[fromCourse.activityFeatureKey]
);

export const selectCourseId = createSelector(
	selectActivityState,
	fromCourse.getSelectedCourseId
);
export const selectTotalCount = createSelector(selectActivityState, fromCourse.getCount);
export const selectSearchLoading = createSelector(
	selectActivityState,
	fromCourse.getLoading
);

export const { selectIds, selectEntities, selectAll } =
	fromCourse.adapter.getSelectors(selectActivityState);

export const selectCourseResults = createSelector(
	selectAll,
	selectCourseId,
	(data, searchId) => {
		return data.filter((a) => searchId === a.course);
	}
);
