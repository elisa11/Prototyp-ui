import { createFeatureSelector, createSelector } from '@ngrx/store';
import * as fromEquipment from '../reducers/equipment.reducer';
import { Equipment } from '@elisa11/elisa-api-angular-client';
import { equipmentFeatureKey, State } from '../reducers/equipment.reducer';
import { RoomsState } from '../reducers';
import { selectRoomsState } from './room.selectors';

export const selectEquipmentState =
  createSelector(selectRoomsState, (s)=> s[fromEquipment.equipmentFeatureKey]);

export const selectSelectedIds = createSelector(selectEquipmentState, fromEquipment.getSelectedIds);

export const selectTotalCount = createSelector(selectEquipmentState, fromEquipment.getCount);

export const { selectIds, selectEntities } =
  fromEquipment.adapter.getSelectors(selectEquipmentState);

export const selectSelectedEquipment = createSelector(
  selectEntities,
  selectSelectedIds,
  (entities, selectedId) => {
    return selectedId.map((id)=> entities[id]).filter((d): d is Equipment => d!=null);
  }
);

export const selectAllEquipments = createSelector(selectEntities,
  selectSelectedIds,
  (data, ids) => {
    return ids.map((id) => data[id]);
  }
)
