import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { SnackEffects } from '@core/generics/snack.effects';
import { Store } from '@ngrx/store';
import { MatSnackBar } from '@angular/material/snack-bar';
import { catchError, map, switchMap, withLatestFrom } from 'rxjs/operators';
import { timetablesActions } from '../actions';
import { TimetableDataService } from '../../services/timetable-data.service';
import {
	loadTimetablesSuccess,
	loadTimetablesFailure,
	createTimetableFailure,
	createTimetableSuccess,
	deleteTimetable,
} from '../actions/timetables.actions';
import { from, of } from 'rxjs';
import { schemaSelectors } from '@core/store/selectors';
import { Timetable, TimetablesService } from '@elisa11/elisa-api-angular-client';
import { marker } from '@biesbjerg/ngx-translate-extract-marker';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { LoadingDialogComponent } from '../../../../dialogs/loading-dialog/loading-dialog.component';
import { timetablesSelector } from '../selectors';

// const tag = 'TimetablesEffects';

@Injectable()
export class TimetablesEffects extends SnackEffects {
	private dialogRef: MatDialogRef<LoadingDialogComponent>;

	loadTimetables$ = createEffect(() =>
		this.actions$.pipe(
			ofType(timetablesActions.loadTimetables),
			withLatestFrom(this.store.select(schemaSelectors.selectScheme)),
			map((value) => {
				console.log({ load: value });
				return { scheme: value[1].name };
			}),
			switchMap((scheme) =>
				from(
					this.timetableDataService.listTimetables(scheme.scheme).pipe(
						map((timetablesList) =>
							loadTimetablesSuccess({
								timetables: timetablesList.results,
							})
						),
						catchError((error) => of(loadTimetablesFailure({ error })))
					)
				)
			)
		)
	);

	postTimetable$ = createEffect(() =>
		this.actions$.pipe(
			ofType(timetablesActions.createTimetable),
			withLatestFrom(this.store.select(schemaSelectors.selectScheme)),
			map((value) => {
				return { scheme: value[1].name, name: value[0].name };
			}),
			switchMap((values) => {
				const timetable: Timetable = {
					created_at: '',
					id: 0,
					// major_version: 0,
					// minor_version: 0,
					owner: 0,
					updated_at: '',
					name: values.name,
				};
				this.dialogRef = this.dialog.open(LoadingDialogComponent, {
					data: {
						text: 'Prebieha vytváranie rozvrhu.',
					},
					disableClose: true,
				});
				return this.timetableService.timetablesCreate(values.scheme, timetable).pipe(
					map((timetable) => createTimetableSuccess({ timetable: timetable })),
					catchError((error) => of(createTimetableFailure({ error: error })))
				);
			})
		)
	);

	branchTimetable$ = createEffect(() =>
		this.actions$.pipe(
			ofType(timetablesActions.branchTimetable),
			withLatestFrom(
				this.store.select(schemaSelectors.selectScheme),
				this.store.select(timetablesSelector.selectTimetable)
			),
			map((value) => {
				return { scheme: value[1].name, name: value[0].name, timetableId: value[2].id };
			}),
			switchMap((values) => {
				const timetable: Timetable = {
					//TODO: Add id of timetable to branch
					created_at: '',
					id: 0,
					owner: 0,
					updated_at: '',
					name: values.name,
				};
				this.dialogRef = this.dialog.open(LoadingDialogComponent, {
					data: {
						text: 'Prebieha vytváranie rozvrhu.',
					},
					disableClose: true,
				});
				return this.timetableService
					.timetablesCloneCreate(values.scheme, values.timetableId, timetable)
					.pipe(
						map((timetable) => createTimetableSuccess({ timetable: timetable })),
						catchError((error) => of(createTimetableFailure({ error: error })))
					);
			})
		)
	);

	postTimetableSuccess = createEffect(
		() =>
			this.actions$.pipe(
				ofType(timetablesActions.createTimetableSuccess),
				map(() => {
					this.dialogRef && this.dialogRef.close();
					this.openSnackbar('Rozvrh bol úspešne vytvorený.');
				})
			),
		{ dispatch: false }
	);

	postTimetableFailure$ = createEffect(
		() =>
			this.actions$.pipe(
				ofType(timetablesActions.createTimetableFailure),
				map(() => {
					this.dialogRef && this.dialogRef.close();
					this.openSnackbar('Nastala chyba pri vytváraní rozvrhu.');
					// this.openSnackbar(this.localeService.translate(marker('timetable.post.error')))
				})
			),
		{ dispatch: false }
	);

	deleteTimetable$ = createEffect(() =>
		this.actions$.pipe(
			ofType(timetablesActions.deleteTimetable),
			withLatestFrom(this.store.select(schemaSelectors.selectScheme)),
			map((data) => {
				return { id: data[0].id, scheme: data[1].name };
			}),
			switchMap((data) => {
				return this.timetableDataService.deleteTimetable(data.scheme, data.id).pipe(
					map(() => timetablesActions.deleteTimetableSuccess({ id: data.id })),
					catchError((err) =>
						of(timetablesActions.deleteTimetableFailure({ error: err }))
					)
				);
			})
		)
	);

	deleteTimetableSuccess = createEffect(
		() =>
			this.actions$.pipe(
				ofType(timetablesActions.deleteTimetableSuccess),
				map(() => this.openSnackbar('Rozvrh bol úspešne odstránený.'))
			),
		{ dispatch: false }
	);

	deleteTimetableFailure$ = createEffect(
		() =>
			this.actions$.pipe(
				ofType(timetablesActions.deleteTimetableFailure),
				map(
					() => this.openSnackbar('Nastala chyba pri odstraňovaní rozvrhu.')
					// this.openSnackbar(this.localeService.translate(marker('timetable.delete.error')))
				)
			),
		{ dispatch: false }
	);

	constructor(
		private actions$: Actions,
		private store: Store,
		private timetableDataService: TimetableDataService,
		private timetableService: TimetablesService,
		private dialog: MatDialog,
		snack: MatSnackBar
	) {
		super(snack);
	}
}
