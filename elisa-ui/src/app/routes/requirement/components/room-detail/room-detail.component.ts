import { Component, Input } from '@angular/core';
import {
	Department,
	Room,
	RoomCategory,
	RoomEquipment,
} from '@elisa11/elisa-api-angular-client';

@Component({
	selector: 'app-room-detail',
	templateUrl: './room-detail.component.html',
})
export class RoomDetailComponent {
	@Input() room!: Room;
	// @Input() roomType!: RoomCategory;
	// @Input() roomEquipment!: RoomEquipment[];
	// @Input() department!: Department;

	constructor() {}
}
