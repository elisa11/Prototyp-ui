import { createSelector } from '@ngrx/store';
import { selectRoomsState } from './room.selectors';
import * as fromType from '../reducers/room-type.reducer';

export const selectTypesState = createSelector(
	selectRoomsState,
	(s) => s[fromType.roomTypeFeatureKey]
);

export const selectSelectedIds = createSelector(
	selectTypesState,
	fromType.getSelectedIds
);
export const selectSelectedId = createSelector(selectTypesState, fromType.getSelectedId);

export const { selectEntities } = fromType.adapter.getSelectors(selectTypesState);

export const selectTypeList = createSelector(
	selectEntities,
	selectSelectedIds,
	(dict, ids) => {
		return ids.map((id) => dict[id]);
	}
);
