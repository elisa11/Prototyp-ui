import { Pipe, PipeTransform } from '@angular/core';
import { Period } from '@elisa11/elisa-api-angular-client';

@Pipe({
	name: 'periodName',
})
export class PeriodNamePipe implements PipeTransform {
	transform(value: Period): string {
		return value ? value.name : '';
	}
}
