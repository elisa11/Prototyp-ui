import { createSelector } from '@ngrx/store';
import { selectTimetablesState } from './timetables.selectors';
import * as fromRequirements from '../reducers/requirement.reducer';
import { Collision, Requirement } from '@elisa11/elisa-api-angular-client';

export const selectRequirementsState = createSelector(
	selectTimetablesState,
	(s) => s[fromRequirements.requirementsFeatureKey]
);

export const selectSelectedIds = createSelector(
	selectRequirementsState,
	fromRequirements.getSelectedIds
);
export const selectTotalCount = createSelector(
	selectRequirementsState,
	fromRequirements.getCount
);
export const selectSearchLoading = createSelector(
	selectRequirementsState,
	fromRequirements.isLoading
);

export const { selectIds, selectEntities } = fromRequirements.adapter.getSelectors(
	selectRequirementsState
);

export const selectSelectedResults = createSelector(
	selectEntities,
	selectSelectedIds,
	(data, ids) => {
		return ids.map((id) => data[id]).filter((d): d is Requirement => d != null);
	}
);
export const selectAllRequirements = createSelector(
	selectEntities,
	selectSelectedIds,
	(data, ids) => {
		return ids.map((id) => data[id]);
	}
);

export const selectSearchRequirements = createSelector(
	selectEntities,
	selectSelectedIds,
	(data, ids) => {
		return ids.map((id) => data[id]).filter((e): e is Requirement => e != null);
	}
);

export const selectRequirement = createSelector(
	selectRequirementsState,
	fromRequirements.getRequirement
);
