export interface IPage {
	count?: number;
	next?: string;
	previous?: string;
	results?: Array<any>;
}
