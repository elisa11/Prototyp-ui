import { createSelector } from '@ngrx/store';
import { selectDepartmentState } from './department.selectors';
import * as fromUserDepartment from '../reducers/user-department.reducer';

export const selectDepartmentUserDepartmentState = createSelector(
	selectDepartmentState,
	(s) => s[fromUserDepartment.userDepartmentFeatureKey]
);

export const selectSelectedId = createSelector(
	selectDepartmentUserDepartmentState,
	fromUserDepartment.getSelectedId
);

export const selectTotalCount = createSelector(
	selectDepartmentUserDepartmentState,
	fromUserDepartment.getTotal
);

export const selectLoading = createSelector(
	selectDepartmentUserDepartmentState,
	fromUserDepartment.getLoading
);

export const {
	selectIds: selectUserDepartmentIds,
	selectEntities: selectUserDepartmentEntities,
	selectAll: selectAllUserDepartments,
} = fromUserDepartment.adapter.getSelectors(selectDepartmentUserDepartmentState);

export const selectUserDepartmentByDepartmentId = createSelector(
	selectUserDepartmentEntities,
	selectSelectedId,
	(entities, id) => {
		return id && entities[id] ? entities[id].data : [];
	}
);
