import {
	BlankEnum,
	EmploymentEnum,
	User,
	UserDepartment as UD,
} from '@elisa11/elisa-api-angular-client';

export class UserDepartmentModel implements User, UD {
	id: number;
	username: string;
	groups: string[];
	first_name: string;
	last_name: string;
	title_after: string | null;
	title_before: string | null;

	constructor(arg: User, ud: UD) {
		Object.assign(this, { ...arg, ...ud });
	}

	department: number;
	employment: EmploymentEnum | BlankEnum;
	user: number;
}
