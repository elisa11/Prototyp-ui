import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DepartmentDetailComponent } from './components/department-detail/department-detail.component';
import { DepartmentRoutingModule } from './department-routing.module';
import { DepartmentListComponent } from './components/department-list/department-list.component';
import { SharedModule } from '@shared/shared.module';
import { TranslateModule } from '@ngx-translate/core';
import { MATERIAL_MODULES } from './index';
import { EffectsModule } from '@ngrx/effects';
import { DepartmentEffects, UserDepartmentEffects, UserEffects } from './store/effects';
import { StoreModule } from '@ngrx/store';
import * as fromDepartment from './store/reducers';
import { CollectionPageComponent } from './containers/collection-page/collection-page.component';
import { SelectedDepartmentPageComponent } from './containers/selected-department-page/selected-department-page.component';
import { ViewDepartmentPageComponent } from './containers/view-department-page/view-department-page.component';
import { UserDepartmentTableComponent } from './components/user-department-table/user-department-table.component';

@NgModule({
	declarations: [
		DepartmentDetailComponent,
		DepartmentListComponent,
		CollectionPageComponent,
		SelectedDepartmentPageComponent,
		ViewDepartmentPageComponent,
		UserDepartmentTableComponent,
	],
	imports: [
		CommonModule,
		DepartmentRoutingModule,
		SharedModule,
		TranslateModule.forChild(),
		...MATERIAL_MODULES,
		StoreModule.forFeature(fromDepartment.departmentsFeatureKey, fromDepartment.reducers),
		EffectsModule.forFeature([DepartmentEffects, UserDepartmentEffects, UserEffects]),
	],
})
export class DepartmentModule {}
