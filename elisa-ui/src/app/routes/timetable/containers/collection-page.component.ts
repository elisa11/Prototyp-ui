import { ChangeDetectionStrategy, Component } from '@angular/core';
import { AbstractCollectionPageComponent } from '@core/generics/abstract-collection-page.component';
import { Store } from '@ngrx/store';
import { PaginatorFilter } from '@data/filter/paginator.filter';
import { combineLatest } from 'rxjs';
import { map } from 'rxjs/operators';
import { Router } from '@angular/router';
import { timetableEventsSelector } from '../store/selectors';
import { TimetableEventFilterModel } from '../model/timetable-event-filter.model';
import { TimetableEventModel } from '../model/timetable-event.model';
import {
	selectAllTimetableEvents,
	selectSearchTimetableEvents,
} from '../store/selectors/timetables-events.selectors';
import { selectAllActivities } from '../store/selectors/activity.selectors';
import { selectAllGroups } from '../store/selectors/groups.selectors';
import { selectAllUsers } from '../../user/store/selectors/user.selectors';
import { searchRoomSelectors } from '../../room/store/selectors';
import { searchDepartmentSelectors } from '../../department/store/selectors';
@Component({
	template: ` <app-timetable></app-timetable> `,
	changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CollectionPageComponent extends AbstractCollectionPageComponent<TimetableEventModel> {
	private filterChange: PaginatorFilter = {
		limit: this.pagination[0],
		offset: 0,
	} as PaginatorFilter;

	constructor(private store: Store, private router: Router) {
		super();
		// this.data$ = combineLatest([
		// 	store.select(selectSearchTimetableEvents),
		// 	store.select(selectAllActivities),
		// 	store.select(selectAllGroups),
		// 	store.select(searchRoomSelectors.selectSearchResults),
		// 	store.select(selectAllUsers),
		// 	store.select(searchDepartmentSelectors.selectSearchResults),
		// ]).pipe(
		// 	map((data) => {
		// 		return {
		// 			event: data[0] ?? [],
		// 			activity: data[1] ?? [],
		// 			groups: data[2] ?? [],
		// 			rooms: data[3] ?? [],
		// 			teachers: data[4] ?? [],
		// 			departments: data[5] ?? [],
		// 		};
		// 	}),
		// 	map((value) => {
		// 		return value.event.map((e) => {
		// 			return new TimetableEventModel(
		// 				e,
		// 				value.activity.find((a) => e.activity.id === a.id),
		// 				value.rooms,
		// 				value.groups,
		// 				value.teachers,
		// 				value.departments
		// 			);
		// 		});
		// 	})
		// );
		// this.totalCount$ = store.select(timetableEventsSelector.selectTotalCount);
	}

	handleFilterChange(filterChange: PaginatorFilter) {
		this.filterChange = filterChange;
	}

	handleRoomFilterChange(filterChange: TimetableEventFilterModel) {}
}
