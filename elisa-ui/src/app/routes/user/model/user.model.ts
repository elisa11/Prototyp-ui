import {
  Group, Role, User,
} from '@elisa11/elisa-api-angular-client';

export class UserModel{
  id: number;
  first_name: string;
  groups?: string[];
  last_name: string;
  title_after: string | null;
  title_before: string | null;
  username: string;
  public constructor(init?: User, groups?: string[]) {
    Object.assign(this, init);
    this.groups = groups;

  }
}
