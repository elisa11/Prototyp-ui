import { createAction, props } from '@ngrx/store';
import { PaginatedDepartmentList } from '@elisa11/elisa-api-angular-client';
import { PaginatorFilter } from '@data/filter/paginator.filter';
import { AdvancedFilter } from '../../model/advanced-filter.model';

export const searchDepartment = createAction(
	'[Timetable - Filter] Search Departments',
	props<{ query: PaginatorFilter }>()
);

export const searchDepartmentSuccess = createAction(
	'[Timetable - Filter] Search Departments Success',
	props<{ data: PaginatedDepartmentList }>()
);

export const searchDepartmentFailure = createAction(
	'[Timetable - Filter] Search Departments Failure',
	props<{ error: string }>()
);

export const getFaculties = createAction('[Timetable - Filter] Get Faculties');

export const getFacultiesSuccess = createAction(
	'[Timetable - Filter] Get Faculties Success',
	props<{ data: PaginatedDepartmentList }>()
);

export const getFacultiesFailure = createAction(
	'[Timetable - Filter] Get Faculties Failure',
	props<{ error: string }>()
);

export const getFilterOptions = createAction('[Timetable - Filter] Get Filter Options');
export const getFilterOptionsSuccess = createAction(
	'[Timetable - Filter] Get Filter Options Success',
	props<{ data: AdvancedFilter }>()
);
export const getFilterOptionsFailure = createAction(
	'[Timetable - Filter] Get Filter Options Failure',
	props<{ error: string }>()
);
