import { Injectable } from '@angular/core';
import { isNotNullOrUndefined } from 'codelyzer/util/isNotNullOrUndefined';
import {
	AuthService as AuthEndpointService,
	Login,
} from '@elisa11/elisa-api-angular-client';
import { catchError, first, map, tap } from 'rxjs/operators';
import { TokenPayload } from '../store/model/token.model';
import { Observable, of } from 'rxjs';
import { JwtHelperService } from '@auth0/angular-jwt';
import { NGXLogger } from 'ngx-logger';
import { AuthResponse } from '../store/model/auth-response.model';
import { BasicUtils } from '@shared/utils/basic.utils';
import { HttpClient } from '@angular/common/http';

const tag = 'AuthService';

@Injectable({
	providedIn: 'root',
})
export class AuthService {
	private readonly token_key = 'access_token';

	constructor(
		private service: AuthEndpointService,
		private http: HttpClient,
		private jwtHelper: JwtHelperService,
		private logger: NGXLogger
	) {}

	get currentTokenPayload(): TokenPayload {
		try {
			return this.jwtHelper.decodeToken<TokenPayload>(this.token);
		} catch (e) {
			if (e instanceof TypeError) {
				return null;
			}
			this.logger.error(tag, 'tokenParsed', e);
		}

		return null;
	}

	get token(): string {
		const token = localStorage.getItem(this.token_key) || null;
		if (isNotNullOrUndefined(token) || BasicUtils.isNotEmptyString(token)) return token;
		throw new TypeError('No Token');
	}

	public isUserAuthenticated(): Observable<boolean> {
		try {
			return this.isTokenExpired().pipe(map((value) => !value));
		} catch (e) {
			if (e instanceof TypeError) {
				return of(false);
			}
			this.logger.error(tag, 'isAuthenticated', e);
			return of(false);
		}
	}

	private isTokenExpired(): Observable<boolean> {
		const expired = this.jwtHelper.isTokenExpired(this.token);
		if (expired) {
			return this.refreshToken().pipe(
				first(),
				tap((x) => {
					if (!x) {
						this.deleteData();
					}
				}),
				catchError((err) => {
					this.logger.error(tag, 'isTokenExpired', err);
					this.deleteData();
					return of(false);
				})
			);
		}
		return of(expired);
	}

	logIn(credentials: Login): Observable<{ payload: TokenPayload; token: string }> {
		this.logger.debug(tag, 'logIn', credentials);
		return this.service.authLoginCreate(credentials).pipe(
			map((value: Login) => {
				const token = value as unknown as AuthResponse;
				this.logger.debug(tag, 'login data', token);
				localStorage.setItem(this.token_key, token.access);
				return { payload: this.currentTokenPayload, token: token.access };
			}),
			catchError((err) => {
				this.logger.error(tag, 'logIn', err);
				return of(err);
			})
		);
	}

	deleteData(): void {
		localStorage.clear();
	}

	logout(): void {
		this.service
			.authLogoutCreate()
			.pipe(first())
			.subscribe(() => {
				this.logger.debug(tag, 'Successfully logged out from server');
			});
		this.deleteData();
	}

	refreshToken() {
		try {
			return this.service.authRefreshCreate({ refresh: '', access: this.token }).pipe(
				tap((x) => {
					this.logger.debug(tag, 'token refresh');
					const token = x as unknown as AuthResponse;
					localStorage.setItem(this.token_key, token.access);
				}),
				map(() => true),
				catchError((error) => {
					this.logger.error(tag, 'refreshToken', error);
					return of(false);
				})
			);
		} catch (e) {
			return of(false);
		}
	}
}
