import { ChangeDetectionStrategy, Component } from '@angular/core';
import { AbstractCollectionPageComponent } from '@core/generics/abstract-collection-page.component';
import { Store } from '@ngrx/store';
import { PaginatorFilter } from '@data/filter/paginator.filter';
import { searchCourse } from '../../store/actions/course-api.actions';
import { courseSelector, searchCourseSelector } from '../../store/selectors';
import { CourseSerializerFull } from '@elisa11/elisa-api-angular-client';
import { Router } from '@angular/router';
import { CourseModel } from '../../model/course.model';

@Component({
	template: `
		<app-course-list
			[data]="data$ | async"
			[resultsLength]="totalCount$ | async"
			[pagination]="pagination"
			[isLoading]="loading$ | async"
			(onFilterChange)="handleFilterChange($event)"
      (onNewCourse)="handleNewCourse($event)"
		></app-course-list>
	`,
	changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CollectionPageComponent extends AbstractCollectionPageComponent<CourseSerializerFull> {

  constructor(private store: Store, private router: Router) {
		super();
		store.dispatch(
			searchCourse({ query: { limit: this.pagination[0], offset: 0 } as PaginatorFilter })
		);
		this.loading$ = store.select(searchCourseSelector.selectSearchLoading);
		this.data$ = store.select(searchCourseSelector.selectSearchResults);
		this.totalCount$ = store.select(courseSelector.selectCourseCount);
	}

	handleFilterChange(filterChange: PaginatorFilter) {
		this.store.dispatch(searchCourse({ query: filterChange }));
	}

  handleNewCourse($event: CourseModel | undefined) {
    this.router.navigate(['course','form'], {state: $event});
  }
}
