import { createSelector } from '@ngrx/store';
import {
	adapter,
	getCount,
	getLoading,
	studyFormFeatureKey,
} from '@shared/store/reducers/study-form.reducer';
import { selectSharedState } from '@shared/store/selectors/shared.selectors';

export const selectStudyFormState = createSelector(
	selectSharedState,
	(state) => state[studyFormFeatureKey]
);
export const { selectIds, selectEntities, selectAll } =
	adapter.getSelectors(selectStudyFormState);
export const selectLoading = createSelector(selectStudyFormState, getLoading);
export const selectTotalCount = createSelector(selectStudyFormState, getCount);
