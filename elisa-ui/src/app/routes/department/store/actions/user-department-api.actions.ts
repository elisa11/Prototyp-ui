import { createAction, props } from '@ngrx/store';
import { PaginatorFilter } from '@data/filter/paginator.filter';
import { PaginatedUserDepartmentList } from '@elisa11/elisa-api-angular-client';

export const loadUserDepartments = createAction(
	'[Department - User-Department/Api] Load UserDepartments',
	props<{ query: PaginatorFilter }>()
);

export const loadUserDepartmentsSuccess = createAction(
	'[Department - User-Department/Api] Load UserDepartments Success',
	props<{ data: PaginatedUserDepartmentList; query: PaginatorFilter }>()
);

export const loadUserDepartmentsFailure = createAction(
	'[Department - User-Department/Api] Load UserDepartments Failure',
	props<{ error: any }>()
);
