import { createSelector } from '@ngrx/store';
import { selectRoomsState } from './room.selectors';
import * as fromRoomEquipment from '../reducers/room-equipment.reducer';

export const selectEntitiesState = createSelector(
	selectRoomsState,
	(s) => s[fromRoomEquipment.roomEquipmentFeatureKey]
);
export const selectSelectedId = createSelector(
	selectEntitiesState,
  fromRoomEquipment.getSelectedId
);

export const selectTotalCount = createSelector(
	selectEntitiesState,
  fromRoomEquipment.getTotal
);

export const { selectIds: selectIds, selectEntities: selectEntities } =
  fromRoomEquipment.adapter.getSelectors(selectEntitiesState);

export const selectEquipment = createSelector(
	selectEntities,
	selectSelectedId,
	(entities, id) => {
		return id && entities[id] ? entities[id] : ({} as fromRoomEquipment.RoomEquipmentMap);
	}
);
