import { createReducer, on } from '@ngrx/store';
import { createEntityAdapter, EntityAdapter, EntityState } from '@ngrx/entity';
import { Activity } from '@elisa11/elisa-api-angular-client';
import {
  activityActions
} from '../actions';

export const activityFeatureKey = 'activities';

export interface State extends EntityState<Activity> {
  readonly selectedIds: number[];
  readonly count: number;
  readonly loading: boolean;
}

export const adapter: EntityAdapter<Activity> = createEntityAdapter<Activity>({
  selectId: (model: Activity) => model.id,
  sortComparer: false,
});

export const initialState = adapter.getInitialState({
  selectedIds: [],
  count: 0,
  loading: false
});

export const reducer = createReducer(
  initialState,
  on(activityActions.loadActivities, (state) =>({
    ...state,
    loading: true,
  })),
  on(activityActions.loadActivitiesSuccess, (state, {activities}) =>
    adapter.upsertMany(activities, {
      ...state,
      selectedIds: activities.map((activities) => activities.id),
      count: activities.length,
      loading: false,
    })
  ),
  on(activityActions.loadActivitiesFailure, (state, { error }) => ({
    ...state,
    loading: false,
    error: error
  })),
);

export const getSelectedIds = (state: State) => state.selectedIds;
export const getLoading = (state: State) => state.loading;
export const getCount = (state: State) => state.count;
