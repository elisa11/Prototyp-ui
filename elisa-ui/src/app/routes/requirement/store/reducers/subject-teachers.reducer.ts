import { createReducer, on } from '@ngrx/store';
import { createEntityAdapter, EntityAdapter, EntityState } from '@ngrx/entity';
import { subjectTeachersApiActions } from '../actions';
import { SubjectTeacherModel } from '../../model/subject-teacher.model';
import { isNotNullOrUndefined } from 'codelyzer/util/isNotNullOrUndefined';

export const userSubjectFeatureKey = 'userSubject';

export interface State extends EntityState<SubjectTeacherModel> {
	readonly selectedIds: number[];
	readonly count: number;
	readonly loading: boolean;
}

export const adapter: EntityAdapter<SubjectTeacherModel> =
	createEntityAdapter<SubjectTeacherModel>({
		selectId: (model) => model.user.id,
		sortComparer: false,
	});

export const initialState: State = adapter.getInitialState({
	selectedIds: [],
	count: 0,
	loading: false,
});

export const reducer = createReducer(
	initialState,
	on(subjectTeachersApiActions.loadUserSubjects, (state) => ({
		...state,
		loading: true,
	})),
	on(subjectTeachersApiActions.loadUserSubjectsByCourse, (state) => ({
		...state,
		loading: true,
	})),
	on(subjectTeachersApiActions.loadUserSubjectsFailure, (state) => ({
		...state,
		loading: false,
	})),
	on(subjectTeachersApiActions.resetUserSubjectsByCourse, (state) => ({
		...state,
		selectedIds: [],
	})),
	on(subjectTeachersApiActions.loadUserSubjectsSuccess, (state: State, { data }) => {
		const parsedData = data.results.map((d) => {
			return {
				user: d.user,
				roles: data.results.filter((fd) => fd.user.id === d.user.id).map((fd) => fd.role),
			} as SubjectTeacherModel;
		});
		const newData = [];
		parsedData.forEach((d) => {
			if (
				!isNotNullOrUndefined(
					newData.find((nd) => nd.user && d.user && nd.user.id === d.user.id)
				)
			) {
				newData.push(d);
			}
		});
		return adapter.upsertMany(newData, {
			...state,
			selectedIds: newData.map((d) => d.user.id),
			count: newData.length,
			loading: false,
		});
	})
);

export const getSelectedId = (state: State) => state.selectedIds;
export const getCount = (state: State) => state.count;
export const getLoading = (state: State) => state.loading;
