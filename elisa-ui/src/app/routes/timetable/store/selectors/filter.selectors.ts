import { createFeatureSelector, createSelector } from '@ngrx/store';
import * as fromFilter from '../reducers/filter.reducer';
import { Activity } from '@elisa11/elisa-api-angular-client';
import { selectTimetablesState } from './timetables.selectors';

export const selectFilterState = createSelector(
	selectTimetablesState,
	(s) => s[fromFilter.filterFeatureKey]
);

export const { selectIds, selectEntities } =
	fromFilter.adapter.getSelectors(selectFilterState);

export const selectFaculties = createSelector(selectFilterState, fromFilter.getFaculties);

export const selectFacultiesEmpty = createSelector(
	selectFaculties,
	(faculties) => faculties.length === 0
);

export const selectFilterOptions = createSelector(
	selectFilterState,
	(selectFilterState) => {
		return {
			faculties: selectFilterState.faculties,
			studyPlans: selectFilterState.studyPlans,
			years: selectFilterState.years,
			specializations: selectFilterState.specializations,
		};
	}
);

export const selectFilterOptionsLoading = createSelector(
	selectFilterState,
	fromFilter.isLoading
);

export const selectFilterOptionsEmpty = createSelector(
	//TODO: make better
	selectFaculties,
	(faculties) => faculties.length === 0
);
