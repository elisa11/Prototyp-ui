import { User, UserSubjectRole } from '@elisa11/elisa-api-angular-client';

export interface SubjectTeacherModel {
	readonly roles: UserSubjectRole[];
	readonly user: User;
}
