import * as departmentActions from './department.actions';
import * as userApiActions from './user-api.actions';
import * as departmentApiActions from './department-api.actions';
import * as userDepartmentApiActions from './user-department-api.actions';

export {
	departmentActions,
	departmentApiActions,
	userApiActions,
	userDepartmentApiActions,
};
