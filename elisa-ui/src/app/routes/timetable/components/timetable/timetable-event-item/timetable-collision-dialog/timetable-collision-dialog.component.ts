import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Collision, CollisionStatusEnum } from '@elisa11/elisa-api-angular-client';
import { Store } from '@ngrx/store';
import { updateCollision } from 'src/app/routes/timetable/store/actions/timetables-collisions.actions';

@Component({
	selector: 'app-timetable-collision-dialog',
	templateUrl: './timetable-collision-dialog.component.html',
	styleUrls: ['./timetable-collision-dialog.component.scss'],
})
export class TimetableCollisionDialogComponent implements OnInit {
	constructor(
		public dialogRef: MatDialogRef<TimetableCollisionDialogComponent>,
		@Inject(MAT_DIALOG_DATA) public data,
		private store: Store
	) {}

	ngOnInit(): void {}

	close() {
		this.dialogRef.close();
	}

	toggle(collision: Collision) {
		let newCollision = {
			id: collision.id,
		} as Collision;
		newCollision.status =
			collision.status === CollisionStatusEnum.Error
				? CollisionStatusEnum.Warning
				: CollisionStatusEnum.Error;

		this.store.dispatch(
			updateCollision({
				collision: newCollision,
			})
		);
	}
}
