import { createSelector } from '@ngrx/store';
import { selectUsersState } from './user.selectors';
import * as fromDepartment from '../reducers/department.reducer';

export const selectDepartmentEntitiesState = createSelector(
	selectUsersState,
	(state) => state[fromDepartment.departmentFeatureKey]
);

export const selectSelectedUserId = createSelector(
	selectDepartmentEntitiesState,
	fromDepartment.getSelectedUserId
);

export const {
	selectIds: selectDepartmentIds,
	selectEntities: selectDepartmentEntities,
	selectAll: selectAllDepartments,
	selectTotal: selectTotalDepartments,
} = fromDepartment.adapter.getSelectors(selectDepartmentEntitiesState);

export const selectDepartmentByUserId = createSelector(
	selectDepartmentEntities,
	selectSelectedUserId,
	(entities, id) => {
		return id && entities[id] ? entities[id].data : [];
	}
);
