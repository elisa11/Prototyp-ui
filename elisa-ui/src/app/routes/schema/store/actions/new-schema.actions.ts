import { createAction, props } from '@ngrx/store';
import { Version } from '@elisa11/elisa-api-angular-client';
import { NewVersionFormModel } from '@shared/model/new-version-form.model';

export const addNewSchema = createAction(
	'[Schema - NewSchema] Add NewSchema',
	props<{ data: NewVersionFormModel }>()
);
export const importSchema = createAction(
	'[Schema - NewSchema] Import Schema',
	props<{ version: Version }>()
);
export const importSchemaDone = createAction('[Schema - NewSchema] Import Schema Done');
export const importSchemaFailed = createAction(
	'[Schema - NewSchema] Import Schema Failed',
	props<{ status: number }>()
);

export const selectPeriod = createAction(
	'[Schema - NewSchema]  select Period',
	props<{ id: number }>()
);

export const selectParent = createAction(
	'[Schema - NewSchema]  select Parent',
	props<{ id: number }>()
);

export const importSchemaManual = createAction(
	'[Schema - NewSchema]  Import Schema Manual',
	props<{ version: Version }>()
);
export const importSchemaManualDone = createAction(
	'[Schema - NewSchema]  Import Schema Manual Done'
);
export const importSchemaManualFailed = createAction(
	'[Schema - NewSchema]  Import Schema Manual Failed',
	props<{ status: number }>()
);

export const deleteSchema = createAction(
	'[Schema - NewSchema]  Delete Schema',
	props<{ id: number }>()
);
export const deleteSchemaDone = createAction(
	'[Schema - NewSchema]  Delete Schema Done',
	props<{ id: number }>()
);
export const deleteSchemaFailed = createAction(
	'[Schema - NewSchema]  Delete Schema Failed',
	props<{ status: number }>()
);

export const updateSchema = createAction(
  '[Schema - UpdateSchema]  Update Schema',
  props<{ version: Version }>()
);
export const updateSchemaDone = createAction(
  '[Schema - UpdateSchema]  Update Schema Done',
);
export const updateSchemaFailed = createAction(
  '[Schema - UpdateSchema]  Update Schema Failed',
  props<{ status: number }>()
);
