import { createFeatureSelector, createSelector } from '@ngrx/store';
import {
	getLanguage,
	languageFeatureKey,
	LanguageState,
} from '@core/store/reducers/language.reducer';

export const selectLanguageState =
	createFeatureSelector<LanguageState>(languageFeatureKey);

export const selectLanguage = createSelector(selectLanguageState, getLanguage);
