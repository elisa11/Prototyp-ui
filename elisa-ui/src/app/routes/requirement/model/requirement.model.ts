import {
	ActivityCategory,
	CourseSerializerFull,
	Department,
	Group,
	Requirement,
	RequirementActivities,
	RequirementPart,
	Room,
	StateEnum,
	User,
} from '@elisa11/elisa-api-angular-client';
import { RequirementPartModel } from './requirement-part.model';
import { BasicUtils } from '@shared/utils/basic.utils';
import { CommentModel } from '@data/model/comment.model';
import { SubjectTeacherModel } from './subject-teacher.model';

export class RequirementModel {
	id: number;
	course: CourseSerializerFull;
	department: Department;
	state: StateEnum;
	groups_count: number;
	students_count: number;
	requirement_teachers: SubjectTeacherModel[]; // volat courses/id/teachers
	max_parallel_courses?: number;
	max_serial_courses: number;
	exercise_odd_week: boolean;
	exercise_after_lecture: boolean;
	created_by: User;
	created_at: string;
	updated_at: string;
	requirement_parts: RequirementPartModel[];
	requirement_comments: CommentModel[];
	readonly groups: Group[];
	students_count_extra: number;
	requirement_activities: Array<RequirementActivities>;

	constructor(
		requirement: Requirement,
		users: User[],
		categories: ActivityCategory[],
		teachers: SubjectTeacherModel[],
		rooms: Room[],
		groups?: Group[]
	) {
		Object.assign(this, requirement);
		const grouped = BasicUtils.groupBy<RequirementPart>(
			requirement.requirement_parts,
			'category'
		);
		console.log('requirement parts', grouped);
		this.requirement_comments = requirement.requirement_comments.map(
			(v) => new CommentModel(v, users)
		);
		this.requirement_parts = Object.keys(grouped).map(
			(part) => new RequirementPartModel(grouped[part], rooms, categories)
		);
		this.groups = groups;
		this.requirement_teachers = teachers;
		this.department = {
			...requirement.course.department,
			...requirement.department,
		} as Department;
	}
}
