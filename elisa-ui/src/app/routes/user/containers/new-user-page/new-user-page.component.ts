import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { UserModel } from '../../model/user.model';
import { UserActions, UserApiActions } from '../../store/actions';

@Component({
  template: `<app-user-form
    [userExistsData]="userExistsData$"
    (onSubmit)="handleSubmit($event)"
    (onPut)="handlePut($event)"
    (onCancel)="handleCancelled()"
    (onBack)="handleBack($event)">
  </app-user-form>
	`,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NewUserPageComponent implements OnInit {
  userExistsData$: UserModel | undefined = this.router.getCurrentNavigation().extras.state as UserModel;

  ngOnInit(): void {
  }
  constructor(private router: Router, private store: Store) {
  }

  handleCancelled() {
    this.store.dispatch(UserActions.cancelUser());
  }

  handleBack(path: string) {
    return this.router.navigate([path]);
  }

  handleSubmit(data:UserModel) {
    console.log("Submit form");
    this.store.dispatch(UserApiActions.postUser({ data }));
  }
  handlePut(data:UserModel){
    console.log("Put form");
    this.store.dispatch(UserApiActions.putUser({data}));
  }
}
