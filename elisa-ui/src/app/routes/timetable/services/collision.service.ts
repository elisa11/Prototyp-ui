import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
	providedIn: 'root',
})
export class CollisionService {
	private collisionSubject = new BehaviorSubject<boolean>(false);

	collision$ = this.collisionSubject.asObservable();

	updateCollision(collision: boolean) {
		this.collisionSubject.next(collision);
	}
}
