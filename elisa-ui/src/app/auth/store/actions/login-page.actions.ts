import { createAction, props } from '@ngrx/store';
import { Login } from '@elisa11/elisa-api-angular-client';

export const login = createAction(
	'[Auth - Login Page] Login',
	props<{ credentials: Login }>()
);
