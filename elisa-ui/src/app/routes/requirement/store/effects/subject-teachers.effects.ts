import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { requirementActions, subjectTeachersApiActions } from '../actions';
import { catchError, filter, map, switchMap, tap, withLatestFrom } from 'rxjs/operators';
import { requirementSelectors } from '../selectors';
import { Store } from '@ngrx/store';
import { SnackEffects } from '@core/generics/snack.effects';
import { MatSnackBar } from '@angular/material/snack-bar';
import { CoursesCacheService } from '../../../course/services/courses-cache.service';
import { of, zip } from 'rxjs';
import { marker } from '@biesbjerg/ngx-translate-extract-marker';
import { NGXLogger } from 'ngx-logger';
import { LocaleService } from '@core/service/locale/locale.service';
import { schemaSelectors } from '@core/store/selectors';
import { PaginatorFilter } from '@data/filter/paginator.filter';
import { resetCachedData } from '@core/store/actions/settings.actions';
import { PaginatedSubjectTeachersList } from '@elisa11/elisa-api-angular-client';
import { isNotNullOrUndefined } from 'codelyzer/util/isNotNullOrUndefined';

const tag = 'SubjectTeachersEffects';

@Injectable()
export class SubjectTeachersEffects extends SnackEffects {
	resetData$ = createEffect(
		() =>
			this.actions$.pipe(
				ofType(resetCachedData),
				tap(() => this.service.clearCache())
			),
		{ dispatch: false }
	);

	selectReq$ = createEffect(() =>
		this.actions$.pipe(
			ofType(requirementActions.selectRequirement),
			filter(({ id }) => isNotNullOrUndefined(id)),
			withLatestFrom(this.store.select(requirementSelectors.selectEntities)),
			map((value) => value[1][value[0].id]),
			map((data) =>
				subjectTeachersApiActions.loadUserSubjects({
					query: { offset: 0, limit: 5, id: data.course.id } as PaginatorFilter,
				})
			)
		)
	);
	loadUserSubjectsFailed$ = createEffect(
		() =>
			this.actions$.pipe(
				ofType(subjectTeachersApiActions.loadUserSubjectsFailure),
				tap((err) =>
					this.openSnackbar(
						this.localeService.translate(marker('requirement.user-subjects.error'))
					)
				)
			),
		{ dispatch: false }
	);

	constructor(
		private actions$: Actions,
		private store: Store,
		private logger: NGXLogger,
		private localeService: LocaleService,
		private service: CoursesCacheService,
		snack: MatSnackBar
	) {
		super(snack);
	}

	// todo pagination...
	private parallelFetch = ({ query, scheme }) => {
		const limit = 25;
		return this.service
			.listTeachers(query.id, scheme, {
				...query,
				offset: 0,
				limit,
			} as PaginatorFilter)
			.pipe(
				switchMap((value) => {
					const count = value.count;
					if (count > limit) {
						let req = Math.floor(count / limit) - 1;
						req = req + (count % limit > 0 ? 1 : 0);
						return zip(
							of(value),
							...[...Array(req).keys()].map((index) =>
								this.service.listTeachers(query.id, scheme, {
									...query,
									offset: (1 + index) * limit,
									limit,
								} as PaginatorFilter)
							)
						).pipe(
							map((data) => {
								return {
									count: data[0].count,
									results: data.flatMap((list) => list.results),
								} as PaginatedSubjectTeachersList;
							})
						);
					} else {
						return of(value);
					}
				}),
				map((data) => subjectTeachersApiActions.loadUserSubjectsSuccess({ data })),
				catchError((error) => {
					this.logger.error(tag, 'loadUserSubjects$', error);
					return of(subjectTeachersApiActions.loadUserSubjectsFailure({ error }));
				})
			);
	};

	selectCourse$ = createEffect(() =>
		this.actions$.pipe(
			ofType(subjectTeachersApiActions.loadUserSubjectsByCourse),
			filter(({ cid }) => isNotNullOrUndefined(cid)),
			withLatestFrom(this.store.select(schemaSelectors.selectScheme)),
			map((value) => {
				return { query: { id: value[0].cid } as PaginatorFilter, scheme: value[1].name };
			}),
			switchMap(this.parallelFetch),
			catchError((error) => {
				this.logger.error(tag, 'loadUserSubjects$', error);
				return of(subjectTeachersApiActions.loadUserSubjectsFailure({ error }));
			})
		)
	);
	loadUserSubjects$ = createEffect(() =>
		this.actions$.pipe(
			ofType(subjectTeachersApiActions.loadUserSubjects),
			withLatestFrom(this.store.select(schemaSelectors.selectScheme)),
			map((value) => {
				return { query: value[0].query, scheme: value[1].name };
			}),
			withLatestFrom(this.store.select(requirementSelectors.selectSelectedRequirement)),
			map((values) => {
				const data = values[0];
				if (data.query.id) {
					return data;
				} else {
					return { ...data, query: { ...data.query, id: values[1].course.id } };
				}
			}),
			switchMap(this.parallelFetch),
			catchError((error) => {
				this.logger.error(tag, 'loadUserSubjects$', error);
				return of(subjectTeachersApiActions.loadUserSubjectsFailure({ error }));
			})
		)
	);
}
