import { createAction, props } from '@ngrx/store';
import { PaginatedUserList, User } from '@elisa11/elisa-api-angular-client';
import { PaginatorFilter } from '@data/filter/paginator.filter';
import { UserModel } from '../../model/user.model';

export const searchUsers = createAction(
	'[[User - User/Api]] Search Users',
	props<{ query: PaginatorFilter }>()
);

export const searchUsersSuccess = createAction(
	'[[User - User/Api]] Search Users Success',
	props<{ users: PaginatedUserList }>()
);

export const searchUsersFailure = createAction(
	'[[User - User/Api]] Search Users Failure',
	props<{ errorMsg: string }>()
);

export const postUser = createAction(
  '[User - User/Api Post User',
  props<{ data: UserModel }>()
);

export const postUserSuccess = createAction(
  '[User - User/Api Create (Post) User Success',
  props<{ data: User }>()
);

export const postUserFailure = createAction(
  '[User - User/Api Create (Post) User Failure',
  props<{ error: any }>()
);

export const putUser = createAction(
  '[User - User/Api Put User',
  props<{ data: UserModel }>()
);

export const putUserSuccess = createAction(
  '[User - User/Api Put User Success',
  props<{ data: User }>()
);

export const putUserFailure = createAction(
  '[User - User/Api Put User Failure',
  props<{ error: any }>()
);

export const deleteUser = createAction(
  '[User - User/Api] Delete User',
  props<{ id: number }>()
);

export const deleteUserDone = createAction(
  '[User - User/Api] Delete User Done',
  props<{ id: number }>()
);

export const deleteUserFailed = createAction(
  '[User - User/Api] Delete User Failed',
  props<{ error: any }>()
);

export const setRoleUser = createAction(
  '[User - User/Api] Set Role User',
  props<{ id: number, role: string}>()
);

export const setRoleUserDone = createAction(
  '[User - User/Api] Set Role User Done',
);

export const setRoleUserFailed = createAction(
  '[User - User/Api] Set Role User Failed',
  props<{ error: any }>()
);

export const removeRoleUser = createAction(
  '[User - User/Api] Remove Role User',
  props<{ id: number, role: string}>()
);

export const removeRoleUserDone = createAction(
  '[User - User/Api] Remove Role User Done',
);

export const removeRoleUserFailed = createAction(
  '[User - User/Api] Remove Role User Failed',
  props<{ error: any }>()
);

