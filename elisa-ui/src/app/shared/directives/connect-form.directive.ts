import { Directive, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { FormGroupDirective } from '@angular/forms';
import { Store } from '@ngrx/store';
import {
	submitFormError,
	submitFormSuccess,
	updateForm,
} from '@shared/store/actions/form.actions';
import { Subscription } from 'rxjs';
import { Actions, ofType } from '@ngrx/effects';
import { debounceTime, filter, take } from 'rxjs/operators';
import { selectFormState } from '@shared/store/selectors/form.selectors';
import { Paths } from '@shared/store/reducers/form.reducer';
import { NGXLogger } from 'ngx-logger';

@Directive({
	selector: '[appConnectForm]',
})
export class ConnectFormDirective implements OnInit, OnDestroy {
	@Input() formPath: Paths;
	@Input() debounce = 300;
	@Output() error = new EventEmitter();
	@Output() success = new EventEmitter();
	formChange: Subscription;
	formSuccess: Subscription;
	formError: Subscription;

	constructor(
		private formGroupDirective: FormGroupDirective,
		private actions$: Actions,
		private store: Store,
		private logger: NGXLogger
	) {}

	ngOnInit() {
		// Update the form value based on the state
		this.store
			.select(selectFormState)
			.pipe(take(1))
			.subscribe((formValue) => {
				this.logger.debug(
					'ConnectFormDirective',
					this.formPath,
					formValue[this.formPath]
				);
				this.formGroupDirective.form.patchValue(formValue[this.formPath]);
			});

		this.formChange = this.formGroupDirective.form.valueChanges
			.pipe(debounceTime(this.debounce))
			.subscribe((value) => {
				this.store.dispatch(updateForm({ path: this.formPath, value }));
			});

		this.formSuccess = this.actions$
			.pipe(
				ofType(submitFormSuccess),
				filter(({ path }) => path === this.formPath)
			)
			.subscribe(() => {
				this.formGroupDirective.form.reset();
				this.success.emit();
			});

		this.formError = this.actions$
			.pipe(
				ofType(submitFormError),
				filter(({ path }) => path === this.formPath)
			)
			.subscribe(({ error }) => this.error.emit(error));
	}

	ngOnDestroy(): void {
		this.formChange.unsubscribe();
		this.formError.unsubscribe();
		this.formSuccess.unsubscribe();
	}
}
