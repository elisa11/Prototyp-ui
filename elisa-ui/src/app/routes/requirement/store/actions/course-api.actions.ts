import { createAction, props } from '@ngrx/store';
import { PaginatedCourseSerializerFullList } from '@elisa11/elisa-api-angular-client';

export const loadCourses = createAction(
	'[Requirement - Course/Api] Load Courses',
	props<{ search: string }>()
);

export const loadCoursesSuccess = createAction(
	'[Requirement - Course/Api] Load Courses Success',
	props<{ data: PaginatedCourseSerializerFullList }>()
);

export const loadCoursesFailure = createAction(
	'[Requirement - Course/Api] Load Courses Failure',
	props<{ error: any }>()
);
