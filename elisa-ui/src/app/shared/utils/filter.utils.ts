import { PaginatorFilter } from '@data/filter/paginator.filter';
import { BasicUtils } from '@shared/utils/basic.utils';

export const FilterUtils = {
	loadFilterFromSession(moduleId: string): PaginatorFilter {
		const storedFilter = JSON.parse(
			localStorage.getItem(moduleId + '.filter')
		) as PaginatorFilter;
		if (storedFilter) {
			BasicUtils.replaceNullsWithUndefined(storedFilter);
		}
		return storedFilter;
	},
	storeCurrentFilterToSession(storedFilter: PaginatorFilter, moduleId: string) {
		localStorage.setItem(moduleId + '.filter', JSON.stringify(storedFilter));
	},
};
