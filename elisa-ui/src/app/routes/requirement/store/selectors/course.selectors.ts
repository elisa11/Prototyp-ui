import { createSelector } from '@ngrx/store';
import * as fromCourse from '../reducers/course.reducer';
import { CourseSerializerFull } from '@elisa11/elisa-api-angular-client';
import { selectRequirementsState } from './requirement.selectors';
import { DepartmentCourse } from '@data/model/department-course.model';

export const selectCourseState = createSelector(
	selectRequirementsState,
	(s) => s[fromCourse.courseFeatureKey]
);

export const selectSearchIds = createSelector(
	selectCourseState,
	fromCourse.getSelectedIds
);
export const selectTotalCount = createSelector(selectCourseState, fromCourse.getCount);
export const selectSearchLoading = createSelector(
	selectCourseState,
	fromCourse.getLoading
);

export const { selectIds, selectEntities } =
	fromCourse.adapter.getSelectors(selectCourseState);

export const selectSearchResults = createSelector(
	selectEntities,
	selectSearchIds,
	(data, searchIds) => {
		return searchIds
			.map((id) => data[id])
			.filter((d): d is CourseSerializerFull => d != null);
	}
);

export const selectDepartments = createSelector(selectSearchResults, (data) => {
	return [...new Set(data.map((c) => new DepartmentCourse(c.department)))];
});

export const selectGroupedData = createSelector(
	selectDepartments,
	selectSearchResults,
	(data, courses) => {
		return data.map((d) => {
			return {
				...d,
				courses: courses.filter((c) => c.department.id === d.id),
			} as DepartmentCourse;
		});
	}
);
