import { createFeatureSelector, createSelector } from '@ngrx/store';
import { roomsFeatureKey, RoomsState } from '../reducers';
import * as fromRoom from '../reducers/room.reducer';
import { Room } from '@elisa11/elisa-api-angular-client';

export const selectRoomsState = createFeatureSelector<RoomsState>(roomsFeatureKey);

export const selectEntityState = createSelector(
	selectRoomsState,
	(s) => s[fromRoom.roomFeatureKey]
);

export const selectSelectedId = createSelector(selectEntityState, fromRoom.getSelectId);

export const selectTotalCount = createSelector(selectEntityState, fromRoom.getCount);

export const { selectIds: selectIds, selectEntities: selectEntities } =
	fromRoom.adapter.getSelectors(selectEntityState);

export const selectSelectedRoom = createSelector(
	selectEntities,
	selectSelectedId,
	(entities, selectedId) => {
		return selectedId && entities ? entities[selectedId] : ({} as Room);
	}
);
