import { createSelector } from '@ngrx/store';
import {State } from '../../../user/store/reducers/search.reducer';
import { selectSchemaEntities, selectSchemasState } from './schema.selectors';
import { searchSchemaFeatureKey } from '../reducers/search-schema.reducer';
import { Version } from '@elisa11/elisa-api-angular-client';

export const selectSearchState = createSelector(
	selectSchemasState,
	(state) => state[searchSchemaFeatureKey]
);

export const selectSearchSchemaIds = createSelector(selectSearchState,(state) => {return state.ids});
export const selectSearchQuery = createSelector(selectSearchState, (state) => {return state.query});
export const selectSearchLoading = createSelector(selectSearchState, (state) => {return state.loading});
export const selectSearchError = createSelector(selectSearchState, (state) => {return state.error});

export const selectSearchResults = createSelector(
	selectSchemaEntities,
	selectSearchSchemaIds,
	(versions, searchIds) => {
		return searchIds
			.map((id) => versions[id])
			.filter((schema): schema is Version => schema != null);
	}
);
