import { Pipe, PipeTransform } from '@angular/core';
import { Group } from '@elisa11/elisa-api-angular-client';

@Pipe({
	name: 'viewGroup',
})
export class ViewGroupPipe implements PipeTransform {
	transform(value: Group): string {
		return value.abbr ? `${value.name} ( ${value.abbr} )` : value.name;
	}
}
