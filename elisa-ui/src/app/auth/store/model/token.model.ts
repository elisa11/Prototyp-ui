export interface TokenPayload {
	token_type: string;
	exp: number;
	jti: string;
	user_id: number;
	roles: number[];
	name: string;
}
