import { createReducer, on } from '@ngrx/store';
import { createEntityAdapter, EntityAdapter, EntityState } from '@ngrx/entity';
import { RoomCategory } from '@elisa11/elisa-api-angular-client';
import { typeApiActions } from '../actions';

export const roomTypeFeatureKey = 'roomType';

export interface State extends EntityState<RoomCategory> {
	readonly selectedIds: number[];
	readonly selectedId: number;
}
// todo: nahradit so shared
export const adapter: EntityAdapter<RoomCategory> = createEntityAdapter<RoomCategory>({
	selectId: (model: RoomCategory) => model.id,
	sortComparer: false,
});
export const initialState: State = adapter.getInitialState({
	selectedId: null,
	selectedIds: [],
});

export const reducer = createReducer(
	initialState,
	on(typeApiActions.loadRoomTypeSuccess, (state: State, { data }) =>
		adapter.upsertMany(data, {
			...state,
			selectedIds: data.map((d) => d.id),
		})
	),
	on(typeApiActions.selectRoomType, (state: State, { id }) => ({
		...state,
		selectedId: id,
	}))
);

export const getSelectedIds = (state: State) => state.selectedIds;
export const getSelectedId = (state: State) => state.selectedId;
