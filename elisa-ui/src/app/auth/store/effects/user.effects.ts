import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { UserCacheService } from '@core/service/cache/user-cache.service';
import { authActions, authApiActions, userActions } from '../actions';
import { auditTime, catchError, map, switchMap } from 'rxjs/operators';
import { of } from 'rxjs';
import { NGXLogger } from 'ngx-logger';

const tag = 'UserEffects';

@Injectable()
export class UserEffects {
	authInitSuccess$ = createEffect(() =>
		this.actions$.pipe(
			ofType(authActions.authInitSuccess),
			auditTime(1000),
			map((action) => userActions.loadLoggedUser({ uid: action.payload.user_id }))
		)
	);
	loginSuccess$ = createEffect(() =>
		this.actions$.pipe(
			ofType(authApiActions.loginSuccess),
			auditTime(1000),
			map((action) => userActions.loadLoggedUser({ uid: action.payload.user_id }))
		)
	);
	loadLoggedUser$ = createEffect(() =>
		this.actions$.pipe(
			ofType(userActions.loadLoggedUser),
			switchMap(({ uid }) =>
				this.service.getUser(uid).pipe(
					map((data) => userActions.loadLoggedUserSuccess({ data })),
					catchError((error) => {
						this.logger.warn(tag, 'loadLoggedUser$', error);
						return of(userActions.loadLoggedUserFailure({ uid, error }));
					})
				)
			)
		)
	);

	constructor(
		private actions$: Actions,
		private service: UserCacheService,
		private logger: NGXLogger
	) {}
}
