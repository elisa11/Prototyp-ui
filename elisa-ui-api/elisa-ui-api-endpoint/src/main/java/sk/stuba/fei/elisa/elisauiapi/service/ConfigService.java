package sk.stuba.fei.elisa.elisauiapi.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sk.stuba.fei.elisa.elisauiapi.config.PublicConfig;
import sk.stuba.fei.elisa.elisauiapi.model.Config;

import java.util.HashMap;
import java.util.Map;

@Service
public class ConfigService {

    private final PublicConfig uiProperties;

    public ConfigService(@Autowired PublicConfig uiProperties) {
        this.uiProperties = uiProperties;
    }

    public Config getConfig() {
        Config config = new Config();

        Map<String, Object> properties = new HashMap<>();
        properties.put("service", uiProperties.getService());
        properties.put("param", uiProperties.getParam());
        properties.put("log", uiProperties.getLog());

        config.setProperties(properties);

        return config;
    }
}
