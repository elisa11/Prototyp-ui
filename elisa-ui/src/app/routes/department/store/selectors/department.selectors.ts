import { createFeatureSelector, createSelector } from '@ngrx/store';
import { departmentsFeatureKey, DepartmentsState } from '../reducers';
import * as fromDepartment from '../reducers/department.reducer';

export const selectDepartmentState =
	createFeatureSelector<DepartmentsState>(departmentsFeatureKey);

export const selectEntitiesState = createSelector(
	selectDepartmentState,
	(state) => state[fromDepartment.departmentFeatureKey]
);

export const selectSelectedId = createSelector(
	selectEntitiesState,
	fromDepartment.getSelectId
);

export const selectTotalCount = createSelector(
	selectEntitiesState,
	fromDepartment.getCount
);

export const { selectIds: selectIds, selectEntities: selectEntities } =
	fromDepartment.adapter.getSelectors(selectEntitiesState);

export const selectSelectedDepartment = createSelector(
	selectEntities,
	selectSelectedId,
	(entities, selectedId) => {
		return selectedId && entities[selectedId];
	}
);
