import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { SnackEffects } from '@core/generics/snack.effects';
import { MatSnackBar } from '@angular/material/snack-bar';
import { resetCachedData } from '@core/store/actions/settings.actions';
import { catchError, filter, map, switchMap, tap, withLatestFrom } from 'rxjs/operators';
import { RequirementsCacheService } from '@core/service/cache/requirements-cache.service';
import { commentsApiActions } from '../actions';
import { Store } from '@ngrx/store';
import { requirementSelectors } from '../selectors';
import { schemaSelectors } from '@core/store/selectors';
import { of } from 'rxjs';
import { formActions } from '@shared/store/actions';
import { Paths } from '@shared/store/reducers/form.reducer';
import { marker } from '@biesbjerg/ngx-translate-extract-marker';
import { LocaleService } from '@core/service/locale/locale.service';
import { CommentSerializerPost } from '@elisa11/elisa-api-angular-client';

@Injectable()
export class CommentsEffects extends SnackEffects {
	resetData$ = createEffect(
		() =>
			this.actions$.pipe(
				ofType(resetCachedData),
				tap(() => this.service.clearCache())
			),
		{ dispatch: false }
	);

	addComment$ = createEffect(() =>
		this.actions$.pipe(
			ofType(commentsApiActions.addComment),
			withLatestFrom(this.store.select(requirementSelectors.selectSelectedId)),
			map((value) => {
				return { rid: value[1], comment: value[0].data };
			}),
			withLatestFrom(this.store.select(schemaSelectors.selectScheme)),
			map((value) => {
				return { ...value[0], schema: value[1].name };
			}),
			switchMap((data) => {
				return this.service
					.postComment(data.schema, data.rid, {
						...data.comment,
					} as CommentSerializerPost)
					.pipe(
						map((value) => formActions.submitFormSuccess({ path: Paths.comment })),
						catchError((err) =>
							of(commentsApiActions.addCommentFailure({ error: err.message }))
						)
					);
			})
		)
	);
	addCommentSuccess$ = createEffect(() =>
		this.actions$.pipe(
			ofType(formActions.submitFormSuccess),
			filter((value) => value.path === Paths.comment),
			withLatestFrom(this.store.select(requirementSelectors.selectSelectedId)),
			map((data) => commentsApiActions.loadComments({ rid: data[1] }))
		)
	);
	addCommentFailed$ = createEffect(() =>
		this.actions$.pipe(
			ofType(commentsApiActions.addCommentFailure),
			tap((err) =>
				this.openSnackbar(this.localeService.translate(marker('comment.add.error')))
			),
			map((error) => formActions.submitFormError({ path: Paths.comment, error }))
		)
	);
	// todo: tento effect vyuzit aj pri notifikacii cez websocket B-)
	loadComments$ = createEffect(() =>
		this.actions$.pipe(
			ofType(commentsApiActions.loadComments),
			withLatestFrom(this.store.select(schemaSelectors.selectScheme)),
			map((data) => {
				return { rid: data[0].rid, scheme: data[1].name };
			}),
			switchMap((data) => {
				return this.service.getComments(data.scheme, data.rid).pipe(
					map((value) =>
						commentsApiActions.loadCommentsSuccess({
							data: value,
							rid: data.rid,
						})
					),
					catchError((err) =>
						of(commentsApiActions.loadCommentsFailure({ error: err.message }))
					)
				);
			})
		)
	);
	loadCommentsFailed$ = createEffect(
		() =>
			this.actions$.pipe(
				ofType(commentsApiActions.loadCommentsFailure),
				tap((err) =>
					this.openSnackbar(this.localeService.translate(marker('comment.load.error')))
				)
			),
		{ dispatch: false }
	);

	deleteComments$ = createEffect(() =>
		this.actions$.pipe(
			ofType(commentsApiActions.deleteComment),
			withLatestFrom(this.store.select(schemaSelectors.selectScheme)),
			map((data) => {
				return { cid: data[0].cid, scheme: data[1].name };
			}),
			withLatestFrom(this.store.select(requirementSelectors.selectSelectedId)),
			map((value) => {
				return { ...value[0], rid: value[1] };
			}),
			switchMap((data) => {
				return this.service.deleteComment(data.scheme, data.rid, data.cid).pipe(
					map((value) =>
						commentsApiActions.loadComments({
							rid: data.rid,
						})
					),
					catchError((err) =>
						of(commentsApiActions.deleteCommentFailure({ error: err.message }))
					)
				);
			})
		)
	);
	deleteCommentsFailed$ = createEffect(
		() =>
			this.actions$.pipe(
				ofType(commentsApiActions.deleteCommentFailure),
				tap((err) =>
					this.openSnackbar(this.localeService.translate(marker('comment.delete.error')))
				)
			),
		{ dispatch: false }
	);

	editComments$ = createEffect(() =>
		this.actions$.pipe(
			ofType(commentsApiActions.editComment),
			withLatestFrom(this.store.select(schemaSelectors.selectScheme)),
			map((data) => {
				return { comment: data[0].data, scheme: data[1].name };
			}),
			withLatestFrom(this.store.select(requirementSelectors.selectSelectedId)),
			map((value) => {
				return { ...value[0], rid: value[1] };
			}),
			switchMap((data) => {
				return this.service
					.updateComment(data.scheme, data.rid, data.comment.id, data.comment)
					.pipe(
						map(() =>
							commentsApiActions.loadComments({
								rid: data.rid,
							})
						),
						catchError((err) =>
							of(commentsApiActions.editCommentFailure({ error: err.message }))
						)
					);
			})
		)
	);
	editCommentsFailed$ = createEffect(
		() =>
			this.actions$.pipe(
				ofType(commentsApiActions.editCommentFailure),
				tap((err) =>
					this.openSnackbar(this.localeService.translate(marker('comment.edit.error')))
				)
			),
		{ dispatch: false }
	);

	constructor(
		private actions$: Actions,
		private service: RequirementsCacheService,
		private store: Store,
		private localeService: LocaleService,
		snack: MatSnackBar
	) {
		super(snack);
	}
}
