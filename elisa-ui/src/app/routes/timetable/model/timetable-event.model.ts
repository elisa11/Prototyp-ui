import { Activity } from '@elisa11/elisa-api-angular-client';
import { DayEnum } from '@elisa11/elisa-api-angular-client';
import { Event, Group, Room, User, Department } from '@elisa11/elisa-api-angular-client';

export class TimetableEventModel {
	readonly id: number;
	readonly activity: Activity;
	day?: DayEnum;
	time?: string | null;
	readonly created_at: string;
	readonly updated_at: string;
	timetable: number;
	rooms: Room[];
	groups: Group[];
	teachers: User[];
	departments: Department[];

	constructor(
		event: Event,
		activity: Activity,
		rooms: Room[],
		groups: Group[],
		teachers: User[],
		departments: Department[]
	) {
		Object.assign(this, event);
		this.activity = activity;
		this.rooms = rooms;
		this.groups = groups;
		this.teachers = teachers;
		this.departments = departments;
	}
}
