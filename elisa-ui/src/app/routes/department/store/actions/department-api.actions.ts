import { createAction, props } from '@ngrx/store';
import { PaginatedDepartmentList } from '@elisa11/elisa-api-angular-client';
import { PaginatorFilter } from '@data/filter/paginator.filter';

export const searchDepartment = createAction(
	'[Department - Department/Api] Search Departments',
	props<{ query: PaginatorFilter }>()
);

export const searchDepartmentSuccess = createAction(
	'[Department - Department/Api] Search Departments Success',
	props<{ data: PaginatedDepartmentList }>()
);

export const searchDepartmentFailure = createAction(
	'[Department - Department/Api] Search Departments Failure',
	props<{ error: string }>()
);

export const getAllDepartments = createAction(
	'[Department - Department/Api] Get All Departments'
);
export const getAllDepartmentsSuccess = createAction(
	'[Department - Department/Api] Get All Departments Success',
	props<{ data: PaginatedDepartmentList }>()
);
export const getAllDepartmentsFailure = createAction(
	'[Department - Department/Api] Get All Departments Failure',
	props<{ error: string }>()
);
