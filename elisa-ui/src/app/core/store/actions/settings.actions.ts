import { createAction } from '@ngrx/store';

export const resetCachedData = createAction(
	'[Core - Settings] Reset Cached Data In Services'
);
