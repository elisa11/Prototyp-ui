import { createFeatureSelector, createSelector } from '@ngrx/store';
import * as fromAuth from '../reducers/auth.reducer';
import * as fromLoginPage from '../reducers/login-page.reducer';
import { authFeatureKey, AuthState } from '../reducers';

export const selectAuthState = createFeatureSelector<AuthState>(authFeatureKey);

export const selectAuthStatusState = createSelector(
	selectAuthState,
	(state) => state.status
);

export const selectPayload = createSelector(selectAuthStatusState, fromAuth.getPayload);
export const selectLoggedUser = createSelector(
	selectAuthStatusState,
	fromAuth.getLoggedUser
);

export const selectToken = createSelector(selectAuthStatusState, fromAuth.getToken);

export const selectIsValid = createSelector(selectAuthStatusState, fromAuth.isValid);

export const selectLoggedIn = createSelector(
	selectToken,
	selectIsValid,
	(payload, valid) => !!payload && payload.length > 0 && valid
);

export const selectLoginPageState = createSelector(
	selectAuthState,
	(state) => state.loginPage
);

export const selectLoginPageError = createSelector(
	selectLoginPageState,
	fromLoginPage.getError
);

export const selectLoginPagePending = createSelector(
	selectLoginPageState,
	fromLoginPage.getPending
);
