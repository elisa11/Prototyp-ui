import { ChangeDetectionStrategy, Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { combineLatest, Observable } from 'rxjs';
import { Department } from '@elisa11/elisa-api-angular-client';
import { UserDepartmentModel } from '@data/model/user-department.model';
import {
	departmentSelectors,
	userDepartmentSelectors,
	userSelectors,
} from '../../store/selectors';
import { map, take } from 'rxjs/operators';
import { PaginatorFilter } from '@data/filter/paginator.filter';
import { userDepartmentApiActions } from '../../store/actions';
import { AbstractSelectedPageComponent } from '@core/generics/abstract-selected-page.component';

@Component({
	selector: 'app-selected-department-page',
	template: `
		<app-department-detail
			[data]="data$ | async"
			[pagination]="pagination"
			[userData]="userData$ | async"
			[userLoading]="userLoading$ | async"
			[usersLength]="userLength$ | async"
			(onBack)="back($event)"
			(onUserFilterChange)="handleUserFilterChange($event)"
		></app-department-detail>
	`,
	changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SelectedDepartmentPageComponent extends AbstractSelectedPageComponent<Department> {
	userLoading$: Observable<boolean>;
	userLength$: Observable<number>;
	userData$: Observable<UserDepartmentModel[]>;

	constructor(private store: Store) {
		super();
		this.userLoading$ = store.select(userDepartmentSelectors.selectLoading);
		this.userLength$ = store.select(userDepartmentSelectors.selectTotalCount);
		this.data$ = store.select(departmentSelectors.selectSelectedDepartment);
		this.userData$ = combineLatest([
			store.select(userDepartmentSelectors.selectUserDepartmentByDepartmentId),
			store.select(userSelectors.selectUsersByDepartmentId),
		]).pipe(
			map((data) =>
				data[0].map(
					(ud) =>
						new UserDepartmentModel(
							data[1].find((u) => ud.user === u.id),
							ud
						)
				)
			)
		);
	}

	handleUserFilterChange(filter: PaginatorFilter) {
		this.store
			.select(departmentSelectors.selectSelectedId)
			.pipe(take(1))
			.subscribe((id) =>
				this.store.dispatch(
					userDepartmentApiActions.loadUserDepartments({ query: { ...filter, id } })
				)
			);
	}
}
