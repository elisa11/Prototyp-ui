import { Pipe, PipeTransform } from '@angular/core';
import { CourseSerializerFull } from '@elisa11/elisa-api-angular-client';

@Pipe({
	name: 'courseName',
})
export class CourseNamePipe implements PipeTransform {
	transform(value: CourseSerializerFull): string {
		if (value) {
			if (value.code) {
				return `${value.name} ( ${value.code} )`;
			}
			return value.name;
		} else return '';
	}
}
