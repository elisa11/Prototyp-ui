import { Component, EventEmitter, Output } from '@angular/core';
import { CourseSerializerFull } from '@elisa11/elisa-api-angular-client';
import { AbstractTableComponent } from '@core/generics/abstract-table.component';
import { ConfirmDialogComponent } from '../../../../dialogs/confirm-dialog/confirm-dialog.component';
import { ConfirmDialogModel } from '../../../../dialogs/confirm-dialog/confirm-dialog.model';
import { Store } from '@ngrx/store';
import { MatDialog } from '@angular/material/dialog';
import { CourseModel } from '../../model/course.model';
import { deleteCourse } from '../../store/actions/course-api.actions';

@Component({
	selector: 'app-course-list',
	templateUrl: './course-list.component.html',
})
export class CourseListComponent extends AbstractTableComponent<CourseSerializerFull> {
	displayedColumns = ['code', 'name', 'department', 'completion','btn'];
  @Output()
  private onNewCourse = new EventEmitter();
	constructor(private store: Store,
              private dialog: MatDialog) {
		super();
	}

  newCourse() {
    console.log('New Course click')
    this.onNewCourse.emit();
  }

  editCourse($event: MouseEvent, row: CourseModel) {
    this.onNewCourse.emit(row);
    $event.stopImmediatePropagation();
  }

  deleteItem($event: MouseEvent, row: any) {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      data: {
        title: "Vymazanie kurzu",
        // title: marker('dialog.course.title'),
        question: "Chcete skutočne vymazať kurz?",
        // question: marker('dialog.course.question'),
        noLabel: "Nie",
        // noLabel: marker('dialog.course.no-label'),
        yesLabel: "Áno",
        // yesLabel: marker('dialog.course.yes-label'),
      } as ConfirmDialogModel,
      disableClose: true,
    });
    dialogRef.afterClosed().subscribe(dialogRes =>{
      if (dialogRes){
        this.store.dispatch(deleteCourse({id:row.id}));
      }
    })
    $event.stopImmediatePropagation();
  }
}
