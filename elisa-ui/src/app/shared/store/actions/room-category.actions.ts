import { createAction, props } from '@ngrx/store';
import { PaginatedRoomCategoryList } from '@elisa11/elisa-api-angular-client';

export const loadRoomCategories = createAction(
	'[Enum - RoomCategory] Load RoomCategories'
);

export const loadRoomCategoriesSuccess = createAction(
	'[Enum - RoomCategory] Load RoomCategories Success',
	props<{ data: PaginatedRoomCategoryList }>()
);

export const loadRoomCategoriesFailure = createAction(
	'[Enum - RoomCategory] Load RoomCategories Failure',
	props<{ error: any }>()
);
