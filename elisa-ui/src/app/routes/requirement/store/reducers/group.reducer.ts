import { createReducer, on } from '@ngrx/store';
import { createEntityAdapter, EntityAdapter, EntityState } from '@ngrx/entity';
import { Group } from '@elisa11/elisa-api-angular-client';
import { groupApiActions, searchGroupActions } from '../actions';

export const groupFeatureKey = 'group';

export interface State extends EntityState<Group> {
	readonly selectedIds: number[];
	readonly count: number;
	readonly loading: boolean;
}

export const adapter: EntityAdapter<Group> = createEntityAdapter<Group>({
	selectId: (model) => model.id,
	sortComparer: false,
});

export const initialState = adapter.getInitialState({
	selectedIds: [],
	count: 0,
	loading: false,
});

export const reducer = createReducer(
	initialState,
	on(groupApiActions.loadGroup, (state) => ({ ...state, loading: true })),
	on(groupApiActions.loadGroupBySP, (state) => ({ ...state, loading: true })),
	on(groupApiActions.resetGroupBySP, (state) => ({ ...state, selectedIds: [] })),
	on(groupApiActions.loadGroupSuccess, (state, { data }) =>
		adapter.upsertMany(data.results, {
			...state,
			selectedIds: data.results.map((course) => course.id),
			count: data.count,
			loading: false,
		})
	),
	on(searchGroupActions.searchGroupsSuccess, (state, { data }) =>
		adapter.upsertMany(data.results, state)
	),
	on(groupApiActions.loadGroupFailure, (state) => ({
		...state,
		loading: false,
	}))
);

export const getSelectedIds = (state: State) => state.selectedIds;
export const getLoading = (state: State) => state.loading;
export const getCount = (state: State) => state.count;
