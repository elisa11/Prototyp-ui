export abstract class AbstractCacheService {
	abstract clearCache(): void;
}
