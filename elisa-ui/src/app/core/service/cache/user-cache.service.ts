import { Injectable } from '@angular/core';
import { AbstractCacheService } from './abstract-cache.service';
import { PaginatedUserList, Role, RoleEnum, User, UsersService } from '@elisa11/elisa-api-angular-client';
import { forkJoin, Observable, of } from 'rxjs';
import { map, shareReplay, switchMap } from 'rxjs/operators';
import { PaginatorFilter } from '@data/filter/paginator.filter';
import { BasicUtils } from '@shared/utils/basic.utils';
import { UserDepartmentsCacheService } from '@core/service/cache/user-departments-cache.service';
import { UserModel } from '../../../routes/user/model/user.model';
import { addBodyClass } from '@angular/cdk/schematics';

@Injectable({
	providedIn: 'root',
})
export class UserCacheService extends AbstractCacheService {
	private getUserCache = new Map();
	private listUsersCache = new Map();
	private getUsersByDepartmentCache = new Map();

	constructor(
		private service: UsersService,
		private userDepartmentService: UserDepartmentsCacheService
	) {
		super();
	}

	clearCache(): void {
		this.getUserCache.clear();
		this.listUsersCache.clear();
		this.getUsersByDepartmentCache.clear();
	}

	getUser(id: number): Observable<User> {
		const cached = this.getUserCache.get(id);
		if (cached) {
			return cached;
		}

		const response = this.service
			.usersRetrieve(id)
			.pipe(shareReplay({ bufferSize: 1, refCount: true }));
		this.getUserCache.set(id, response);
		return response;
	}

	listUsers(filter: PaginatorFilter): Observable<PaginatedUserList> {
		const cached = this.listUsersCache.get(filter);
		if (cached) {
			return cached;
		}

		const response = this.service
			.usersList(
				null,
				filter.limit,
				filter.offset,
				BasicUtils.getSortBy(filter),
				filter.search
			)
			.pipe(shareReplay({ bufferSize: 1, refCount: true }));
		this.listUsersCache.set(filter, response);
		return response;
	}

	getUsersByDepartment(
		departmentId: number,
		filter: PaginatorFilter
	): Observable<PaginatedUserList> {
		const cached = this.getUsersByDepartmentCache.get({ departmentId, filter });
		if (cached) {
			return cached;
		}

		const response = this.userDepartmentService
			.getUserDepartments(undefined, departmentId, filter)
			.pipe(
				map((data) => {
					return { total: data.count, ids: data.results.map((d) => d.user) };
				}),
				switchMap((list) =>
					forkJoin([
						of(list.total),
						...list.ids.map((id) => this.service.usersRetrieve(id)),
					])
				),
				map((value) => {
					return { count: value[0], results: value.slice(1) } as PaginatedUserList;
				})
			)
			.pipe(shareReplay({ bufferSize: 1, refCount: true }));
		this.getUsersByDepartmentCache.set({ departmentId, filter }, response);
		return response;
	}

	setUserForCreateOrPatch(model:UserModel){
	  return{
      id: model.id,
      username:model.username,
      first_name: model.first_name,
      last_name: model.last_name,
      title_after: model.title_after,
      title_before: model.title_before,
      groups: model.groups,
    }
  }

	createUser(model: UserModel):Observable<any>{
	  return this.service.usersCreate(this.setUserForCreateOrPatch(model));
  }

  deleteUser(uid: number): Observable<any> {
    return this.service.usersDestroy(uid);
  }

  putUser(model: UserModel): Observable<any>{
    return this.service.usersUpdate(model.id,this.setUserForCreateOrPatch(model))
  }

  setRoleForUser(uid: number, roleUser: string): Observable<any>{
    let userRole = {
      role: RoleEnum[roleUser]
    } as Role
	  return this.service.usersSetRoleCreate(uid, userRole);
  }
  removeRoleForUser(uid: number, roleUser: string): Observable<any>{
    let userRole = {
      role: RoleEnum[roleUser]
    } as Role
    return this.service.usersRemoveRoleCreate(uid, userRole);
  }
}
