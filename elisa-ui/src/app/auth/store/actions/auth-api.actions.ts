import { createAction, props } from '@ngrx/store';
import { TokenPayload } from '../model/token.model';

export const refreshToken = createAction('[Auth - Auth/API] Refresh Token');

export const loginSuccess = createAction(
	'[Auth - Auth/API] Login Success',
	props<{ payload: TokenPayload; token: string }>()
);

export const loginFailure = createAction(
	'[Auth - Auth/API] Login Failure',
	props<{ error: any }>()
);
