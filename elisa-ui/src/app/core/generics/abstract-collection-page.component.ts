import { Component } from '@angular/core';
import { CommonText } from '@data/common-text';
import { Observable } from 'rxjs';

@Component({ template: '' })
export abstract class AbstractCollectionPageComponent<T> {
	pagination = CommonText.pagination; // todo: settings selector
	loading$: Observable<boolean>;
	data$: Observable<T[]>;
	totalCount$: Observable<number>;
}
