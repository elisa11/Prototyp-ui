import { Injectable } from '@angular/core';
import {
	ActivatedRouteSnapshot,
	CanActivate,
	Router,
	RouterStateSnapshot,
	UrlTree,
} from '@angular/router';
import { Observable, of } from 'rxjs';
import { Store } from '@ngrx/store';
import { NGXLogger } from 'ngx-logger';
import {
	catchError,
	exhaustMap,
	filter,
	map,
	switchMap,
	take,
	tap,
} from 'rxjs/operators';
import { schemaSelectors } from '@core/store/selectors';
import { RoomsCacheService } from '../services/rooms-cache.service';
import { roomActions } from '../store/actions';
import { roomSelectors, searchRoomSelectors } from '../store/selectors';

const tag = 'RoomExistsGuard';

@Injectable({
	providedIn: 'root',
})
export class RoomExistsGuard implements CanActivate {
	constructor(
		private store: Store,
		private service: RoomsCacheService,
		private logger: NGXLogger,
		private router: Router
	) {}

	canActivate(
		route: ActivatedRouteSnapshot,
		state: RouterStateSnapshot
	): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
		return this.waitForCollectionToLoad().pipe(
			switchMap(() => this.hasRoom(route.params.id))
		);
	}

	private waitForCollectionToLoad(): Observable<boolean> {
		return this.store.select(searchRoomSelectors.selectSearchLoading).pipe(
			filter((loading) => !loading),
			take(1)
		);
	}

	private hasRoomInStore(id: number): Observable<boolean> {
		return this.store.select(roomSelectors.selectEntities).pipe(
			map((entities) => !!entities[id]),
			take(1)
		);
	}

	private hasRoomInApi(id: number): Observable<boolean> {
		return this.store.select(schemaSelectors.selectScheme).pipe(
			exhaustMap((version) =>
				this.service.getRoom(version.name, id).pipe(
					map((entity) => roomActions.loadRoom({ data: entity })),
					tap((action) => this.store.dispatch(action)),
					map((user) => !!user),
					catchError(() => {
						this.router.navigate(['/404']);
						return of(false);
					})
				)
			)
		);
	}

	private hasRoom(id: number): Observable<boolean> {
		return this.hasRoomInStore(id).pipe(
			switchMap((inStore) => {
				if (inStore) {
					this.logger.debug(tag, 'hasRoom', 'in store');
					return of(inStore);
				}
				this.logger.debug(tag, 'hasRoom try API');
				return this.hasRoomInApi(id);
			})
		);
	}
}
