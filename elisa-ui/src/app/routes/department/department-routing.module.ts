import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from '../../auth/services/auth.guard';
import { ViewDepartmentPageComponent } from './containers/view-department-page/view-department-page.component';
import { CollectionPageComponent } from './containers/collection-page/collection-page.component';
import { DepartmentExistsGuard } from './guards/department-exists.guard';

const routes: Routes = [
	{
		path: '',
		component: CollectionPageComponent,
		canActivate: [AuthGuard],
	},
	{
		path: ':id',
		component: ViewDepartmentPageComponent,
		canActivate: [AuthGuard, DepartmentExistsGuard],
	},
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule],
})
export class DepartmentRoutingModule {}
