import { createSelector } from '@ngrx/store';
import * as fromDepartment from '../reducers/user-department.reducer';
import { selectUsersState } from './user.selectors';

export const selectUserDepartmentEntitiesState = createSelector(
	selectUsersState,
	(state) => state[fromDepartment.userDepartmentFeatureKey]
);

export const selectSelectedUserId = createSelector(
	selectUserDepartmentEntitiesState,
	fromDepartment.getSelectedUserId
);

export const {
	selectIds: selectUserDepartmentIds,
	selectEntities: selectUserDepartmentEntities,
	selectAll: selectAllUserDepartments,
} = fromDepartment.adapter.getSelectors(selectUserDepartmentEntitiesState);

export const selectUserDepartmentByUserId = createSelector(
	selectUserDepartmentEntities,
	selectSelectedUserId,
	(entities, id) => {
		return id && entities[id] ? entities[id].data : [];
	}
);
