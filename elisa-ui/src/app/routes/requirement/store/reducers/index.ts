import * as fromRequirement from './requirement.reducer';
import * as fromRoot from '@core/store/reducers';
import { Action, combineReducers } from '@ngrx/store';
import * as fromSearch from './search-requirement.reducer';
import * as fromCourse from './course.reducer';
import * as fromRoom from './room.reducer';
import * as fromGroup from './group.reducer';
import * as fromUser from './user.reducer';
import * as fromUserSubject from './subject-teachers.reducer';
import * as fromActivity from './activity.reducer';
import * as fromSearchUser from './search-user.reducer';
import * as fromSearchGroup from './search-group.reducer';

export const requirementFeatureKey = 'requirements';

export interface RequirementsState extends fromRoot.AppState {
	[fromRequirement.requirementFeatureKey]: fromRequirement.State;
	[fromSearch.searchRequirementFeatureKey]: fromSearch.State;
	[fromCourse.courseFeatureKey]: fromCourse.State;
	[fromRoom.roomFeatureKey]: fromRoom.State;
	[fromGroup.groupFeatureKey]: fromGroup.State;
	[fromUser.userFeatureKey]: fromUser.State;
	[fromUserSubject.userSubjectFeatureKey]: fromUserSubject.State;
	[fromActivity.activityFeatureKey]: fromActivity.State;
	[fromSearchUser.searchUserFeatureKey]: fromSearchUser.State;
	[fromSearchGroup.searchGroupFeatureKey]: fromSearchGroup.State;
}

export function reducers(state: RequirementsState | undefined, action: Action) {
	return combineReducers({
		[fromRequirement.requirementFeatureKey]: fromRequirement.reducer,
		[fromSearch.searchRequirementFeatureKey]: fromSearch.reducer,
		[fromCourse.courseFeatureKey]: fromCourse.reducer,
		[fromRoom.roomFeatureKey]: fromRoom.reducer,
		[fromGroup.groupFeatureKey]: fromGroup.reducer,
		[fromUser.userFeatureKey]: fromUser.reducer,
		[fromUserSubject.userSubjectFeatureKey]: fromUserSubject.reducer,
		[fromActivity.activityFeatureKey]: fromActivity.reducer,
		[fromSearchUser.searchUserFeatureKey]: fromSearchUser.reducer,
		[fromSearchGroup.searchGroupFeatureKey]: fromSearchGroup.reducer,
	})(state, action);
}
