import { MatTableModule } from '@angular/material/table';
import { MatListModule } from '@angular/material/list';
import { MatIconModule } from '@angular/material/icon';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { MatSortModule } from '@angular/material/sort';
import { MatSelectModule } from '@angular/material/select';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatChipsModule } from '@angular/material/chips';

export const MATERIAL_MODULES = [
	MatTableModule,
	MatListModule,
	MatIconModule,
	MatPaginatorModule,
	MatCardModule,
	MatButtonModule,
	MatInputModule,
	MatSortModule,
	MatSelectModule,
  MatTooltipModule,
  MatChipsModule
];
