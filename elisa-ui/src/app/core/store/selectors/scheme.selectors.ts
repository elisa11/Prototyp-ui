import { createFeatureSelector, createSelector } from '@ngrx/store';
import {
	getImporting,
	getScheme,
	schemaFeatureKey,
	State,
} from '@core/store/reducers/schema.reducer';

export const selectSchemaState = createFeatureSelector<State>(schemaFeatureKey);

export const selectScheme = createSelector(selectSchemaState, getScheme);
export const selectImporting = createSelector(selectSchemaState, getImporting);
