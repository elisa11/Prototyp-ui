import { createSelector } from '@ngrx/store';
import * as fromSearchGroup from '../reducers/search-group.reducer';
import { selectRequirementsState } from './requirement.selectors';
import { selectEntities } from './group.selectors';
import { Group } from '@elisa11/elisa-api-angular-client';

export const selectUserState = createSelector(
	selectRequirementsState,
	(s) => s[fromSearchGroup.searchGroupFeatureKey]
);

export const selectSearchIds = createSelector(selectUserState, fromSearchGroup.getIds);
export const selectSearchLoading = createSelector(
	selectUserState,
	fromSearchGroup.getLoading
);

export const selectSearchResults = createSelector(
	selectEntities,
	selectSearchIds,
	(data, searchIds) => {
		return searchIds.map((id) => data[id]).filter((d): d is Group => d != null);
	}
);
