import { createSelector } from '@ngrx/store';
import * as fromUser from '../reducers/user.reducer';
import { User } from '@elisa11/elisa-api-angular-client';
import { selectRequirementsState } from './requirement.selectors';

export const selectUserState = createSelector(
	selectRequirementsState,
	(s) => s[fromUser.userFeatureKey]
);

export const selectSelectedIds = createSelector(selectUserState, fromUser.getSelectedIds);
export const selectSearchLoading = createSelector(selectUserState, fromUser.getLoading);

export const { selectEntities } = fromUser.adapter.getSelectors(selectUserState);

export const selectSelected = createSelector(
	selectEntities,
	selectSelectedIds,
	(data, searchIds) => {
		return searchIds.map((id) => data[id]).filter((d): d is User => d != null);
	}
);
