export interface CollisionDialogModel {
  title: string;
  ok_label: string;
  no_label: string;
  information: string;
}
