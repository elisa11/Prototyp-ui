import { createAction, props } from '@ngrx/store';
import { UserDepartments } from '../reducers/user-department.reducer';

export const loadUserDepartments = createAction(
	'[User - User-Department/API] Load User Departments'
);

export const loadUserDepartmentsSuccess = createAction(
	'[User - User-Department/API] Load User Departments Success',
	props<{ data: UserDepartments }>()
);

export const loadUserDepartmentsFailure = createAction(
	'[User - User-Department/API] Load User Departments Failure',
	props<{ error: any }>()
);
