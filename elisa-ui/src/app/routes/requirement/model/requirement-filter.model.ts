import { StateEnum } from '@elisa11/elisa-api-angular-client';

export interface RequirementFilter {
	courseId: number;
	departmentId: number;
	status: StateEnum;
}
