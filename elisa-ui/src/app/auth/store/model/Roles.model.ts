/**
 * Helper class which stores statically
 * different types of roles from the server.
 * Each object instance holds RoleAuth authorization
 * object type which manages access to different
 * navigation menu items, buttons pages and other
 * access points of the client.
 */
export class RoleManager {
	public static readonly MAIN_TIMETABLE_CREATOR = {
		id: 1,
		name: 'MAIN_TIMETABLE_CREATOR',
		authorization: {
			creating: true,
			importing: true,
			updating: true,
			deleting: true,
			requesting: true,
			managing: true,
		},
	};

	public static readonly LOCAL_TIMETABLE_CREATOR = {
		id: 2,
		name: 'LOCAL_TIMETABLE_CREATOR',
		authorization: {
			creating: true,
			importing: false,
			updating: true,
			deleting: false,
			requesting: true,
			managing: true,
		},
	};

	public static readonly TEACHER = {
		id: 3,
		name: 'TEACHER',
		authorization: {
			creating: false,
			importing: false,
			updating: false,
			deleting: false,
			requesting: true,
			managing: true,
		},
	};

	public static readonly STUDENT = {
		id: 4,
		name: 'STUDENT',
		authorization: {
			creating: false,
			importing: false,
			updating: false,
			deleting: false,
			requesting: true,
			managing: false,
		},
	};

	public static readonly ALL = [
		RoleManager.MAIN_TIMETABLE_CREATOR,
		RoleManager.LOCAL_TIMETABLE_CREATOR,
		RoleManager.TEACHER,
		RoleManager.STUDENT,
	];

	public static readonly CREATORS = [
		RoleManager.MAIN_TIMETABLE_CREATOR,
		RoleManager.LOCAL_TIMETABLE_CREATOR,
	];

	public static readonly CREATORS_ID = [
		RoleManager.MAIN_TIMETABLE_CREATOR.id,
		RoleManager.LOCAL_TIMETABLE_CREATOR.id,
	];

	public static readonly EXCEPT_STUDENT = [...RoleManager.CREATORS, RoleManager.TEACHER];

	public static readonly EXCEPT_STUDENT_ID = [
		...RoleManager.CREATORS_ID,
		RoleManager.TEACHER.id,
	];

	public static readonly ID_ALL = [
		RoleManager.MAIN_TIMETABLE_CREATOR.id,
		RoleManager.LOCAL_TIMETABLE_CREATOR.id,
		RoleManager.TEACHER.id,
		RoleManager.STUDENT.id,
	];
}

export interface RoleAuth {
	creating?: boolean;
	importing?: boolean;
	updating?: boolean;
	deleting?: boolean;
	requesting?: boolean;
	managing?: boolean;
}

export interface Role {
	id: number;
	name?: string;
	authorization?: RoleAuth;
}
