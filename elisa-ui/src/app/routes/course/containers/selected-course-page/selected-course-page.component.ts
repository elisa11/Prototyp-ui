import { ChangeDetectionStrategy, Component } from '@angular/core';
import { AbstractSelectedPageComponent } from '@core/generics/abstract-selected-page.component';
import {
	CourseSerializerFull,
	DepartmentSerializerShort,
	User,
} from '@elisa11/elisa-api-angular-client';
import { Store } from '@ngrx/store';
import { courseSelector } from '../../store/selectors';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Component({
	selector: 'app-selected-course-page',
	template: `
		<app-course-detail
			[data]="data$ | async"
			[department]="department$ | async"
			[teacher]="teacher$ | async"
			(onBack)="back($event)"
		></app-course-detail>
	`,
	changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SelectedCoursePageComponent extends AbstractSelectedPageComponent<CourseSerializerFull> {
	department$: Observable<DepartmentSerializerShort>;
	teacher$: Observable<User>;

	constructor(private store: Store) {
		super();
		this.data$ = store.select(courseSelector.selectSelectedCourse);
		this.department$ = this.data$.pipe(
			map((value) =>
				value ? value.department : ({ name: '' } as DepartmentSerializerShort)
			)
		); // todo: fetch department by dep id + rozsirit zobrazovane data
		this.teacher$ = this.data$.pipe(
			map((value) => (value ? value.teacher : ({} as User)))
		);
	}
}
