import { ChangeDetectionStrategy, Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { searchUsers } from '../../store/actions/user-api.actions';
import { PaginatorFilter } from '@data/filter/paginator.filter';
import {
	selectSearchUserLoading,
	selectSearchUserResults,
} from '../../store/selectors/search-user.selectors';
import { selectUserCount } from '../../store/selectors/user.selectors';
import { AbstractCollectionPageComponent } from '@core/generics/abstract-collection-page.component';
import { User } from '@elisa11/elisa-api-angular-client';
import { Router } from '@angular/router';
import { UserModel } from '../../model/user.model';

@Component({
	template: `
		<app-user-list
			[data]="data$ | async"
			[isLoading]="loading$ | async"
			[pagination]="pagination"
			[resultsLength]="totalCount$ | async"
			(onFilterChange)="handleFilterChange($event)"
      (onNewUser)="handleNewUser($event)"
		></app-user-list>
	`,
	changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CollectionPageComponent extends AbstractCollectionPageComponent<User> {
	constructor(private store: Store,
              private router: Router) {
		super();
		store.dispatch(
			searchUsers({ query: { limit: this.pagination[0], offset: 0 } as PaginatorFilter })
		);
		this.loading$ = store.select(selectSearchUserLoading);
		this.data$ = store.select(selectSearchUserResults);
		this.totalCount$ = store.select(selectUserCount);
	}

	handleFilterChange(filterChange: PaginatorFilter) {
		this.store.dispatch(searchUsers({ query: filterChange }));
	}

  handleNewUser($event: UserModel | undefined) {
    this.router.navigate(['user','form'], {state:$event});
  }
}
