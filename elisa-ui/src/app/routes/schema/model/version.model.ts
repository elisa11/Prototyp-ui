import { Period, Version, VersionStatusEnum } from '@elisa11/elisa-api-angular-client';

export class VersionModel implements Version {
	readonly id: number;
	name: string;
	parent_schema: string;
	children: VersionModel[];
	periodData: Period;
	status: VersionStatusEnum;
	parent: number | null;
	period: number | null;
	updated_at: string;

	constructor(arg: Version, period?: Period) {
		Object.assign(this, arg);
		this.periodData = period;
		this.children = [];
	}
}
