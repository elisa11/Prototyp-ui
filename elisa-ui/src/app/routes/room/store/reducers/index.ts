import * as fromRoot from '@core/store/reducers';
import { Action, combineReducers } from '@ngrx/store';
import * as fromDepartment from './department.reducer';
import * as fromRoom from './room.reducer';
import * as fromSearchRoom from './search-room.reducer';
import * as fromRoomType from './room-type.reducer';
import * as fromRoomEquipment from './room-equipment.reducer';
import * as fromEquipment from './equipment.reducer';

export const roomsFeatureKey = 'rooms';

export interface RoomsState extends fromRoot.AppState {
	[fromDepartment.departmentFeatureKey]: fromDepartment.State;
	[fromRoom.roomFeatureKey]: fromRoom.State;
	[fromSearchRoom.searchRoomFeatureKey]: fromSearchRoom.State;
	[fromRoomType.roomTypeFeatureKey]: fromRoomType.State;
	[fromRoomEquipment.roomEquipmentFeatureKey]: fromRoomEquipment.State;
	[fromEquipment.equipmentFeatureKey]: fromEquipment.State;
}

export function reducers(state: RoomsState | undefined, action: Action) {
	return combineReducers({
		[fromDepartment.departmentFeatureKey]: fromDepartment.reducer,
		[fromRoom.roomFeatureKey]: fromRoom.reducer,
		[fromSearchRoom.searchRoomFeatureKey]: fromSearchRoom.reducer,
		[fromRoomType.roomTypeFeatureKey]: fromRoomType.reducer,
		[fromRoomEquipment.roomEquipmentFeatureKey]: fromRoomEquipment.reducer,
		[fromEquipment.equipmentFeatureKey]: fromEquipment.reducer,
	})(state, action);
}
