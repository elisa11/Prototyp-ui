import { Injectable } from '@angular/core';
import {
	ActivatedRouteSnapshot,
	CanActivate,
	Router,
	RouterStateSnapshot,
	UrlTree,
} from '@angular/router';
import { Observable, of } from 'rxjs';
import { Store } from '@ngrx/store';
import { NGXLogger } from 'ngx-logger';
import {
	catchError,
	exhaustMap,
	filter,
	map,
	switchMap,
	take,
	tap,
} from 'rxjs/operators';
import { schemaSelectors } from '@core/store/selectors';
import { GroupsCacheService } from '../services/groups-cache.service';
import { groupSelectors, searchGroupSelectors } from '../store/selectors';
import { groupActions } from '../store/actions';

const tag = 'GroupExistsGuard';

@Injectable({
	providedIn: 'root',
})
export class GroupExistsGuard implements CanActivate {
	constructor(
		private store: Store,
		private service: GroupsCacheService,
		private logger: NGXLogger,
		private router: Router
	) {}

	canActivate(
		route: ActivatedRouteSnapshot,
		state: RouterStateSnapshot
	): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
		return this.waitForCollectionToLoad().pipe(
			switchMap(() => this.hasGroup(route.params.id))
		);
	}

	private waitForCollectionToLoad(): Observable<boolean> {
		return this.store.select(searchGroupSelectors.selectSearchLoading).pipe(
			filter((loading) => !loading),
			take(1)
		);
	}

	private hasGroupInStore(id: number): Observable<boolean> {
		return this.store.select(groupSelectors.selectEntities).pipe(
			map((entities) => !!entities[id]),
			take(1)
		);
	}

	private hasGroupInApi(id: number): Observable<boolean> {
		return this.store.select(schemaSelectors.selectScheme).pipe(
			exhaustMap((version) =>
				this.service.getGroup(id, version.name).pipe(
					map((entity) => groupActions.loadGroup({ data: entity })),
					tap((action) => this.store.dispatch(action)),
					map((user) => !!user),
					catchError(() => {
						this.router.navigate(['/404']);
						return of(false);
					})
				)
			)
		);
	}

	private hasGroup(id: number): Observable<boolean> {
		return this.hasGroupInStore(id).pipe(
			switchMap((inStore) => {
				if (inStore) {
					this.logger.debug(tag, 'hasGroup', 'in store');
					return of(inStore);
				}
				this.logger.debug(tag, 'hasGroup try API');
				return this.hasGroupInApi(id);
			})
		);
	}
}
