import { Injectable } from '@angular/core';
import { Observable, zip } from 'rxjs';
import {
	Department,
	DepartmentsService,
	PaginatedDepartmentList,
} from '@elisa11/elisa-api-angular-client';
import { catchError, exhaustMap, map, shareReplay } from 'rxjs/operators';
import { AbstractCacheService } from './abstract-cache.service';
import { PaginatorFilter } from '@data/filter/paginator.filter';
import { BasicUtils } from '@shared/utils/basic.utils';
import { isNotNullOrUndefined } from 'codelyzer/util/isNotNullOrUndefined';
import { NGXLogger } from 'ngx-logger';
import { UserDepartmentsCacheService } from '@core/service/cache/user-departments-cache.service';

const tag = 'DepartmentsCacheService';

@Injectable({
	providedIn: 'root',
})
export class DepartmentsCacheService extends AbstractCacheService {
	private listDepartmentsCache = new Map();
	private getDepartmentCache = new Map();
	private getDepartmentByUserCache = new Map();

	constructor(
		private service: DepartmentsService,
		private userService: UserDepartmentsCacheService,
		private logger: NGXLogger
	) {
		super();
	}

	listDepartments(
		filter: PaginatorFilter,
		schema?: string,
		parent?: number,
		mainDepartment?: boolean
	): Observable<PaginatedDepartmentList> {
		const key = { filter, parent, mainDepartment };
		const obs = this.listDepartmentsCache.get(key);
		if (obs) {
			return obs;
		}
		const response = this.service
			.departmentsList(
				schema,
				filter.limit,
				filter.offset
				// BasicUtils.getSortBy(filter),
				// parent,
				// mainDepartment,
				// filter.search
			)
			.pipe(shareReplay({ bufferSize: 1, refCount: true }));
		this.listDepartmentsCache.set(key, response);
		return response;
	}

	clearCache(): void {
		this.listDepartmentsCache.clear();
		this.getDepartmentCache.clear();
		this.getDepartmentByUserCache.clear();
	}

	getDepartment(schema: string = ' ', id: number): Observable<Department> {
		this.logger.debug(tag, 'getDepartment', id);
		const obs = this.getDepartmentCache.get(id);
		if (obs) {
			return obs;
		}
		const response = this.service
			.departmentsRetrieve(schema, String(id))
			.pipe(shareReplay({ bufferSize: 1, refCount: true }));
		this.getDepartmentCache.set(id, response);
		return response;
	}

	getDepartmentByUser(schema: string = '', uid: number): Observable<Array<Department>> {
		const obs = this.getDepartmentByUserCache.get(uid);
		if (obs) {
			return obs;
		}

		const response = this.userService.getUserDepartments(uid, null).pipe(
			map((res) => res.results.map((value) => value.department)),
			exhaustMap((ids: number[]) =>
				zip(
					...ids.map((id) =>
						this.service
							.departmentsRetrieve(schema, String(id))
							.pipe(catchError(() => null))
					)
				)
			),
			map((data) => data.filter((d) => isNotNullOrUndefined(d))),
			catchError(() => {
				return [];
			}),
			shareReplay({ bufferSize: 1, refCount: true })
		);

		this.getDepartmentByUserCache.set(uid, response);
		return response;
	}
}
