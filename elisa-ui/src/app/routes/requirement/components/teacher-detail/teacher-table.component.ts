import { Component } from '@angular/core';
import { AbstractTableComponent } from '@core/generics/abstract-table.component';
import { SubjectTeacherModel } from '../../model/subject-teacher.model';

@Component({
	selector: 'app-teacher-table',
	templateUrl: './teacher-table.component.html',
})
export class TeacherTableComponent extends AbstractTableComponent<SubjectTeacherModel> {
	displayedColumns = ['username', 'name', 'role'];

	constructor() {
		super();
	}
}
