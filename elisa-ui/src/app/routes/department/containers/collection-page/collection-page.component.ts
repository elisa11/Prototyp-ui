import { ChangeDetectionStrategy, Component } from '@angular/core';
import { AbstractCollectionPageComponent } from '@core/generics/abstract-collection-page.component';
import { Department } from '@elisa11/elisa-api-angular-client';
import { PaginatorFilter } from '@data/filter/paginator.filter';
import { Store } from '@ngrx/store';
import { departmentApiActions } from '../../store/actions';
import { departmentSelectors, searchDepartmentSelectors } from '../../store/selectors';

@Component({
	selector: 'app-collection-page',
	template: `
		<app-department-list
			[data]="data$ | async"
			[isLoading]="loading$ | async"
			[pagination]="pagination"
			[resultsLength]="totalCount$ | async"
			(onFilterChange)="handleFilterChange($event)"
		></app-department-list>
	`,
	changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CollectionPageComponent extends AbstractCollectionPageComponent<Department> {
	constructor(private store: Store) {
		super();
		store.dispatch(
			departmentApiActions.searchDepartment({
				query: { limit: this.pagination[0], offset: 0 } as PaginatorFilter,
			})
		);
		this.loading$ = store.select(searchDepartmentSelectors.selectSearchLoading);
		this.data$ = store.select(searchDepartmentSelectors.selectSearchResults);
		this.totalCount$ = store.select(departmentSelectors.selectTotalCount);
	}

	handleFilterChange(filterChange: PaginatorFilter) {
		this.store.dispatch(departmentApiActions.searchDepartment({ query: filterChange }));
	}
}
