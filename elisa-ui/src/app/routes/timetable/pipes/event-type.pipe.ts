import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
	name: 'eventType',
})
export class EventTypePipe implements PipeTransform {
	transform(id: number): string {
		return id === 1 ? 'P' : id === 2 ? 'C' : 'L';
	}
}
