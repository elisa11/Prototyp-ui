import { createReducer, on } from '@ngrx/store';
import { createEntityAdapter, EntityAdapter, EntityState } from '@ngrx/entity';
import { CourseSerializerFull } from '@elisa11/elisa-api-angular-client';
import { courseApiActions } from '../actions';

export const courseFeatureKey = 'course';

export interface State extends EntityState<CourseSerializerFull> {
	readonly selectedIds: number[];
	readonly count: number;
	readonly loading: boolean;
}

export const adapter: EntityAdapter<CourseSerializerFull> =
	createEntityAdapter<CourseSerializerFull>({
		selectId: (model: CourseSerializerFull) => model.id,
		sortComparer: false,
	});

export const initialState = adapter.getInitialState({
	selectedIds: [],
	count: 0,
	loading: false,
});

export const reducer = createReducer(
	initialState,
	on(courseApiActions.loadCoursesSuccess, (state, { data }) =>
		adapter.upsertMany(data.results, {
			...state,
			selectedIds: data.results.map((course) => course.id),
			count: data.count,
			loading: false,
		})
	),
	on(courseApiActions.loadCourses, (state) => ({ ...state, loading: true })),
	on(courseApiActions.loadCoursesFailure, (state) => ({ ...state, loading: false }))
);

export const getSelectedIds = (state: State) => state.selectedIds;
export const getLoading = (state: State) => state.loading;
export const getCount = (state: State) => state.count;
