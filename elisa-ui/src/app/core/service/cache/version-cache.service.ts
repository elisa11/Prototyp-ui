import { Injectable } from '@angular/core';
import {
	PaginatedVersionList,
	Version,
	VersionsService,
} from '@elisa11/elisa-api-angular-client';
import { forkJoin, Observable, of } from 'rxjs';
import { first, map, shareReplay } from 'rxjs/operators';
import { PaginatorFilter } from '@data/filter/paginator.filter';
import { NGXLogger } from 'ngx-logger';
import { CommonText } from '@data/common-text';
import { VersionModel } from '../../../routes/schema/model/version.model';
import { AbstractCacheService } from '@core/service/cache/abstract-cache.service';
import { BasicUtils } from '@shared/utils/basic.utils';

const tag = 'VersionCacheService';

@Injectable({
	providedIn: 'root',
})
export class VersionCacheService extends AbstractCacheService {
	private readonly getVersionsCache = new Map();
	private readonly getVersionTreeCache = new Map();
	private readonly getVersionCache = new Map();

	constructor(private service: VersionsService, private logger: NGXLogger) {
		super();
	}

	getVersions(filter: PaginatorFilter): Observable<PaginatedVersionList> {
		const cached = this.getVersionsCache.get(filter);
		if (cached) {
			this.logger.debug(tag, 'getVersions', 'returning cached versions');
			return cached;
		}

		const response = this.service
			.versionsList(
				filter ? filter.id : undefined,
				filter ? filter.limit : undefined,
				filter ? filter.offset : undefined,
				BasicUtils.getSortBy(filter),
				filter ? filter.search : undefined
			)
			.pipe(shareReplay({ bufferSize: 1, refCount: true }));
		this.getVersionsCache.set(filter, response);
		this.logger.debug(tag, 'getVersions', 'returning fresh versions');
		return response;
	}

	getVersion(id: number): Observable<Version> {
		const cached = this.getVersionCache.get(id);
		if (cached) {
			this.logger.debug(tag, 'getVersions', 'returning cached version', id);
			return cached;
		}

		const response = this.service
			.versionsRetrieve(id)
			.pipe(shareReplay({ bufferSize: 1, refCount: true }));
		this.getVersionCache.set(id, response);
		this.logger.debug(tag, 'getVersion', 'returning fresh version', id);
		return response;
	}

	addVersion(version: Version): Observable<Version> {
		return this.service.versionsCreate(version);
	}

	deleteVersion(id: number) {
		return this.service.versionsDestroy(id);
	}

	getVersionTree() {
		const cached = this.getVersionsCache.get('tree');
		if (cached) {
			this.logger.debug(tag, 'getVersions', 'returning cached versions');
			return cached;
		}

		return this.getVersions({
			offset: 0,
			limit: CommonText.pagination[0],
		} as PaginatorFilter).pipe(
			map((response) => {
				const requestSize = 10;
				let req = response.count / requestSize;
				req = req + (response.count % requestSize > 0 ? 1 : 0);
				const observables: Observable<PaginatedVersionList>[] = [];
				[...Array(req).keys()].forEach((index) => {
					observables.push(
						this.getVersions({
							offset: index * requestSize,
							limit: requestSize,
						} as PaginatorFilter).pipe(first())
					);
				});

				return forkJoin([of(response), ...observables]);
			}),
			map((forkData: Observable<PaginatedVersionList[]>) => {
				return forkData.pipe(first()).subscribe((joinData) => {
					const versions = [...joinData.flatMap((d) => d.results)].sort(
						(a, b) => a.id - b.id
					);
					const root = new VersionModel(versions[0]);
					versions.slice(1, versions.length).forEach((v) => {
						// todo: generovanie stromovej struktury
					});
				});
			}),
			shareReplay({ bufferSize: 1, refCount: true })
		);
	}

	clearCache(): void {
		this.getVersionsCache.clear();
		this.getVersionTreeCache.clear();
		this.getVersionCache.clear();
	}
}
