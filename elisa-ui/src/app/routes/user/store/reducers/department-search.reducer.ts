import { createReducer, on } from '@ngrx/store';
import {
	loadUserDepartments,
	loadUserDepartmentsFailure,
} from '../actions/user-department-api.actions';
import { loadDepartmentsSuccess } from '../actions/department-api.actions';

export const departmentSearchFeatureKey = 'departmentSearch';

export interface State {
	readonly loading: boolean;
	readonly error: string;
}

export const initialState: State = {
	loading: false,
	error: null,
};

export const reducer = createReducer(
	initialState,
	on(loadUserDepartments, (state: State) => ({
		...state,
		loading: true,
	})),
	on(loadDepartmentsSuccess, (state: State) => ({
		...state,
		error: '',
		loading: false,
	})),
	on(loadUserDepartmentsFailure, (state: State, error: any) => ({
		...state,
		loading: false,
		error: error.message,
	}))
);

export const getLoading = (state: State) => state.loading;
export const getError = (state: State) => state.error;
