import { createAction, props } from '@ngrx/store';
import { PaginatorFilter } from '@data/filter/paginator.filter';
import { PaginatedRoomList, Room } from '@elisa11/elisa-api-angular-client';
import { Capacity } from '../../model/capacity.model';
import { RoomModel } from '../../model/room.model';

export const searchRoom = createAction(
	'[Room - Room/Api] Search Rooms',
	props<{
		filters: { departments: number[]; type: number[]; capacity: Capacity };
		query: PaginatorFilter;
	}>()
);

export const searchRoomSuccess = createAction(
	'[Room - Room/Api] Search Rooms Success',
	props<{ data: PaginatedRoomList }>()
);

export const searchRoomFailure = createAction(
	'[Room - Room/Api] Search Rooms Failure',
	props<{ error: string }>()
);

export const postRoom = createAction(
  '[Room - Room/Api Create (Post) Room',
  props<{ data: RoomModel }>()
);

export const postRoomSuccess = createAction(
  '[Room - Room/Api Create (Post) Room Success',
  props<{ data: Room }>()
);

export const postRoomFailure = createAction(
  '[Room - Room/Api Create (Post) Room Failure',
  props<{ error: any }>()
);

export const putRoom = createAction(
  '[Room - Room/Api Put Room',
  props<{ data: RoomModel }>()
);

export const putRoomSuccess = createAction(
  '[Room - Room/Api Put Room Success',
  props<{ data: Room }>()
);

export const putRoomFailure = createAction(
  '[Room - Room/Api Put Room Failure',
  props<{ error: any }>()
);


export const deleteRoom = createAction(
  '[Room - Room/Api] Delete Room',
  props<{ id: number }>()
);

export const deleteRoomDone = createAction(
  '[Room - Room/Api] Delete Room Done',
  props<{ id: number }>()
);

export const deleteRoomFailed = createAction(
  '[Room - Room/Api] Delete Room Failed',
  props<{ error: any }>()
);
