import { createReducer, on } from '@ngrx/store';
import { searchUserActions } from '../actions';

export const searchUserFeatureKey = 'searchUser';

export interface State {
	readonly ids: number[];
	readonly loading: boolean;
	readonly error: string;
	readonly query: string;
}

export const initialState: State = {
	ids: [],
	loading: false,
	error: '',
	query: '',
};

export const reducer = createReducer(
	initialState,
	on(searchUserActions.searchUsers, (state, { search }) => ({
		...state,
		query: search,
		loading: true,
	})),
	on(searchUserActions.searchUsersSuccess, (state, { data }) => ({
		...state,
		ids: data.results.map((x) => x.id),
		loading: false,
	})),
	on(searchUserActions.searchUsersFailure, (state, { error }) => ({
		...state,
		error: error,
		loading: false,
	}))
);

export const getIds = (state: State) => state.ids;

export const getQuery = (state: State) => state.query;

export const getLoading = (state: State) => state.loading;

export const getError = (state: State) => state.error;
