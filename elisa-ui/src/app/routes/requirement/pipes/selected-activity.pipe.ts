import { Pipe, PipeTransform } from '@angular/core';
import { ActivityModel } from '../model/activity.model';

@Pipe({
	name: 'selectedActivity',
	pure: false,
})
export class SelectedActivityPipe implements PipeTransform {
	transform(value: number, show: { activity: ActivityModel; show: boolean }[]): boolean {
		return show.length > value ? show[value].show : false;
	}
}
