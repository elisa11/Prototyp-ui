import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { SnackEffects } from '@core/generics/snack.effects';
import { MatSnackBar } from '@angular/material/snack-bar';
import { resetCachedData } from '@core/store/actions/settings.actions';
import {
	catchError,
	debounceTime,
	map,
	skip,
	switchMap,
	takeUntil,
	tap,
	withLatestFrom,
} from 'rxjs/operators';
import { CoursesCacheService } from '../../../course/services/courses-cache.service';
import { asyncScheduler, of, zip } from 'rxjs';
import { courseApiActions } from '../actions';
import { schemaSelectors } from '@core/store/selectors';
import { Store } from '@ngrx/store';
import { PaginatorFilter } from '@data/filter/paginator.filter';
import { PaginatedCourseSerializerFullList } from '@elisa11/elisa-api-angular-client';
import { LocaleService } from '@core/service/locale/locale.service';
import { marker } from '@biesbjerg/ngx-translate-extract-marker';
import { NGXLogger } from 'ngx-logger';

const tag = 'CourseEffects';

@Injectable()
export class CourseEffects extends SnackEffects {
	resetData$ = createEffect(
		() =>
			this.actions$.pipe(
				ofType(resetCachedData),
				tap(() => this.service.clearCache())
			),
		{ dispatch: false }
	);

	loadCourses$ = createEffect(
		() =>
			({ debounce = 300, scheduler = asyncScheduler } = {}) =>
				this.actions$.pipe(
					ofType(courseApiActions.loadCourses),
					debounceTime(debounce, scheduler),
					withLatestFrom(this.store.select(schemaSelectors.selectScheme)),
					map((value) => {
						return {
							search: value[0].search,
							version: value[1].name,
						};
					}),
					switchMap(({ search, version }) => {
						if (search === null || search === undefined || search.length === 0) {
							return of(
								courseApiActions.loadCoursesSuccess({ data: { count: 0, results: [] } })
							);
						}

						const nextSearch$ = this.actions$.pipe(
							ofType(courseApiActions.loadCourses),
							skip(1)
						);
						const limit = 25;
						return this.service
							.listCourses({ search, limit } as PaginatorFilter, version)
							.pipe(
								takeUntil(nextSearch$),
								switchMap((value) => {
									const count = value.count;
									if (count > limit) {
										let req = Math.floor(count / limit) - 1;
										req = req + (count % limit > 0 ? 1 : 0);
										return zip(
											of(value),
											...[...Array(req).keys()].map((index) =>
												this.service.listCourses(
													{
														search,
														offset: (1 + index) * limit,
														limit,
													} as PaginatorFilter,
													version
												)
											)
										).pipe(
											map((data) => {
												return {
													count: data[0].count,
													results: data.flatMap((list) => list.results),
												} as PaginatedCourseSerializerFullList;
											})
										);
									} else {
										return of(value);
									}
								}),
								map((data) => courseApiActions.loadCoursesSuccess({ data })),
								catchError((err) => {
									this.logger.error(tag, 'loadCourses$', err);
									return of(courseApiActions.loadCoursesFailure({ error: err.message }));
								})
							);
					})
				)
	);
	searchFailed$ = createEffect(
		() =>
			this.actions$.pipe(
				ofType(courseApiActions.loadCoursesFailure),
				tap((err) =>
					this.openSnackbar(
						this.localeService.translate(marker('requirement.course.error'))
					)
				)
			),
		{ dispatch: false }
	);

	constructor(
		private actions$: Actions,
		private store: Store,
		private localeService: LocaleService,
		private logger: NGXLogger,
		private service: CoursesCacheService,
		snack: MatSnackBar
	) {
		super(snack);
	}
}
