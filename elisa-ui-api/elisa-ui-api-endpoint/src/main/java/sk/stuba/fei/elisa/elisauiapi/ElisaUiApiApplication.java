package sk.stuba.fei.elisa.elisauiapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ElisaUiApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(ElisaUiApiApplication.class, args);
    }

}
