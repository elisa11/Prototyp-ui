import { createAction, props } from '@ngrx/store';
import { Activity } from '@elisa11/elisa-api-angular-client';

export const loadActivities = createAction(
  '[Timetable - Activity] Load Activities',
);

export const loadActivitiesSuccess = createAction(
  '[Timetable - Activity] Load Activities Success',
  props<{ activities: Activity[] }>()
);

export const loadActivitiesFailure = createAction(
  '[Timetable - Activity] Load Activities Failure',
  props<{ error: any }>()
);
