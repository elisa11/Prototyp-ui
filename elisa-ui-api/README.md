# Spring Boot Middleware

This project uses Java 11 and Spring Boot, and acts as environment delivery server for Angular Frontend.

## Development server

Run `java -Dspring.profiles.active=dev -jar config-server.jar` for a dev server.

## Initial project setup

Commands to run before first run.

### Project setup

- In `elisa-ui-api/elisa-ui-api-endpoint/src/main/resources/application-dev.yml` file update `url` and `port` to match Virtual Machine address
- In `elisa-ui-api/elisa-ui-api-endpoint` run: `mvn clean package`
- In `elisa-ui-api/elisa-ui-api-endpoint/target` run: `java -Dspring.profiles.active=dev -jar config-server.jar`
