import { createAction } from '@ngrx/store';

export const initApp = createAction('[Core - App] Initialize App');
