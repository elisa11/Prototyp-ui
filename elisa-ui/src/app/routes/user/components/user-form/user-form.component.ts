import { Component, EventEmitter, Input, NgZone, OnDestroy, OnInit, Output, ViewChild } from '@angular/core';
import { CdkTextareaAutosize } from '@angular/cdk/text-field';
import { AbstractControl, FormBuilder, FormGroup, FormGroupDirective, Validators } from '@angular/forms';
import { Paths } from '@shared/store/reducers/form.reducer';
import { CommonText } from '@data/common-text';
import { Subject, Subscription } from 'rxjs';
import { NGXLogger } from 'ngx-logger';
import { Store } from '@ngrx/store';
import { take } from 'rxjs/operators';
import { UserModel } from '../../model/user.model';
import { UserApiActions } from '../../store/actions';
import { RoleEnum } from '@elisa11/elisa-api-angular-client';

const tag = 'UserFormComponent'

@Component({
  selector: 'app-user-form',
  templateUrl: './user-form.component.html'
})

export class UserFormComponent implements OnInit, OnDestroy {
  @ViewChild('autosize') autosize: CdkTextareaAutosize;
  userForm: FormGroup;
  controls: { [p: string]: AbstractControl };
  path: Paths.course;
  private moduleId = 'user';
  moduleTitle = CommonText.navLinks.find((value) => value.link === this.moduleId).label;
  private unsubscribe$ = new Subject();
  @Input()
  userExistsData!: UserModel | undefined;

  @Output() private onSubmit = new EventEmitter<UserModel>();
  @Output() private onPut = new EventEmitter<UserModel>();
  @Output() private onCancel = new EventEmitter<string>();
  @Output() private onBack = new EventEmitter<string>();
  private errorSub: Subscription;

  constructor(
    private ngZone: NgZone,
    private fb: FormBuilder,
    private logger: NGXLogger,
    private store: Store
  ) {}

  get formData(): any {
    return this.userForm.value;
  }

  ngOnInit(): void {
    const numberValidation = Validators.compose([Validators.min(0)]);
    if (this.userExistsData !== undefined && this.userExistsData){
      this.userForm = this.fb.group({
        id: [this.userExistsData.id, numberValidation],
        userName: [this.userExistsData.username, Validators.required],
        titleBefore: [this.userExistsData.title_before],
        titleAfter: [this.userExistsData.title_after],
        firstName: [this.userExistsData.first_name,Validators.required],
        lastName: [this.userExistsData.last_name, Validators.required],
        groups: [null]
      });
    }
    else{
      this.userForm = this.fb.group({
        id: ['',numberValidation],
        userName: [''],
        titleBefore: [''],
        titleAfter: [''],
        firstName: [''],
        lastName: [''],
        groups: [null]
      });
    }
    this.controls = this.userForm.controls;
    this.errorSub = this.userForm.valueChanges.subscribe((value) => {
      const errors = [];
      Object.keys(this.userForm.controls).forEach((key) => {
        // Get errors of every form control
        errors.push({ key, err: this.userForm.get(key).errors });
      });
      console.log(errors);
    })
  }


  ngOnDestroy(): void {
    this.unsubscribe$.next(this);
    this.unsubscribe$.complete();
  }

  triggerResize(): void {
    this.ngZone.onStable
      .pipe(take(1))
      .subscribe(() => this.autosize.resizeToFitContent(true));
  }

  back(): void {
    return this.onBack.emit(this.moduleId);
  }

  onUserCancel(): void {
    this.logger.debug(tag, 'onUserCancel');
    this.onCancel.emit(this.moduleId);
    this.back();
  }

  submit(): void {
    let rolesToSubmit: string[] = [];
    if (this.formData.groups.length > 0){
      this.formData.groups.forEach((g)=> {
        for (let r in RoleEnum){
          if(r == g){
            rolesToSubmit.push(RoleEnum[r]);
            break;
          }
        }
      })
    }
    const item = {
      id: this.formData.id,
      username: this.formData.userName,
      first_name: this.formData.firstName,
      last_name: this.formData.lastName,
      title_after: this.formData.titleAfter,
      title_before: this.formData.titleBefore,
      groups: rolesToSubmit
    } as UserModel;
    this.logger.debug(tag, 'onSubmit');
    if (this.userExistsData!==undefined){
      this.onPut.emit(item);
    }
    else{
      this.onSubmit.emit(item);
    }
    this.back();
  }

  resetForm(form: FormGroupDirective) {
    form.resetForm();
  }

  handleSelectedGroups($event: string[]) {
    console.log($event);
    this.formData.groups = $event;
  }

  removeRoleFromUser(roleToRemove: string) {
    if (this.userExistsData !== undefined) {
      this.store.dispatch(UserApiActions.removeRoleUser({id:this.userExistsData.id, role:roleToRemove }))
    }
  }

  addRoleToUser(roleToAdd: string) {
    if (this.userExistsData !== undefined) {
      this.store.dispatch(UserApiActions.setRoleUser({id:this.userExistsData.id, role: roleToAdd }))
    }
  }
}
