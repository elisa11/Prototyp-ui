import { createAction, props } from '@ngrx/store';
import { PaginatedStudyFormList } from '@elisa11/elisa-api-angular-client';

export const loadStudyForms = createAction('[Enum - StudyForm] Load StudyForms');

export const loadStudyFormsSuccess = createAction(
	'[Enum - StudyForm] Load StudyForms Success',
	props<{ data: PaginatedStudyFormList }>()
);

export const loadStudyFormsFailure = createAction(
	'[Enum - StudyForm] Load StudyForms Failure',
	props<{ error: any }>()
);
