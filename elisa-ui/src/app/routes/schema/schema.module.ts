import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SchemaRoutingModule } from './schema-routing.module';
import { SharedModule } from '@shared/shared.module';
import { TranslateModule } from '@ngx-translate/core';
import { ReactiveFormsModule } from '@angular/forms';
import { SchemaTableComponent } from './components/schema-table/schema-table.component';
import { SchemaTreeComponent } from './components/schema-tree/schema-tree.component';
import { StoreModule } from '@ngrx/store';
import * as fromSchema from './store/reducers';
import { EffectsModule } from '@ngrx/effects';
import {
	DepartmentEffects,
	NewSchemaEffects,
	PeriodEffects,
	SchemaEffects,
} from './store/effects';
import { SchemaViewPageComponent } from './containers/schema-view-page/schema-view-page.component';
import { NewSchemaFormComponent } from './components/new-schema-form/new-schema-form.component';
import { PeriodTableComponent } from './components/period-table/period-table.component';
import { MATERIAL_MODULES, PIPES } from './index';

@NgModule({
	declarations: [
		SchemaTableComponent,
		SchemaTreeComponent,
		SchemaViewPageComponent,
		NewSchemaFormComponent,
		PeriodTableComponent,
		...PIPES,
	],
	imports: [
		CommonModule,
		SharedModule,
		TranslateModule.forChild(),
		SchemaRoutingModule,
		...MATERIAL_MODULES,
		ReactiveFormsModule,
		StoreModule.forFeature(fromSchema.schemaFeatureKey, fromSchema.reducers),
		EffectsModule.forFeature([
			SchemaEffects,
			PeriodEffects,
			DepartmentEffects,
			NewSchemaEffects,
		]),
	],
})
export class SchemaModule {}
