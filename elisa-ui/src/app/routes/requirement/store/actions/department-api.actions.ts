import { createAction, props } from '@ngrx/store';

export const loadDepartments = createAction(
	'[DepartmentApi] Load Departments',
	props<{ ids: number[] }>()
);

export const loadDepartmentsSuccess = createAction(
	'[DepartmentApi] Load Departments Success',
	props<{ data: any }>()
);

export const loadDepartmentsFailure = createAction(
	'[DepartmentApi] Load Departments Failure',
	props<{ error: any }>()
);
