import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Room, RoomsService } from '@elisa11/elisa-api-angular-client';
import { publishReplay, refCount } from 'rxjs/operators';
import { AbstractCacheService } from '@core/service/cache/abstract-cache.service';
import { PaginatorFilter } from '@data/filter/paginator.filter';
import { BasicUtils } from '@shared/utils/basic.utils';
import { RoomFilterModel } from '../model/room-filter.model';
import { PaginatedRoomList } from '@elisa11/elisa-api-angular-client';
import { Capacity } from '../model/capacity.model';
import { RoomModel } from '../model/room.model';

@Injectable({
	providedIn: 'root',
})
export class RoomsCacheService extends AbstractCacheService {
	private listRoomsCache = new Map();
	private getRoomCache = new Map();

	constructor(private service: RoomsService) {
		super();
	}

	capacityValidator(roomFilter, capacity): Capacity {
    if (roomFilter != undefined){
      if (roomFilter.capacity != undefined){
        capacity = {
          max:roomFilter.capacity.max,
          min: roomFilter.capacity.min
        }
      }
      else if (roomFilter.type != undefined && roomFilter.capacity == undefined){
        capacity = {
          max: 1000,
          min : 0
        }
      }
    }
    return capacity;

  }
	listRooms(
		timetableVersion: string,
		roomFilter: RoomFilterModel,
		paginatorFilter: PaginatorFilter
	): Observable<PaginatedRoomList> {
		const key = { timetableVersion, roomFilter, paginatorFilter };
		const obs = this.listRoomsCache.get(key);
		if (obs) {
			return obs;
		}
		let capacity: Capacity = {
		  max: undefined,
      min: undefined
		} ;
		capacity = this.capacityValidator(roomFilter, capacity);

		const response = this.service
			.roomsList(
				timetableVersion,
				roomFilter ? roomFilter.departments : undefined,
				paginatorFilter ? paginatorFilter.id : undefined,
				paginatorFilter ? paginatorFilter.limit : undefined,
				roomFilter ? capacity.max : undefined,
				roomFilter ? capacity.min : undefined,
				paginatorFilter ? paginatorFilter.offset : undefined,
				BasicUtils.getSortBy(paginatorFilter),
				roomFilter ? roomFilter.type : undefined,
				paginatorFilter ? paginatorFilter.search : undefined
			)
			.pipe(publishReplay(1), refCount());
		this.listRoomsCache.set(key, response);
		return response;
	}

	getRoom(schema: string, id: number): Observable<Room> {
		const obs = this.getRoomCache.get(id);
		if (obs) {
			return obs;
		}
		const response = this.service
			.roomsRetrieve(schema, id)
			.pipe(publishReplay(1), refCount());
		this.getRoomCache.set(id, response);
		return response;
	}

	setRoomForPutOrPost(model:RoomModel){
    return {
      capacity: model.capacity,
      department: model.department.id,
      id: model.id,
      name: model.name,
      room_type: model.type.id
    } as Room;
  }

  postRoom(scheme: string, roomModel: RoomModel) {
    return this.service.roomsCreate(scheme, this.setRoomForPutOrPost(roomModel));
  }

  putRoom(scheme: string, roomModel: RoomModel) {
    return this.service.roomsUpdate(scheme, roomModel.id, this.setRoomForPutOrPost(roomModel));
  }
  deleteRoom(schema: string, rid: number): Observable<any> {
    return this.service.roomsDestroy(schema, rid);
  }
	clearCache(): void {
		this.listRoomsCache.clear();
		this.getRoomCache.clear();
	}
}
