import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { schemaActions } from '@core/store/actions';
import { catchError, map, switchMap, withLatestFrom } from 'rxjs/operators';
import { userSubjectRolesActions } from '@shared/store/actions';
import { schemaSelectors } from '@core/store/selectors';
import { of } from 'rxjs';
import { UserSubjectRolesService } from '@elisa11/elisa-api-angular-client';
import { NGXLogger } from 'ngx-logger';
import { Store } from '@ngrx/store';

const tag = 'UserSubjectRoleEffects';

@Injectable()
export class UserSubjectRoleEffects {
	loadSchemasDialogSuccess$ = createEffect(() =>
		this.actions$.pipe(
			ofType(schemaActions.loadSchemasImportDone),
			map(() => userSubjectRolesActions.loadUserSubjectRoles())
		)
	);

	loadActivityCategories$ = createEffect(() =>
		this.actions$.pipe(
			ofType(userSubjectRolesActions.loadUserSubjectRoles),
			withLatestFrom(this.store.select(schemaSelectors.selectScheme)),
			map((data) => data[1].name),
			switchMap((schema) => {
				return this.service.userSubjectRolesList(schema, 1, 0).pipe(
					switchMap((res) => {
						return this.service.userSubjectRolesList(schema, res.count, 0).pipe(
							map((data) =>
								userSubjectRolesActions.loadUserSubjectRolesSuccess({ data })
							),
							catchError((err) => {
								this.logger.error(tag, 'loadActivityCategories$', err);
								return of(
									userSubjectRolesActions.loadUserSubjectRolesFailure({ error: err })
								);
							})
						);
					}),
					catchError((err) => {
						this.logger.error(tag, 'loadActivityCategories$', err);
						return of(
							userSubjectRolesActions.loadUserSubjectRolesFailure({ error: err })
						);
					})
				);
			})
		)
	);

	constructor(
		private actions$: Actions,
		private service: UserSubjectRolesService,
		private logger: NGXLogger,
		private store: Store
	) {}
}
