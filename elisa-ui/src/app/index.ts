import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatMenuModule } from '@angular/material/menu';
import { MatIconModule } from '@angular/material/icon';
import { MatDividerModule } from '@angular/material/divider';
import { MatBadgeModule } from '@angular/material/badge';
import { MatButtonModule } from '@angular/material/button';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatListModule } from '@angular/material/list';
import { MatCardModule } from '@angular/material/card';
import { MatInputModule } from '@angular/material/input';
import { MatDialogModule } from '@angular/material/dialog';
import { MatSelectModule } from '@angular/material/select';
import { ConfirmDialogComponent } from './dialogs/confirm-dialog/confirm-dialog.component';
import { SchemaDialogComponent } from './dialogs/schema-dialog/schema-dialog.component';
import { LoadingDialogComponent } from './dialogs/loading-dialog/loading-dialog.component';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { CollisionDialogComponent } from './dialogs/collision-dialog/collision-dialog.component';

export const MATERIAL_MODULES = [
	MatMenuModule,
	MatIconModule,
	MatDividerModule,
	MatBadgeModule,
	MatButtonModule,
	MatToolbarModule,
	MatListModule,
	MatCardModule,
	MatInputModule,
	MatProgressSpinnerModule,
	MatSnackBarModule,
	MatTooltipModule,
	MatDialogModule,
	MatSelectModule,
  MatSlideToggleModule,
];

export const DIALOGS = [
	ConfirmDialogComponent,
	SchemaDialogComponent,
	LoadingDialogComponent,
  CollisionDialogComponent,
];
