import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { Group } from '@elisa11/elisa-api-angular-client';
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';
import { FormBuilder, FormGroupDirective, Validators } from '@angular/forms';
import { Paths } from '@shared/store/reducers/form.reducer';
import { debounceTime, takeUntil } from 'rxjs/operators';
import { Subject, Subscription } from 'rxjs';
import { formActions } from '@shared/store/actions';
import { Store } from '@ngrx/store';

@Component({
	selector: 'app-group-select',
	templateUrl: './group-select.component.html',
})
export class GroupSelectComponent implements OnInit, OnDestroy {
	selectedGroups: Group[] = [];
	@Input() filtered: Group[];
	@Input() loading: boolean;
	formGroup = this.fb.group({
		groups: [undefined, Validators.required],
	});
	controls = {
		groups: this.formGroup.controls.groups,
	};
	@Input() readonly: boolean = false;
	path = Paths.group;
	private unsubscribe$ = new Subject();
	@Output()
	private onGroupChange = new EventEmitter<string>();
	private filteredGroupSub: Subscription;

	constructor(private fb: FormBuilder, private store: Store) {}

	@Input() set data(input: Group[]) {
		if (input) {
			this.selectedGroups = input;
		}
	}

	ngOnDestroy(): void {
		this.unsubscribe$.next(this);
		this.unsubscribe$.complete();
	}

	resetForm(form: FormGroupDirective) {
		form.resetForm();
	}

	ngOnInit(): void {
		this.selectedGroups = [];
		this.filteredGroupSub = this.formGroup
			.get('groups')
			.valueChanges.pipe(debounceTime(250), takeUntil(this.unsubscribe$))
			.subscribe((data) => {
				this.onGroupChange.emit(data);
			});
	}

	dropGroupChip(event: CdkDragDrop<Group[]>): void {
		moveItemInArray(this.selectedGroups, event.previousIndex, event.currentIndex);
	}

	removeGroup(group: Group): void {
		const index = this.selectedGroups.indexOf(group);

		if (index >= 0) {
			this.selectedGroups.splice(index, 1);
		}
	}

	selectedGroup(event: MatAutocompleteSelectedEvent, form: FormGroupDirective): void {
		this.selectedGroups.push(event.option.value);
		this.resetForm(form);
		this.store.dispatch(formActions.submitFormSuccess({ path: this.path }));
	}
}
