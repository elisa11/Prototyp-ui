import { createSelector } from '@ngrx/store';
import {
	activityCategoryFeatureKey,
	adapter,
	getCount,
	getLoading,
	isEmpty,
} from '@shared/store/reducers/activity-category.reducer';
import { selectSharedState } from '@shared/store/selectors/shared.selectors';

export const selectActivityCategoryState = createSelector(
	selectSharedState,
	(state) => state[activityCategoryFeatureKey]
);
export const { selectIds, selectEntities, selectAll } = adapter.getSelectors(
	selectActivityCategoryState
);
export const selectLoading = createSelector(selectActivityCategoryState, getLoading);
export const selectTotalCount = createSelector(selectActivityCategoryState, getCount);
export const selectIsEmpty = createSelector(selectActivityCategoryState, isEmpty);
