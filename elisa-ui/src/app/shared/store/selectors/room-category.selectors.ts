import { createSelector } from '@ngrx/store';
import {
	adapter,
	getCount,
	getLoading,
	roomTypesFeatureKey,
} from '@shared/store/reducers/room-category.reducer';
import { selectSharedState } from '@shared/store/selectors/shared.selectors';

export const selectRoomCategoryState = createSelector(
	selectSharedState,
	(state) => state[roomTypesFeatureKey]
);
export const { selectIds, selectEntities, selectAll } = adapter.getSelectors(
	selectRoomCategoryState
);
export const selectLoading = createSelector(selectRoomCategoryState, getLoading);
export const selectTotalCount = createSelector(selectRoomCategoryState, getCount);
