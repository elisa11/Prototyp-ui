import { TeacherType } from '@data/model/teacher-type.model';

export class TypeGuardsUtils {
	public static isTeacherType(obj: TeacherType): obj is TeacherType {
		return obj !== undefined && obj.id !== undefined && obj.value !== undefined;
	}
}
