import { createReducer, on } from '@ngrx/store';
import { createEntityAdapter, EntityAdapter, EntityState } from '@ngrx/entity';
import { Activity } from '@elisa11/elisa-api-angular-client';
import { activityActions } from '../actions';

export const activityFeatureKey = 'activity';

export interface State extends EntityState<Activity> {
	readonly selectedCourseId: number;
	readonly count: number;
	readonly loading: boolean;
}

export const adapter: EntityAdapter<Activity> = createEntityAdapter<Activity>({
	selectId: (model: Activity) => model.id,
	sortComparer: false,
});

export const initialState = adapter.getInitialState({
	selectedCourseId: undefined,
	count: 0,
	loading: false,
});

export const reducer = createReducer(
	initialState,
	on(activityActions.loadActivities, (state, { cid }) => ({
		...state,
		selectedCourseId: cid,
		loading: true,
	})),
	on(activityActions.resetActivities, (state) => ({
		...state,
		selectedCourseId: undefined,
	})),
	on(activityActions.loadActivitiesSuccess, (state, { data }) =>
		adapter.upsertMany(data.results, {
			...state,
			loading: false,
		})
	),
	on(activityActions.loadActivitiesFailure, (state) => ({ ...state, loading: false }))
);

export const getSelectedCourseId = (state: State) => state.selectedCourseId;
export const getLoading = (state: State) => state.loading;
export const getCount = (state: State) => state.count;
