import {
	BlankEnum,
	Department,
	EmploymentEnum,
	UserDepartment,
} from '@elisa11/elisa-api-angular-client';

export class DepartmentData implements Department, UserDepartment {
	abbr: string | null;
	department: number;
	employment: EmploymentEnum | BlankEnum;
	id: number;
	name: string;
	parent: number | null;
	user: number;

	constructor(ud: UserDepartment, dep: Department) {
		Object.assign(this, { ...dep, ...ud });
	}
}
