import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
	name: 'capacity',
})
export class CapacityPipe implements PipeTransform {
	transform(value: { min: number; max: number }): string {
		return value.max ? `${value.min} - ${value.max}` : `${value.min} - max`;
	}
}
