import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import {
	loadMenuItems,
	loadVersion,
	loadVersionSuccessful,
} from '@core/store/actions/layout.actions';
import { authActions } from '../../../auth/store/actions';
import {
	catchError,
	exhaustMap,
	first,
	map,
	switchMap,
	tap,
	withLatestFrom,
} from 'rxjs/operators';
import { AppVersionCheckService } from '@core/service/app-version-check.service';
import {
	loadSchemasDialog,
	loadSchemasDialogCancel,
	loadSchemasDialogRedirect,
	loadSchemasDialogSuccess,
} from '@core/store/actions/schema.actions';
import { SchemaDialogComponent } from '../../../dialogs/schema-dialog/schema-dialog.component';
import { PaginatedVersionList, Version } from '@elisa11/elisa-api-angular-client';
import { MatDialog } from '@angular/material/dialog';
import { VersionCacheService } from '@core/service/cache/version-cache.service';
import { PaginatorFilter } from '@data/filter/paginator.filter';
import { HttpErrorResponse } from '@angular/common/http';
import { combineLatest, of } from 'rxjs';
import { NGXLogger } from 'ngx-logger';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { selectPayload } from '../../../auth/store/selectors/auth.selectors';
import { loadRoles } from '../../../auth/store/actions/role.actions';
import { schemaSelectors } from '@core/store/selectors';

const tag = 'LayoutEffects';

@Injectable()
export class LayoutEffects {
	refreshMenu$ = createEffect(() =>
		this.actions$.pipe(
			ofType(authActions.validToken),
			exhaustMap((action) => {
				return combineLatest([
					this.store.select(selectPayload),
					this.store.select(schemaSelectors.selectScheme),
				]).pipe(
					map((data) => loadMenuItems({ token: data[0], schema: data[1] })),
					tap((data) =>
						this.store.dispatch(
							loadRoles({ rolesId: data.token ? data.token.roles ?? [] : [] })
						)
					)
				);
			})
		)
	);

	loadVersion$ = createEffect(() =>
		this.actions$.pipe(
			ofType(loadVersion),
			map((action) => {
				const version = this.versionCheckService.actual;
				return loadVersionSuccessful({ version });
			})
		)
	);

	selectSchema$ = createEffect(() =>
		this.actions$.pipe(
			ofType(loadSchemasDialog),
			exhaustMap(() => {
				return this.versionService.getVersions({} as PaginatorFilter).pipe(
					first(),
					catchError((err: HttpErrorResponse) => {
						if (err.status !== 404) {
							this.logger.error(tag, 'fetch versions error', err);
						}
						return of({ count: -1, results: [] } as PaginatedVersionList);
					})
				);
			}),
			exhaustMap((versions) => {
				const dialogRef = this.dialog.open(SchemaDialogComponent, {
					data: { ...versions } as PaginatedVersionList,
					disableClose: true,
				});

				return dialogRef.afterClosed();
			}),
			withLatestFrom(this.store.select(schemaSelectors.selectScheme)),
			map((result) => {
				const res = result[0] as { redirect: boolean; version: Version };
				if (res && !res.redirect && res.version) {
					return loadSchemasDialogSuccess({ data: res.version, old: result[1] });
				} else if (!res.redirect) {
					return loadSchemasDialogCancel();
				} else {
					return loadSchemasDialogRedirect();
				}
			})
		)
	);

	loadSchemasDialogSuccess$ = createEffect(() =>
		this.actions$.pipe(
			ofType(loadSchemasDialogSuccess),
			map((value) => value.data),
			switchMap((schema) =>
				combineLatest([this.store.select(selectPayload), of(schema)]).pipe(
					map((data) => loadMenuItems({ token: data[0], schema: data[1] }))
				)
			)
		)
	);

	addSchema$ = createEffect(
		() =>
			this.actions$.pipe(
				ofType(loadSchemasDialogRedirect),
				tap((value) => {
					this.router.navigate(['version']);
				})
			),
		{ dispatch: false }
	);

	constructor(
		private versionCheckService: AppVersionCheckService,
		private actions$: Actions,
		private versionService: VersionCacheService,
		private logger: NGXLogger,
		private router: Router,
		private store: Store,
		private dialog: MatDialog
	) {}
}
