import { createSelector } from '@ngrx/store';
import { selectEntities, selectGroupState } from './group.selectors';
import {
	getError,
	getIds,
	getLoading,
	getQuery,
	searchGroupFeatureKey,
} from '../reducers/search-group.reducer';
import { Group } from '@elisa11/elisa-api-angular-client';

export const selectSearchCourseState = createSelector(
	selectGroupState,
	(state) => state[searchGroupFeatureKey]
);

export const selectSearchIds = createSelector(selectSearchCourseState, getIds);
export const selectSearchQuery = createSelector(selectSearchCourseState, getQuery);
export const selectSearchLoading = createSelector(selectSearchCourseState, getLoading);
export const selectSearchError = createSelector(selectSearchCourseState, getError);

export const selectSearchResults = createSelector(
	selectEntities,
	selectSearchIds,
	(data, searchIds) => {
		return searchIds
			.map((id) => data[id])
			.filter((entity): entity is Group => entity != null);
	}
);
