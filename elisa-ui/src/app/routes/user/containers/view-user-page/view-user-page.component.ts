import { ChangeDetectionStrategy, Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { map } from 'rxjs/operators';
import { UserActions } from '../../store/actions';
import { AbstractViewPageComponent } from '@core/generics/abstract-view-page.component';

@Component({
	template: ` <app-selected-user-page (onBack)="back($event)"></app-selected-user-page> `,
	changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ViewUserPageComponent extends AbstractViewPageComponent {
	constructor(router: Router, store: Store, route: ActivatedRoute) {
		super(router);
		this.actionsSubscription = route.params
			.pipe(map((params) => UserActions.selectUser({ id: params.id })))
			.subscribe((action) => store.dispatch(action));
	}
}
