import { Equipment } from './equipment';

export interface RoomEquipment {
	count: number;
	equipment: Equipment;
	room_id: number;
}
