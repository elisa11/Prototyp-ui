import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from '../../auth/services/auth.guard';
import { CollectionPageComponent } from './containers/collection-page/collection-page.component';
import { ViewGroupPageComponent } from './containers/view-group-page/view-group-page.component';
import { GroupExistsGuard } from './guards';

const routes: Routes = [
	{
		path: '',
		component: CollectionPageComponent,
		canActivate: [AuthGuard],
	},
	{
		path: ':id',
		component: ViewGroupPageComponent,
		canActivate: [AuthGuard, GroupExistsGuard],
	},
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule],
})
export class GroupRoutingModule {}
