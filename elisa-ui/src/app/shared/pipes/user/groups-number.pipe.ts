import { Pipe, PipeTransform } from '@angular/core';
import { isNotNullOrUndefined } from 'codelyzer/util/isNotNullOrUndefined';
import { User } from '@elisa11/elisa-api-angular-client';

@Pipe({
	name: 'groupsNumber',
})
export class GroupsNumberPipe implements PipeTransform {
	transform(user: User): number {
		if (isNotNullOrUndefined(user.groups)) {
			return user.groups.length;
		}
		return 0;
	}
}
