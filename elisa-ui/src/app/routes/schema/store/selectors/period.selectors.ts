import { createSelector } from '@ngrx/store';
import * as fromPeriod from '../reducers/period.reducer';
import { selectSchemasState } from './schema.selectors';
import { selectSelectedPeriodIds } from './period-api.selectors';

export const selectPeriodEntitiesState = createSelector(
	selectSchemasState,
	(state) => state[fromPeriod.periodFeatureKey]
);

export const {
	selectIds: selectPeriodIds,
	selectEntities: selectPeriodEntities,
	selectAll: selectAllPeriods,
} = fromPeriod.adapter.getSelectors(selectPeriodEntitiesState);

export const selectPeriodCount = createSelector(
	selectPeriodEntitiesState,
	fromPeriod.getCount
);

export const selectSelectedPeriods = createSelector(
	selectPeriodEntities,
	selectSelectedPeriodIds,
	(entities, entityIds) => entityIds.map((id) => entities[id])
);
