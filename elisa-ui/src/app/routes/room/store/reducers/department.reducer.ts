import { createReducer, on } from '@ngrx/store';
import { Department } from '@elisa11/elisa-api-angular-client';
import { createEntityAdapter, EntityAdapter, EntityState } from '@ngrx/entity';
import { departmentApiActions, scrollApiActions } from '../actions';

export const departmentFeatureKey = 'department';

export interface State extends EntityState<Department> {
	readonly count: number;
	readonly lastLoaded: number;
}

export const adapter: EntityAdapter<Department> = createEntityAdapter<Department>({
	selectId: (department: Department) => department.id,
	sortComparer: false,
});

export const initialState: State = adapter.getInitialState({
	count: null,
	lastLoaded: 0,
});

export const reducer = createReducer(
	initialState,
	on(departmentApiActions.loadDepartmentListSuccess, (state: State, { data }) =>
		adapter.upsertMany(data, state)
	),
	on(departmentApiActions.loadDepartmentSuccess, (state: State, { data }) =>
		adapter.upsertOne(data, state)
	),
	on(scrollApiActions.loadDepartmentScrollsSuccess, (state: State, { data }) =>
		adapter.upsertMany(data.results, {
			...state,
			lastLoaded: state.lastLoaded + data.results.length,
			count: data.count,
		})
	)
);

export const getCount = (state: State) => state.count;
export const getLastLoaded = (state: State) => state.lastLoaded;
