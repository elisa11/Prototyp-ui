import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { schemaActions } from '@core/store/actions';
import { catchError, map, switchMap, withLatestFrom } from 'rxjs/operators';
import { studyFormActions } from '@shared/store/actions';
import { EnumsService } from '@elisa11/elisa-api-angular-client';
import { NGXLogger } from 'ngx-logger';
import { schemaSelectors } from '@core/store/selectors';
import { of } from 'rxjs';
import { Store } from '@ngrx/store';

const tag = 'StudyFormEffects';

@Injectable()
export class StudyFormEffects {
	loadSchemasDialogSuccess$ = createEffect(() =>
		this.actions$.pipe(
			ofType(schemaActions.loadSchemasImportDone),
			map(() => studyFormActions.loadStudyForms())
		)
	);

	loadStudyForms$ = createEffect(() =>
		this.actions$.pipe(
			ofType(studyFormActions.loadStudyForms),
			withLatestFrom(this.store.select(schemaSelectors.selectScheme)),
			map((data) => data[1].name),
			switchMap((schema) => {
				return this.service.enumsStudyFormList(schema, 1, 0).pipe(
					switchMap((res) => {
						return this.service.enumsStudyFormList(schema, res.count, 0).pipe(
							map((data) => studyFormActions.loadStudyFormsSuccess({ data })),
							catchError((err) => {
								this.logger.error(tag, 'loadStudyForms$', err);
								return of(studyFormActions.loadStudyFormsFailure({ error: err }));
							})
						);
					}),
					catchError((err) => {
						this.logger.error(tag, 'loadStudyForms$', err);
						return of(studyFormActions.loadStudyFormsFailure({ error: err }));
					})
				);
			})
		)
	);

	constructor(
		private actions$: Actions,
		private service: EnumsService,
		private store: Store,
		private logger: NGXLogger
	) {}
}
