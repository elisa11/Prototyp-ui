import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { SnackEffects } from '@core/generics/snack.effects';
import { Store } from '@ngrx/store';
import { MatSnackBar } from '@angular/material/snack-bar';
import { resetCachedData } from '@core/store/actions/settings.actions';
import { catchError, map, mergeMap, tap } from 'rxjs/operators';
import { of } from 'rxjs';
import { loadUserDepartments } from '../actions/user-department-api.actions';
import { userDepartmentApiActions } from '../actions';
import { UserDepartmentsCacheService } from '@core/service/cache/user-departments-cache.service';

@Injectable()
export class UserDepartmentEffects extends SnackEffects {
	resetData$ = createEffect(
		() =>
			this.actions$.pipe(
				ofType(resetCachedData),
				tap(() => this.service.clearCache())
			),
		{ dispatch: false }
	);

	loadUserDepartments$ = createEffect(() =>
		this.actions$.pipe(
			ofType(loadUserDepartments),
			map(({ query }) => query),
			mergeMap((data) =>
				this.service.getUserDepartments(null, data.id, data).pipe(
					map((list) =>
						userDepartmentApiActions.loadUserDepartmentsSuccess({
							data: list,
							query: data,
						})
					),
					catchError((error) =>
						of(userDepartmentApiActions.loadUserDepartmentsFailure({ error }))
					)
				)
			)
		)
	);

	loadUserDepartmentsFailure$ = createEffect(
		() =>
			this.actions$.pipe(
				ofType(userDepartmentApiActions.loadUserDepartmentsFailure),
				tap((err) => this.openSnackbar(err.error))
			),
		{ dispatch: false }
	);

	constructor(
		private actions$: Actions,
		private store: Store,
		private service: UserDepartmentsCacheService,
		snack: MatSnackBar
	) {
		super(snack);
	}
}
