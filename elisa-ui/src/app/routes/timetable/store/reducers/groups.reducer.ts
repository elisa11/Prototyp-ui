import { createReducer, on } from '@ngrx/store';
import { Group } from '@elisa11/elisa-api-angular-client';
import { groupsActions } from '../actions';
import { createEntityAdapter, EntityAdapter, EntityState } from '@ngrx/entity';

export const groupsFeatureKey = 'groups';

export interface State extends EntityState<Group> {
  readonly selectedIds: number[];
  readonly count: number;
  readonly loading: boolean;
}

export const adapter: EntityAdapter<Group> = createEntityAdapter<Group>({
  selectId: (model) => model.id,
  sortComparer: false,
});

export const initialState = adapter.getInitialState({
  selectedIds: [],
  count: 0,
  loading: false,
});

export const reducer = createReducer(
  initialState,
  on(groupsActions.loadGroups, (state) =>({
    ...state,
    loading: true,
  })),
  on(groupsActions.loadGroupsSuccess, (state, {groups}) =>
    adapter.upsertMany(groups, {
    ...state,
    selectedIds: groups.map((x) => x.id),
    count: groups.length,
    loading: false,
  })),
  on(groupsActions.loadGroupsFailure, (state, { error }) => ({
    ...state,
    data: [],
    loading: false,
    error: error }))
);
export const getSelectedIds = (state: State) => state.selectedIds;
export const getLoading = (state: State) => state.loading;
export const getCount = (state: State) => state.count;

