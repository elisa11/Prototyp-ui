package sk.stuba.fei.elisa.elisauiapi.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.util.Map;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Config {

    private Map<String, Object> properties;
}
