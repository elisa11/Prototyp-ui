import { PaginatedDepartmentList } from '@elisa11/elisa-api-angular-client';
import { PaginatedGroupList } from '@elisa11/elisa-api-angular-client';

export interface AdvancedFilter {
	faculties: PaginatedDepartmentList;
	studyPlans: PaginatedGroupList;
	years: PaginatedGroupList;
	specializations: PaginatedGroupList;
}
