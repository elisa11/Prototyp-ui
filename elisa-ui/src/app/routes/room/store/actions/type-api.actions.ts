import { createAction, props } from '@ngrx/store';
import { RoomCategory } from '@elisa11/elisa-api-angular-client';

export const loadRoomTypeSuccess = createAction(
	'[Room - RoomType/Api] Search RoomTypes Success',
	props<{ data: RoomCategory[] }>()
);

export const loadRoomTypeFailure = createAction(
	'[Room - RoomType/Api] Search RoomTypes Failure',
	props<{ error: string }>()
);

export const selectRoomType = createAction(
	'[Room - RoomType/Api] Select RoomType',
	props<{ id: number }>()
);
