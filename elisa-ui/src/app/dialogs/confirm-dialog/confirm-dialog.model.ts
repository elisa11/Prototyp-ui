export interface ConfirmDialogModel {
	title: string;
	question: string;
	noLabel: string;
	yesLabel: string;
}
