import { Pipe, PipeTransform } from '@angular/core';
import { EmploymentEnum } from '@elisa11/elisa-api-angular-client';
import { marker } from '@biesbjerg/ngx-translate-extract-marker';

@Pipe({
	name: 'employment',
})
export class EmploymentPipe implements PipeTransform {
	transform(value: EmploymentEnum): string {
		switch (value) {
			case 'externy':
				return marker('user.detail.employment.external');
			case 'interny':
				return marker('user.detail.employment.internal');
			default:
				return '';
		}
	}
}
