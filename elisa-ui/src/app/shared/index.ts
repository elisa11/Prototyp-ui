import { BreadcrumbsComponent } from './components/breadcrumbs/breadcrumbs.component';
import { MatBadgeIconDirective } from './directives/mat-badge-icon.directive';
import { FlagPipe } from '@shared/pipes/flag.pipe';
import { FullnamePipe } from '@shared/pipes/user/fullname.pipe';
import { CollisionIconPipe } from '@shared/pipes/collision/collision-icon.pipe';
import { CollisionColorPipe } from '@shared/pipes/collision/collision-color.pipe';
import { IsStringEmptyPipe } from '@shared/pipes/is-string-empty.pipe';
import { GroupsNumberPipe } from '@shared/pipes/user/groups-number.pipe';
import { EmploymentPipe } from '@shared/pipes/user/employment.pipe';
import { RoomPipe } from '@shared/pipes/timetable/room.pipe';
import { HideElementPipe } from '@shared/pipes/hide-element.pipe';
import { MatDialogModule } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatCardModule } from '@angular/material/card';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { DepartmentNamePipe } from '@shared/pipes/department-name.pipe';
import { SchemaTranslatePipe } from '@shared/pipes/schema-translate.pipe';
import { ConnectFormDirective } from '@shared/directives/connect-form.directive';
import { RoomAutocompleteComponent } from './components/room-autocomplete/room-autocomplete.component';

export const MATERIAL_MODULES = [
	MatDialogModule,
	MatFormFieldModule,
	MatInputModule,
	MatCardModule,
	MatIconModule,
	MatButtonModule,
];

export const SHARED_COMPONENTS = [BreadcrumbsComponent, RoomAutocompleteComponent];

export const SHARED_DIRECTIVES = [MatBadgeIconDirective, ConnectFormDirective];

export const SHARED_PIPES = [
	FlagPipe,
	FullnamePipe,
	CollisionIconPipe,
	CollisionColorPipe,
	IsStringEmptyPipe,
	GroupsNumberPipe,
	EmploymentPipe,
	RoomPipe,
	HideElementPipe,
	SchemaTranslatePipe,
	DepartmentNamePipe,
];

export * from './components/breadcrumbs/breadcrumbs.component';
export * from '../routes/requirement/components/time-picker/time-picker.component';
export * from './directives/mat-badge-icon.directive';
export * from './pipes/flag.pipe';
export * from './pipes/user/fullname.pipe';
export * from './pipes/user/employment.pipe';
export * from './pipes/user/groups-number.pipe';
export * from './pipes/collision/collision-color.pipe';
export * from './pipes/collision/collision-icon.pipe';
export * from './pipes/is-string-empty.pipe';
export * from './directives/mat-badge-icon.directive';
export * from './pipes/timetable/room.pipe';
export * from './pipes/hide-element.pipe';
export * from './pipes/department-name.pipe';
