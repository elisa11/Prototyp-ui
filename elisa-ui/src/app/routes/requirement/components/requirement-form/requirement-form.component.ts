import {
	Component,
	EventEmitter,
	Input,
	NgZone,
	OnDestroy,
	OnInit,
	Output,
	QueryList,
	ViewChild,
	ViewChildren,
} from '@angular/core';
import { CommonText } from '@data/common-text';
import {
	AbstractControl,
	FormBuilder,
	FormGroup,
	FormGroupDirective,
	Validators,
} from '@angular/forms';
import {
	ActivityCategory,
	CourseSerializerFull,
	Department,
	Group,
	RequirementActivities,
	Room,
	RoomCategory,
	StateEnum,
	StudyForm,
	User,
	UserSubjectRole,
} from '@elisa11/elisa-api-angular-client';
import { Subject, Subscription } from 'rxjs';
import { DepartmentCourse } from '@data/model/department-course.model';
import { startWith, take, takeUntil } from 'rxjs/operators';
import { CdkTextareaAutosize } from '@angular/cdk/text-field';
import { TeacherSelectComponent } from '../teacher-select/teacher-select.component';
import { RequirementModel } from '../../model/requirement.model';
import { MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';
import { NGXLogger } from 'ngx-logger';
import moment from 'moment/moment';
import { ActivityModel } from '../../model/activity.model';
import { SubjectTeacherModel } from '../../model/subject-teacher.model';
import { GroupSelectComponent } from '../group-select/group-select.component';
import { RoomSelectionComponent } from '../room-selection/room-selection.component';
import { TimePickerComponent } from '../time-picker/time-picker.component';
import { RequirementPartModel } from '../../model/requirement-part.model';
import { Paths } from '@shared/store/reducers/form.reducer';
import { Store } from '@ngrx/store';
import {
	validateActivityDuration,
} from '../../validators/async-activity-duration.validator';

const tag = 'RequirementFormComponent';

@Component({
	selector: 'app-requirement-form',
	templateUrl: './requirement-form.component.html',
  styleUrls: ['./requirement-form.component.scss'],
})
export class RequirementFormComponent implements OnInit, OnDestroy {
	@ViewChild('autosize') autosize: CdkTextareaAutosize;
	@ViewChild('teacherSelect', { static: true }) teacherSelect: TeacherSelectComponent;
	@ViewChild('groupSelect', { static: true }) groupSelect: GroupSelectComponent;
	@ViewChildren(RoomSelectionComponent)
	roomSelections!: QueryList<RoomSelectionComponent>;
	@ViewChildren(TimePickerComponent) timePickers!: QueryList<TimePickerComponent>;
	requirementForm: FormGroup;
	days = CommonText.daysValues;
	eventType = CommonText.eventStatusValues;
	@Input()
	requirementExistsData!: RequirementModel | undefined;
	@Input()
	courseGroupOptions!: DepartmentCourse[];

	@Input() groupsData!: {
		filtered: Group[];
		data: Group[];
		loading: boolean;
	};
	@Input() activityData!: {
		data: ActivityModel[];
		categories: ActivityCategory[];
		studyForms: StudyForm[];
	};
	@Input() user!: User;
	@Input() teacherData!: {
		filtered: User[];
		types: UserSubjectRole[];
		data: SubjectTeacherModel[];
		loading: boolean;
	};
	@Input() loadingCourses!: boolean;
	@Input() roomData!: {
		filtered: Room[];
		types: RoomCategory[];
		loading: boolean;
	};
	controls: { [p: string]: AbstractControl };
	activities: ActivityModel[];
	path: Paths.requirement;
	private moduleId = 'requirement';
	moduleTitle = CommonText.navLinks.find((value) => value.link === this.moduleId).label;
	private unsubscribe$ = new Subject();
	@Output() private onCourseChanged = new EventEmitter<string>();
	@Output() private onTeacherChanged = new EventEmitter<string>();
	@Output() private onGroupChanged = new EventEmitter<string>();
	private courseGroupOptionsSub: Subscription;
	@Output() private onSubmit = new EventEmitter<RequirementModel>();
	@Output() private onPut = new EventEmitter<RequirementModel>();
	@Output() private onCancel = new EventEmitter<string>();
	@Output() private onBack = new EventEmitter<string>();
	@Output() private onCourseSelected = new EventEmitter<CourseSerializerFull>();
	@Output() private onCourseReset = new EventEmitter<void>();
	@Output() private onRoomFilterChange = new EventEmitter<{
		room: string;
		capacity: { min: number; max: number };
		type: RoomCategory[];
	}>();
	private errorSub: Subscription;

	constructor(
		private ngZone: NgZone,
		private fb: FormBuilder,
		private logger: NGXLogger,
		private store: Store
	) {}

	get formData(): any {
		return this.requirementForm.value;
	}

	courseDisplay(data: CourseSerializerFull) {
		return data && data.name && data.code ? `${data.name} ( ${data.code} )` : '';
	}

	ngOnDestroy(): void {
		this.unsubscribe$.next(this);
		this.unsubscribe$.complete();
	}

	triggerResize(): void {
		this.ngZone.onStable
			.pipe(take(1))
			.subscribe(() => this.autosize.resizeToFitContent(true));
	}

	ngOnInit(): void {
		const numberValidation = Validators.compose([Validators.required, Validators.min(0)]);
    if(this.requirementExistsData !== undefined && this.requirementExistsData) {
      this.requirementForm = this.fb.group({
        teachers: [this.requirementExistsData.requirement_teachers, Validators.required],
        course: [this.requirementExistsData.course, Validators.required],
        split: [
          undefined,
          Validators.compose([Validators.required]),
          validateActivityDuration(this.store),
        ],
        message: [undefined],
        groupsNum: [this.requirementExistsData.groups_count, numberValidation],
        studentsNum: [this.requirementExistsData.students_count, numberValidation],
        studentsNumExtra: [this.requirementExistsData.students_count_extra, numberValidation],
        maxParallel: [this.requirementExistsData.max_parallel_courses],
        oddWeek: [this.requirementExistsData.exercise_odd_week],
        afterLecture: [this.requirementExistsData.exercise_after_lecture],
        maxSeries: [this.requirementExistsData.max_serial_courses],
        created_by: [this.requirementExistsData.created_by]
      });
    }
    else {
      this.requirementForm = this.fb.group({
        teachers: [undefined, Validators.required],
        course: [undefined, Validators.required],
        split: [
          undefined,
          Validators.compose([Validators.required]),
          validateActivityDuration(this.store),
        ],
        message: [undefined],
        groupsNum: ['', numberValidation],
        studentsNum: ['', numberValidation],
        studentsNumExtra: ['', numberValidation],
        maxParallel: [''],
        oddWeek: [false],
        afterLecture: [false],
        maxSeries: [''],
      });
    }
		this.controls = this.requirementForm.controls;

		this.errorSub = this.requirementForm.valueChanges.subscribe((value) => {
			const errors = [];
			Object.keys(this.requirementForm.controls).forEach((key) => {
				// Get errors of every form control
				errors.push({ key, err: this.requirementForm.get(key).errors });
			});
			console.log(errors);
		});

		this.courseGroupOptionsSub = this.requirementForm
			.get(`course`)
			.valueChanges.pipe(startWith(''), takeUntil(this.unsubscribe$))
			.subscribe((value) => this.onCourseChanged.emit(value));
	}

	back(): void {
		return this.onBack.emit(this.moduleId);
	}

	onRequirementCancel(): void {
		this.logger.debug(tag, 'onRequirementCancel');
		this.onCancel.emit(this.moduleId);
    this.back();
  }

	submit(): void {
		const courseData = this.formData.course;
		const createdAt = moment().toISOString();

		const time = this.timePickers.map((c) => c);
		const rooms = this.roomSelections.map((c) => c);
		const parts = time.map((t) => {
			return {
				category: t.label,
				time: t.time,
				room: rooms.find((r) => r.label.id === t.label.id).formData.room,
			} as RequirementPartModel;
		});
		let maxParallelCourses = this.formData.maxParallel;
    if (maxParallelCourses == ""){
      maxParallelCourses = undefined;
    }
		const item = {
			id: 0,
			updated_at: createdAt,
			created_at: createdAt,
			created_by: this.user,
			department: courseData ? ({ ...courseData.department } as Department) : undefined,
			state: StateEnum.Created,
			requirement_teachers: this.teacherSelect.selectedTeachers,
			course: courseData,
			groups_count: this.formData.groupsNum,
			students_count: this.formData.studentsNum,
			students_count_extra: this.formData.studentsNumExtra,
			max_serial_courses: this.formData.maxSeries,
			requirement_activities: this.formData.split,
			max_parallel_courses: maxParallelCourses,
			exercise_after_lecture: this.formData.afterLecture,
			exercise_odd_week: this.formData.oddWeek,
			groups: this.groupSelect.selectedGroups,
			requirement_parts: parts,
		} as RequirementModel;
		if (this.formData.message && this.formData.message.length > 0) {
			item.requirement_comments = [
				{
					id: null,
					text: this.formData.message,
					created_by: this.user.id,
					created_at: createdAt,
					createdByUser: this.user,
				},
			];
		}
		this.logger.debug(tag, 'onSubmit', item);
		if (this.requirementExistsData!==undefined) {
      item.id = this.requirementExistsData.id;
      this.onPut.emit(item);
    }
		else{
		  this.onSubmit.emit(item);
		}
		this.back();
	}

	handleSplitChange(data: ActivityModel[]) {
		this.activities = data;
		this.requirementForm.patchValue({
			split: data.map((a) => {
				return {
					duration: a.duration,
					accepted: false,
					category: a.category.id,
				} as RequirementActivities;
			}),
		});
	}

	courseSelected(event: MatAutocompleteSelectedEvent) {
		this.onCourseSelected.emit(event.option.value);
	}

	handleTeacherChange(teacher: string) {
		this.onTeacherChanged.emit(teacher);
	}

	handleGroupChange(group: string) {
		this.onGroupChanged.emit(group);
	}

	handleRoomFilterChange(data: {
		room: string;
		capacity: { min: number; max: number };
		type: RoomCategory[];
	}) {
		this.onRoomFilterChange.emit(data);
	}

	resetCourse() {
		this.controls.course.reset();
		this.onCourseReset.emit();
	}

	resetForm(form: FormGroupDirective) {
		form.resetForm();
	}

	handleTeacherDataChange(data: SubjectTeacherModel[]) {
		this.requirementForm.patchValue({ teachers: data });
	}
}
