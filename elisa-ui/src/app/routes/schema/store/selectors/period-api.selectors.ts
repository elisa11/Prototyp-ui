import { createSelector } from '@ngrx/store';
import * as fromSearchPeriod from '../reducers/search-period.reducer';
import { selectSchemasState } from './schema.selectors';

export const selectPeriodEntitiesState = createSelector(
	selectSchemasState,
	(state) => state[fromSearchPeriod.searchPeriodFeatureKey]
);

export const selectSelectedPeriodIds = createSelector(
	selectPeriodEntitiesState,
	fromSearchPeriod.getIds
);

export const selectPeriodError = createSelector(
	selectPeriodEntitiesState,
	fromSearchPeriod.getError
);

export const selectPeriodLoading = createSelector(
	selectPeriodEntitiesState,
	fromSearchPeriod.getLoading
);

export const selectPeriodQuery = createSelector(
	selectPeriodEntitiesState,
	fromSearchPeriod.getQuery
);
