import {
	Activity,
	ActivityCategory,
	StudyMethod,
} from '@elisa11/elisa-api-angular-client';

export class ActivityModel {
	category: ActivityCategory;
	course: number | null;
	duration: number;
	readonly id: number;
	form: StudyMethod;

	constructor(unit: Activity, activityCategory?: ActivityCategory, form?: StudyMethod) {
		Object.assign(this, unit);
		this.category = activityCategory;
		this.form = form;
	}
}
