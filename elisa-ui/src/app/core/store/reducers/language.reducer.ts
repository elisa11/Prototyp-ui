import { createReducer, on } from '@ngrx/store';
import { loadLanguage } from '@core/store/actions/language.actions';

export const languageFeatureKey = 'language';

export interface LanguageState {
	readonly selectedLanguage: string;
}

export const initialState: LanguageState = {
	selectedLanguage: 'sk',
};

export const reducer = createReducer(
	initialState,
	on(
		loadLanguage,
		(state, { lang }) => ({ ...state, selectedLanguage: lang } as LanguageState)
	)
);

export const getLanguage = (state: LanguageState) => state.selectedLanguage;
