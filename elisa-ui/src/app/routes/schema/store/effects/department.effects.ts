import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import {
	loadDepartments,
	loadDepartmentsFailure,
	loadDepartmentsSuccess,
} from '../actions/department.actions';
import { DepartmentsCacheService } from '@core/service/cache/departments-cache.service';
import { catchError, map, switchMap, tap, withLatestFrom } from 'rxjs/operators';
import { of, zip } from 'rxjs';
import { resetCachedData } from '@core/store/actions/settings.actions';
import { loadPeriodsFailure } from '../actions/period.actions';
import { MatSnackBar } from '@angular/material/snack-bar';
import { searchPeriodsSuccess } from '../actions/period-api.actions';
import { NGXLogger } from 'ngx-logger';
import { SnackEffects } from '@core/generics/snack.effects';
import { Store } from '@ngrx/store';
import { schemaSelectors } from '@core/store/selectors';

const tag = 'DepartmentEffects';

@Injectable()
export class DepartmentEffects extends SnackEffects {
	resetData$ = createEffect(
		() =>
			this.actions$.pipe(
				ofType(resetCachedData),
				tap(() => this.service.clearCache())
			),
		{ dispatch: false }
	);

	searchPeriodsSuccess$ = createEffect(() =>
		this.actions$.pipe(
			ofType(searchPeriodsSuccess),
			map((value) => value.periods.results.map((p) => p.department)),
			map((ids) => loadDepartments({ ids }))
		)
	);

	// todo: z nejakeho pozoruhodneho dovodu neemituje forkjoin
	loadDepartments$ = createEffect(() =>
		this.actions$.pipe(
			ofType(loadDepartments),
			map((action) => [...new Set(action.ids)]),
			tap((x) => this.logger.debug(tag, 'loadDepartments$', x)),
			withLatestFrom(this.store.select(schemaSelectors.selectScheme)),
			map(([action, scheme]) => {
				console.log(tag, 'loadDepartments$', scheme);

				return { ids: action, version: scheme.name };
			}),
			switchMap(({ ids, version }) =>
				zip(...ids.map((id) => this.service.getDepartment(version, id))).pipe(
					tap((x) => this.logger.debug(tag, 'loadDepartments$ inside pipe', x)),
					map((data) => loadDepartmentsSuccess({ data })),
					catchError((error) => of(loadDepartmentsFailure({ error })))
				)
			)
		)
	);

	loadDepartmentsFailure$ = createEffect(
		() =>
			this.actions$.pipe(
				ofType(loadPeriodsFailure),
				tap((err) => this.openSnackbar(err.error))
			),
		{ dispatch: false }
	);

	constructor(
		private actions$: Actions,
		private service: DepartmentsCacheService,
		snack: MatSnackBar,
		private logger: NGXLogger,
		private store: Store
	) {
		super(snack);
	}
}
