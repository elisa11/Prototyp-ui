import { createAction, props } from '@ngrx/store';
import { PaginatedRoomList } from '@elisa11/elisa-api-angular-client';
import { RoomFilterModel } from '../../../room/model/room-filter.model';

export const loadRooms = createAction(
	'[Requirement - Room/Api] Load  Rooms',
	props<{ search: string; roomFilter: RoomFilterModel }>()
);

export const getRoomsByRequirement = createAction(
	'[Requirement - Room/Api] Get Rooms by Requirement',
	props<{ ids: number[] }>()
);

export const loadRoomsSuccess = createAction(
	'[Requirement - Room/Api] Load  Rooms Success',
	props<{ data: PaginatedRoomList }>()
);

export const loadRoomsFailure = createAction(
	'[Requirement - Room/Api] Load  Rooms Failure',
	props<{ error: any }>()
);
