import { createReducer, on } from '@ngrx/store';
import { User } from '@elisa11/elisa-api-angular-client';
import { createEntityAdapter, EntityAdapter, EntityState } from '@ngrx/entity';
import { searchUsersSuccess } from '../actions/user-api.actions';
import { UserActions, UserApiActions } from '../actions';

export const userFeatureKey = 'users';

export interface State extends EntityState<User> {
	readonly selectedUserId: number;
	readonly count: number;
}

export const adapter: EntityAdapter<User> = createEntityAdapter<User>({
	selectId: (model: User) => model.id,
	sortComparer: false,
});
export const initialState: State = adapter.getInitialState({
	selectedUserId: null,
	count: 0,
});

export const reducer = createReducer(
	initialState,
	on(searchUsersSuccess, (state, { users }) =>
		adapter.upsertMany(users.results, {
			...state,
			count: users.count,
		})
	),
	on(UserActions.loadUser, (state, { user }) => adapter.upsertOne(user, state)),
	on(UserActions.selectUser, (state, { id }) => ({ ...state, selectedUserId: id })),
  on(UserApiActions.deleteUserDone, (state, { id }) =>
    adapter.removeOne(id, state))
);

export const getSelectId = (state: State) => state.selectedUserId;
export const getCount = (state: State) => state.count;
