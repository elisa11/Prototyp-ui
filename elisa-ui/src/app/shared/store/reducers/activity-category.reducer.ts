import { createReducer, on } from '@ngrx/store';
import { createEntityAdapter, EntityAdapter, EntityState } from '@ngrx/entity';
import { ActivityCategory } from '@elisa11/elisa-api-angular-client';
import { activityCategoryActions } from '@shared/store/actions';

export const activityCategoryFeatureKey = 'activityCategory';

export interface State extends EntityState<ActivityCategory> {
	count: 0;
	loading: false;
}

export const adapter: EntityAdapter<ActivityCategory> =
	createEntityAdapter<ActivityCategory>({
		selectId: (model: ActivityCategory) => model.id,
		sortComparer: false,
	});

export const initialState = adapter.getInitialState({
	count: 0,
	loading: false,
});

export const reducer = createReducer(
	initialState,
	on(activityCategoryActions.loadActivityCategories, (state) => ({
		...state,
		loading: true,
	})),
	on(activityCategoryActions.loadActivityCategoriesFailure, (state) => ({
		...state,
		loading: false,
	})),
	on(activityCategoryActions.loadActivityCategoriesSuccess, (state, { data }) =>
		adapter.upsertMany(data.results, { ...state, count: data.count, loading: false })
	)
);

export const getCount = (state) => state.count;
export const getLoading = (state) => state.loading;
export const isEmpty = (state) => state.count === 0;
