import { Pipe, PipeTransform } from '@angular/core';
import { marker } from '@biesbjerg/ngx-translate-extract-marker';
import { LocaleService } from '@core/service/locale/locale.service';

@Pipe({
	name: 'detailTitle',
})
export class DetailTitlePipe implements PipeTransform {
	constructor(private translate: LocaleService) {}

	transform(value: number, ...args: unknown[]): unknown {
		if (value) {
			return `${this.translate.translate(
				marker('requirement.detail.title.number')
			)} ${value}`;
		}
		return this.translate.translate(marker('requirement.detail.title.new'));
	}
}
