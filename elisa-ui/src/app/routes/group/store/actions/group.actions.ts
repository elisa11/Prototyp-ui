import { createAction, props } from '@ngrx/store';
import { Group } from '@elisa11/elisa-api-angular-client';

export const loadGroup = createAction(
	'[Group - Group] Load Group',
	props<{ data: Group }>()
);

export const selectGroup = createAction(
	'[Group - Group] Select Group',
	props<{ id: number }>()
);
