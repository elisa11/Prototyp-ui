import { Injectable } from '@angular/core';
import { PaginatorFilter } from '@data/filter/paginator.filter';
import { Observable } from 'rxjs';
import {
	PaginatedPeriodList,
	Period,
	PeriodsService,
} from '@elisa11/elisa-api-angular-client';
import { BasicUtils } from '@shared/utils/basic.utils';
import { shareReplay } from 'rxjs/operators';
import { NGXLogger } from 'ngx-logger';
import { AbstractCacheService } from '@core/service/cache/abstract-cache.service';

const tag = 'PeriodCacheService';

@Injectable({
	providedIn: 'root',
})
export class PeriodCacheService extends AbstractCacheService {
	private listPeriodsCache = new Map();
	private getPeriodCache = new Map();

	constructor(private logger: NGXLogger, private service: PeriodsService) {
		super();
	}

	getPeriod(id: number): Observable<Period> {
		this.logger.debug(tag, 'getPeriod', id);
		const cached = this.getPeriodCache.get(id);
		if (cached) {
			this.logger.debug(tag, 'getPeriod cached', id);
			return cached;
		}

		const response = this.service
			.periodsRetrieve(id)
			.pipe(shareReplay({ bufferSize: 1, refCount: true }));
		this.getPeriodCache.set(id, response);
		return response;
	}

	listPeriods(filter: PaginatorFilter): Observable<PaginatedPeriodList> {
		const cached = this.listPeriodsCache.get(filter);
		if (cached) {
			return cached;
		}

		const response = this.service
			.periodsList(
				filter.limit,
				filter.offset,
				BasicUtils.getSortBy(filter),
				filter.search
			)
			.pipe(shareReplay({ bufferSize: 1, refCount: true }));
		this.listPeriodsCache.set(filter, response);
		return response;
	}

	clearCache(): void {
		this.listPeriodsCache.clear();
		this.getPeriodCache.clear();
	}
}
