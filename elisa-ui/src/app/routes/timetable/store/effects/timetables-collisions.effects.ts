import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { SnackEffects } from '@core/generics/snack.effects';
import { Store } from '@ngrx/store';
import { MatSnackBar } from '@angular/material/snack-bar';
import { TimetableDataService } from '../../services/timetable-data.service';
import { TimetablesService } from '@elisa11/elisa-api-angular-client';
import { timetablesCollisionsActions } from '../actions';
import { catchError, map, switchMap, withLatestFrom } from 'rxjs/operators';
import { schemaSelectors } from '@core/store/selectors';
import { of } from 'rxjs';
import {
	loadTimetableCollisionFailure,
	loadTimetableCollisionsFailure,
	loadTimetableCollisionsSuccess,
	loadTimetableCollisionSuccess,
	updateCollisionFailure,
	updateCollisionSuccess,
} from '../actions/timetables-collisions.actions';
import { timetablesSelector } from '../selectors';
import { timetableSelected } from '../actions/timetables.actions';
// const tag = 'TimetablesEffects';

@Injectable()
export class TimetablesCollisionsEffects extends SnackEffects {
	loadTimetableCollisions$ = createEffect(() =>
		this.actions$.pipe(
			ofType(timetablesCollisionsActions.loadTimetableCollisions),
			withLatestFrom(
				this.store.select(schemaSelectors.selectScheme),
				this.store.select(timetablesSelector.selectTimetable)
			),
			map((value) => {
				return {
					scheme: value[1].name,
					tt: value[2],
				};
			}),
			switchMap((values) =>
				this.timetableDataService.getAllCollisions(values.scheme, values.tt).pipe(
					map((collisionList) =>
						loadTimetableCollisionsSuccess({
							collisions: collisionList.results,
						})
					),
					catchError((error) => of(loadTimetableCollisionsFailure({ error })))
				)
			)
		)
	);
	loadTimetableCollision$ = createEffect(() =>
		this.actions$.pipe(
			ofType(timetablesCollisionsActions.loadTimetableCollision),
			withLatestFrom(
				this.store.select(schemaSelectors.selectScheme),
				this.store.select(timetablesSelector.selectTimetable)
			),
			map((value) => {
				return {
					scheme: value[1].name,
					tt: value[2],
					coll_id: value[0].collision,
				};
			}),
			switchMap((values) =>
				this.timetableDataService
					.getCollision(values.scheme, values.tt, values.coll_id)
					.pipe(
						map((collisionDb) =>
							loadTimetableCollisionSuccess({
								collision: collisionDb,
							})
						),
						catchError((error) => of(loadTimetableCollisionFailure({ error })))
					)
			)
		)
	);

	updateCollision$ = createEffect(() =>
		this.actions$.pipe(
			ofType(timetablesCollisionsActions.updateCollision),
			withLatestFrom(
				this.store.select(schemaSelectors.selectScheme),
				this.store.select(timetablesSelector.selectTimetable)
			),
			map((value) => {
				return {
					scheme: value[1].name,
					timetable: value[2],
					collision: value[0].collision,
				};
			}),
			switchMap((values) => {
				return this.timetableService
					.timetablesCollisionsPartialUpdate(
						values.scheme,
						values.collision.id,
						values.timetable.id,
						values.collision
					)
					.pipe(
						map((collision) =>
							updateCollisionSuccess({
								collision: collision,
							})
						),
						catchError((error) => of(updateCollisionFailure({ error: error })))
					);
			})
		)
	);

	updateCollisionSuccess$ = createEffect(
		() =>
			this.actions$.pipe(
				ofType(updateCollisionSuccess),
				map(() => {
					this.openSnackbar('Kolízia bola upravená.');
				})
			),
		{ dispatch: false }
	);

	updateCollisionFailure$ = createEffect(
		() =>
			this.actions$.pipe(
				ofType(updateCollisionFailure),
				map(() => {
					this.openSnackbar('Nastala chyba pri upravovaní kolízie.');
					// this.openSnackbar(this.localeService.translate(marker('timetable-events.put.error')))
				})
			),
		{ dispatch: false }
	);

	constructor(
		private actions$: Actions,
		private store: Store,
		private timetableDataService: TimetableDataService,
		private timetableService: TimetablesService,
		snack: MatSnackBar
	) {
		super(snack);
	}
}
