import { createSelector } from '@ngrx/store';
import { selectTimetablesState } from './timetables.selectors';
import * as fromTimetablesCollisions from '../reducers/timetables-collisions.reducer';
import { Collision } from '@elisa11/elisa-api-angular-client';

export const selectTimetablesCollisionsState = createSelector(
	selectTimetablesState,
	(s) => s[fromTimetablesCollisions.timetablesCollisionsFeatureKey]
);

export const selectSelectedIds = createSelector(
	selectTimetablesCollisionsState,
	fromTimetablesCollisions.getSelectedIds
);
export const selectTotalCount = createSelector(
	selectTimetablesCollisionsState,
	fromTimetablesCollisions.getCount
);
export const selectSearchLoading = createSelector(
	selectTimetablesCollisionsState,
	fromTimetablesCollisions.getLoading
);

export const { selectIds, selectEntities } =
	fromTimetablesCollisions.adapter.getSelectors(selectTimetablesCollisionsState);

export const selectSelectedResults = createSelector(
	selectEntities,
	selectSelectedIds,
	(data, ids) => {
		return ids.map((id) => data[id]).filter((d): d is Collision => d != null);
	}
);
export const selectAllTimetableCollisions = createSelector(
	selectEntities,
	selectSelectedIds,
	(data, ids) => {
		return ids.map((id) => data[id]);
	}
);

export const selectSearchTimetableCollisions = createSelector(
	selectEntities,
	selectSelectedIds,
	(data, ids) => {
		return ids.map((id) => data[id]).filter((e): e is Collision => e != null);
	}
);

export const selectTimetablesCollisionsByEventId = (eventId: number) =>
	createSelector(selectEntities, selectSelectedIds, (data, ids) => {
		return ids
			.map((id) => data[id])
			.filter(
				(e): e is Collision =>
					(e != null && e.first_event.id === eventId) || e.second_event.id === eventId
			);
	});
