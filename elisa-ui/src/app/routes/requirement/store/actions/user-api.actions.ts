import { createAction, props } from '@ngrx/store';
import { User } from '@elisa11/elisa-api-angular-client';

export const loadUsers = createAction(
	'[Requirement - User/Api] Load Users',
	props<{ ids: number[] }>()
);

export const loadUsersSuccess = createAction(
	'[Requirement - User/Api] Load Users Success',
	props<{ data: User[] }>()
);

export const loadUsersFailure = createAction(
	'[Requirement - User/Api] Load Users Failure',
	props<{ error: any }>()
);
