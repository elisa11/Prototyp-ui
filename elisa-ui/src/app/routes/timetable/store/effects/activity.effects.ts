import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { SnackEffects } from '@core/generics/snack.effects';
import { Store } from '@ngrx/store';
import { MatSnackBar } from '@angular/material/snack-bar';
import {
  catchError,
  map,
  switchMap,
  withLatestFrom,
} from 'rxjs/operators';
import { activityActions, groupsActions } from '../actions';
import { TimetableDataService } from '../../services/timetable-data.service';
import { loadGroupsFailure, loadGroupsSuccess } from '../actions/groups.actions';
import { from, of } from 'rxjs';
import { schemaSelectors } from '@core/store/selectors';
import { loadActivitiesFailure, loadActivitiesSuccess } from '../actions/activity.actions';

const tag = 'ActivityEffects';

@Injectable()
export class ActivityEffects extends SnackEffects {
  loadActivities$ = createEffect(()=>
    this.actions$.pipe(
      ofType(activityActions.loadActivities),
      withLatestFrom(this.store.select(schemaSelectors.selectScheme)),
      map((value)=> {
          return {scheme: value[1].name}
        }
      ),
      switchMap((scheme)=>
        from(this.timetableService.listActivities(scheme.scheme).pipe(
          map((activityList) => loadActivitiesSuccess({
            activities: activityList.results
          })),
          catchError((error)=>of(loadActivitiesFailure({error})))
        ))
      )
    )
  );

  constructor(
    private actions$: Actions,
    private store: Store,
    private timetableService: TimetableDataService,
    snack: MatSnackBar
  ) {
    super(snack);
  }
}
