import {
	ChangeDetectorRef,
	Component,
	EventEmitter,
	Input,
	OnChanges,
	OnInit,
	Output,
	SimpleChanges,
} from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { first, Observable, tap, shareReplay } from 'rxjs';
import { Collision, CollisionStatusEnum, Event } from '@elisa11/elisa-api-angular-client';
import { Activity, EventWrapper } from '../../../model/activity.model';
import { TimetableCollisionDialogComponent } from './timetable-collision-dialog/timetable-collision-dialog.component';
import { CollisionService } from '../../../services/collision.service';
import { TimetableEventOptionsDialogComponent } from './timetable-event-options-dialog/timetable-event-options-dialog.component';
import { Store } from '@ngrx/store';
import { requirementActions } from '../../../store/actions';
import { putTimetableEvent } from '../../../store/actions/timetables-events.actions';
import {
	selectTimetablesCollisionsByEventId,
	selectTimetablesCollisionsState,
} from '../../../store/selectors/timetables-collisions.selectors';

@Component({
	selector: 'app-timetable-event-item',
	templateUrl: './timetable-event-item.component.html',
	styleUrls: ['./timetable-event-item.component.scss'],
})
export class TimetableEventItemComponent implements OnInit {
	@Input() event: EventWrapper;
	@Output() eventChange = new EventEmitter<EventWrapper>();

	color: string = 'blue';
	constructor(
		private matDialog: MatDialog,
		private store: Store,
		private cdr: ChangeDetectorRef
	) {}
	isErrorCollision = false;
	collisions$: Observable<Collision[]>;

	ngOnInit(): void {
		this.getColorFromActivityCourseCode();
		this.collisions$ = this.store
			.select(selectTimetablesCollisionsByEventId(this.event.event.id))
			.pipe(shareReplay({ refCount: true, bufferSize: 1 }));

		this.collisions$.subscribe((collisions) => {
			this.calcIsErrorCollision(collisions);
		});
	}

	getColorFromActivityCourseCode() {
		this.color =
			'hsl(' +
			((this.event.event.activity.course * 20) % 360) +
			', ' +
			(100 - this.event.event.activity.category * 5) +
			'%, ' +
			(40 + this.event.event.activity.category * 5) +
			'%)';
	}

	calcIsErrorCollision(collisions: Collision[]) {
		this.isErrorCollision =
			collisions.length > 0 &&
			collisions.find((collision) => collision.status === CollisionStatusEnum.Error) !==
				undefined;
		this.cdr.detectChanges();
	}

	openCollision($event) {
		$event.stopPropagation();
		const dialogRef = this.matDialog.open(TimetableCollisionDialogComponent, {
			data: {
				collisions$: this.collisions$,
			},
			disableClose: false,
		});

		dialogRef
			.afterClosed()
			.pipe(first())
			.subscribe((result) => {
				if (result) {
				}
			});
	}

	openEventOptionsDialog() {
		this.store.dispatch(
			requirementActions.setSelectedRequirementByCourse({
				courseId: this.event.event.activity.course,
			})
		);

		const dialogRef = this.matDialog.open(TimetableEventOptionsDialogComponent, {
			data: {
				event: this.event,
			},
			disableClose: false,
		});

		dialogRef
			.afterClosed()
			.pipe(first())
			.subscribe((result) => {
				if (result) {
					let newEvent = {
						id: this.event.event.id,
						rooms: [result.id],
					} as Event;
					this.store.dispatch(
						putTimetableEvent({
							event: newEvent,
						})
					);
				}
			});
	}
}
