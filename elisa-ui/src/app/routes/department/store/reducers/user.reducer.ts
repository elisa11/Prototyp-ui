import { createReducer, on } from '@ngrx/store';
import { User } from '@elisa11/elisa-api-angular-client';
import { createEntityAdapter, EntityAdapter, EntityState } from '@ngrx/entity';
import { departmentActions, userApiActions } from '../actions';

export type Users = { id: number; data: User[] };
export const userFeatureKey = 'user';

export interface State extends EntityState<Users> {
	readonly selectedDepartmentId: number | null;
}

export const adapter: EntityAdapter<Users> = createEntityAdapter<Users>({
	selectId: (ud: Users) => ud.id,
	sortComparer: false,
});

export const initialState: State = adapter.getInitialState({
	selectedDepartmentId: null,
});

export const reducer = createReducer(
	initialState,
	on(departmentActions.selectDepartment, (state: State, { id }) => ({
		...state,
		selectedDepartmentId: id,
	})),
	on(userApiActions.loadUsersSuccess, (state: State, { data }) =>
		adapter.upsertOne(data, state)
	)
);

export const getSelectedDepartmentId = (state: State) => state.selectedDepartmentId;
