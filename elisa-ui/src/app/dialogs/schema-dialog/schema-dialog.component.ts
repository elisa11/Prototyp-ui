import { Component, Inject, OnInit } from '@angular/core';
import { PaginatedVersionList, Version } from '@elisa11/elisa-api-angular-client';
import { FormControl, Validators } from '@angular/forms';
import { NGXLogger } from 'ngx-logger';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';

const tag = 'SchemaDialogComponent';

@Component({
	selector: 'app-schema-dialog',
	templateUrl: './schema-dialog.component.html',
})
export class SchemaDialogComponent implements OnInit {
	selected = new FormControl(null, { validators: [Validators.required] });
	versions: Version[] = [];

	constructor(
		private logger: NGXLogger,
		private dialogRef: MatDialogRef<SchemaDialogComponent>,
		@Inject(MAT_DIALOG_DATA) public data: PaginatedVersionList
	) {}

	ngOnInit(): void {
		if (this.data.count <= 0) {
			this.logger.debug(tag, 'closing, no data');
			this.addVersion();
		}
		this.versions = this.data.results;
	}

	get result(): { redirect: boolean; version: Version } {
		return { redirect: false, version: this.selected.value as Version };
	}

	addVersion() {
		this.dialogRef.close({ redirect: true, version: null });
	}
}
