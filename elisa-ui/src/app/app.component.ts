import { Component, HostBinding } from '@angular/core';
import skLanguage from './../assets/i18n/sk.json';
import enLanguage from './../assets/i18n/en.json';
import { Router } from '@angular/router';
import { NGXLogger } from 'ngx-logger';
import { environment } from '@environment/environment';
import { LocaleService } from '@core/service/locale/locale.service';
import { Store } from '@ngrx/store';
import { AppVersionCheckService } from '@core/service/app-version-check.service';
import { appActions } from '@core/store/actions';
import { FormControl } from '@angular/forms';
import { OverlayContainer } from '@angular/cdk/overlay';

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.scss'],
})
export class AppComponent {
	title = 'Elisa';

	constructor(
		private versionControl: AppVersionCheckService,
		private localeService: LocaleService,
		private router: Router,
		private logger: NGXLogger,
    private store: Store,
	) {
		logger.updateConfig({
			level: environment.log.level,
			serverLogLevel: environment.log.serverLogLevel,
		});

		localeService.initLocale('sk', skLanguage);
		localeService.setTranslation('en', enLanguage);

		this.store.dispatch(appActions.initApp());
	}
}
