import { createReducer, on } from '@ngrx/store';
import { PaginatorFilter } from '@data/filter/paginator.filter';
import { courseApiActions } from '../actions';

export const searchCourseFeatureKey = 'searchCourse';

export interface SearchState {
	readonly ids: number[];
	readonly loading: boolean;
	readonly error: string;
	readonly query: PaginatorFilter;
}

export const initialState: SearchState = {
	ids: [],
	loading: false,
	error: '',
	query: {} as PaginatorFilter,
};

export const reducer = createReducer(
	initialState,
	on(courseApiActions.searchCourse, (state, { query }) => {
		return query === null
			? {
					ids: [],
					loading: false,
					error: '',
					query,
			  }
			: {
					...state,
					loading: true,
					error: '',
					query,
			  };
	}),
	on(courseApiActions.searchCourseSuccess, (state, { data }) => ({
		ids: data.results.map((user) => user.id),
		loading: false,
		error: '',
		query: state.query,
	})),
	on(courseApiActions.searchCourseFailure, (state, { error }) => ({
		...state,
		loading: false,
		error: error,
	}))
);

export const getIds = (state: SearchState) => state.ids;

export const getQuery = (state: SearchState) => state.query;

export const getLoading = (state: SearchState) => state.loading;

export const getError = (state: SearchState) => state.error;
