import {
	Component,
	EventEmitter,
	Input,
	NgZone,
	OnDestroy,
	OnInit,
	Output,
	ViewChild,
} from '@angular/core';
import {
	CourseSerializerFull,
	Equipment,
	RoomEquipment,
} from '@elisa11/elisa-api-angular-client';
import { Subject, Subscription } from 'rxjs';
import { CdkTextareaAutosize } from '@angular/cdk/text-field';
import { Paths } from '@shared/store/reducers/form.reducer';
import { take } from 'rxjs/operators';
import { CommonText } from '@data/common-text';
import {
	AbstractControl,
	FormBuilder,
	FormGroup,
	FormGroupDirective,
	Validators,
} from '@angular/forms';
import { NGXLogger } from 'ngx-logger';
import { Store } from '@ngrx/store';
import { RoomModel } from '../../model/room.model';
import { RoomEquipmentModel } from '../../model/room-equipment.model';
import { Department } from '@elisa11/elisa-api-angular-client';
import { RoomCategory } from '@elisa11/elisa-api-angular-client';
import { EquipmentModel } from '../../model/equipment.model';

const tag = 'RoomFormComponent';

@Component({
	selector: 'app-room-form',
	templateUrl: './room-form.component.html',
})
export class RoomFormComponent implements OnInit, OnDestroy {
	@ViewChild('autosize') autosize: CdkTextareaAutosize;
	roomForm: FormGroup;
	controls: { [p: string]: AbstractControl };
	path: Paths.room;
	private moduleId = 'room';
	moduleTitle = CommonText.navLinks.find((value) => value.link === this.moduleId).label;
	private unsubscribe$ = new Subject();
	@Input()
	roomExistsData!: RoomModel | undefined;

	// @Output() private onSubmit = new EventEmitter<Room>();
	//TODO: prerobiť na roomModel z Room
	@Output() private onSubmit = new EventEmitter<RoomModel>();
	@Output() private onPut = new EventEmitter<RoomModel>();
	@Output() private onCancel = new EventEmitter<string>();
	@Output() private onBack = new EventEmitter<string>();
	private errorSub: Subscription;
	private equipments: Equipment[];
	private equipmentModels: RoomEquipmentModel[];

	constructor(
		private ngZone: NgZone,
		private fb: FormBuilder,
		private logger: NGXLogger,
		private store: Store
	) {}

	@Input() equipmentData!: EquipmentModel[];

	@Input() set allEquipments(all: Equipment[]) {
		//TODO: upraviť pre správne fungovanie
		const selectedEquipments = new Set<Equipment>(
			this.equipments.map((equip) => {
				return equip;
			})
		);
		this.equipments = all.filter((x) => !selectedEquipments.has(x));
	}

	get formData(): any {
		return this.roomForm.value;
	}

	courseDisplay(data: CourseSerializerFull) {
		return data && data.name && data.code ? `${data.name} ( ${data.code} )` : '';
	}

	ngOnDestroy(): void {
		this.unsubscribe$.next(this);
		this.unsubscribe$.complete();
	}

	triggerResize(): void {
		this.ngZone.onStable
			.pipe(take(1))
			.subscribe(() => this.autosize.resizeToFitContent(true));
	}

	ngOnInit(): void {
		const numberValidation = Validators.compose([Validators.required, Validators.min(0)]);
		if (this.roomExistsData !== undefined && this.roomExistsData) {
			this.roomForm = this.fb.group({
				roomName: [this.roomExistsData.name],
				capacityInRoom: [this.roomExistsData.capacity, numberValidation],
				departmentForRoom: [this.roomExistsData.department.name, numberValidation],
				typeOfRoom: [this.roomExistsData.type.name, numberValidation],
			});
		} else {
			this.roomForm = this.fb.group({
				roomName: ['', Validators.required],
				capacityInRoom: ['', numberValidation],
				departmentForRoom: ['', numberValidation],
				typeOfRoom: ['', numberValidation],
			});
		}
		this.controls = this.roomForm.controls;
		this.errorSub = this.roomForm.valueChanges.subscribe((value) => {
			const errors = [];
			Object.keys(this.roomForm.controls).forEach((key) => {
				// Get errors of every form control
				errors.push({ key, err: this.roomForm.get(key).errors });
			});
			console.log(errors);
		});
	}

	back(): void {
		return this.onBack.emit(this.moduleId);
	}

	onRoomCancel(): void {
		this.logger.debug(tag, 'onRoomCancel');
		this.onCancel.emit(this.moduleId);
		this.back();
	}

	submit(): void {
		//TODO: upraviť IDčka, aby zodpovedali department a type a sfunkčniť post a put
		let dep: Department = {
			id: 0,
			name: this.formData.departmentForRoom,
		};
		let type: RoomCategory = {
			id: 0,
			name: this.formData.typeOfRoom,
		};
		let item: RoomModel = {
			capacity: this.formData.capacityInRoom,
			department: dep,
			equipment: [],
			id: 0,
			name: this.formData.roomName,
			type: type,
		};
		this.logger.debug(tag, 'onSubmit');
		if (this.roomExistsData !== undefined) {
			item.id = this.roomExistsData.id;
			this.onPut.emit(item);
		} else {
			this.onSubmit.emit(item);
		}
		this.back();
	}

	resetForm(form: FormGroupDirective) {
		form.resetForm();
	}

	handleSplitChange(data: RoomEquipmentModel[]) {
		//TODO: upraviť pre správne fungovanie
		this.equipmentModels = data;
		this.roomForm.patchValue({
			split: data.map((a) => {
				return {
					count: a.countEquipment,
					equipment: a.facilityEquipment,
				} as RoomEquipment;
			}),
		});
	}
}
