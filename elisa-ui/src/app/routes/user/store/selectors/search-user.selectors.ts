import { createSelector } from '@ngrx/store';
import { selectUserEntities, selectUsersState } from './user.selectors';
import {
	getError,
	getIds,
	getLoading,
	getQuery,
	searchFeatureKey,
} from '../reducers/search.reducer';
import { User } from '@elisa11/elisa-api-angular-client';

export const selectSearchUserState = createSelector(
	selectUsersState,
	(state) => state[searchFeatureKey]
);

export const selectSearchUserIds = createSelector(selectSearchUserState, getIds);
export const selectSearchUserQuery = createSelector(selectSearchUserState, getQuery);
export const selectSearchUserLoading = createSelector(selectSearchUserState, getLoading);
export const selectSearchUserError = createSelector(selectSearchUserState, getError);

export const selectSearchUserResults = createSelector(
	selectUserEntities,
	selectSearchUserIds,
	(users, searchIds) => {
		return searchIds.map((id) => users[id]).filter((user): user is User => user != null);
	}
);
