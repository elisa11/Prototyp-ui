import { Component, ElementRef, HostBinding, OnDestroy, ViewChild } from '@angular/core';
import { MenuItem } from '@core/model/menu-item.model';
import { isNotNullOrUndefined } from 'codelyzer/util/isNotNullOrUndefined';
import { Router } from '@angular/router';
import { combineLatest, Subject, Subscription } from 'rxjs';
import { map, take, takeUntil, throttleTime } from 'rxjs/operators';
import { NGXLogger } from 'ngx-logger';
import { TokenPayload } from '../../auth/store/model/token.model';
import { AuthService } from '../../auth/services/auth.service';
import { SettingsService } from '@core/service/settings.service';
import { LocaleService } from '@core/service/locale/locale.service';
import { Store } from '@ngrx/store';
import { selectLoggedIn, selectPayload } from '../../auth/store/selectors/auth.selectors';
import { logout } from '../../auth/store/actions/auth.actions';
import { loadLanguage } from '@core/store/actions/language.actions';
import { selectLanguage } from '@core/store/selectors/language.selectors';
import {
	selectAppVersion,
	selectMenuItems,
} from '@core/store/selectors/layout.selectors';
import { loadSchemasDialog } from '@core/store/actions/schema.actions';
import { schemaSelectors } from '@core/store/selectors';
import { FormControl } from '@angular/forms';
import { OverlayContainer } from '@angular/cdk/overlay';

const tag = 'ToolbarComponent';

@Component({
	selector: 'app-toolbar',
	templateUrl: './toolbar.component.html',
})
export class ToolbarComponent implements OnDestroy {
	title = 'Elisa';
	refreshTimerSub: Subscription;
	settingsSub: Subscription;
	tokenSub: Subscription;
	unsubscribe: Subject<void> = new Subject<void>();

	constructor(
		private router: Router,
		private authService: AuthService,
		private settingsService: SettingsService,
		private localeService: LocaleService,
		private logger: NGXLogger,
		private store: Store,
    private overlay: OverlayContainer
  ) {
		this.tokenSub = this.store
			.select<TokenPayload>(selectPayload)
			.pipe(takeUntil(this.unsubscribe), throttleTime(250))
			.subscribe((token) => {
				try {
					this.logger.debug(tag, 'access token', authService.token);
					this.updateByPayload(token);
				} catch (e) {
					this.logger.debug(tag, 'No token found');
				}
			});

		// todo
		// this.versionSub = this.versionService.version$
		//   .pipe(takeUntil(this.unsubscribe), throttleTime(250))
		//   .subscribe((value) => {
		//     this.logger.debug(tag, 'version change');
		//     this.scheme = value ? value.name ?? this.defaultScheme : this.defaultScheme;
		//     this.menuItems = [];
		//     for (const link of this.navLinksMap) {
		//       if (link) {
		//         this.addNavLink(link);
		//       }
		//     }
		//     this.logger.debug(tag, 'actual scheme', this.scheme);
		//   });

		this.settingsSub = settingsService.settings
			.pipe(takeUntil(this.unsubscribe), throttleTime(250))
			.subscribe((settings) => {
				if (this.refreshTimerSub) {
					this.refreshTimerSub.unsubscribe();
				}

				// todo
				// this.refreshTimerSub = interval(settings.notificationInterval * 1000)
				//   .pipe(takeUntil(this.unsubscribe), throttleTime(500))
				//   .subscribe(() => {
				//     const req = this.menuItems.find((item) => item.link === 'requirement');
				//     if (req) {
				//       req.badge = this.mock.requirements.filter((r) =>
				//         RequirementUtils.isRequirementOpen({
				//           ...r
				//         } as RequirementData)
				//       ).length;
				//     }
				//   });
			});
	}

  @HostBinding('class') className = '';
  nightModeControl = new FormControl(false);

  ngOnInit():void{
    this.nightModeControl.valueChanges.subscribe((darkMode) => {
      const darkClassName = 'darkMode';
      const bodyElement = document.body;
      this.className = darkMode ? darkClassName : '';
      if (darkMode) {
        this.overlay.getContainerElement().classList.add(darkClassName);
        bodyElement.classList.add(darkClassName);
      } else {
        this.overlay.getContainerElement().classList.remove(darkClassName);
        bodyElement.classList.remove(darkClassName);
      }
    });
  }

	get languages() {
		return this.localeService.lang;
	}

	data$ = combineLatest([
		this.store.select(selectPayload),
		this.store.select(selectLanguage),
		this.store.select(selectMenuItems),
		this.store.select(selectAppVersion),
		this.store.select(schemaSelectors.selectScheme),
		this.store.select(selectLoggedIn),
	]).pipe(
		map(([token, lang, menuItems, version, scheme, loggedIn]) => {
			return {
				token,
				lang,
				menuItems,
				version,
				scheme,
				loggedIn,
			};
		})
	);

	ngOnDestroy(): void {
		this.unsubscribe.next();
		this.unsubscribe.complete();
	}

	hasBadge(item: MenuItem): boolean {
		if (item) {
			return isNotNullOrUndefined(item.badge) && item.badge > 0;
		}
		return false;
	}

	getBadges(item: MenuItem): number {
		if (this.hasBadge(item)) {
			return item.badge;
		} else {
			return 0;
		}
	}

	navigate(item: MenuItem): void {
		this.router.navigateByUrl(item.link);
	}

	openHome(): void {
		this.router.navigateByUrl('');
	}

	showUser(): void {
		this.store
			.select(selectPayload)
			.pipe(take(1))
			.subscribe((payload: TokenPayload) => {
				if (payload && payload.user_id) {
					this.router.navigate(['user', payload.user_id]);
				} else {
					this.logger.debug(tag, 'No user id found');
				}
			});
	}

	chooseScheme(): void {
		this.store.dispatch(loadSchemasDialog());
	}

	logOut(): void {
		this.store.dispatch(logout({}));
	}

	openSettings(): void {
		this.router.navigateByUrl('settings');
	}

	changeLanguage(lang: string) {
		this.store.dispatch(loadLanguage({ lang }));
	}

	private updateByPayload(token: TokenPayload) {
		this.logger.debug(tag, 'updateByPayload', token);
		if (token) {
			this.settingsService.fetch(token.user_id);
			this.logger.debug(tag, 'token payload', token);
		}
	}
}
