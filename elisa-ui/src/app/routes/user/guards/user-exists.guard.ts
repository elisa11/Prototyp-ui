import { Injectable } from '@angular/core';
import {
	ActivatedRouteSnapshot,
	CanActivate,
	Router,
	RouterStateSnapshot,
	UrlTree,
} from '@angular/router';
import { Observable, of } from 'rxjs';
import { Store } from '@ngrx/store';
import { UserCacheService } from '@core/service/cache/user-cache.service';
import { catchError, filter, map, switchMap, take, tap } from 'rxjs/operators';
import { selectUserEntities } from '../store/selectors/user.selectors';
import { UserActions } from '../store/actions';
import { selectSearchUserLoading } from '../store/selectors/search-user.selectors';
import { NGXLogger } from 'ngx-logger';

const tag = 'UserExistsGuard';

@Injectable({
	providedIn: 'root',
})
export class UserExistsGuard implements CanActivate {
	constructor(
		private store: Store,
		private userService: UserCacheService,
		private logger: NGXLogger,
		private router: Router
	) {}

	canActivate(
		route: ActivatedRouteSnapshot,
		state: RouterStateSnapshot
	): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
		return this.waitForCollectionToLoad().pipe(
			switchMap(() => this.hasUser(route.params.id))
		);
	}

	private waitForCollectionToLoad(): Observable<boolean> {
		return this.store.select(selectSearchUserLoading).pipe(
			filter((loading) => !loading),
			take(1)
		);
	}

	/**
	 * This method checks if a user with the given ID is already registered
	 * in the Store
	 */
	private hasUserInStore(id: number): Observable<boolean> {
		return this.store.select(selectUserEntities).pipe(
			map((entities) => !!entities[id]),
			take(1)
		);
	}

	/**
	 * This method loads a user with the given ID from the API and caches
	 * it in the store, returning `true` or `false` if it was found.
	 */
	private hasUserInApi(id: number): Observable<boolean> {
		return this.userService.getUser(id).pipe(
			map((userEntity) => UserActions.loadUser({ user: userEntity })),
			tap((action) => this.store.dispatch(action)),
			map((user) => !!user),
			catchError(() => {
				this.router.navigate(['/404']);
				return of(false);
			})
		);
	}

	/**
	 * `hasUser` composes `hasUserInStore` and `hasUserInApi`. It first checks
	 * if the user is in store, and if not it then checks if it is in the
	 * API.
	 */
	private hasUser(id: number): Observable<boolean> {
		return this.hasUserInStore(id).pipe(
			switchMap((inStore) => {
				if (inStore) {
					this.logger.debug(tag, 'hasUser', 'in store');
					return of(inStore);
				}
				this.logger.debug(tag, 'hasUser try API');
				return this.hasUserInApi(id);
			})
		);
	}
}
