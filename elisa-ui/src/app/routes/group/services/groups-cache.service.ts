import { Injectable } from '@angular/core';
import {
	Group,
	GroupsService,
	PaginatedGroupList,
} from '@elisa11/elisa-api-angular-client';
import { Observable } from 'rxjs';
import { shareReplay } from 'rxjs/operators';
import { AbstractCacheService } from '@core/service/cache/abstract-cache.service';
import { PaginatorFilter } from '@data/filter/paginator.filter';
import { BasicUtils } from '@shared/utils/basic.utils';

@Injectable({
	providedIn: 'root',
})
export class GroupsCacheService extends AbstractCacheService {
	private listGroupsCache = new Map();
	private getGroupCache = new Map();

	constructor(private service: GroupsService) {
		super();
	}

	listGroups(
		filter: PaginatorFilter,
		schema: string,
		parent?: number,
		studyPlan?: number
	): Observable<PaginatedGroupList> {
		const obs = this.listGroupsCache.get(filter);
		if (obs) {
			return obs;
		}

		const response = this.service
			.groupsList(
				schema,
				undefined,
				undefined,
				filter.limit,
				undefined,
				filter.offset,
				BasicUtils.getSortBy(filter),
				[parent],
				filter.search,
				studyPlan
			)
			.pipe(shareReplay({ bufferSize: 1, refCount: true }));
		this.listGroupsCache.set(filter, response);
		return response;
	}

	getStudyPlans(schema: string): Observable<PaginatedGroupList> {
		const obs = this.listGroupsCache.get('studyPlan');
		if (obs) {
			return obs;
		}

		const response = this.service //values needed for proper filtering
			.groupsList(
				schema,
				undefined, //id
				undefined, //level
				undefined, //limit
				undefined, //name
				undefined, //offset
				undefined, //ordering
				[1, 2, 3], //parent
				undefined, //search
				undefined, //studyPlanId
				undefined, //observe
				undefined //reportProgress
			)
			.pipe(shareReplay({ bufferSize: 1, refCount: true }));
		this.listGroupsCache.set('studyPlan', response);
		return response;
	}

	getStudyYears(schema: string): Observable<PaginatedGroupList> {
		const obs = this.listGroupsCache.get('studyYears');
		if (obs) {
			return obs;
		}

		const response = this.service //values needed for proper filtering
			.groupsList(
				schema,
				undefined, //id
				3, //level
				undefined, //limit
				['1', '2', '3', '4'], //nameIn
				undefined, //offset
				undefined, //ordering
				undefined, //parentIn
				undefined, //search
				undefined, //studyPlanId
				undefined, //observe
				undefined //reportProgress
			)
			.pipe(shareReplay({ bufferSize: 1, refCount: true }));
		this.listGroupsCache.set('studyYears', response);
		return response;
	}

	getSpecializations(schema: string): Observable<PaginatedGroupList> {
		const obs = this.listGroupsCache.get('specializations');
		if (obs) {
			return obs;
		}

		const response = this.service //values needed for proper filtering
			.groupsList(
				schema,
				undefined, //id
				4, //level
				undefined, //limit
				undefined, //name
				undefined, //offset
				undefined, //ordering
				undefined, //parent
				undefined, //search
				undefined, //studyPlanId
				undefined, //observe
				undefined //reportProgress
			)
			.pipe(shareReplay({ bufferSize: 1, refCount: true }));
		this.listGroupsCache.set('specializations', response);
		return response;
	}

	getGroup(id: number, schema: string): Observable<Group> {
		const obs = this.listGroupsCache.get(id);
		if (obs) {
			return obs;
		}
		const response = this.service
			.groupsRetrieve(schema, id)
			.pipe(shareReplay({ bufferSize: 1, refCount: true }));
		this.getGroupCache.set(id, response);
		return response;
	}

	clearCache(): void {
		this.getGroupCache.clear();
		this.listGroupsCache.clear();
	}
}
