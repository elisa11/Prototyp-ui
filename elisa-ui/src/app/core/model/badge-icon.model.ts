export interface BadgeIconData {
	color: string;
	backgroundColor: string;
	name: string;
}
