import { Component, Input, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';

@Component({
	selector: 'app-timetable-filter-item',
	templateUrl: './timetable-filter-item.component.html',
	styleUrls: ['./timetable-filter-item.component.scss'],
})
export class TimetableFilterItemComponent implements OnInit {
	@Input() control: FormControl;
	@Input() label: string;
	@Input() options$: Observable<any[]> = new Observable<any[]>();
	@Input() isLoading = false;

	constructor() {}

	ngOnInit(): void {}
}
