import { Pipe, PipeTransform } from '@angular/core';
import { Department, DepartmentSerializerShort } from '@elisa11/elisa-api-angular-client';

@Pipe({
	name: 'departmentName',
})
export class DepartmentNamePipe implements PipeTransform {
	transform(value: Department | DepartmentSerializerShort | number | string): string {
		if (typeof value === 'string') {
			return value;
		} else if (typeof value === 'number') {
			return '';
		} else {
			return value ? value.name ?? '' : '';
		}
	}
}
