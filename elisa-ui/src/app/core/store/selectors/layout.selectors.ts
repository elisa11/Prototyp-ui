import { createFeatureSelector, createSelector } from '@ngrx/store';
import {
	getMenuItems,
	getVersion,
	layoutFeatureKey,
	LayoutState,
} from '@core/store/reducers/layout.reducer';

export const selectLayoutState = createFeatureSelector<LayoutState>(layoutFeatureKey);

export const selectMenuItems = createSelector(selectLayoutState, getMenuItems);

export const selectAppVersion = createSelector(selectLayoutState, getVersion);
