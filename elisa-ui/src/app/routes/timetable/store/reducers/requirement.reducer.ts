import { createReducer, on } from '@ngrx/store';
import { createEntityAdapter, EntityAdapter, EntityState } from '@ngrx/entity';
import { Requirement } from '@elisa11/elisa-api-angular-client';
import { requirementActions } from '../actions';

export const requirementsFeatureKey = 'requirements';

export interface State extends EntityState<Requirement> {
	readonly selectedIds: number[];
	readonly count: number;
	readonly error: string;
	readonly loading: boolean;
	readonly requirement: Requirement;
}

export const adapter: EntityAdapter<Requirement> = createEntityAdapter<Requirement>({
	selectId: (model: Requirement) => model.id,
	sortComparer: false,
});

export const initialState = adapter.getInitialState({
	selectedIds: [],
	count: 0,
	loading: false,
	requirement: null,
});

export const reducer = createReducer(
	initialState,
	on(requirementActions.loadRequirements, (state) => ({
		...state,
		loading: true,
	})),

	on(requirementActions.loadRequirementsSuccess, (state: State, { data }) =>
		adapter.upsertMany(data.results, {
			...state,
			selectedIds: data.results.map((x) => x.id),
			count: data.results.length,
			loading: false,
			requirement: state.requirement ? state.requirement : data.results[0],
		})
	),
	on(requirementActions.loadRequirementsFailure, (state, { error }) => ({
		...state,
		loading: false,
		error: error,
	})),
	on(requirementActions.requirementSelected, (state, { requirement }) => ({
		...state,
		requirement: requirement,
	})),
	on(requirementActions.setSelectedRequirementByCourse, (state, { courseId }) => ({
		...state,
		requirement: Object.values(state.entities).find((x) => x.course.id === courseId),
	}))
);

export const getSelectedIds = (state: State) => state.selectedIds;
export const isLoading = (state: State) => state.loading;
export const getCount = (state: State) => state.count;
export const getRequirement = (state: State) => state.requirement;
