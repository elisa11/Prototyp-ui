import { Group } from '@elisa11/elisa-api-angular-client';

export interface UserFormModel {
  id: number;
  username: string;
  title_before: string;
  title_after: string;
  first_name: string;
  last_name: string;
  groups: Group[];
}
