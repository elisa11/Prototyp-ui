import { ChangeDetectionStrategy, Component } from '@angular/core';
import { AbstractSelectedPageComponent } from '@core/generics/abstract-selected-page.component';
import { Room } from '@elisa11/elisa-api-angular-client';
import { Store } from '@ngrx/store';
import {
	departmentSelectors,
	roomEquipmentSelectors,
	roomSelectors,
	typeSelectors,
} from '../../store/selectors';
import { combineLatest } from 'rxjs';
import { map } from 'rxjs/operators';
import { RoomEquipmentMap } from '../../store/reducers/room-equipment.reducer';
import { RoomModel } from '../../model/room.model';

@Component({
	selector: 'app-selected-room-page',
	template: `
		<app-room-detail
			[data]="data$ | async"
			[pagination]="pagination"
			(onBack)="back($event)"
		></app-room-detail>
	`,
	changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SelectedRoomPageComponent extends AbstractSelectedPageComponent<RoomModel> {
	// todo equipment table component
	constructor(private store: Store) {
		super();
		this.data$ = combineLatest([
			store.select(roomSelectors.selectSelectedRoom),
			store.select(typeSelectors.selectEntities),
			store.select(roomEquipmentSelectors.selectEquipment),
			store.select(departmentSelectors.selectEntities),
		]).pipe(
			map((data) => {
				return {
					room: data[0] ?? ({} as Room),
					types: data[1],
					equipment: data[2] ?? ({} as RoomEquipmentMap),
					department: data[3] ?? [],
				};
			}),
			map(
				({ room, equipment, department, types }) =>
					new RoomModel(
						room,
						types[room.room_type],
						equipment.data,
						department[room.department]
					)
			)
		);
	}
}
