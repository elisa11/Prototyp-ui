import { createReducer, on } from '@ngrx/store';
import { MenuItem, MenuItems } from '@core/model/menu-item.model';
import { loadMenuItems, loadVersionSuccessful } from '@core/store/actions/layout.actions';
import { TokenPayload } from '../../../auth/store/model/token.model';
import { isNotNullOrUndefined } from 'codelyzer/util/isNotNullOrUndefined';
import { Version } from '@elisa11/elisa-api-angular-client';
import { CommonText } from '@data/common-text';

export const layoutFeatureKey = 'layout';

export interface LayoutState {
	readonly menuItems: MenuItem[];
	readonly version: string;
}

export const initialState: LayoutState = {
	menuItems: [],
	version: '0.0.0',
} as LayoutState;

export const reducer = createReducer(
	initialState,
	on(loadMenuItems, (state, { schema, token }) => ({
		...state,
		scheme: schema,
		menuItems: filterMenuItems(state, token, schema),
	})),
	on(loadVersionSuccessful, (state, payload) => ({ ...state, version: payload.version }))
);

export const getVersion = (state: LayoutState) => state.version;
export const getMenuItems = (state: LayoutState) => state.menuItems;

function filterMenuItems(
	state: LayoutState,
	payload: TokenPayload,
	schema: Version
): MenuItems {
	let menuItems = [];
	const roles = payload ? payload.roles ?? [] : [];
	const navLinksMap = CommonText.navLinks.filter(hasRole(roles));
	for (const navLink of navLinksMap) {
		if (navLink) {
			menuItems = addNavLink(schema, menuItems, navLink);
		}
	}
	return menuItems;
}

function addNavLink(schema: Version, menuItems: MenuItems, navLink: MenuItem): MenuItems {
	if (navLink.version && navLink.version == true) {
		if (schema && isNotNullOrUndefined(schema.id)) {
			menuItems.push(navLink);
		}
	} else {
		menuItems.push(navLink);
	}
	return menuItems;
}

function hasRole(userRoles: number[]) {
	return (menuItem: MenuItem) => {
		return menuItem.forRoles.some((menuRole) => userRoles.includes(menuRole));
	};
}
