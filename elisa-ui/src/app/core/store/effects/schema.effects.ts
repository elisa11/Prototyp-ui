import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { ImportService } from '@elisa11/elisa-api-angular-client';
import { loadSchemasDialogSuccess } from '@core/store/actions/schema.actions';
import { catchError, exhaustMap, map, tap, withLatestFrom } from 'rxjs/operators';
import { NGXLogger } from 'ngx-logger';
import { Store } from '@ngrx/store';
import { schemaActions } from '@core/store/actions';
import { schemaSelectors } from '@core/store/selectors';
import { of } from 'rxjs';
import { LocaleService } from '@core/service/locale/locale.service';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { LoadingDialogComponent } from '../../../dialogs/loading-dialog/loading-dialog.component';
import { marker } from '@biesbjerg/ngx-translate-extract-marker';
import { SnackEffects } from '@core/generics/snack.effects';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ConfirmDialogModel } from '../../../dialogs/confirm-dialog/confirm-dialog.model';
import { ConfirmDialogComponent } from '../../../dialogs/confirm-dialog/confirm-dialog.component';

const tag = 'SchemaEffects';

@Injectable()
export class SchemaEffects extends SnackEffects {
	loadSchemasDialogSuccess$ = createEffect(() =>
		this.actions$.pipe(
			ofType(loadSchemasDialogSuccess),
			exhaustMap(({ data, old }) => {
				this.logger.debug(tag, 'load schemas, old:', old, 'new:', data);
				if (data && old && old.id !== data.parent) {
					localStorage.setItem('schema', JSON.stringify(data));
					const dialogRef = this.dialog.open(ConfirmDialogComponent, {
						data: {
							title: marker('dialog.import.title'),
							question: marker('dialog.import.question'),
							noLabel: marker('dialog.import.no-label'),
							yesLabel: marker('dialog.import.yes-label'),
						} as ConfirmDialogModel,
						disableClose: true,
					});
					return dialogRef.afterClosed();
				}
				return of(false);
			}),
			map((importData) => {
				this.logger.debug(tag, 'import data', importData);
				if (importData) {
					this.dialogRef = this.dialog.open(LoadingDialogComponent, {
						data: {
							text: this.localeService.translate(marker('dialog.loading.importing')),
						},
						disableClose: true,
					});
					return schemaActions.loadSchemasImportStart();
				} else {
					return schemaActions.loadSchemasImportDone();
				}
			})
		)
	);

	loadSchemasImportStart$ = createEffect(() =>
		this.actions$.pipe(
			ofType(schemaActions.loadSchemasImportStart),
			withLatestFrom(this.store.select(schemaSelectors.selectScheme)),
			map((latest) => latest[1]),
			exhaustMap((schema) => {
				return this.importService
					.importForVersionCreate(schema.name, { periods: [schema.period] })
					.pipe(
						map(() => schemaActions.loadSchemasImportDone()),
						catchError((err) => {
							this.logger.error(err);
							return of(schemaActions.loadSchemasImportFailed());
						})
					);
			})
		)
	);
	loadSchemasImportDone$ = createEffect(
		() =>
			this.actions$.pipe(
				ofType(schemaActions.loadSchemasImportDone),
				tap((err) => this.dialogRef && this.dialogRef.close())
			),
		{ dispatch: false }
	);

	loadSchemasImportFailed$ = createEffect(
		() =>
			this.actions$.pipe(
				ofType(schemaActions.loadSchemasImportFailed),
				tap((err) => {
					if (this.dialogRef) {
						this.dialogRef.close();
					}
					this.openSnackbar(this.localeService.translate(marker('error.import.msg')));
				})
			),
		{ dispatch: false }
	);

	constructor(
		private actions$: Actions,
		private logger: NGXLogger,
		private store: Store,
		private importService: ImportService,
		private localeService: LocaleService,
		private dialog: MatDialog,
		snack: MatSnackBar
	) {
		super(snack);
	}

	private dialogRef: MatDialogRef<LoadingDialogComponent>;
}
