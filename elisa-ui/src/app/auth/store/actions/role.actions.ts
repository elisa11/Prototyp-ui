import { createAction, props } from '@ngrx/store';

export const loadRoles = createAction(
	'[Auth - Role] Load Roles',
	props<{ rolesId: number[] }>()
);

export const notAuthorized = createAction('[Auth - Role] Not Authorized');
