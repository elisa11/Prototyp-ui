import { Pipe, PipeTransform } from '@angular/core';
import { SubjectTeacherModel } from '../model/subject-teacher.model';
import { FullnamePipe } from '@shared/pipes/user/fullname.pipe';

@Pipe({
	name: 'teacherFilter',
})
export class TeacherFilterPipe implements PipeTransform {
	constructor(private pipe: FullnamePipe) {}

	transform(value: SubjectTeacherModel[], id: number): string {
		const r = value
			? value
					.filter((d) => d.roles.map((r) => (r ? r.id : -1)).includes(id))
					.map((d) => this.pipe.transform(d.user))
			: [];
		return r.join(', ');
	}
}
