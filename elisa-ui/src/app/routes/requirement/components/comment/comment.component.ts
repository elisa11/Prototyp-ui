import {
	Component,
	EventEmitter,
	Input,
	NgZone,
	OnInit,
	Output,
	ViewChild,
} from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { CdkTextareaAutosize } from '@angular/cdk/text-field';
import { take } from 'rxjs/operators';
import { CommentModel } from '@data/model/comment.model';
import { Comments } from '@elisa11/elisa-api-angular-client';
import moment from 'moment/moment';
import { Paths } from '@shared/store/reducers/form.reducer';

@Component({
	selector: 'app-comment',
	templateUrl: './comment.component.html',
})
export class CommentComponent implements OnInit {
	path = Paths.comment;
	@Output()
	private onSubmit = new EventEmitter<CommentModel>();
	@Output()
	private onDeleteComment = new EventEmitter<number>();
	@Output()
	private onEditComment = new EventEmitter<CommentModel>();

	commentForm: FormGroup;
	@ViewChild('autosize') autosize: CdkTextareaAutosize;
	@Input() loggedUserId!: number;
	@Input()
	comments!: CommentModel[];

	constructor(private ngZone: NgZone, private fb: FormBuilder) {}

	triggerResize(): void {
		// Wait for changes to be applied, then trigger textarea resize.
		this.ngZone.onStable
			.pipe(take(1))
			.subscribe(() => this.autosize.resizeToFitContent(true));
	}

	ngOnInit(): void {
		this.commentForm = this.fb.group({
			message: [''],
		});
	}

	submit(): void {
		console.log('submit');
		const item = {
			created_at: moment().toISOString(),
			created_by: this.loggedUserId,
			text: this.commentForm.value.message,
		} as Comments;
		this.onSubmit.emit(item);
	}

	deleteComment(cid: number) {
		this.onDeleteComment.emit(cid);
	}

	editComment(edit: CommentModel) {
		this.onEditComment.emit(edit);
	}
}
