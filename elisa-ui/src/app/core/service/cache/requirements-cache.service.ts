import { Injectable } from '@angular/core';
import {
	CommentSerializerPost,
	PaginatedCommentsList,
	PaginatedRequirementList,
	PatchedComments,
	Requirement,
	RequirementSerializerPost,
	RequirementSerializerPut,
	RequirementsService,
} from '@elisa11/elisa-api-angular-client';
import { Observable } from 'rxjs';
import { shareReplay } from 'rxjs/operators';
import { AbstractCacheService } from './abstract-cache.service';
import { PaginatorFilter } from '@data/filter/paginator.filter';
import { BasicUtils } from '@shared/utils/basic.utils';
import { RequirementFilter } from '../../../routes/requirement/model/requirement-filter.model';

@Injectable({
	providedIn: 'root',
})
export class RequirementsCacheService extends AbstractCacheService {
	private listRequirementsCache = new Map();
	private getRequirementCache = new Map();

	constructor(private service: RequirementsService) {
		super();
	}

	listRequirements(
		filter?: PaginatorFilter,
		schema?: string,
		requirementFilter?: RequirementFilter
	): Observable<PaginatedRequirementList> {
		const key = { filter, schema, requirementFilter };
		const obs = this.listRequirementsCache.get(key);
		if (obs) {
			return obs;
		}
		const response = this.service
			.requirementsList(
				schema,
				requirementFilter ? requirementFilter.courseId : undefined,
				requirementFilter ? requirementFilter.departmentId : undefined,
				filter ? filter.limit : undefined,
				filter ? filter.offset : undefined,
				filter ? BasicUtils.getSortBy(filter) : undefined,
				requirementFilter ? requirementFilter.status : undefined
			)
			.pipe(shareReplay({ bufferSize: 1, refCount: true }));
		this.listRequirementsCache.set(key, response);
		return response;
	}

	getRequirement(id: number, scheme: string): Observable<Requirement> {
		const key = { id, scheme };
		const obs = this.getRequirementCache.get(key);
		if (obs) {
			return obs;
		}
		const response = this.service
			.requirementsRetrieve(scheme, id)
			.pipe(shareReplay({ bufferSize: 1, refCount: true }));
		this.getRequirementCache.set(key, response);
		return response;
	}

	postComment(
		scheme: string,
		requirement: number,
		comment: CommentSerializerPost
	): Observable<CommentSerializerPost> {
		return this.service.requirementsCommentsCreate(scheme, requirement, comment);
	}

	getComments(scheme: string, requirement: number): Observable<PaginatedCommentsList> {
		return this.service.requirementsCommentsList(scheme, requirement);
	}

	deleteComment(scheme: string, requirement: number, comment: number): Observable<any> {
		return this.service.requirementsCommentsDestroy(scheme, comment, requirement);
	}

	updateComment(scheme: string, rid: number, cid: number, comment: PatchedComments) {
		return this.service.requirementsCommentsPartialUpdate(scheme, cid, rid, comment);
	}

	createRequirement(schema: string, requirement: RequirementSerializerPost) {
		return this.service.requirementsCreate(schema, requirement);
	}

	updateRequirement(schema: string, requirement: RequirementSerializerPut) {
		return this.service.requirementsUpdate(schema, requirement.id, requirement);
	}

	deleteRequirement(schema: string, rid: number): Observable<any> {
		return this.service.requirementsDestroy(schema, rid);
	}

	clearCache(): void {
		this.listRequirementsCache.clear();
		this.getRequirementCache.clear();
	}
}
