import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';
import {
	MATERIAL_MODULES,
	SHARED_COMPONENTS,
	SHARED_DIRECTIVES,
	SHARED_PIPES,
} from './index';
import { NgxMaskModule } from 'ngx-mask';
import { DigitOnlyModule } from '@uiowa/digit-only';
import { TranslateModule } from '@ngx-translate/core';
import { StoreModule } from '@ngrx/store';
import * as fromShared from './store/reducers';
import { EffectsModule } from '@ngrx/effects';
import { ActivityCategoryEffects, RoomCategoryEffects } from '@shared/store/effects';
import { StudyFormEffects } from './store/effects/study-form.effects';
import { UserSubjectRoleEffects } from './store/effects/user-subject-role.effects';
import { RoomAutocompleteComponent } from './components/room-autocomplete/room-autocomplete.component';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';

@NgModule({
	declarations: [
		...SHARED_COMPONENTS,
		...SHARED_DIRECTIVES,
		...SHARED_PIPES,
		RoomAutocompleteComponent,
	],
	exports: [
		DigitOnlyModule,
		FlexLayoutModule,
		FormsModule,
		...SHARED_COMPONENTS,
		...SHARED_DIRECTIVES,
		...SHARED_PIPES,
	],
	imports: [
		NgxMaskModule.forChild(),
		DigitOnlyModule,
		CommonModule,
		MatProgressSpinnerModule,
		RouterModule,
		MatAutocompleteModule,
		ReactiveFormsModule,
		FormsModule,
		TranslateModule.forChild(),
		FlexLayoutModule,
		...MATERIAL_MODULES,
		StoreModule.forFeature(fromShared.sharedFeatureKey, fromShared.reducers),
		EffectsModule.forFeature([
			RoomCategoryEffects,
			ActivityCategoryEffects,
			StudyFormEffects,
			UserSubjectRoleEffects,
		]),
	],
	providers: [...SHARED_PIPES],
})
export class SharedModule {}
