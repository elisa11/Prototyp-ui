import { createFeatureSelector, createSelector } from '@ngrx/store';
import { schemaFeatureKey, SchemaState } from '../reducers';
import * as fromSchema from '../reducers/schema.reducer';

export const selectSchemasState = createFeatureSelector<SchemaState>(schemaFeatureKey);

export const selectSchemaEntitiesState = createSelector(
	selectSchemasState,
	(state) => state[fromSchema.schemaFeatureKey]
);

export const {
	selectIds: selectSchemaIds,
	selectEntities: selectSchemaEntities,
	selectAll: selectAllSchemas,
} = fromSchema.adapter.getSelectors(selectSchemaEntitiesState);

export const selectSchemaCount = createSelector(
	selectSchemaEntitiesState,
	fromSchema.getCount
);
