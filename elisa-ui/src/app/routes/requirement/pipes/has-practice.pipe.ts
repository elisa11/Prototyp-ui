import { Pipe, PipeTransform } from '@angular/core';
import { ActivityModel } from '../model/activity.model';
import { isNotNullOrUndefined } from 'codelyzer/util/isNotNullOrUndefined';

@Pipe({
	name: 'hasPractice',
	pure: false,
})
export class HasPracticePipe implements PipeTransform {
	transform(value: ActivityModel[]): boolean {
		return isNotNullOrUndefined(
			value.find((v) => v.category.name.toLowerCase().includes('cvičenie'))
		); // zavisi od DB bude treba updatovat...
	}
}
