import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({ template: '' })
export abstract class AbstractDetailComponent<T> {
	@Output()
	private onBack = new EventEmitter<string>();
	@Input()
	data: T;

	back(path: string): void {
		this.onBack.emit(path);
	}
}
