import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AuthRoutingModule } from './auth-routing.module';
import { AuthorizeComponent } from './routes/authorize/authorize.component';
import { LoginComponent } from './routes/login/login.component';
import { ReactiveFormsModule } from '@angular/forms';
import { MATERIAL_MODULES } from './index';
import { FlexModule } from '@angular/flex-layout';
import { TranslateModule } from '@ngx-translate/core';
import { StoreModule } from '@ngrx/store';
import * as fromAuth from './store/reducers';
import { AuthEffects, UserEffects } from './store/effects';
import { EffectsModule } from '@ngrx/effects';

@NgModule({
	declarations: [LoginComponent, AuthorizeComponent],
	imports: [
		CommonModule,
		AuthRoutingModule,
		TranslateModule.forChild(),
		StoreModule.forFeature({
			name: fromAuth.authFeatureKey,
			reducer: fromAuth.reducers,
		}),
		EffectsModule.forFeature([AuthEffects, UserEffects]),
		ReactiveFormsModule,
		...MATERIAL_MODULES,
		FlexModule,
	],
})
export class AuthModule {}
