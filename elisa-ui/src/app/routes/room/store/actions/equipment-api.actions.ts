import { createAction, props } from '@ngrx/store';
import { Equipment } from '@elisa11/elisa-api-angular-client';

export const loadEquipments = createAction(
  '[Room - Equipment/API] Load Equipment',
);

export const loadEquipmentsSuccess = createAction(
  '[Room - Equipment/API] Load Equipment Success',
  props<{ data: Equipment[]}>()
);

export const loadEquipmentsFailure = createAction(
  '[Room - Equipment/API] Load Equipment Failure',
  props<{ error: any }>()
);
