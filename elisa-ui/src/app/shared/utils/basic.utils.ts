import { PaginatorFilter } from '@data/filter/paginator.filter';

export const BasicUtils = {
	isEmptyString(str: string): boolean {
		return !str || 0 === str.length;
	},
	getSortBy(filter: PaginatorFilter): string {
		if (filter && filter.sortBy) {
			return `${filter.sortBy ? (filter.asc ? '' : '-') : ''}${filter.sortBy ?? ''}`;
		} else {
			return null;
		}
	},
	replaceNullsWithUndefined(val: any): any {
		if (val === null || val === undefined) {
			return undefined;
		}

		if (!Array.isArray(val)) {
			for (const key in val) {
				if (val[key] === null) {
					val[key] = undefined;
				}
			}
		}
		return val;
	},
	isNotEmptyString(token: string) {
		return !BasicUtils.isEmptyString(token);
	},
	groupBy<TItem>(xs: TItem[], key: string): { [key: string]: TItem[] } {
		return xs.reduce(function (rv, x) {
			(rv[x[key]] = rv[x[key]] || []).push(x);
			return rv;
		}, {});
	},
	onlyUniqueFilter<Item>(value: Item, index: number, self: Item[]) {
		return self.indexOf(value) === index;
	},
};
