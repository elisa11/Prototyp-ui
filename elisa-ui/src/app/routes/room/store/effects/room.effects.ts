import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { RoomsCacheService } from '../../services/rooms-cache.service';
import { resetCachedData } from '@core/store/actions/settings.actions';
import {
	catchError,
	debounceTime,
	map,
	skip,
	switchMap,
	takeUntil,
	tap,
	withLatestFrom,
} from 'rxjs/operators';
import { SnackEffects } from '@core/generics/snack.effects';
import { MatSnackBar } from '@angular/material/snack-bar';
import { asyncScheduler, EMPTY, of } from 'rxjs';
import { roomApiActions } from '../actions';
import { schemaSelectors } from '@core/store/selectors';
import { Store } from '@ngrx/store';
import { deleteRoom } from '../actions/room-api.actions';
import { PaginatorFilter } from '@data/filter/paginator.filter';

@Injectable()
export class RoomEffects extends SnackEffects {
	resetData$ = createEffect(
		() =>
			this.actions$.pipe(
				ofType(resetCachedData),
				tap(() => this.service.clearCache())
			),
		{ dispatch: false }
	);

	search$ = createEffect(
		() =>
			({ debounce = 300, scheduler = asyncScheduler } = {}) =>
				this.actions$.pipe(
					ofType(roomApiActions.searchRoom),
					debounceTime(debounce, scheduler),
					withLatestFrom(this.store.select(schemaSelectors.selectScheme)),
					map((value) => {
						return {
							query: value[0].query,
							filters: value[0].filters,
							version: value[1].name,
						};
					}),
					switchMap(({ query, filters, version }) => {
						if (query === null) {
							return EMPTY;
						}

						const nextSearch$ = this.actions$.pipe(
							ofType(roomApiActions.searchRoom),
							skip(1)
						);

						return this.service.listRooms(version, filters, query).pipe(
							takeUntil(nextSearch$),
							map((data) => roomApiActions.searchRoomSuccess({ data })),
							catchError((err) =>
								of(roomApiActions.searchRoomFailure({ error: err.message }))
							)
						);
					})
				)
	);

	searchFailed$ = createEffect(
		() =>
			this.actions$.pipe(
				ofType(roomApiActions.searchRoomFailure),
				tap((err) => this.openSnackbar(err.error))
			),
		{ dispatch: false }
	);

  postRoom$ = createEffect(()=>
    this.actions$.pipe(
      ofType(roomApiActions.postRoom),
      withLatestFrom(this.store.select(schemaSelectors.selectScheme)),
      map((value)=> {
          return {scheme: value[1].name, room: value[0].data}
        }
      ),
      switchMap((values)=>{
        return this.service.postRoom(values.scheme, values.room)
          .pipe(
            map((room) => roomApiActions.postRoomSuccess({data:room})),
            catchError((err) =>
              of(roomApiActions.postRoomFailure({error:err})))
          );
      })
    ),
  );

  postRoomSuccess$ = createEffect(()=>
    this.actions$.pipe(
      ofType(roomApiActions.postRoomSuccess),
      map(() => {
        this.openSnackbar("Miestnosť bola vytvorená.");
        // this.openSnackbar(this.localeService.translate(marker('room.post.msg')))
        return roomApiActions.searchRoom({
          filters: { capacity: undefined, departments: [], type: [] },
          query: {} as PaginatorFilter});
      })
    )
  );

  postRoomFailure$ = createEffect(()=>
    this.actions$.pipe(
      ofType(roomApiActions.postRoomFailure),
      map(() => {
        this.openSnackbar("Miestnosť nebola úspešne vytvorená.");
        // this.openSnackbar(this.localeService.translate(marker('room.post.error')))
        return roomApiActions.searchRoom({
          filters: { capacity: undefined, departments: [], type: [] },
          query: {} as PaginatorFilter});
      })
    )
  );

  putRoom$ = createEffect(()=>
    this.actions$.pipe(
      ofType(roomApiActions.putRoom),
      withLatestFrom(this.store.select(schemaSelectors.selectScheme)),
      map((value)=> {
          return {scheme: value[1].name, room: value[0].data}
        }
      ),
      switchMap((values)=>{
        return this.service.putRoom(values.scheme, values.room)
          .pipe(
            map((room) => roomApiActions.putRoomSuccess({data:room})),
            catchError((err) =>
              of(roomApiActions.putRoomFailure({error:err})))
          );
      })
    ),
  );

  putRoomSuccess$ = createEffect(()=>
    this.actions$.pipe(
      ofType(roomApiActions.putRoomSuccess),
      map(() => {
        this.openSnackbar("Miestnosť bola upravená.");
        // this.openSnackbar(this.localeService.translate(marker('room.put.msg')))
        return roomApiActions.searchRoom({
          filters: { capacity: undefined, departments: [], type: [] },
          query: {} as PaginatorFilter});
      })
    )
  );

  putRoomFailure$ = createEffect(()=>
    this.actions$.pipe(
      ofType(roomApiActions.putRoomFailure),
      map(() => {
        this.openSnackbar("Miestnosť nebola úspešne upravená.");
        // this.openSnackbar(this.localeService.translate(marker('room.put.error')))
        return roomApiActions.searchRoom({
          filters: { capacity: undefined, departments: [], type: [] },
          query: {} as PaginatorFilter});
      })
    )
  );

  deleteRoom = createEffect(() =>
    this.actions$.pipe(
      ofType(deleteRoom),
      withLatestFrom(this.store.select(schemaSelectors.selectScheme)),
      map((data) => {
        return { id: data[0].id, scheme: data[1].name };
      }),
      switchMap((data) => {
        return this.service.deleteRoom(data.scheme, data.id).pipe(
          map(()=> roomApiActions.deleteRoomDone({id:data.id})),
          catchError ((err) =>
            of(roomApiActions.deleteRoomFailed({error: err}))
          )
        );
      })
    ),
  );

  deleteRoomDone$ = createEffect(() =>
    this.actions$.pipe(
      ofType(roomApiActions.deleteRoomDone),
      map(() => {
        this.openSnackbar("Miestnosť bola vymazaná.");
        // this.openSnackbar(this.localeService.translate(marker('room.delete.msg')))
        return roomApiActions.searchRoom({
          filters: { capacity: undefined, departments: [], type: [] },
          query: {} as PaginatorFilter});
      })
    )
  );
  deleteRoomFailed$ = createEffect(() =>
      this.actions$.pipe(
        ofType(roomApiActions.deleteRoomFailed),
        map((err) =>
            this.openSnackbar("Nastala chyba pri vymazávaní miestnosti.")
          // this.openSnackbar(this.localeService.translate(marker('room.delete.error')))
        )
      ),
    { dispatch: false }
  );

	constructor(
		private actions$: Actions,
		private service: RoomsCacheService,
		private store: Store,
		snack: MatSnackBar
	) {
		super(snack);
	}
}
