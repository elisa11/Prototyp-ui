import { Component, EventEmitter, Input, Output } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Period, Version } from '@elisa11/elisa-api-angular-client';
import { Paths } from '@shared/store/reducers/form.reducer';
import { SchemaTableComponent } from '../schema-table/schema-table.component';

@Component({
  providers:[SchemaTableComponent],
	selector: 'app-new-schema-form',
	templateUrl: './new-schema-form.component.html',
})
export class NewSchemaFormComponent {
	path = Paths.newVersion;
	@Output()
	onSubmit = new EventEmitter();
	@Output()
	onSuccess = new EventEmitter<Paths>();
	@Output()
	onError = new EventEmitter<any>();
	private fg: FormGroup = null;
	/**
	 * Performance life hack, kvoli change detection
	 */
	controls = {
		name: this.fg
			? this.fg.get('name')
			: new FormControl('', [Validators.pattern(/^(?!pg_)\w+/), Validators.required]),
		period: this.fg ? this.fg.get('period') : new FormControl('', Validators.required),
		parent: this.fg ? this.fg.get('parent') : new FormControl(''),
	};

	get formGroup() {
		return this.fg;
	}

	@Input()
	set formGroup(fg: FormGroup) {
		this.fg = fg;
		this.controls = {
			name: this.fg
				? this.fg.get('name')
				: new FormControl('', [Validators.pattern(/^(?!pg_)\w+/), Validators.required]),
			period: this.fg ? this.fg.get('period') : new FormControl('', Validators.required),
			parent: this.fg ? this.fg.get('parent') : new FormControl(''),
		};
	}

	displayObjectFn(obj: Period | Version): string {
		return obj && obj.name ? obj.name : '';
	}

	submit() {
    this.onSubmit.emit(this.formGroup.value);
    this.schemaComponent.refreshTable();
  }

	constructor(private schemaComponent: SchemaTableComponent){}
}
