import { createSelector } from '@ngrx/store';
import { selectDepartmentState, selectEntities } from './department.selectors';
import {
	getError,
	getIds,
	getLoading,
	getQuery,
	searchDepartmentFeatureKey,
} from '../reducers/search-department.reducer';
import { Department } from '@elisa11/elisa-api-angular-client';

export const selectSearchDepartmentState = createSelector(
	selectDepartmentState,
	(s) => s[searchDepartmentFeatureKey]
);

export const selectSearchIds = createSelector(selectSearchDepartmentState, getIds);
export const selectSearchQuery = createSelector(selectSearchDepartmentState, getQuery);
export const selectSearchLoading = createSelector(
	selectSearchDepartmentState,
	getLoading
);
export const selectSearchError = createSelector(selectSearchDepartmentState, getError);

export const selectSearchResults = createSelector(
	selectEntities,
	selectSearchIds,
	(data, searchIds) => {
		return searchIds.map((id) => data[id]).filter((d): d is Department => d != null);
	}
);
