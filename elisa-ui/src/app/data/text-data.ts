export interface TextData {
	id: number;
	value: string;
}
