import { Component, EventEmitter, Output } from '@angular/core';
import { User } from '@elisa11/elisa-api-angular-client';
import { AbstractTableComponent } from '@core/generics/abstract-table.component';
import { ConfirmDialogComponent } from '../../../../dialogs/confirm-dialog/confirm-dialog.component';
import { ConfirmDialogModel } from '../../../../dialogs/confirm-dialog/confirm-dialog.model';
import { Store } from '@ngrx/store';
import { MatDialog } from '@angular/material/dialog';
import { UserModel } from '../../model/user.model';
import { deleteUser } from '../../store/actions/user-api.actions';

@Component({
	selector: 'app-user-list',
	templateUrl: './user-list.component.html',
})
export class UserListComponent extends AbstractTableComponent<User> {
	displayedColumns = ['id', 'username', 'first_name', 'last_name', 'groups', 'btn'];
  @Output()
  private onNewUser = new EventEmitter();

  constructor(private store: Store,
              private dialog: MatDialog) {
		super();
	}

  newUser() {
    console.log('New User click')
    this.onNewUser.emit();
  }

  editUser($event: MouseEvent, row: UserModel) {
    this.onNewUser.emit(row);
    $event.stopImmediatePropagation();
  }

  deleteItem($event: MouseEvent, row: any) {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      data: {
        title: "Vymazanie osoby",
        // title: marker('dialog.course.title'),
        question: "Chcete vymazať osobu?",
        // question: marker('dialog.course.question'),
        noLabel: "Nie",
        // noLabel: marker('dialog.course.no-label'),
        yesLabel: "Áno",
        // yesLabel: marker('dialog.course.yes-label'),
      } as ConfirmDialogModel,
      disableClose: true,
    });
    dialogRef.afterClosed().subscribe(dialogRes =>{
      if (dialogRes){
        this.store.dispatch(deleteUser({id:row.id}));
      }
    })
    $event.stopImmediatePropagation();
  }
}
