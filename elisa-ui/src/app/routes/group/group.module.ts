import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { GroupRoutingModule } from './group-routing.module';
import { GroupListComponent } from './components/group-list/group-list.component';
import { GroupDetailComponent } from './components/group-detail/group-detail.component';
import { SharedModule } from '@shared/shared.module';
import { TranslateModule } from '@ngx-translate/core';
import { MATERIAL_MODULES } from './index';
import { EffectsModule } from '@ngrx/effects';
import { GroupEffects } from './store/effects';
import { ViewGroupPageComponent } from './containers/view-group-page/view-group-page.component';
import { CollectionPageComponent } from './containers/collection-page/collection-page.component';
import { SelectedGroupPageComponent } from './containers/selected-group-page/selected-group-page.component';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { StoreModule } from '@ngrx/store';
import * as fromGroup from './store/reducers';

@NgModule({
	declarations: [
		GroupListComponent,
		GroupDetailComponent,
		ViewGroupPageComponent,
		CollectionPageComponent,
		SelectedGroupPageComponent,
	],
	imports: [
		CommonModule,
		GroupRoutingModule,
		TranslateModule.forChild(),
		SharedModule,
		...MATERIAL_MODULES,
		StoreModule.forFeature(fromGroup.groupsFeatureKey, fromGroup.reducers),
		EffectsModule.forFeature([GroupEffects]),
		MatProgressSpinnerModule,
	],
})
export class GroupModule {}
