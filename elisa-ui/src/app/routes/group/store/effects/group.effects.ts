import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { SnackEffects } from '@core/generics/snack.effects';
import { Store } from '@ngrx/store';
import { MatSnackBar } from '@angular/material/snack-bar';
import { GroupsCacheService } from '../../services/groups-cache.service';
import { resetCachedData } from '@core/store/actions/settings.actions';
import {
	catchError,
	debounceTime,
	map,
	skip,
	switchMap,
	takeUntil,
	tap,
	withLatestFrom,
} from 'rxjs/operators';
import { asyncScheduler, EMPTY, of } from 'rxjs';
import { courseApiActions } from '../../../course/store/actions';
import { schemaSelectors } from '@core/store/selectors';
import { groupApiActions } from '../actions';

@Injectable()
export class GroupEffects extends SnackEffects {
	resetData$ = createEffect(
		() =>
			this.actions$.pipe(
				ofType(resetCachedData),
				tap(() => this.service.clearCache())
			),
		{ dispatch: false }
	);

	search$ = createEffect(
		() =>
			({ debounce = 300, scheduler = asyncScheduler } = {}) =>
				this.actions$.pipe(
					ofType(groupApiActions.searchGroup),
					debounceTime(debounce, scheduler),
					withLatestFrom(this.store.select(schemaSelectors.selectScheme)),
					map((value) => {
						return {
							query: value[0].query,
							filters: value[0].filters,
							version: value[1].name,
						};
					}),
					switchMap(({ query, filters, version }) => {
						if (query === null) {
							return EMPTY;
						}

						const nextSearch$ = this.actions$.pipe(
							ofType(courseApiActions.searchCourse),
							skip(1)
						);

						return this.service
							.listGroups(
								query,
								version,
								filters ? filters.parent : undefined,
								filters ? filters.studyPlan : undefined
							)
							.pipe(
								takeUntil(nextSearch$),
								map((data) => groupApiActions.searchGroupSuccess({ data })),
								catchError((err) =>
									of(groupApiActions.searchGroupFailure({ error: err.message }))
								)
							);
					})
				)
	);

	searchFailed$ = createEffect(
		() =>
			this.actions$.pipe(
				ofType(groupApiActions.searchGroupFailure),
				tap((err) => this.openSnackbar(err.error))
			),
		{ dispatch: false }
	);

	constructor(
		private actions$: Actions,
		private service: GroupsCacheService,
		private store: Store,
		snack: MatSnackBar
	) {
		super(snack);
	}
}
