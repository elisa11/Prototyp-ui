import { Component, EventEmitter, Input, Output } from '@angular/core';
import { SelectionModel } from '@angular/cdk/collections';
import { PeriodModel } from '../../model/period.model';
import { AbstractTableComponent } from '@core/generics/abstract-table.component';
import { Subscription } from 'rxjs';

@Component({
	selector: 'app-period-table',
	templateUrl: './period-table.component.html',
})
export class PeriodTableComponent extends AbstractTableComponent<PeriodModel> {
	displayedColumns = [
		'select',
		'id',
		'name',
		'startDate',
		'endDate',
		'active',
		'department',
	];
	selection: SelectionModel<PeriodModel>;
	@Output()
	onSelectionChange: EventEmitter<PeriodModel> = new EventEmitter<PeriodModel>();
	@Input() selected: number;
	private selection$: Subscription;

	constructor() {
		super();
		const initialSelection = [];
		const allowMultiSelect = false;
		this.selection = new SelectionModel<PeriodModel>(allowMultiSelect, initialSelection);
		this.selection$ = this.selection.changed.subscribe((data) =>
			this.onSelectionChange.emit(data.added[0])
		);
	}

	@Input()
	set data(data: PeriodModel[]) {
		this.dataSource.data = data;
		if (this.selected && this.selection) {
			this.selection.select(data.find((value) => value.id === this.selected));
		}
	}
}
