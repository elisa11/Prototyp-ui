import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { SnackEffects } from '@core/generics/snack.effects';
import { Store } from '@ngrx/store';
import { MatSnackBar } from '@angular/material/snack-bar';
import { catchError, filter, map, switchMap, withLatestFrom } from 'rxjs/operators';
import { timetableEventsActions } from '../actions';
import { TimetableDataService } from '../../services/timetable-data.service';
import {
	loadTimetableEventsSuccess,
	loadTimetableEventsFailure,
	importTimetableEventsSuccess,
	importTimetableEventsFailure,
	putTimetableEventSuccess,
	putTimetableEventFailure,
	postTimetableEventSuccess,
	postTimetableEventFailure,
	deleteTimetableEventFailure,
	deleteTimetableEventSuccess,
} from '../actions/timetables-events.actions';
import { from, of, take } from 'rxjs';
import { schemaSelectors } from '@core/store/selectors';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { LoadingDialogComponent } from '../../../../dialogs/loading-dialog/loading-dialog.component';
import { timetablesSelector } from '../selectors';

@Injectable()
export class TimetablesEventsEffects extends SnackEffects {
	private dialogRef: MatDialogRef<LoadingDialogComponent>;

	loadTimetableEvents$ = createEffect(() =>
		this.actions$.pipe(
			ofType(timetableEventsActions.loadTimetableEvents),
			withLatestFrom(
				this.store.select(schemaSelectors.selectScheme),
				this.store.select(timetablesSelector.selectTimetable)
			),
			map((value) => {
				// console.log('loadTimetableEvents MAP');
				return {
					scheme: value[1].name,
					timetable: value[2],
					activityCategoryIdIn: value[0].activityCategoryIdIn,
					activityCourseDepartmentIdIn: value[0].activityCourseDepartmentIdIn,
					// activityCourseId: value[0].activityCourseId,
					// activityId: value[0].activityId,
					// day: value[0].day,
					groupsIdIn: value[0].groupsIdIn,
					roomId: value[0].roomId,
				};
			}),
			switchMap((values) => {
				console.log({ values: values });
				if (!values.timetable) {
					// Return an observable that waits for the timetable to be loaded
					return this.store.select(timetablesSelector.selectTimetable).pipe(
						filter((timetable) => !!timetable),
						take(1),
						switchMap((timetable) =>
							this.timetableService.getTimetableEvents(
								values.scheme,
								timetable.id,
								values.activityCategoryIdIn,
								values.activityCourseDepartmentIdIn,
								null,
								null,
								null,
								values.groupsIdIn,
								values.roomId
							)
						),
						map((eventList) =>
							loadTimetableEventsSuccess({
								events: eventList.results,
							})
						),
						catchError((error) => of(loadTimetableEventsFailure({ error })))
					);
				} else {
					return this.timetableService
						.getTimetableEvents(
							values.scheme,
							values.timetable.id,
							values.activityCategoryIdIn,
							values.activityCourseDepartmentIdIn,
							null,
							null,
							null,
							// values.activityCourseId,
							// values.activityId,
							// values.day,
							values.groupsIdIn,
							values.roomId
						)
						.pipe(
							map((eventList) =>
								loadTimetableEventsSuccess({
									events: eventList.results,
								})
							),
							catchError((error) => of(loadTimetableEventsFailure({ error })))
						);
				}
			})
		)
	);

	postTimetableEvents$ = createEffect(() =>
		this.actions$.pipe(
			ofType(timetableEventsActions.postTimetableEvent),
			withLatestFrom(
				this.store.select(schemaSelectors.selectScheme),
				this.store.select(timetablesSelector.selectTimetable)
			),
			map((value) => {
				return {
					scheme: value[1].name,
					timetable: value[2],
					event: value[0].event,
				};
			}),
			switchMap((values) => {
				this.dialogRef = this.dialog.open(LoadingDialogComponent, {
					data: {
						text: 'Prebieha vkladanie eventu do rozvrhu.',
					},
					disableClose: true,
				});
				return this.timetableService
					.putTimetableEvent(values.scheme, values.timetable, values.event)
					.pipe(
						map((event) =>
							postTimetableEventSuccess({
								event: event,
							})
						),
						catchError((error) => of(postTimetableEventFailure({ error: error })))
					);
			})
		)
	);

	postTimetableEventsSuccess$ = createEffect(
		() =>
			this.actions$.pipe(
				ofType(postTimetableEventSuccess),
				map(() => {
					this.dialogRef && this.dialogRef.close();
					this.openSnackbar('Event bol úspešne vložený.');
				})
			),
		{ dispatch: false }
	);

	postTimetableEventsFailure$ = createEffect(
		() =>
			this.actions$.pipe(
				ofType(postTimetableEventFailure),
				map(() => {
					this.dialogRef && this.dialogRef.close();
					this.openSnackbar('Nastala chyba pri vkladní eventu do rozvrhu.');
					// this.openSnackbar(this.localeService.translate(marker('timetable-events.put.error')))
				})
			),
		{ dispatch: false }
	);

	putTimetableEvents$ = createEffect(() =>
		this.actions$.pipe(
			ofType(timetableEventsActions.putTimetableEvent),
			withLatestFrom(
				this.store.select(schemaSelectors.selectScheme),
				this.store.select(timetablesSelector.selectTimetable)
			),
			map((value) => {
				return {
					scheme: value[1].name,
					timetable: value[2],
					event: value[0].event,
				};
			}),
			switchMap((values) => {
				this.dialogRef = this.dialog.open(LoadingDialogComponent, {
					data: {
						text: 'Prebieha úprava eventu v rozvrhu.',
					},
					disableClose: true,
				});
				return this.timetableService
					.putTimetableEvent(values.scheme, values.timetable, values.event)
					.pipe(
						map((event) =>
							putTimetableEventSuccess({
								event: event,
							})
						),
						catchError((error) => of(putTimetableEventFailure({ error: error })))
					);
			})
		)
	);

	putTimetableEventsSuccess$ = createEffect(
		() =>
			this.actions$.pipe(
				ofType(putTimetableEventSuccess),
				map(() => {
					this.dialogRef && this.dialogRef.close();
					this.openSnackbar('Event bol úspešne upravený.');
				})
			),
		{ dispatch: false }
	);

	putTimetableEventsFailure$ = createEffect(
		() =>
			this.actions$.pipe(
				ofType(putTimetableEventFailure),
				map(() => {
					this.dialogRef && this.dialogRef.close();
					this.openSnackbar('Nastala chyba pri upravovaní eventu pre rozvrh.');
					// this.openSnackbar(this.localeService.translate(marker('timetable-events.put.error')))
				})
			),
		{ dispatch: false }
	);

	deleteTimetableEvents$ = createEffect(() =>
		this.actions$.pipe(
			ofType(timetableEventsActions.deleteTimetableEvent),
			withLatestFrom(
				this.store.select(schemaSelectors.selectScheme),
				this.store.select(timetablesSelector.selectTimetable)
			),
			map((value) => {
				return {
					scheme: value[1].name,
					timetable: value[2],
					event: value[0].event,
				};
			}),
			switchMap((values) => {
				this.dialogRef = this.dialog.open(LoadingDialogComponent, {
					data: {
						text: 'Prebieha vymazávanie eventu pre rozvrhu.',
					},
					disableClose: true,
				});
				return this.timetableService
					.deleteTimetableEvent(values.scheme, values.timetable, values.event)
					.pipe(
						map((event) =>
							putTimetableEventSuccess({
								event: event,
							})
						),
						catchError((error) => of(putTimetableEventFailure({ error: error })))
					);
			})
		)
	);

	deleteTimetableEventsSuccess$ = createEffect(
		() =>
			this.actions$.pipe(
				ofType(deleteTimetableEventSuccess),
				map(() => {
					this.dialogRef && this.dialogRef.close();
					this.openSnackbar('Event bol úspešne vymazaný.');
				})
			),
		{ dispatch: false }
	);

	deleteTimetableEventsFailure$ = createEffect(
		() =>
			this.actions$.pipe(
				ofType(deleteTimetableEventFailure),
				map(() => {
					this.dialogRef && this.dialogRef.close();
					this.openSnackbar('Nastala chyba pri vymazávaní eventu pre rozvrh.');
					// this.openSnackbar(this.localeService.translate(marker('timetable-events.delete.error')))
				})
			),
		{ dispatch: false }
	);

	importTimetableEvents$ = createEffect(() =>
		this.actions$.pipe(
			ofType(timetableEventsActions.importTimetableEvents),
			withLatestFrom(
				this.store.select(schemaSelectors.selectScheme),
				this.store.select(timetablesSelector.selectTimetable)
			),
			map((value) => {
				return {
					scheme: value[1].name,
					timetable: value[2],
				};
			}),
			switchMap((values) => {
				this.dialogRef = this.dialog.open(LoadingDialogComponent, {
					data: {
						text: 'Prebieha import eventov do rozvrhu.',
					},
					disableClose: true,
				});
				return this.timetableService
					.importTimetableEvents(values.scheme, values.timetable)
					.pipe(
						map((timetable) =>
							importTimetableEventsSuccess({
								timetable: timetable,
							})
						),
						catchError((error) => of(importTimetableEventsFailure({ error: error })))
					);
			})
		)
	);

	importTimetableEventsSuccess$ = createEffect(
		() =>
			this.actions$.pipe(
				ofType(importTimetableEventsSuccess),
				map(() => {
					this.dialogRef && this.dialogRef.close();
					this.openSnackbar('Eventy pre rozvrh boli úspešne načítané.');
				})
			),
		{ dispatch: false }
	);

	importTimetableEventsFailure$ = createEffect(
		() =>
			this.actions$.pipe(
				ofType(importTimetableEventsFailure),
				map(() => {
					this.dialogRef && this.dialogRef.close();
					this.openSnackbar('Nastala chyba pri načítavaní eventov pre rozvrh.');
					// this.openSnackbar(this.localeService.translate(marker('timetable-events.import.error')))
				})
			),
		{ dispatch: false }
	);
	constructor(
		private actions$: Actions,
		private store: Store,
		private timetableService: TimetableDataService,
		private dialog: MatDialog,
		snack: MatSnackBar
	) {
		super(snack);
	}
}
