import { Pipe, PipeTransform } from '@angular/core';
import { StateEnum } from '@elisa11/elisa-api-angular-client';
import { CommonText } from '@data/common-text';

@Pipe({
	name: 'statusIcon',
})
export class StatusIconPipe implements PipeTransform {
	transform(status: StateEnum, ...args: unknown[]): unknown {
		return (
			CommonText.StatusValues.find((value) => {
				return value.value === status;
			}).icon ?? 'error'
		);
	}
}
