import { createSelector } from '@ngrx/store';
import {
	adapter,
	getCount,
	getLoading,
	userSubjectRoleFeatureKey,
} from '@shared/store/reducers/user-subject-role.reducer';
import { selectSharedState } from '@shared/store/selectors/shared.selectors';

export const selectUserSubjectRoleState = createSelector(
	selectSharedState,
	(state) => state[userSubjectRoleFeatureKey]
);
export const { selectIds, selectEntities, selectAll } = adapter.getSelectors(
	selectUserSubjectRoleState
);
export const selectLoading = createSelector(selectUserSubjectRoleState, getLoading);
export const selectTotalCount = createSelector(selectUserSubjectRoleState, getCount);
