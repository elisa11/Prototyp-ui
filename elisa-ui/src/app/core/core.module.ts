import { NgModule } from '@angular/core';
import { HttpClientModule, HttpClientXsrfModule } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { environment } from '@environment/environment';
import {
	ApiModule as ElisaApiModule,
	Configuration as ElisaConfiguration,
} from '@elisa11/elisa-api-angular-client';
import { TranslateModule } from '@ngx-translate/core';
import { registerLocaleData } from '@angular/common';
import localeCs from '@angular/common/locales/cs';
import localeSk from '@angular/common/locales/sk';
import {
	dateAdapterProvider,
	dateFormatsProvider,
	dateLocaleProvider,
	localeProvider,
} from '@core/provider/locale.provider';
import { NgxMaskModule } from 'ngx-mask';
import { LoggerModule } from 'ngx-logger';
import { JwtModule } from '@auth0/angular-jwt';
import { errorInterceptorProvider } from '@core/provider/error-interceptor.provider';
import { jwtInterceptorProvider } from '@core/provider/jwt-interceptor.provider';
import { MomentProvider } from '@core/provider/moment.provider';
import { ChipProvider } from '@core/provider/chip.provider';
import { PaginatorProvider } from '@core/provider/paginator.provider';
import { StepperProvider } from '@core/provider/stepper.provider';
import { StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { EffectsModule } from '@ngrx/effects';
import { RouterEffects } from '@core/store/effects/router.effects';
import { StoreRouterConnectingModule } from '@ngrx/router-store';
import { metaReducers, ROOT_REDUCERS } from '@core/store/reducers';
import { LayoutEffects } from './store/effects/layout.effects';
import { LanguageEffects } from './store/effects/language.effects';
import { SnackBarConfigProvider } from '@core/provider/snack-config.provider';
import { tooltipProvider } from '@core/provider/tooltip.provider';
import { ErrorStateMatcherProvider } from '@core/provider/error-state-matcher.provider';
import { SchemaEffects } from './store/effects/schema.effects';
import { AppEffects } from '@core/store/effects/app.effects';
import { RequirementStatisticsEffects } from './store/effects/requirement-statistics.effects';

export function tokenGetter(): string {
	return localStorage.getItem('access_token');
}

export function elisaApiConfigFactory(): ElisaConfiguration {
	return new ElisaConfiguration({
		basePath: environment.service.elisa.url,
		apiKeys: {},
	});
}

registerLocaleData(localeCs, 'cs');
registerLocaleData(localeSk, 'sk');

@NgModule({
	imports: [
		BrowserModule,
		BrowserAnimationsModule,
		HttpClientModule,
		ElisaApiModule.forRoot(elisaApiConfigFactory),
		NgxMaskModule.forRoot(),
		HttpClientXsrfModule.withOptions({ headerName: 'Authorization' }),
		JwtModule.forRoot({
			config: {
				tokenGetter,
				allowedDomains: [environment.service.elisa.url],
				throwNoTokenError: true,
				disallowedRoutes: [/\/api-gateway\/.*/],
			},
		}),
		TranslateModule.forRoot({
			useDefaultLang: false,
		}),
		LoggerModule.forRoot({
			level: environment.log.level,
			serverLogLevel: environment.log.serverLogLevel,
		}),
		StoreModule.forRoot(ROOT_REDUCERS, {
			metaReducers,
			runtimeChecks: {
				// strictStateImmutability and strictActionImmutability are enabled by default
				strictStateSerializability: true,
				strictActionSerializability: false,
				strictActionWithinNgZone: true,
				strictActionTypeUniqueness: true,
			},
		}),
		EffectsModule.forRoot([
			RouterEffects,
			LayoutEffects,
			LanguageEffects,
			SchemaEffects,
			AppEffects,
		]),
		StoreRouterConnectingModule.forRoot(),
		StoreDevtoolsModule.instrument({
			name: 'Elisa-UI',
			maxAge: 30,
			logOnly: environment.param.environment === 'prod',
		}),
		EffectsModule.forFeature([RequirementStatisticsEffects]),
	],
	providers: [
		localeProvider,
		errorInterceptorProvider,
		jwtInterceptorProvider,
		dateLocaleProvider,
		dateAdapterProvider,
		dateFormatsProvider,
		MomentProvider,
		ChipProvider,
		PaginatorProvider,
		StepperProvider,
		SnackBarConfigProvider,
		tooltipProvider,
		ErrorStateMatcherProvider,
	],
})
export class CoreModule {}
