export interface MenuItem {
	label: string;
	link?: string;
	icon?: string;
	badge?: number;
	showOnSmall?: boolean;
	showOnMedium?: boolean;
	showOnLarge?: boolean;
	version?: boolean;
	forRoles: number[];
}

export type MenuItems = MenuItem[];
