import { createSelector } from '@ngrx/store';
import * as fromRoom from '../reducers/room.reducer';
import { selectTimetablesState } from './timetables.selectors';
import { Room } from '@elisa11/elisa-api-angular-client';

export const selectRoomState = createSelector(
	selectTimetablesState,
	(s) => s[fromRoom.roomFeatureKey]
);

export const selectSearchIds = createSelector(selectRoomState, fromRoom.getSelectedIds);
export const selectTotalCount = createSelector(selectRoomState, fromRoom.getCount);
export const selectSearchLoading = createSelector(selectRoomState, fromRoom.getLoading);

export const { selectIds, selectEntities } =
	fromRoom.adapter.getSelectors(selectRoomState);

export const selectSearchResults = createSelector(
	selectEntities,
	selectSearchIds,
	(data, searchIds) => {
		return searchIds.map((id) => data[id]).filter((d): d is Room => d != null);
	}
);
