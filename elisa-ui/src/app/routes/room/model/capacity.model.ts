export interface Capacity {
	min: number;
	max: number;
}
