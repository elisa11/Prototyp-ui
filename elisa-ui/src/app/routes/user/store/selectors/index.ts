import * as userSelector from './user.selectors';
import * as searchSelector from './search-user.selectors';
import * as userDepartmentSelector from './user-department.selectors';
import * as departmentSearchSelector from './department-search.selectors';
import * as departmentSelector from './department.selectors';

export {
	searchSelector,
	userSelector,
	userDepartmentSelector,
	departmentSearchSelector,
	departmentSelector,
};
