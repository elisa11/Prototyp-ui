import { createReducer, on } from '@ngrx/store';
import { createEntityAdapter, EntityAdapter, EntityState } from '@ngrx/entity';
import { UserDepartment } from '@elisa11/elisa-api-angular-client';
import { UserActions, UserDepartmentApiActions } from '../actions';

export type UserDepartments = { id: number; data: UserDepartment[] };

export const userDepartmentFeatureKey = 'userDepartment';

export interface State extends EntityState<UserDepartments> {
	readonly selectedUserId: number | null;
}

export const adapter: EntityAdapter<UserDepartments> =
	createEntityAdapter<UserDepartments>({
		selectId: (ud: UserDepartments) => ud.id,
		sortComparer: false,
	});

export const initialState: State = adapter.getInitialState({
	selectedUserId: null,
});

export const reducer = createReducer(
	initialState,
	on(UserActions.selectUser, (state: State, { id }) => ({
		...state,
		selectedUserId: id,
	})),
	on(UserDepartmentApiActions.loadUserDepartmentsSuccess, (state: State, { data }) =>
		adapter.upsertOne(data, state)
	)
);

export const getSelectedUserId = (state: State) => state.selectedUserId;
