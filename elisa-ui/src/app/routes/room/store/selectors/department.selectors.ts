import { createSelector } from '@ngrx/store';
import { selectRoomsState } from './room.selectors';
import * as fromDep from '../reducers/department.reducer';

export const selectDepartmentState = createSelector(
	selectRoomsState,
	(s) => s[fromDep.departmentFeatureKey]
);

export const { selectIds: selectIds, selectEntities: selectEntities } =
	fromDep.adapter.getSelectors(selectDepartmentState);

export const selectTotalCount = createSelector(selectDepartmentState, fromDep.getCount);
export const selectLastLoaded = createSelector(
	selectDepartmentState,
	fromDep.getLastLoaded
);
