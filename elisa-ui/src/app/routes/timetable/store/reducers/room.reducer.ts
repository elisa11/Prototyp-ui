import { createReducer, on } from '@ngrx/store';
import { createEntityAdapter, EntityAdapter, EntityState } from '@ngrx/entity';
import { Room } from '@elisa11/elisa-api-angular-client';
import { roomActions } from '../actions';

export const roomFeatureKey = 'room';

export interface State extends EntityState<Room> {
	readonly selectedIds: number[];
	readonly count: number;
	readonly loading: boolean;
}

export const adapter: EntityAdapter<Room> = createEntityAdapter<Room>({
	selectId: (model: Room) => model.id,
	sortComparer: false,
});

export const initialState = adapter.getInitialState({
	selectedIds: [],
	count: 0,
	loading: false,
});

export const reducer = createReducer(
	initialState,
	on(roomActions.loadRoomsSuccess, (state, { data }) =>
		adapter.upsertMany(data.results, {
			...state,
			selectedIds: data.results.map((r) => r.id),
			count: data.count,
			loading: false,
		})
	),
	on(roomActions.loadRooms, (state) => ({ ...state, loading: true })),
	on(roomActions.loadRoomsFailure, (state) => ({ ...state, loading: false }))
);

export const getSelectedIds = (state: State) => state.selectedIds;
export const getLoading = (state: State) => state.loading;
export const getCount = (state: State) => state.count;
