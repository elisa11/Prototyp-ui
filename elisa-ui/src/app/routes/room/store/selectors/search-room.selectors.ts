import { createSelector } from '@ngrx/store';
import * as fromRoomSearch from '../reducers/search-room.reducer';
import { Room } from '@elisa11/elisa-api-angular-client';
import { selectEntities, selectRoomsState } from './room.selectors';

export const selectSearchState = createSelector(
	selectRoomsState,
	(s) => s[fromRoomSearch.searchRoomFeatureKey]
);

export const selectSearchIds = createSelector(selectSearchState, fromRoomSearch.getIds);
export const selectSearchQuery = createSelector(
	selectSearchState,
	fromRoomSearch.getQuery
);
export const selectSearchLoading = createSelector(
	selectSearchState,
	fromRoomSearch.getLoading
);
export const selectSearchError = createSelector(
	selectSearchState,
	fromRoomSearch.getError
);

export const selectSearchResults = createSelector(
	selectEntities,
	selectSearchIds,
	(data, searchIds) => {
		return searchIds.map((id) => data[id]).filter((d): d is Room => d != null);
	}
);
