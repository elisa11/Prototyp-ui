import { Injectable } from '@angular/core';
import {
	ActivatedRouteSnapshot,
	CanActivate,
	Router,
	RouterStateSnapshot,
	UrlTree,
} from '@angular/router';
import { Observable, of } from 'rxjs';
import {
	catchError,
	exhaustMap,
	filter,
	map,
	switchMap,
	take,
	tap,
} from 'rxjs/operators';
import { requirementSelectors, searchRequirementSelectors } from '../store/selectors';
import { schemaSelectors } from '@core/store/selectors';
import { requirementActions } from '../store/actions';
import { Store } from '@ngrx/store';
import { NGXLogger } from 'ngx-logger';
import { RequirementsCacheService } from '@core/service/cache/requirements-cache.service';

const tag = 'RequirementExistsGuard';

@Injectable({
	providedIn: 'root',
})
export class RequirementExistsGuard implements CanActivate {
	constructor(
		private store: Store,
		private service: RequirementsCacheService,
		private logger: NGXLogger,
		private router: Router
	) {}

	canActivate(
		route: ActivatedRouteSnapshot,
		state: RouterStateSnapshot
	): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
		return this.waitForCollectionToLoad().pipe(
			switchMap(() => this.hasRequirement(route.params.id))
		);
	}

	private waitForCollectionToLoad(): Observable<boolean> {
		return this.store.select(searchRequirementSelectors.selectSearchLoading).pipe(
			filter((loading) => !loading),
			take(1)
		);
	}

	private hasRequirementInStore(id: number): Observable<boolean> {
		return this.store.select(requirementSelectors.selectEntities).pipe(
			map((entities) => !!entities[id]),
			take(1)
		);
	}

	private hasRequirementInApi(id: number): Observable<boolean> {
		return this.store.select(schemaSelectors.selectScheme).pipe(
			exhaustMap((version) =>
				this.service.getRequirement(id, version.name).pipe(
					map((entity) => requirementActions.loadRequirement({ data: entity })),
					tap((action) => this.store.dispatch(action)),
					map((req) => !!req),
					catchError(() => {
						this.router.navigate(['/404']);
						return of(false);
					})
				)
			)
		);
	}

	private hasRequirement(id: number): Observable<boolean> {
		return this.hasRequirementInStore(id).pipe(
			switchMap((inStore) => {
				if (inStore) {
					this.logger.debug(tag, 'hasRequirement', 'in store');
					return of(inStore);
				}
				this.logger.debug(tag, 'hasRequirement try API');
				return this.hasRequirementInApi(id);
			})
		);
	}
}
