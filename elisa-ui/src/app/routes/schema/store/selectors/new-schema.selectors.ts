import { createSelector } from '@ngrx/store';
import { selectSchemasState } from './schema.selectors';
import * as fromFilterParent from '../reducers/new-schema.reducer';

export const selectNewSchemaState = createSelector(
	selectSchemasState,
	(state) => state[fromFilterParent.newSchemaFeatureKey]
);

export const selectParentId = createSelector(
	selectNewSchemaState,
	fromFilterParent.getParentId
);

export const selectPeriodId = createSelector(
	selectNewSchemaState,
	fromFilterParent.getPeriodId
);

export const selectImportLoading = createSelector(
	selectNewSchemaState,
	fromFilterParent.getImportLoading
);
