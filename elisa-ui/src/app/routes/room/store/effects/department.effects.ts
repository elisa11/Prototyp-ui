import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { MatSnackBar } from '@angular/material/snack-bar';
import { SnackEffects } from '@core/generics/snack.effects';
import { DepartmentsCacheService } from '@core/service/cache/departments-cache.service';
import { departmentApiActions, roomApiActions } from '../actions';
import { catchError, map, switchMap, tap, withLatestFrom } from 'rxjs/operators';
import { of, zip } from 'rxjs';
import { resetCachedData } from '@core/store/actions/settings.actions';
import { NGXLogger } from 'ngx-logger';
import { schemaSelectors } from '@core/store/selectors';

const tag = 'DepartmentEffects';

@Injectable()
export class DepartmentEffects extends SnackEffects {
	resetData$ = createEffect(
		() =>
			this.actions$.pipe(
				ofType(resetCachedData),
				tap(() => this.service.clearCache())
			),
		{ dispatch: false }
	);

	loadDepartments$ = createEffect(() =>
		this.actions$.pipe(
			ofType(roomApiActions.searchRoomSuccess),
			map((action) => action.data.results.map((r) => r.department)),
			withLatestFrom(this.store.select(schemaSelectors.selectScheme)),
			map(([action, scheme]) => {
				console.log(tag, 'loadDepartments$', scheme);

				return { ids: action, version: scheme.name };
			}),
			switchMap(({ ids, version }) => {
				console.log(tag, 'loadDepartments$', ids, version);
				if (ids === null || ids.length == 0) {
					return of(departmentApiActions.loadDepartmentListSuccess({ data: [] }));
				}

				return zip(...ids.map((id) => this.service.getDepartment(version, id))).pipe(
					map((data) => departmentApiActions.loadDepartmentListSuccess({ data })),
					catchError((error) => {
						this.logger.error(tag, 'loadDepartments$', error);
						return of(departmentApiActions.loadDepartmentListFailure({ error }));
					})
				);
			})
		)
	);

	loadDepartmentsFailure$ = createEffect(
		() =>
			this.actions$.pipe(
				ofType(departmentApiActions.loadDepartmentListFailure),
				tap((err) => this.openSnackbar(err.error))
			),
		{ dispatch: false }
	);

	constructor(
		private actions$: Actions,
		private service: DepartmentsCacheService,
		private store: Store,
		private logger: NGXLogger,
		snack: MatSnackBar
	) {
		super(snack);
	}
}
