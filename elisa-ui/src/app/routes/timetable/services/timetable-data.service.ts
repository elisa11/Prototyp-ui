import { Injectable } from '@angular/core';
import { Observable, shareReplay } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {
	GroupsService,
	PaginatedGroupList,
	TimetablesService,
	PaginatedEventList,
	Timetable,
	PaginatedActivityList,
	ActivitiesService,
	PaginatedCollisionList,
	Collision,
	Event,
} from '@elisa11/elisa-api-angular-client';
import { PaginatedTimetableList } from '@elisa11/elisa-api-angular-client';

@Injectable({
	providedIn: 'root',
})
export class TimetableDataService {
	httpOptions = {
		headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
	};

	constructor(
		private http: HttpClient,
		private groupService: GroupsService,
		private timetablesService: TimetablesService,
		private activitiesService: ActivitiesService
	) {}

	listGroups(schema: string): Observable<PaginatedGroupList> {
		return this.groupService.groupsList(schema, undefined, 500);
		// .pipe(shareReplay({ bufferSize: 1, refCount: true }));
	}

	listActivities(schema: string): Observable<PaginatedActivityList> {
		return this.activitiesService.activitiesList(schema);
	}

	// TIMETABLES

	listTimetables(schema: string): Observable<PaginatedTimetableList> {
		return this.timetablesService.timetablesList(schema);
	}

	postTimetable(schema: string, name: string): Observable<Timetable> {
		const timetable: Timetable = {
			created_at: '',
			id: 0,
			// major_version: 0,
			// minor_version: 0,
			owner: 0,
			updated_at: '',
			name: name,
		};
		return this.timetablesService.timetablesCreate(schema, timetable);
	}

	deleteTimetable(schema: string, id: number): Observable<any> {
		return this.timetablesService.timetablesDestroy(schema, id);
	}

	// TIMETABLES COLLISIONS
	getAllCollisions(
		schema: string,
		timetable: Timetable
	): Observable<PaginatedCollisionList> {
		return this.timetablesService.timetablesCollisionsList(schema, timetable.id);
	}

	getCollision(
		schema: string,
		timetable: Timetable,
		collision: number
	): Observable<Collision> {
		return this.timetablesService.timetablesCollision(schema, collision, timetable.id);
	}

	updateCollision(): Observable<Collision> {
		return null;
	}

	//TIMETABLE EVENTS

	getTimetableEvents(
		schema: string,
		timetableId: number,
		activityCategoryIdIn?: Array<number>,
		activityCourseDepartmentIdIn?: Array<number>,
		activityCourseId?: number,
		activityId?: number,
		day?:
			| 'FRIDAY'
			| 'MONDAY'
			| 'SATURDAY'
			| 'SUNDAY'
			| 'THURSDAY'
			| 'TUESDAY'
			| 'WEDNESDAY',
		groupsIdIn?: Array<number>,
		roomId?: number
	): Observable<PaginatedEventList> {
		return this.timetablesService.timetablesEventsList(
			schema,
			timetableId,
			activityCategoryIdIn,
			activityCourseDepartmentIdIn,
			null,
			null,
			null,
			groupsIdIn,
			null,
			null,
			[roomId]
		);
		//.pipe(shareReplay({ bufferSize: 1, refCount: true }))
	}

	postTimetableEvents(
		schema: string,
		timetable: Timetable,
		event: Event
	): Observable<Event> {
		return this.timetablesService.timetablesEventsCreate(schema, timetable.id, event);
	}

	getTimetableEvent(
		schema: string,
		timetable: Timetable,
		event: Event
	): Observable<Event> {
		return this.timetablesService.timetablesEvent(schema, event.id, timetable.id);
	}

	putTimetableEvent(
		schema: string,
		timetable: Timetable,
		event: Event
	): Observable<Event> {
		return this.timetablesService.timetablesEventsPartialUpdate(
			schema,
			event.id,
			timetable.id,
			event
		);
	}

	deleteTimetableEvent(
		schema: string,
		timetable: Timetable,
		event: Event
	): Observable<Event> {
		return this.timetablesService.timetablesEventsDestroy(schema, event.id, timetable.id);
	}

	importTimetableEvents(schema: string, timetable: Timetable) {
		console.log('ID timetable ' + timetable.id);
		return this.timetablesService.timetablesImportEventsCreate(
			schema,
			timetable.id,
			timetable
		);
	}
}
