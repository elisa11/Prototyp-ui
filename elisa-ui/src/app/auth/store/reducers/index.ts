import { Action, combineReducers } from '@ngrx/store';
import * as fromAuth from './auth.reducer';
import * as fromRoot from '@core/store/reducers';
import * as fromLoginPage from './login-page.reducer';
import * as fromRole from './role.reducer';

export const authFeatureKey = 'auth';

export interface AuthState {
	[fromAuth.statusFeatureKey]: fromAuth.State;
	[fromLoginPage.loginPageFeatureKey]: fromLoginPage.State;
	[fromRole.roleFeatureKey]: fromRole.State;
}

export interface State extends fromRoot.AppState {
	[authFeatureKey]: AuthState;
}

export function reducers(state: AuthState | undefined, action: Action) {
	return combineReducers({
		[fromAuth.statusFeatureKey]: fromAuth.reducer,
		[fromRole.roleFeatureKey]: fromRole.reducer,
		[fromLoginPage.loginPageFeatureKey]: fromLoginPage.reducer,
	})(state, action);
}
