import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { SnackEffects } from '@core/generics/snack.effects';
import { MatSnackBar } from '@angular/material/snack-bar';
import { catchError, map, skip, switchMap, takeUntil, tap, take } from 'rxjs/operators';
import { filterActions } from '../actions';
import { of, forkJoin, Observable, combineLatest, withLatestFrom } from 'rxjs';
import { DepartmentsCacheService } from '@core/service/cache/departments-cache.service';
import { PaginatorFilter } from '@data/filter/paginator.filter';
import { PaginatedDepartmentList } from '@elisa11/elisa-api-angular-client';
import { GroupsCacheService } from 'src/app/routes/group/services/groups-cache.service';
import { Store } from '@ngrx/store';
import { schemaSelectors } from '@core/store/selectors';
const tag = 'DepartmentEffects';

@Injectable()
export class DepartmentEffects extends SnackEffects {
	getDepartments$ = createEffect(() =>
		this.actions$.pipe(
			ofType(filterActions.getFaculties),
			withLatestFrom(this.store.select(schemaSelectors.selectScheme)),
			map((value) => {
				return {
					version: value[1].name,
				};
			}),
			switchMap(({ version }) => {
				const nextSearch$ = this.actions$.pipe(
					ofType(filterActions.getFaculties),
					skip(1)
				);
				let query = { limit: 15, offset: 0 } as PaginatorFilter;

				return this.departmentsService.listDepartments(query, version).pipe(
					takeUntil(nextSearch$),
					map((data) => filterActions.getFacultiesSuccess({ data })),
					catchError((err) =>
						of(filterActions.getFacultiesFailure({ error: err.message }))
					)
				);
			})
		)
	);
	getFilterOptions$ = createEffect(() =>
		this.actions$.pipe(
			ofType(filterActions.getFilterOptions),
			withLatestFrom(this.store.select(schemaSelectors.selectScheme)),
			map((value) => {
				return {
					version: value[1].name,
				};
			}),
			switchMap(({ version }) => {
				const nextSearch$ = this.actions$.pipe(
					ofType(filterActions.getFilterOptions),
					skip(1)
				);
				let query = { limit: 100, offset: 0 } as PaginatorFilter;

				const observables = [
					this.departmentsService.listDepartments(query, version),
					this.groupsService.getStudyPlans(version),
					this.groupsService.getStudyYears(version),
					this.groupsService.getSpecializations(version),
				];
				return combineLatest(observables).pipe(
					take(1),
					map(([faculties, studyPlans, years, specializations]) => {
						const data = {
							faculties,
							studyPlans,
							years,
							specializations,
						};
						console.log({ data: data });
						return filterActions.getFilterOptionsSuccess({ data });
					}),
					catchError((err) =>
						of(filterActions.getFilterOptionsFailure({ error: err.message }))
					)
				);
			})
		)
	);

	getDepartmentsFailed$ = createEffect(
		() =>
			this.actions$.pipe(
				ofType(filterActions.getFacultiesFailure),
				tap((err) => this.openSnackbar(err.error))
			),
		{ dispatch: false }
	);

	constructor(
		private actions$: Actions,
		private departmentsService: DepartmentsCacheService,
		private groupsService: GroupsCacheService,
		private store: Store,
		snack: MatSnackBar
	) {
		super(snack);
	}
}
