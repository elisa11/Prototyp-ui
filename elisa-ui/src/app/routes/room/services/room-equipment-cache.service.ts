import { Injectable } from '@angular/core';
import { AbstractCacheService } from '@core/service/cache/abstract-cache.service';
import {
  RoomEquipmentService,
} from '@elisa11/elisa-api-angular-client';
import { PaginatorFilter } from '@data/filter/paginator.filter';
import { Observable } from 'rxjs';
import { shareReplay } from 'rxjs/operators';
import { PaginatedRoomEquipmentList } from '@elisa11/elisa-api-angular-client';
import { BasicUtils } from '@shared/utils/basic.utils';

@Injectable({
	providedIn: 'root',
})
export class RoomEquipmentCacheService extends AbstractCacheService {
	private listRoomEquipmentCache = new Map();

	constructor(private roomService: RoomEquipmentService) {
		super();
	}

	listRoomEquipment(
		filter: PaginatorFilter,
		schema: string,
		roomId: number
	): Observable<PaginatedRoomEquipmentList> {
		const key = { filter, schema, roomId };
		const obs = this.listRoomEquipmentCache.get(key);
		if (obs) {
			return obs;
		}

		const response = this.roomService
			.roomEquipmentList(
				schema,
				undefined,
				filter.limit,
				filter.offset,
				BasicUtils.getSortBy(filter),
				roomId,
				filter.search
			)
			.pipe(shareReplay({ bufferSize: 1, refCount: true }));
		this.listRoomEquipmentCache.set(key, response);
		return response;
	}

	clearCache(): void {
		this.listRoomEquipmentCache.clear();
	}
}
