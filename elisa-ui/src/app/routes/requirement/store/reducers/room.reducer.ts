import { createReducer, on } from '@ngrx/store';
import { createEntityAdapter, EntityAdapter, EntityState } from '@ngrx/entity';
import { Room } from '@elisa11/elisa-api-angular-client';
import { roomApiActions } from '../actions';

export const roomFeatureKey = 'room';

export interface State extends EntityState<Room> {
	readonly selectedIds: number[];
	readonly count: number;
	readonly loading: boolean;
}

export const adapter: EntityAdapter<Room> = createEntityAdapter<Room>({
	selectId: (model: Room) => model.id,
	sortComparer: false,
});

export const initialState = adapter.getInitialState({
	selectedIds: [],
	count: 0,
	loading: false,
});

export const reducer = createReducer(
	initialState,
	on(roomApiActions.loadRoomsSuccess, (state, { data }) =>
		adapter.upsertMany(data.results, {
			...state,
			selectedIds: data.results.map((r) => r.id),
			count: data.count,
			loading: false,
		})
	),
	on(roomApiActions.loadRooms, (state) => ({ ...state, loading: true })),
	on(roomApiActions.loadRoomsFailure, (state) => ({ ...state, loading: false }))
);

export const getSelectedIds = (state: State) => state.selectedIds;
export const getLoading = (state: State) => state.loading;
export const getCount = (state: State) => state.count;
