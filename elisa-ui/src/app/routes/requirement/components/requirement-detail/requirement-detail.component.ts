import { Component, EventEmitter, Input, Output } from '@angular/core';
import { AbstractDetailComponent } from '@core/generics/abstract-detail.component';
import { RequirementModel } from '../../model/requirement.model';
import { CommonText } from '@data/common-text';
import { CommentModel } from '@data/model/comment.model';
import { PaginatorFilter } from '@data/filter/paginator.filter';
import { ActivityModel } from '../../model/activity.model';
import { SubjectTeacherModel } from '../../model/subject-teacher.model';

@Component({
	selector: 'app-requirement-detail',
	templateUrl: './requirement-detail.component.html',
})
export class RequirementDetailComponent extends AbstractDetailComponent<RequirementModel> {
	moduleId = 'requirement';
	moduleTitle = CommonText.navLinks.find((value) => value.link === this.moduleId).label;
	@Input()
	loggedUserId: number;
	@Input()
	pagination: number[] = [5];
	@Output()
	private onNewComment = new EventEmitter<CommentModel>();
	@Output()
	private onEditComment = new EventEmitter<CommentModel>();
	@Output()
	private onDeleteComment = new EventEmitter<number>();
	@Output()
	private onSubjectTeachersFilterChange = new EventEmitter<PaginatorFilter>();
	@Input()
	activityData: ActivityModel[] = [];
	constructor() {
		super();
	}

	subjectTeachersFilterChange(filter: PaginatorFilter) {
		this.onSubjectTeachersFilterChange.emit(filter);
	}

	handleNewComment(data: CommentModel) {
		this.onNewComment.emit(data);
	}

	handleDeleteComment(cid: number) {
		this.onDeleteComment.emit(cid);
	}

	handleEditComment(commentModel: CommentModel) {
		this.onEditComment.emit(commentModel);
	}
}
