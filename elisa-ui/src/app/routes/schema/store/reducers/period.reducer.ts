import { createReducer, on } from '@ngrx/store';
import { createEntityAdapter, EntityAdapter, EntityState } from '@ngrx/entity';
import { Period } from '@elisa11/elisa-api-angular-client';
import { loadParentPeriodsSuccess, loadPeriodsSuccess } from '../actions/period.actions';
import { searchPeriodsSuccess } from '../actions/period-api.actions';

export const periodFeatureKey = 'period';

export interface State extends EntityState<Period> {
	readonly count: number;
}

export const adapter: EntityAdapter<Period> = createEntityAdapter<Period>({
	selectId: (period: Period) => period.id,
	sortComparer: false,
});

export const initialState: State = adapter.getInitialState({ count: 0 });

export const reducer = createReducer(
	initialState,
	on(loadPeriodsSuccess, (state: State, { data }) => adapter.upsertMany(data, state)),
	on(loadParentPeriodsSuccess, (state: State, { data }) =>
		adapter.upsertMany(data, state)
	),
	on(searchPeriodsSuccess, (state: State, { periods }) =>
		adapter.upsertMany(periods.results, {
			...state,
			count: periods.count,
		})
	)
);

export const getCount = (state: State) => state.count;
