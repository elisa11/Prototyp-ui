import { MatIconModule } from '@angular/material/icon';
import { MatCardModule } from '@angular/material/card';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatTableModule } from '@angular/material/table';
import { MatSortModule } from '@angular/material/sort';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatListModule } from '@angular/material/list';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';

export const MATERIAL_MODULES = [
	MatIconModule,
	MatCardModule,
	MatInputModule,
	MatButtonModule,
  MatListModule,
	MatTooltipModule,
	MatTableModule,
	MatSortModule,
	MatPaginatorModule,
  MatProgressSpinnerModule,

];
