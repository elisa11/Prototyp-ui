import { createReducer, on } from '@ngrx/store';
import { submitFormSuccess, updateForm } from '@shared/store/actions/form.actions';
import { NewVersionFormModel } from '@shared/model/new-version-form.model';
import { CommentFormModel } from '@shared/model/comment-form.model';
import moment from 'moment/moment';
import { SubjectTeacherModel } from '../../../routes/requirement/model/subject-teacher.model';
import { Group } from '@elisa11/elisa-api-angular-client';
import { ActivityModel } from '../../../routes/requirement/model/activity.model';
import { RequirementFormModel } from '@shared/model/requirement-form.model';
import { RoomFormModel } from '@shared/model/room-form.model';
import { CourseFormModel } from '@shared/model/course-form.model';
import { UserFormModel } from '@shared/model/user-form.model';

export const formFeatureKey = 'form';

export interface FormState {
	readonly newVersion: NewVersionFormModel;
	readonly comment: CommentFormModel;
	readonly teacher: SubjectTeacherModel;
	readonly group: Group;
	readonly split: ActivityModel;
	readonly requirement: RequirementFormModel;
	readonly room: RoomFormModel;
	readonly course: CourseFormModel;
	readonly user: UserFormModel;
}

export const initialState: FormState = {
	newVersion: { name: '', period: null },
	comment: { text: '', created_by: 0, created_at: moment().toISOString(), id: 0 },
	teacher: { user: undefined, roles: [] },
	group: {} as Group,
	split: {} as ActivityModel,
	requirement: {} as RequirementFormModel,
  room: {} as RoomFormModel,
  course: {} as CourseFormModel,
  user: {} as UserFormModel
};

export const reducer = createReducer(
	initialState,
	on(updateForm, (state: FormState, { path, value }) => ({ ...state, [path]: value })),
	on(submitFormSuccess, (state: FormState, { path }) => ({
		...state,
		[path]: initialState[path],
	}))
);

export enum Paths {
	newVersion = 'newVersion',
	comment = 'comment',
	teacher = 'teacher',
	group = 'group',
	split = 'split',
	requirement = 'requirement',
  room = 'room',
  course = 'course',
  user = 'user',
}

export const getNewVersion = (state: FormState) => state.newVersion;
export const getComment = (state: FormState) => state.comment;
export const getTeacher = (state: FormState) => state.teacher;
export const getGroup = (state: FormState) => state.group;
export const getSplit = (state: FormState) => state.split;
export const getRequirement = (state: FormState) => state.requirement;
export const getCourse = (state: FormState) => state.course;
export const getRoom = (state: FormState) => state.room;
export const getUser = (state: FormState) => state.user;
