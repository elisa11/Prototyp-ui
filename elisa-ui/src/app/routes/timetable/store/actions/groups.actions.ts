import { createAction, props } from '@ngrx/store';
import { Group } from '@elisa11/elisa-api-angular-client';

export const loadGroups = createAction(
  '[Timetable - Group] Load Groups',
);

export const loadGroupsSuccess = createAction(
  '[Timetable - Group] Load Groups Success',
  props<{ groups: Group[] }>()
);

export const loadGroupsFailure = createAction(
  '[Timetable - Group] Load Groups Failure',
  props<{ error: any }>()
);
