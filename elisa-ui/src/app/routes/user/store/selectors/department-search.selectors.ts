import { createSelector } from '@ngrx/store';
import { selectUsersState } from './user.selectors';
import * as fromDepartmentSearch from '../reducers/department-search.reducer';

export const selectDepartmentSearchState = createSelector(
	selectUsersState,
	(state) => state[fromDepartmentSearch.departmentSearchFeatureKey]
);

export const selectLoading = createSelector(
	selectDepartmentSearchState,
	fromDepartmentSearch.getLoading
);

export const selectError = createSelector(
	selectDepartmentSearchState,
	fromDepartmentSearch.getError
);
