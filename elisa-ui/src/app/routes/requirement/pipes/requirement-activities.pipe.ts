import { Pipe, PipeTransform } from '@angular/core';
import { ActivityModel } from '../model/activity.model';
import { RequirementModel } from '../model/requirement.model';

@Pipe({
	name: 'requirementActivities',
})
export class RequirementActivitiesPipe implements PipeTransform {
	transform(model: RequirementModel): ActivityModel[] {
		const category = model.requirement_parts.map((part) => part.category);
		return model.requirement_activities.map((act) => {
			return {
				...act,
				category: category.find((value) => value.id === act.category),
			} as ActivityModel;
		});
	}
}
