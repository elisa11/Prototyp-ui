import { Component, Inject } from '@angular/core';
import { marker } from '@biesbjerg/ngx-translate-extract-marker';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { CollisionDialogModel } from './collision-dialog.model';

@Component({
  template: `
		<h1 mat-dialog-title>{{ title | translate }}</h1>
		<div mat-dialog-content>
			<p>{{ information | translate }}</p>
		</div>
		<div mat-dialog-actions>
			<button [mat-dialog-close]="false" mat-button>
				<mat-icon class="decline-color">cancel</mat-icon>
				{{ ok_label | translate }}
			</button>
		</div>
	`,
})
export class CollisionDialogComponent {
  // title: string = marker('dialog.collision.title');
  // information: string = marker('dialog.collision.information');
  // ok_label: string = marker('dialog.collision.ok_label');
  title: string = "Collision Dialog Title";
  ok_label: string = "Ignorovať";
  no_label: string = "Zrušiť";
  information: string = "Kolízia.";

  constructor(
    public dialogRef: MatDialogRef<CollisionDialogComponent>,
    @Inject(MAT_DIALOG_DATA) private data: CollisionDialogModel
  ) {
    if (data) {
      if (data.title) {
        this.title = data.title;
      }
      if (data.ok_label){
        this.ok_label = data.ok_label
      }
      if (data.no_label){
        this.no_label = data.no_label
      }
      if (data.information){
        this.information = data.information
      }
    }
  }
}
