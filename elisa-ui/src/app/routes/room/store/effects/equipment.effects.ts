import { Actions, createEffect, ofType } from '@ngrx/effects';
import { equipmentApiActions } from '../actions';
import { catchError, map, switchMap, withLatestFrom } from 'rxjs/operators';
import { schemaSelectors } from '@core/store/selectors';
import { from, of } from 'rxjs';
import { loadEquipmentsFailure, loadEquipmentsSuccess } from '../actions/equipment-api.actions';
import { Injectable } from '@angular/core';
import { SnackEffects } from '@core/generics/snack.effects';
import { Store } from '@ngrx/store';
import { NGXLogger } from 'ngx-logger';
import { MatSnackBar } from '@angular/material/snack-bar';
import { EquipmentsCacheService } from '../../services/equipments-cache.service';

const tag = 'EquipmentEffects';

@Injectable()
export class EquipmentEffects extends SnackEffects {

  loadEquipments = createEffect(()=>
    this.actions$.pipe(
      ofType(equipmentApiActions.loadEquipments),
      withLatestFrom(this.store.select(schemaSelectors.selectScheme)),
      map((value) => {
        return { version: value[1].name };
      }),
      switchMap((scheme) =>
        from(this.service.listEquipment(scheme.version).pipe(
          map((equipmentList) => loadEquipmentsSuccess({
            data: equipmentList.results
          })),
          catchError((error)=>of(loadEquipmentsFailure({error})))
          )
        )
      )
    )
  );

  constructor(
    private actions$: Actions,
    private service: EquipmentsCacheService,
    private store: Store,
    private logger: NGXLogger,
    snack: MatSnackBar
  ) {
    super(snack);
  }
}
