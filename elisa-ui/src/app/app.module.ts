import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { CoreModule } from '@core/core.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AppRoutingModule } from './app-routing.module';
import { SharedModule } from '@shared/shared.module';
import { HomeComponent } from './components/home/home.component';
import { DashboardComponent } from './components/home/dashboard/dashboard.component';
import { ToolbarComponent } from './components/toolbar/toolbar.component';
import { ErrorComponent } from './components/error/error.component';
import { NgxFlagIconCssModule } from 'ngx-flag-icon-css';
import { TranslateModule } from '@ngx-translate/core';
import { ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { DIALOGS, MATERIAL_MODULES } from './index';
import { AuthModule } from './auth/auth.module';
import { BrowserModule } from '@angular/platform-browser';

@NgModule({
	declarations: [
		AppComponent,
		HomeComponent,
		DashboardComponent,
		ToolbarComponent,
		ErrorComponent,
		...DIALOGS,
	],
	imports: [
		CoreModule,
		NgbModule,
		AppRoutingModule,
		SharedModule,
		TranslateModule.forChild(),
		NgxFlagIconCssModule,
		ReactiveFormsModule,
		CommonModule,
    BrowserModule,
		...MATERIAL_MODULES,
		AuthModule,
	],
	bootstrap: [AppComponent],
})
export class AppModule {}
