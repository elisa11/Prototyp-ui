import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RoleManager } from './auth/store/model/Roles.model';
import { ErrorComponent } from './components/error/error.component';
import { HomeComponent } from './components/home/home.component';
import { AuthGuard } from './auth/services/auth.guard';
import { RoleGuard } from './auth/services/role.guard';

const routes: Routes = [
	{
		path: '',
		component: HomeComponent,
		canActivate: [AuthGuard, RoleGuard],
		data: { roles: RoleManager.EXCEPT_STUDENT },
	},
	{
		path: 'auth',
		loadChildren: () => import('./auth/auth.module').then((m) => m.AuthModule),
	},
	{
		path: 'course',
		canLoad: [RoleGuard],
		data: { roles: RoleManager.CREATORS },
		loadChildren: () =>
			import('./routes/course/course.module').then((m) => m.CourseModule),
	},
	{
		path: 'department',
		canLoad: [RoleGuard],
		data: { roles: RoleManager.CREATORS },
		loadChildren: () =>
			import('./routes/department/department.module').then((m) => m.DepartmentModule),
	},
	{
		path: 'group',
		canLoad: [RoleGuard],
		data: { roles: RoleManager.CREATORS },
		loadChildren: () => import('./routes/group/group.module').then((m) => m.GroupModule),
	},
	{
		path: 'room',
		canLoad: [RoleGuard],
		data: { roles: RoleManager.CREATORS },
		loadChildren: () => import('./routes/room/room.module').then((m) => m.RoomModule),
	},
	{
		path: 'settings',
		data: { roles: RoleManager.EXCEPT_STUDENT },
		canLoad: [RoleGuard],
		loadChildren: () =>
			import('./routes/settings/settings.module').then((m) => m.SettingsModule),
	},
	{
		path: 'user',
		canLoad: [RoleGuard],
		data: { roles: RoleManager.EXCEPT_STUDENT },
		loadChildren: () => import('./routes/user/user.module').then((m) => m.UserModule),
	},
	{
		path: 'version',
		canLoad: [RoleGuard],
		data: { roles: RoleManager.CREATORS },
		loadChildren: () =>
			import('./routes/schema/schema.module').then((m) => m.SchemaModule),
	},
	{
		path: 'timetable',
		canLoad: [RoleGuard],
		data: { roles: RoleManager.CREATORS },
		loadChildren: () =>
			import('./routes/timetable/timetable.module').then((m) => m.TimetableModule),
	},
	{
		path: 'requirement',
		data: { roles: RoleManager.EXCEPT_STUDENT },
		canLoad: [RoleGuard],
		loadChildren: () =>
			import('./routes/requirement/requirement.module').then((m) => m.RequirementModule),
	},
	{ path: '404', component: ErrorComponent },
	{ path: '**', component: ErrorComponent },
];

@NgModule({
	imports: [RouterModule.forRoot(routes, { useHash: true })],
	exports: [RouterModule],
})
export class AppRoutingModule {}
