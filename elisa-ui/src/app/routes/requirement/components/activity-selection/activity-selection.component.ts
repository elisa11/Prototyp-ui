import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ActivityCategory, Course, StudyForm } from '@elisa11/elisa-api-angular-client';
import { ActivityModel } from '../../model/activity.model';
import { FormBuilder, FormGroup, FormGroupDirective, Validators } from '@angular/forms';
import { formActions } from '@shared/store/actions';
import { Store } from '@ngrx/store';
import { Paths } from '@shared/store/reducers/form.reducer';

@Component({
	selector: 'app-activity-selection',
	templateUrl: './activity-selection.component.html',
})
export class ActivitySelectionComponent implements OnInit {
	@Input() readOnly: boolean = false;
	categories: ActivityCategory[] = []; // remaining options
	formGroup: FormGroup;
	@Input()
	forms: StudyForm[];
	@Input() course: Course;
	path = Paths.split;
	private activityModels: ActivityModel[] = []; // selected
	@Output() private onChange = new EventEmitter<ActivityModel[]>();
	private firstImport = true;

	constructor(private fb: FormBuilder, private store: Store) {
		this.formGroup = fb.group({
			duration: ['', Validators.compose([Validators.required, Validators.min(0)])],
			type: ['', Validators.required],
		});
	}

	get activities() {
		return this.activityModels;
	}

	@Input() set activities(data: ActivityModel[]) {
		if (this.firstImport) {
			this.activityModels = data;
			const selectedCategories = new Set<ActivityCategory>(data.map((a) => a.category));
			this.categories = this.categories.filter((x) => !selectedCategories.has(x));
			this.onChange.emit(this.activities);
		}
	}

	@Input() set allActivityCategories(all: ActivityCategory[]) {
		const selectedCategories = new Set<ActivityCategory>(
			this.activities.map((a) => a.category)
		);
		this.categories = all.filter((x) => !selectedCategories.has(x));
	}

	ngOnInit(): void {
		this.firstImport = true;
	}
	remove(data: ActivityModel): void {
		this.activities = this.activities.filter((d) => d.category.id !== data.category.id);
		this.categories.push(data.category);
		this.onChange.emit(this.activities);
		this.formGroup.reset();
	}

	add(): void {
		const formData = this.formGroup.value;
		const category = formData.type;
		const duration = formData.duration;

		this.activities.push({
			category,
			duration,
			course: this.course ? this.course.id : undefined,
		} as ActivityModel);
		this.categories = this.categories.filter((x) => x.id !== category.id);
		this.onChange.emit(this.activities);
		this.store.dispatch(formActions.submitFormSuccess({ path: this.path }));
	}

	resetForm(form: FormGroupDirective) {
		form.resetForm();
	}
}
