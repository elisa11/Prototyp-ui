import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { RequirementsCacheService } from '@core/service/cache/requirements-cache.service';
import { SnackEffects } from '@core/generics/snack.effects';
import { MatSnackBar } from '@angular/material/snack-bar';
import { resetCachedData } from '@core/store/actions/settings.actions';
import {
	catchError,
	debounceTime,
	map,
	skip,
	switchMap,
	takeUntil,
	tap,
	withLatestFrom,
} from 'rxjs/operators';
import { asyncScheduler, EMPTY, of } from 'rxjs';
import { schemaSelectors } from '@core/store/selectors';
import { requirementApiActions } from '../actions';
import { Store } from '@ngrx/store';
import { LocaleService } from '@core/service/locale/locale.service';
import { marker } from '@biesbjerg/ngx-translate-extract-marker';
import { NGXLogger } from 'ngx-logger';
import { RequirementTeachers } from '@elisa11/elisa-api-angular-client';
import { RequirementPart } from '@elisa11/elisa-api-angular-client';
import moment from 'moment/moment';
import { formActions } from '@shared/store/actions';
import { Paths } from '@shared/store/reducers/form.reducer';
import { requirementSelectors } from '../selectors';
import { PaginatorFilter } from '@data/filter/paginator.filter';
import { RequirementFilter } from '../../model/requirement-filter.model';
import {
	deleteRequirement,
	deleteRequirementDone,
	deleteRequirementFailed,
} from '../actions/requirement-api.actions';
import { ConfirmDialogComponent } from '../../../../dialogs/confirm-dialog/confirm-dialog.component';
import { ConfirmDialogModel } from '../../../../dialogs/confirm-dialog/confirm-dialog.model';

const tag = 'RequirementEffects';

@Injectable()
export class RequirementEffects extends SnackEffects {
	resetData$ = createEffect(
		() =>
			this.actions$.pipe(
				ofType(resetCachedData),
				tap(() => this.service.clearCache())
			),
		{ dispatch: false }
	);

	search$ = createEffect(
		() =>
			({ debounce = 300, scheduler = asyncScheduler } = {}) =>
				this.actions$.pipe(
					ofType(requirementApiActions.searchRequirement),
					debounceTime(debounce, scheduler),
					withLatestFrom(this.store.select(schemaSelectors.selectScheme)),
					map((value) => {
						return {
							query: value[0].query,
							filter: value[0].filter,
							version: value[1].name,
						};
					}),
					switchMap(({ query, filter, version }) => {
						if (query === null) {
							return EMPTY;
						}

						const nextSearch$ = this.actions$.pipe(
							ofType(requirementApiActions.searchRequirement),
							skip(1)
						);

						return this.service.listRequirements(query, version, filter).pipe(
							takeUntil(nextSearch$),
							map((data) => requirementApiActions.searchRequirementSuccess({ data })),
							catchError((err) => {
								this.logger.error(tag, 'search$', err);
								return of(
									requirementApiActions.searchRequirementFailure({ error: err.message })
								);
							})
						);
					})
				)
	);
	searchFailed$ = createEffect(
		() =>
			this.actions$.pipe(
				ofType(requirementApiActions.searchRequirementFailure),
				tap((err) =>
					this.openSnackbar(
						this.localeService.translate(marker('requirement.search.error'))
					)
				)
			),
		{ dispatch: false }
	);

	post$ = createEffect(() =>
		this.actions$.pipe(
			ofType(requirementApiActions.postRequirement),
			withLatestFrom(this.store.select(schemaSelectors.selectScheme)),
			map((value) => {
				return {
					data: value[0].data,
					version: value[1].name,
				};
			}),
			switchMap(({ data, version }) => {
				const req = {
					course: data.course.id,
					department: data.department.id,
					created_by: data.created_by.id,
					requirement_teachers: data.requirement_teachers.flatMap((d) => {
						this.logger.debug(tag, 'post Requirement -> requirement_teachers', d);
						return d.roles.map((r) => {
							return { teacher: d.user.id, role: r.id } as RequirementTeachers;
						});
					}),
					requirement_parts: data.requirement_parts.flatMap((p) => {
						const room = p.room;
						const category = p.category.id;
						return p.time.flatMap((t) => {
							return t.map((d) => {
								return {
									room: room ? room.id : null,
									category,
									day: d.day,
									time: moment(d.time).format('HH:mm'),
									suitability: d.suitability,
									id: null,
								} as RequirementPart;
							});
						});
					}),
					created_at: data.created_at,
					exercise_after_lecture: data.exercise_after_lecture,
					exercise_odd_week: data.exercise_odd_week,
					groups_count: data.groups_count,
					requirement_comments: data.requirement_comments,
					requirement_activities: data.requirement_activities,
					state: data.state,
					max_parallel_courses: data.max_parallel_courses,
					max_serial_courses: data.max_serial_courses,
					students_count: data.students_count,
					students_count_extra: data.students_count_extra,
					updated_at: data.updated_at,
					id: undefined,
				};
				this.logger.debug(tag, 'post Requirement -> ', req);
				return this.service.createRequirement(version, req).pipe(
					map(() => requirementApiActions.postRequirementSuccess()),
					catchError((err) => {
						this.logger.error(tag, 'createRequirement', err);
						return of(
							requirementApiActions.postRequirementFailure({ error: err.message })
						);
					})
				);
			})
		)
	);

	postSuccess$ = createEffect(() =>
		this.actions$.pipe(
			ofType(requirementApiActions.postRequirementSuccess),
			map(() => {
				this.openSnackbar('Požiadavka bola úspešne vytvorená.');
				return formActions.submitFormSuccess({ path: Paths.requirement });
			})
		)
	);
	postError$ = createEffect(() =>
		this.actions$.pipe(
			ofType(requirementApiActions.postRequirementFailure),
			tap((x) =>
				this.openSnackbar(
					this.localeService.translate(marker('requirement.submit.error'))
				)
			),
			map(({ error }) => formActions.submitFormError({ error, path: Paths.requirement }))
		)
	);

	//TODO: Zistiť, prečo sa vykoná PUT úspešne, ale nič neprepíše..
	put$ = createEffect(() =>
		this.actions$.pipe(
			ofType(requirementApiActions.putRequirement),
			withLatestFrom(this.store.select(schemaSelectors.selectScheme)),
			map((value) => {
				return {
					data: value[0].data,
					version: value[1].name,
				};
			}),
			switchMap(({ data, version }) => {
				const req = {
					course: data.course.id,
					department: data.department.id,
					created_by: data.created_by.id,
					requirement_teachers: data.requirement_teachers.flatMap((d) => {
						this.logger.debug(tag, 'post Requirement -> requirement_teachers', d);
						return d.roles.map((r) => {
							return { teacher: d.user.id, role: r.id } as RequirementTeachers;
						});
					}),
					requirement_parts: data.requirement_parts.flatMap((p) => {
						const room = p.room;
						const category = p.category.id;
						return p.time.flatMap((t) => {
							return t.map((d) => {
								return {
									room: room ? room.id : null,
									category,
									day: d.day,
									time: moment(d.time).format('HH:mm'),
									suitability: d.suitability,
									id: null,
								} as RequirementPart;
							});
						});
					}),
					created_at: data.created_at,
					exercise_after_lecture: data.exercise_after_lecture,
					exercise_odd_week: data.exercise_odd_week,
					groups_count: data.groups_count,
					requirement_comments: data.requirement_comments,
					requirement_activities: data.requirement_activities,
					state: data.state,
					max_parallel_courses: data.max_parallel_courses,
					max_serial_courses: data.max_serial_courses,
					students_count: data.students_count,
					students_count_extra: data.students_count_extra,
					updated_at: data.updated_at,
					id: data.id,
				};
				this.logger.debug(tag, 'put Requirement -> ', req);
				return this.service.updateRequirement(version, req).pipe(
					map(() => requirementApiActions.putRequirementSuccess()),
					catchError((err) => {
						this.logger.error(tag, 'createRequirement', err);
						return of(
							requirementApiActions.putRequirementFailure({ error: err.message })
						);
					})
				);
			})
		)
	);

	putSuccess$ = createEffect(() =>
		this.actions$.pipe(
			ofType(requirementApiActions.putRequirementSuccess),
			map(() => {
				this.openSnackbar('Požiadavka bola úspešne upravená');
				return formActions.submitFormSuccess({ path: Paths.requirement });
			})
		)
	);
	putError$ = createEffect(() =>
		this.actions$.pipe(
			ofType(requirementApiActions.postRequirementFailure),
			tap((x) =>
				this.openSnackbar(
					this.localeService.translate(marker('requirement.submit.error'))
				)
			),
			map(({ error }) => formActions.submitFormError({ error, path: Paths.requirement }))
		)
	);

	deleteRequirement$ = createEffect(() =>
		this.actions$.pipe(
			ofType(deleteRequirement),
			withLatestFrom(this.store.select(schemaSelectors.selectScheme)),
			map((data) => {
				return { id: data[0].id, scheme: data[1].name };
			}),
			switchMap((data) => {
				return this.service.deleteRequirement(data.scheme, data.id).pipe(
					map(() => requirementApiActions.deleteRequirementDone({ id: data.id })),
					catchError((err) =>
						of(requirementApiActions.deleteRequirementFailed({ error: err }))
					)
				);
			})
		)
	);

	deleteRequirementDone$ = createEffect(() =>
		this.actions$.pipe(
			ofType(requirementApiActions.deleteRequirementDone),
			map(() => {
				this.openSnackbar(this.localeService.translate(marker('requirement.delete.msg')));
				return requirementApiActions.searchRequirement({
					query: {} as PaginatorFilter,
					filter: {} as RequirementFilter,
				});
			})
		)
	);
	deleteCommentsFailed$ = createEffect(
		() =>
			this.actions$.pipe(
				ofType(requirementApiActions.deleteRequirementFailed),
				map((err) =>
					this.openSnackbar(
						this.localeService.translate(marker('requirement.delete.error'))
					)
				)
			),
		{ dispatch: false }
	);
	constructor(
		private actions$: Actions,
		private service: RequirementsCacheService,
		private localeService: LocaleService,
		private store: Store,
		private logger: NGXLogger,
		snack: MatSnackBar
	) {
		super(snack);
	}
}
