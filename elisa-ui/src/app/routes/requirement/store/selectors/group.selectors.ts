import { createSelector } from '@ngrx/store';
import * as fromGroup from '../reducers/group.reducer';
import { selectRequirementsState } from './requirement.selectors';
import { Group } from '@elisa11/elisa-api-angular-client';

export const selectGroupState = createSelector(
	selectRequirementsState,
	(s) => s[fromGroup.groupFeatureKey]
);

export const selectSelectedIds = createSelector(
	selectGroupState,
	fromGroup.getSelectedIds
);
export const selectTotalCount = createSelector(selectGroupState, fromGroup.getCount);
export const selectSearchLoading = createSelector(selectGroupState, fromGroup.getLoading);

export const { selectIds, selectEntities } =
	fromGroup.adapter.getSelectors(selectGroupState);

export const selectSelectedResults = createSelector(
	selectEntities,
	selectSelectedIds,
	(data, ids) => {
		return ids.map((id) => data[id]).filter((d): d is Group => d != null);
	}
);
