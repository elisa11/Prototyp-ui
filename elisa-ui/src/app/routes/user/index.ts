import { MatCardModule } from '@angular/material/card';
import { MatIconModule } from '@angular/material/icon';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatChipsModule } from '@angular/material/chips';
import { MatTableModule } from '@angular/material/table';
import { MatSortModule } from '@angular/material/sort';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatButtonModule } from '@angular/material/button';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatInputModule } from '@angular/material/input';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatListModule } from '@angular/material/list';
import { MatSelectModule } from '@angular/material/select';

export const MATERIAL_MODULES = [
	MatCardModule,
	MatIconModule,
	MatFormFieldModule,
	MatChipsModule,
	MatTableModule,
	MatSortModule,
	MatPaginatorModule,
	MatButtonModule,
	MatTooltipModule,
	MatInputModule,
  MatSelectModule,
	MatProgressSpinnerModule,
  MatListModule,
];
