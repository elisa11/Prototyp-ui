import * as groupsActions from './groups.actions';
import * as timetableEventsActions from './timetables-events.actions';
import * as timetablesActions from './timetables.actions';
import * as filterActions from './filter.actions';
import * as activityActions from './activity.actions';
import * as timetablesCollisionsActions from './timetables-collisions.actions';
import * as requirementActions from './requirement.actions';
import * as roomActions from './room.actions';

export {
	groupsActions,
	timetableEventsActions,
	timetablesActions,
	activityActions,
	filterActions,
	timetablesCollisionsActions,
	requirementActions,
	roomActions,
};
