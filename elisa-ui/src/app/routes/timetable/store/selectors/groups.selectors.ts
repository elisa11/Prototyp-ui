import { createSelector } from '@ngrx/store';
import * as fromGroup from '../reducers/groups.reducer';
import { Group } from '@elisa11/elisa-api-angular-client';
import { selectTimetablesState } from './timetables.selectors';

export const selectGroupsState = createSelector(
  selectTimetablesState,
  (s) => s[fromGroup.groupsFeatureKey]
);

export const selectSelectedIds = createSelector(
  selectGroupsState,
  fromGroup.getSelectedIds
);
export const selectTotalCount = createSelector(selectGroupsState, fromGroup.getCount);
export const selectSearchLoading = createSelector(selectGroupsState, fromGroup.getLoading);

export const { selectIds, selectEntities } =
  fromGroup.adapter.getSelectors(selectGroupsState);

export const selectSelectedResults = createSelector(
  selectEntities,
  selectSelectedIds,
  (data, ids) => {
    return ids.map((id) => data[id]).filter((d): d is Group => d != null);
  }
);
export const selectAllGroups = createSelector(  selectEntities,
  selectSelectedIds,
  (data, ids) => {
    return ids.map((id) => data[id]);
  }
)

// export const selectAllGroups = createSelector(selectGroupsState, (state: State)=> state.data)
