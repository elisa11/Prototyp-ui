import { createSelector } from '@ngrx/store';
import { roleFeatureKey } from '../reducers/role.reducer';
import { selectAuthState } from './auth.selectors';

export const selectRoleState = createSelector(
	selectAuthState,
	(state) => state[roleFeatureKey]
);

export const selectRoleIds = createSelector(selectRoleState, (state) => state.rolesId);
