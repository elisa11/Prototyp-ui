import { createAction, props } from '@ngrx/store';
import { PaginatedActivityCategoryList } from '@elisa11/elisa-api-angular-client';

export const loadActivityCategories = createAction(
	'[Enum - ActivityCategory] Load ActivityCategories'
);

export const loadActivityCategoriesSuccess = createAction(
	'[Enum - ActivityCategory] Load ActivityCategories Success',
	props<{ data: PaginatedActivityCategoryList }>()
);

export const loadActivityCategoriesFailure = createAction(
	'[Enum - ActivityCategory] Load ActivityCategories Failure',
	props<{ error: any }>()
);
