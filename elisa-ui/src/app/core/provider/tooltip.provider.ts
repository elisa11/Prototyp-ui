import { Provider } from '@angular/core';
import {
	MAT_TOOLTIP_DEFAULT_OPTIONS,
	MatTooltipDefaultOptions,
} from '@angular/material/tooltip';

export const myCustomTooltipDefaults: MatTooltipDefaultOptions = {
	showDelay: 750,
	hideDelay: 250,
	touchendHideDelay: 1000,
} as MatTooltipDefaultOptions;

export const tooltipProvider: Provider = {
	provide: MAT_TOOLTIP_DEFAULT_OPTIONS,
	useValue: myCustomTooltipDefaults,
};
