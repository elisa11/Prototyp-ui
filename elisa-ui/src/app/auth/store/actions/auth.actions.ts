import { createAction, props } from '@ngrx/store';
import { TokenPayload } from '../model/token.model';

export const authInit = createAction('[Auth - Auth] Init From Session');

export const authInitSuccess = createAction(
	'[Auth - Auth] Init From Session Success',
	props<{ payload: TokenPayload; token: string }>()
);

export const logout = createAction(
	'[Auth - Auth] Logout',
	props<{ returnUrl?: string }>()
);

export const authRedirect = createAction(
	'[Auth - Auth] Redirect',
	props<{ url: string; returnUrl?: string }>()
);

export const refreshTokenValidity = createAction('[Auth - Auth] Refresh Token Validity');

export const validToken = createAction(
	'[Auth - Auth] Valid Token',
	props<{ valid: boolean }>()
);
