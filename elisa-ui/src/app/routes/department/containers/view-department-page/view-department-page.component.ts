import { ChangeDetectionStrategy, Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { map } from 'rxjs/operators';
import { departmentActions } from '../../store/actions';
import { AbstractViewPageComponent } from '@core/generics/abstract-view-page.component';

@Component({
	template: ` <app-selected-department-page
		(onBack)="back($event)"
	></app-selected-department-page>`,
	changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ViewDepartmentPageComponent extends AbstractViewPageComponent {
	constructor(router: Router, store: Store, route: ActivatedRoute) {
		super(router);
		this.actionsSubscription = route.params
			.pipe(map((params) => departmentActions.selectDepartment({ id: params.id })))
			.subscribe((action) => store.dispatch(action));
	}
}
