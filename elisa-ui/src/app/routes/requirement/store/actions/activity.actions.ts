import { createAction, props } from '@ngrx/store';
import { PaginatedActivityList } from '@elisa11/elisa-api-angular-client';

export const loadActivities = createAction(
	'[Requirement - Activity] Load Activities',
	props<{ cid: number }>()
);

export const resetActivities = createAction('[Requirement - Activity] Reset Activities');

export const loadActivitiesSuccess = createAction(
	'[Requirement - Activity] Load Activities Success',
	props<{ data: PaginatedActivityList }>()
);

export const loadActivitiesFailure = createAction(
	'[Requirement - Activity] Load Activities Failure',
	props<{ error: any }>()
);
