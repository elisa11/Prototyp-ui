import { createAction, props } from '@ngrx/store';

export const updateForm = createAction(
	'[Shared - Form] Update Form',
	props<{ path: string; value: any }>()
);

export const submitFormSuccess = createAction(
	'[Shared - Form] Submit Form Success',
	props<{ path: string }>()
);

export const submitFormError = createAction(
	'[Shared - Form] Submit Forms Error',
	props<{ path: string; error: any }>()
);
