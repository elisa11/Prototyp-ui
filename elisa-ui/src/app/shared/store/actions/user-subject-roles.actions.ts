import { createAction, props } from '@ngrx/store';
import { PaginatedUserSubjectRoleList } from '@elisa11/elisa-api-angular-client';

export const loadUserSubjectRoles = createAction(
	'[Enum - UserSubjectRole] Load UserSubjectRoles'
);

export const loadUserSubjectRolesSuccess = createAction(
	'[Enum - UserSubjectRole] Load UserSubjectRoles Success',
	props<{ data: PaginatedUserSubjectRoleList }>()
);

export const loadUserSubjectRolesFailure = createAction(
	'[Enum - UserSubjectRole] Load UserSubjectRoles Failure',
	props<{ error: any }>()
);
