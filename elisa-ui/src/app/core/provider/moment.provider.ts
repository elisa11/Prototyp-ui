import { Provider } from '@angular/core';
import { MAT_MOMENT_DATE_ADAPTER_OPTIONS } from '@angular/material-moment-adapter';

export const MomentProvider: Provider = {
	provide: MAT_MOMENT_DATE_ADAPTER_OPTIONS,
	useValue: { useUtc: true },
};
