import { ChangeDetectionStrategy, Component } from '@angular/core';
import { AbstractCollectionPageComponent } from '@core/generics/abstract-collection-page.component';
import { Store } from '@ngrx/store';
import { PaginatorFilter } from '@data/filter/paginator.filter';
import { roomApiActions } from '../../store/actions';
import { RoomFilterModel } from '../../model/room-filter.model';
import {
	departmentSelectors,
	roomEquipmentSelectors,
	roomSelectors,
	searchRoomSelectors,
	typeSelectors,
} from '../../store/selectors';
import { combineLatest, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { RoomModel } from '../../model/room.model';
import { roomCategorySelectors } from '@shared/store/selectors';
import { RoomCategory } from '@elisa11/elisa-api-angular-client';
import { Router } from '@angular/router';

@Component({
	template: `
		<app-room-list
			[data]="data$ | async"
			[isLoading]="loading$ | async"
			[pagination]="pagination"
			[resultsLength]="totalCount$ | async"
			[roomTypes]="roomTypes$ | async"
			(onFilterChange)="handleFilterChange($event)"
			(roomFilterChange)="handleRoomFilterChange($event)"
      (onNewRoom)="handleNewRoom($event)"
		></app-room-list>
	`,
	changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CollectionPageComponent extends AbstractCollectionPageComponent<RoomModel> {
	private filterChange: PaginatorFilter = {
		limit: this.pagination[0],
		offset: 0,
	} as PaginatorFilter;
	private roomFilter: RoomFilterModel;
	roomTypes$: Observable<RoomCategory[]>;

	constructor(private store: Store, private router: Router) {
		super();
		this.store.dispatch(
			roomApiActions.searchRoom({
				filters: this.roomFilter,
				query: this.filterChange,
			})
		);
		this.roomTypes$ = store.select(roomCategorySelectors.selectAll);
		this.loading$ = store.select(searchRoomSelectors.selectSearchLoading);
		this.data$ = combineLatest([
			store.select(searchRoomSelectors.selectSearchResults),
			store.select(typeSelectors.selectTypeList),
			store.select(roomEquipmentSelectors.selectEntities),
			store.select(departmentSelectors.selectEntities),
		]).pipe(
			map((data) => {
				return {
					rooms: data[0] ?? [],
					types: data[1] ?? [],
					equipment: data[2],
					department: data[3] ?? {},
				};
			}),
			map((value) => {
				return value.rooms.map((r) => {
					return new RoomModel(
						r,
						value.types.find((t) => r.room_type === t.id),
						value.equipment && value.equipment[r.id] ? value.equipment[r.id].data : [],
						value.department[r.department]
					);
				});
			})
		);
		this.totalCount$ = store.select(roomSelectors.selectTotalCount);
	}

	handleFilterChange(filterChange: PaginatorFilter) {
		this.filterChange = filterChange;
		this.store.dispatch(
			roomApiActions.searchRoom({ filters: this.roomFilter, query: filterChange })
		);
	}

	handleRoomFilterChange(filterChange: RoomFilterModel) {
		this.roomFilter = filterChange;
		this.store.dispatch(
			roomApiActions.searchRoom({ filters: filterChange, query: this.filterChange })
		);
	}

  handleNewRoom($event: RoomModel | undefined) {
    this.router.navigate(['room','form'], {state: $event});
  }
}
