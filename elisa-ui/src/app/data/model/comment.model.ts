import { Comments, User } from '@elisa11/elisa-api-angular-client';

export class CommentModel implements Comments {
	text: string;
	createdByUser?: User;
	readonly created_at: string;
	readonly created_by: number;
	readonly id: number;

	constructor(v: Comments, users?: User[]) {
		Object.assign(this, v);
		if (users) {
			this.createdByUser = users.find((u) => u.id === this.created_by);
		}
	}
}
