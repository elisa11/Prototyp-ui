import { createReducer, on } from '@ngrx/store';
import { Collision } from '@elisa11/elisa-api-angular-client';
import { timetablesCollisionsActions } from '../actions';
import { createEntityAdapter, EntityAdapter, EntityState } from '@ngrx/entity';

export const timetablesCollisionsFeatureKey = 'timetables-collisions';

export interface State extends EntityState<Collision> {
	readonly selectedIds: number[];
	readonly count: number;
	readonly loading: boolean;
}

export const adapter: EntityAdapter<Collision> = createEntityAdapter<Collision>({
	selectId: (model) => model.id,
	sortComparer: false,
});
export const initialState = adapter.getInitialState({
	selectedIds: [],
	count: 0,
	loading: false,
});

export const reducer = createReducer(
	initialState,
	on(timetablesCollisionsActions.loadTimetableCollisions, (state) => ({
		...state,
		loading: true,
	})),
	on(
		timetablesCollisionsActions.loadTimetableCollisionsSuccess,
		(state, { collisions }) =>
			adapter.upsertMany(collisions, {
				...state,
				selectedIds: collisions.map((x) => x.id),
				count: collisions.length,
				loading: false,
			})
	),
	on(timetablesCollisionsActions.loadTimetableCollisionsFailure, (state, { error }) => ({
		...state,
		loading: false,
		error: error,
	})),
	on(timetablesCollisionsActions.updateCollision, (state) => ({
		...state,
		loading: true,
	})),
	on(timetablesCollisionsActions.updateCollisionFailure, (state, { error }) => ({
		...state,
		loading: false,
		error: error,
	})),
	on(timetablesCollisionsActions.updateCollisionSuccess, (state, { collision }) =>
		adapter.upsertOne(collision, {
			...state,
			loading: false,
		})
	)
);

export const getSelectedIds = (state: State) => state.selectedIds;
export const getLoading = (state: State) => state.loading;
export const getCount = (state: State) => state.count;
