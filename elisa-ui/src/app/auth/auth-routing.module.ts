import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AuthorizeComponent } from './routes/authorize/authorize.component';
import { LoginComponent } from './routes/login/login.component';

const routes: Routes = [
	{ path: 'login', component: LoginComponent },
	{ path: 'authorize', component: AuthorizeComponent },
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule],
})
export class AuthRoutingModule {}
