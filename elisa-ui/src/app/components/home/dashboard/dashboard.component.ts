import { Component, Input, OnInit } from '@angular/core';

@Component({
	selector: 'app-dashboard',
	templateUrl: './dashboard.component.html',
	styleUrls: ['./dashboard.component.css'],
})
export class DashboardComponent implements OnInit {
	@Input()
	title = 'Nazov udajov';
	@Input()
	data = ['data 1', 'data 2', 'data 3'];

	constructor() {}

	ngOnInit(): void {}
}
