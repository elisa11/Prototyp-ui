import {
	ActivityCategory,
	RequirementPart,
	Room,
} from '@elisa11/elisa-api-angular-client';
import { TimeDataUnit } from './time-data-unit.model';
import { BasicUtils } from '@shared/utils/basic.utils';

export class RequirementPartModel {
	category: ActivityCategory;
	readonly room: Room;
	time: TimeDataUnit[][];

	constructor(
		requirementParts: RequirementPart[],
		rooms: Room[],
		category: ActivityCategory[]
	) {
		const part = requirementParts[0];
		this.room = rooms.find((room) => room.id === part.room);
		this.category = category.find((d) => d.id === part.category);

		const grouped = BasicUtils.groupBy(requirementParts, 'time');
		this.time = Object.keys(grouped).map((value) =>
			grouped[value].map((unit) => new TimeDataUnit(unit))
		);
	}
}
