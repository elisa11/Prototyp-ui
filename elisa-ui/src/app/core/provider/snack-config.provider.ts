import { Provider } from '@angular/core';
import { MAT_SNACK_BAR_DEFAULT_OPTIONS } from '@angular/material/snack-bar';

export const SnackBarConfigProvider: Provider = {
	provide: MAT_SNACK_BAR_DEFAULT_OPTIONS,
	useValue: { duration: 6000 },
};
