import { EventData } from './event.model';
import { Group } from '@elisa11/elisa-api-angular-client';

export class GroupData implements Group {
	public id: number;
	public name: string;
	public abbr: string;
	public parent: number;
	public children: number[];
	public events: EventData[];

	constructor(group: Group) {
		Object.assign(this, group);
	}
}
