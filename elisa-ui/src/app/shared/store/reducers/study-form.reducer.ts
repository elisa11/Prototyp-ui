import { createReducer, on } from '@ngrx/store';
import { createEntityAdapter, EntityAdapter, EntityState } from '@ngrx/entity';
import { StudyForm } from '@elisa11/elisa-api-angular-client';
import { studyFormActions } from '@shared/store/actions';

export const studyFormFeatureKey = 'studyForm';

export interface State extends EntityState<StudyForm> {
	count: 0;
	loading: false;
}

export const adapter: EntityAdapter<StudyForm> = createEntityAdapter<StudyForm>({
	selectId: (model) => model.id,
	sortComparer: false,
});

export const initialState = adapter.getInitialState({
	count: 0,
	loading: false,
});

export const reducer = createReducer(
	initialState,
	on(studyFormActions.loadStudyForms, (state) => ({
		...state,
		loading: true,
	})),
	on(studyFormActions.loadStudyFormsFailure, (state) => ({
		...state,
		loading: false,
	})),
	on(studyFormActions.loadStudyFormsSuccess, (state, { data }) =>
		adapter.upsertMany(data.results, { ...state, count: data.count, loading: false })
	)
);

export const getCount = (state) => state.count;
export const getLoading = (state) => state.loading;
