import { createReducer, on } from '@ngrx/store';
import { createEntityAdapter, EntityAdapter, EntityState } from '@ngrx/entity';
import { Group } from '@elisa11/elisa-api-angular-client';
import { groupActions, groupApiActions } from '../actions';

export const groupFeatureKey = 'group';

export interface State extends EntityState<Group> {
	readonly selectedId: number;
	readonly count: number;
}

export const adapter: EntityAdapter<Group> = createEntityAdapter<Group>({
	selectId: (model: Group) => model.id,
	sortComparer: false,
});

export const initialState: State = adapter.getInitialState({
	selectedId: null,
	count: 0,
});

export const reducer = createReducer(
	initialState,
	on(groupApiActions.searchGroupSuccess, (state: State, { data }) =>
		adapter.upsertMany(data.results, {
			...state,
			count: data.count,
		})
	),
	on(groupActions.loadGroup, (state, { data }) => adapter.upsertOne(data, state)),
	on(groupActions.selectGroup, (state, { id }) => ({ ...state, selectedId: id }))
);

export const getSelectId = (state: State) => state.selectedId;
export const getCount = (state: State) => state.count;
