import { createAction, props } from '@ngrx/store';
import { PaginatedStudyPlanList } from '@elisa11/elisa-api-angular-client';

export const loadStudyPlan = createAction(
	'[Requirement - StudyPlan/Api] Load StudyPlan',
	props<{ cid: number }>()
);

export const loadStudyPlanSuccess = createAction(
	'[Requirement - StudyPlan/Api] Load StudyPlan Success',
	props<{ data: PaginatedStudyPlanList }>()
);

export const loadStudyPlanFailure = createAction(
	'[Requirement - StudyPlan/Api] Load StudyPlan Failure',
	props<{ error: any }>()
);
