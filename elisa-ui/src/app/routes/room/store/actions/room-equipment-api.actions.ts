import { createAction, props } from '@ngrx/store';
import { Equipment, PaginatedEquipmentList, PaginatedRoomEquipmentList } from '@elisa11/elisa-api-angular-client';
import { PaginatorFilter } from '@data/filter/paginator.filter';

export const loadRoomEquipment = createAction(
	'[Room - Equipment/API] Load Room Equipment',
	props<{ query: PaginatorFilter }>()
);

export const loadRoomEquipmentSuccess = createAction(
	'[Room - Equipment/API] Load Room Equipment Success',
	props<{ id: number; data: PaginatedRoomEquipmentList }>()
);

export const loadRoomEquipmentFailure = createAction(
	'[Room - Equipment/API] Load Room Equipment Failure',
	props<{ error: any }>()
);
