import { createAction, props } from '@ngrx/store';
import { TokenPayload } from '../../../auth/store/model/token.model';
import { Version } from '@elisa11/elisa-api-angular-client';

export const loadMenuItems = createAction(
	'[Core - Layout] Load Menu Items',
	props<{ token: TokenPayload; schema: Version }>()
);

export const loadVersion = createAction('[Core - Layout] Load Version');

export const loadVersionSuccessful = createAction(
	'[Core - Layout] Load Version successful',
	props<{ version: string }>()
);
