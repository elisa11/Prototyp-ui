export class Settings {
	autoRoomSelectPopUp: boolean;
	language: string;
	notificationInterval: number;

	constructor(
		autoRoomSelectPopUp: boolean,
		language: string,
		notificationInterval: number
	) {
		this.autoRoomSelectPopUp = autoRoomSelectPopUp;
		this.language = language;
		this.notificationInterval = notificationInterval;
	}
}
