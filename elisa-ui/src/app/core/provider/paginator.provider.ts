import { Provider } from '@angular/core';
import { MatPaginatorIntl } from '@angular/material/paginator';
import { MatPaginationIntlService } from '@core/service/locale/mat-pagination-intl.service';

export const PaginatorProvider: Provider = {
	provide: MatPaginatorIntl,
	useClass: MatPaginationIntlService,
};
