import { createAction, props } from '@ngrx/store';
import { PaginatorFilter } from '@data/filter/paginator.filter';
import { PaginatedPeriodList } from '@elisa11/elisa-api-angular-client';

export const searchPeriods = createAction(
	'[Schema - Period/Api] Search Periods',
	props<{ query: PaginatorFilter }>()
);

export const searchPeriodsSuccess = createAction(
	'[Schema - Period/Api] Search Periods Success',
	props<{ periods: PaginatedPeriodList }>()
);

export const searchPeriodsFailure = createAction(
	'[Schema - Period/Api] Search Periods Failure',
	props<{ error: any }>()
);
