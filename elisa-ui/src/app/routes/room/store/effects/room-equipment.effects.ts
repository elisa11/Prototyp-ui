import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { SnackEffects } from '@core/generics/snack.effects';
import { Store } from '@ngrx/store';
import { MatSnackBar } from '@angular/material/snack-bar';
import { resetCachedData } from '@core/store/actions/settings.actions';
import { catchError, map, switchMap, tap, withLatestFrom } from 'rxjs/operators';
import { RoomEquipmentCacheService } from '../../services/room-equipment-cache.service';
import { roomEquipmentApiActions, roomActions } from '../actions';
import { schemaSelectors } from '@core/store/selectors';
import { of } from 'rxjs';
import { NGXLogger } from 'ngx-logger';
import { PaginatorFilter } from '@data/filter/paginator.filter';

const tag = 'RoomEquipmentEffects';

@Injectable()
export class RoomEquipmentEffects extends SnackEffects {
	resetData$ = createEffect(
		() =>
			this.actions$.pipe(
				ofType(resetCachedData),
				tap(() => this.service.clearCache())
			),
		{ dispatch: false }
	);

	roomSelected$ = createEffect(() =>
		this.actions$.pipe(
			ofType(roomActions.selectRoom),
			map(({ id }) =>
        roomEquipmentApiActions.loadRoomEquipment({ query: { id, offset: 0 } as PaginatorFilter })
			)
		)
	);

	loadRoomEquipment$ = createEffect(() =>
		this.actions$.pipe(
			ofType(roomEquipmentApiActions.loadRoomEquipment),
			withLatestFrom(this.store.select(schemaSelectors.selectScheme)),
			map((value) => {
				return { id: value[0].query.id, filter: value[0].query, version: value[1].name };
			}),
			switchMap(({ id, version, filter }) => {
				this.logger.debug(tag, 'loadRoomEquipment$', id, version, filter);
				if (id === null || version.length == 0) {
					return of(roomEquipmentApiActions.loadRoomEquipmentSuccess({ data: {}, id }));
				}

				return this.service.listRoomEquipment(filter, version, id).pipe(
					map((data) => roomEquipmentApiActions.loadRoomEquipmentSuccess({ id, data })),
					catchError((error) => of(roomEquipmentApiActions.loadRoomEquipmentFailure({ error })))
				);
			})
		)
	);

	loadRoomEquipmentFailure$ = createEffect(
		() =>
			this.actions$.pipe(
				ofType(roomEquipmentApiActions.loadRoomEquipmentFailure),
				tap((err) => this.openSnackbar(err.error))
			),
		{ dispatch: false }
	);

	constructor(
		private actions$: Actions,
		private service: RoomEquipmentCacheService,
		private store: Store,
		private logger: NGXLogger,
		snack: MatSnackBar
	) {
		super(snack);
	}
}
