import { Injectable, Optional, SkipSelf } from '@angular/core';
import { LangChangeEvent, TranslateService } from '@ngx-translate/core';
import { ActivatedRouteSnapshot, Router } from '@angular/router';
import { noop, Observable } from 'rxjs';
import { CommonText } from '@data/common-text';

type ShouldReuseRoute = (
	future: ActivatedRouteSnapshot,
	curr: ActivatedRouteSnapshot
) => boolean;

@Injectable({
	providedIn: 'root',
})
export class LocaleService {
	private initialized = false;

	get tz(): string {
		return 'Europe/Bratislava';
	}

	constructor(
		private router: Router,
		private service: TranslateService,
		@Optional()
		@SkipSelf()
		otherInstance: LocaleService
	) {
		if (otherInstance) throw new Error('LocaleService should have only one instance.');
	}

	get currentLocale(): string {
		return this.service.currentLang;
	}

	get lang(): Array<{ value: string; name: string }> {
		return CommonText.languages;
	}

	initLocale(localeId: string, defaultLanguage: object, defaultLocaleId = localeId) {
		if (this.initialized) return;

		this.setTranslation(localeId, defaultLanguage);
		this.setDefaultLocale(defaultLocaleId);
		this.use(localeId);
		this.subscribeToLangChange();

		this.initialized = true;
	}

	translate(query: string, ...args: any[]): string {
		return this.service.instant(query, args);
	}

	use(value: string): void {
		console.log('use', value);
		this.service.use(value);
	}

	get onLangChange(): Observable<LangChangeEvent> {
		return this.service.onLangChange;
	}

	private setRouteReuse(reuse: ShouldReuseRoute) {
		this.router.routeReuseStrategy.shouldReuseRoute = reuse;
	}

	private setDefaultLocale(localeId: string): void {
		this.service.setDefaultLang(localeId);
	}

	setTranslation(localeId: string, translation: object): void {
		this.service.setTranslation(localeId, translation);
	}

	private subscribeToLangChange(): void {
		this.service.onLangChange.subscribe(async () => {
			const { shouldReuseRoute } = this.router.routeReuseStrategy;

			this.setRouteReuse(() => false);
			this.router.navigated = false;

			await this.router.navigateByUrl(this.router.url).catch(noop);
			this.setRouteReuse(shouldReuseRoute);
		});
	}
}
