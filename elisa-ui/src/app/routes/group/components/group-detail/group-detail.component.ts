import { Component } from '@angular/core';
import { CommonText } from '@data/common-text';
import { AbstractDetailComponent } from '@core/generics/abstract-detail.component';
import { Group } from '@elisa11/elisa-api-angular-client';

@Component({
	selector: 'app-group-detail',
	templateUrl: './group-detail.component.html',
})
export class GroupDetailComponent extends AbstractDetailComponent<Group> {
	moduleId = 'group';
	moduleTitle = CommonText.navLinks.find((value) => value.link === this.moduleId).label;

	constructor() {
		super();
	}
}
