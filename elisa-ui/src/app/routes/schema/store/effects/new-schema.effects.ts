import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import {
  addNewSchema,
  deleteSchema,
  deleteSchemaDone,
  deleteSchemaFailed,
  importSchema,
  importSchemaDone,
  importSchemaFailed,
  importSchemaManual,
  importSchemaManualDone,
  importSchemaManualFailed, updateSchema, updateSchemaDone, updateSchemaFailed,
} from '../actions/new-schema.actions';
import {
	catchError,
	exhaustMap,
	map,
	switchMap,
	tap,
	withLatestFrom,
} from 'rxjs/operators';
import { VersionCacheService } from '@core/service/cache/version-cache.service';
import { ImportService, UpdateService, Version } from '@elisa11/elisa-api-angular-client';
import moment from 'moment/moment';
import { submitFormError, submitFormSuccess } from '@shared/store/actions/form.actions';
import { Paths } from '@shared/store/reducers/form.reducer';
import { of } from 'rxjs';
import { LocaleService } from '@core/service/locale/locale.service';
import { SnackEffects } from '@core/generics/snack.effects';
import { MatSnackBar } from '@angular/material/snack-bar';
import { marker } from '@biesbjerg/ngx-translate-extract-marker';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { LoadingDialogComponent } from '../../../../dialogs/loading-dialog/loading-dialog.component';
import { NGXLogger } from 'ngx-logger';
import { schemaApiActions } from '../actions';
import { PaginatorFilter } from '@data/filter/paginator.filter';
import { resetCachedData } from '@core/store/actions/settings.actions';
import { Store } from '@ngrx/store';
import { schemaApiSelector, schemaSelector } from '../selectors';
import { Router } from '@angular/router';

const tag = 'NewSchemaEffects';

@Injectable()
export class NewSchemaEffects extends SnackEffects {
	private dialogRef: MatDialogRef<LoadingDialogComponent>;

	resetData$ = createEffect(
		() =>
			this.actions$.pipe(
				ofType(resetCachedData),
				tap(() => this.service.clearCache())
			),
		{ dispatch: false }
	);

	addNewSchema$ = createEffect(() =>
		this.actions$.pipe(
			ofType(addNewSchema),
			map((value) => value.data),
			map((value) => {
				return {
					name: value.name,
					period: value.period.id,
					status: 'NEW',
					updated_at: moment().toISOString(true),
					parent: value.parent ? value.parent.id : null,
				} as Version;
			}),
			switchMap((data) =>{
        this.dialogRef = this.dialog.open(LoadingDialogComponent, {
          data: {
            text: this.localeService.translate(marker('dialog.loading.importing')),
          },
          disableClose: true,
        });
        return this.service.addVersion(data).pipe(
          map((versionId) => {
            const newVersion = { ...data, id: versionId as unknown as number }; // lebo service vrati ID verzie, namiesto objektu, nedostatok generovania z OpenAPI, mozno zly config
            this.logger.debug(tag, `New version`, newVersion);
            if (newVersion && newVersion.parent) {
              this.logger.debug(tag, `New version parent`, newVersion);
              return submitFormSuccess({ path: Paths.newVersion });
            }
            this.logger.debug(tag, `New version run import`, newVersion);
            return importSchema({ version: newVersion });
          }),
          catchError((error) => {
            this.logger.error(tag, error);
            return of(submitFormError({ error, path: Paths.newVersion }));
          }),
        );}
			)
		)
	);
	deleteSchema$ = createEffect(
		() =>
			this.actions$.pipe(
				ofType(deleteSchema),
				map(({ id }) => {
					this.logger.debug(tag, 'delete schema', id);
					return this.service.deleteVersion(id).subscribe({
            next:() =>{ deleteSchemaDone({id})},
            error: (error => {
              this.logger.error(tag, 'deleteSchema$', error);
              return deleteSchemaFailed({status: error.status})
            })
					});
				})
			),
		{ dispatch: false }
	);
	deleteSchemaDone$ = createEffect(() =>
		this.actions$.pipe(
			ofType(deleteSchemaDone),
			map(() => {
				this.openSnackbar(this.localeService.translate(marker('schema.delete.msg')));
				return schemaApiActions.searchSchemas({ query: {} as PaginatorFilter });
			})
		)
	);
	deleteSchemaFailed$ = createEffect(() =>
		this.actions$.pipe(
			ofType(deleteSchemaFailed),
			map(() => {
				this.openSnackbar(this.localeService.translate(marker('schema.delete.msg')));
				return schemaApiActions.searchSchemas({ query: {} as PaginatorFilter });
			})
		)
	);
	importSchema$ = createEffect(() =>
		this.actions$.pipe(
			ofType(importSchema),
			map((action) => action.version),
			exhaustMap((version) => {
				return this.importService
					.importForVersionCreate(version.name, { periods: [version.period] })
					.pipe(
						map((value) => importSchemaDone()),
						catchError((error) => {
							this.logger.error(tag, 'importSchema$', error);
							return of(importSchemaFailed({ status: error.status }));
						})
					);
			})
		)
	);
	importSchemaDone$ = createEffect(() =>
		this.actions$.pipe(
			ofType(importSchemaDone),
			map(() => {
				this.dialogRef && this.dialogRef.close();
				this.openSnackbar(this.localeService.translate(marker('schema.import.msg')));
				return submitFormSuccess({ path: Paths.newVersion });
			})
		)
	);
	importSchemaRefresh$ = createEffect(() =>
		this.actions$.pipe(
			ofType(importSchemaDone),
			withLatestFrom(this.store.select(schemaApiSelector.selectSearchQuery)),
			map((data) => {
				return schemaApiActions.searchSchemas({
					query: data[1],
				});
			})
		)
	);
	importSchemaFailed$ = createEffect(() =>
		this.actions$.pipe(
			ofType(importSchemaFailed),
			map(() => {
				this.dialogRef && this.dialogRef.close();
				this.openSnackbar(this.localeService.translate(marker('schema.import.error')));
				return schemaApiActions.searchSchemas({ query: {} as PaginatorFilter });
			})
		)
	);
	importSchemaManual$ = createEffect(() =>
		this.actions$.pipe(
			ofType(importSchemaManual),
			map((action) => action.version),
			switchMap((version) => {
				this.dialogRef = this.dialog.open(LoadingDialogComponent, {
					data: {
						text: this.localeService.translate(marker('dialog.loading.importing')),
					},
					disableClose: true,
				});
				return this.importService
					.importForVersionCreate(version.name, { periods: [version.period] })
					.pipe(
						map((value) => importSchemaManualDone()),
						catchError((err) => {
							this.logger.error(tag, err);
							return of(importSchemaManualFailed({ status: err.status }));
						})
					);
			})
		)
	);
	importSchemaManualDone$ = createEffect(
		() =>
			this.actions$.pipe(
				ofType(importSchemaManualDone),
				map(() => {
					this.dialogRef && this.dialogRef.close();
					this.openSnackbar("Manuálny import bol vykonaný úspešne.");
					// this.openSnackbar(this.localeService.translate(marker('schema.import.msg')));
				})
			),
		{ dispatch: false }
	);
	importSchemaManualFailed$ = createEffect(
		() =>
			this.actions$.pipe(
				ofType(importSchemaManualFailed),
				map(() => {
					this.dialogRef && this.dialogRef.close();
					this.openSnackbar("Nastala chyba pri manuálnom importe.");
					// this.openSnackbar(this.localeService.translate(marker('schema.import.error')));
				})
			),
		{ dispatch: false }
	);

  updateSchema$ = createEffect(() =>
    this.actions$.pipe(
      ofType(updateSchema),
      map((action) => action.version),
      switchMap((version) => {
        this.dialogRef = this.dialog.open(LoadingDialogComponent, {
          data: {
            text: "Prebieha aktualizácia údajov.",
            // text: this.localeService.translate(marker('dialog.loading.update')),
          },
          disableClose: true,
        });
        return this.updateService
          .updateForVersionCreate(version.name, { periods: [version.period] })
          .pipe(
            map(() => updateSchemaDone()),
            catchError((err) => {
              this.logger.error(tag, err);
              return of(updateSchemaFailed({ status: err.status }));
            })
          );
      })
    )
  );
  updateSchemaDone$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(updateSchemaDone),
        map(() => {
          this.dialogRef && this.dialogRef.close();
          this.openSnackbar("Verzia bola úspešne aktualizovaná.")
          // this.openSnackbar(this.localeService.translate(marker('schema.update.msg')));
        })
      ),
    { dispatch: false }
  );
  updateSchemaFailed$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(updateSchemaFailed),
        map(() => {
          this.dialogRef && this.dialogRef.close();
          this.openSnackbar("Nastala chyba pri aktualizácii.");
          // this.openSnackbar(this.localeService.translate(marker('schema.update.error')));
        })
      ),
    { dispatch: false }
  );

	constructor(
		private actions$: Actions,
		private service: VersionCacheService,
		private store: Store,
		private importService: ImportService,
		private localeService: LocaleService,
		private updateService: UpdateService,
		private dialog: MatDialog,
		private logger: NGXLogger,
		snack: MatSnackBar
	) {
		super(snack);
	}
}
