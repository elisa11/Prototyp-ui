import { createAction, props } from '@ngrx/store';
import { User } from '@elisa11/elisa-api-angular-client';

export const cancelUser = createAction('[User] Cancel User');

export const loadUser = createAction('[User - User] Load User', props<{ user: User }>());

export const selectUser = createAction(
	'[User - View User Page] Select User',
	props<{ id: number }>()
);
