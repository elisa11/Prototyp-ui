import { Provider } from '@angular/core';
import { MAT_CHIPS_DEFAULT_OPTIONS } from '@angular/material/chips';
import { COMMA, ENTER } from '@angular/cdk/keycodes';

export const ChipProvider: Provider = {
	provide: MAT_CHIPS_DEFAULT_OPTIONS,
	useValue: { separatorKeyCodes: [ENTER, COMMA] },
};
