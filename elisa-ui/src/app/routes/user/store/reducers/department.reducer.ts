import { createReducer, on } from '@ngrx/store';
import { Department } from '@elisa11/elisa-api-angular-client';
import { createEntityAdapter, EntityAdapter, EntityState } from '@ngrx/entity';
import { DepartmentApiActions, UserActions } from '../actions';

export type Departments = { id: number; data: Department[] };
export const departmentFeatureKey = 'department';

export interface State extends EntityState<Departments> {
	readonly selectedUserId: number | null;
}

export const adapter: EntityAdapter<Departments> = createEntityAdapter<Departments>({
	selectId: (ud: Departments) => ud.id,
	sortComparer: false,
});

export const initialState: State = adapter.getInitialState({
	selectedUserId: null,
});

export const reducer = createReducer(
	initialState,
	on(UserActions.selectUser, (state: State, { id }) => ({
		...state,
		selectedUserId: id,
	})),
	on(DepartmentApiActions.loadDepartmentsSuccess, (state: State, { departments }) =>
		adapter.upsertOne(departments, state)
	)
);

export const getSelectedUserId = (state: State) => state.selectedUserId;
