import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { equipmentApiActions, roomActions, roomApiActions } from '../../store/actions';
import { Equipment, Room } from '@elisa11/elisa-api-angular-client';
import { selectAllEquipments } from '../../store/selectors/equipment.selectors';
import { Observable } from 'rxjs';
import { RoomModel } from '../../model/room.model';

@Component({
  template: `<app-room-form
    [equipmentData]="equipmentsData$ | async"
    [roomExistsData]="roomExistsData$"
    (onSubmit)="handleSubmit($event)"
    (onPut)="handlePut($event)"
    (onCancel)="handleCancelled()"
    (onBack)="handleBack($event)">
  </app-room-form>
	`,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NewRoomPageComponent implements OnInit {
  equipmentsData$: Observable<Equipment[]>;
  roomExistsData$: RoomModel | undefined = this.router.getCurrentNavigation().extras.state as RoomModel;

  constructor(private router: Router, private store: Store) {
    // TODO: dokončiť equipment selectoring, nefunguje správne
    this.equipmentsData$ = this.store.select(selectAllEquipments);
  }

  ngOnInit(): void {
    this.store.dispatch(equipmentApiActions.loadEquipments())
  }

  handleCancelled() {
    this.store.dispatch(roomActions.cancelRoom());
  }

  handleBack(path: string) {
    return this.router.navigate([path]);
  }

  handleSubmit(data: RoomModel) {
    console.log("submitForm");
    this.store.dispatch(roomApiActions.postRoom({ data }));
  }

  handlePut(data: RoomModel) {
    console.log("putForm")
    this.store.dispatch(roomApiActions.putRoom({ data }));
  }
}
