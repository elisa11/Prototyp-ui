import { createReducer, on } from '@ngrx/store';
import { authActions, authApiActions, userActions } from '../actions';
import { TokenPayload } from '../model/token.model';
import { User } from '@elisa11/elisa-api-angular-client';

export const statusFeatureKey = 'status';

export interface State {
	readonly payload: TokenPayload;
	readonly token: string;
	readonly isValid: boolean;
	readonly loggedUser: User;
}

export const initialState: State = {
	loggedUser: null,
	payload: null,
	token: null,
	isValid: false,
};

export const reducer = createReducer(
	initialState,
	on(userActions.loadLoggedUserSuccess, (state, { data }) => ({
		...state,
		loggedUser: data,
	})),
	on(
		authActions.authInitSuccess,
		(state, { payload, token }) =>
			({
				...state,
				payload,
				token,
				isValid: true,
			} as State)
	),
	on(
		authApiActions.loginSuccess,
		(state, { payload, token }) =>
			({
				...state,
				payload,
				token,
				isValid: true,
			} as State)
	),
	on(authActions.logout, () => initialState),
	on(
		authActions.validToken,
		(state, { valid }) => ({ ...state, isValid: valid } as State)
	)
);

export const getPayload = (state: State) => state.payload;
export const getLoggedUser = (state: State) => state.loggedUser;
export const getToken = (state: State) => state.token;
export const isValid = (state: State) => state.isValid;
