// import {
// 	ChangeDetectionStrategy,
// 	Component,
// 	ElementRef,
// 	HostListener,
// 	Input,
// 	OnInit,
// 	QueryList,
// 	Renderer2,
// 	ViewChild,
// 	ViewChildren,
// } from '@angular/core';
// import {
// 	CdkDragDrop,
// 	CdkDragEnter,
// 	CdkDragExit,
// 	CdkDragSortEvent,
// 	CdkDropList,
// 	moveItemInArray,
// 	transferArrayItem,
// } from '@angular/cdk/drag-drop';
// import {
// 	CourseSerializerFull,
// 	DegreeTypeEnum,
// 	Event,
// 	Room,
// 	Timetable,
// } from '@elisa11/elisa-api-angular-client';
// import { BasicUtils } from '@shared/utils/basic.utils';
// import { EventData } from '@data/model/event.model';
// import { TimeDataUnit } from '../../../requirement/model/time-data-unit.model';
// import { MockService } from '@core/service/mock.service';
// import { EventType } from '@data/common-text';
// import { ActionsSubject, select, Store } from '@ngrx/store';
// import { loadGroups } from '../../store/actions/groups.actions';
// import { selectAllGroups } from '../../store/selectors/groups.selectors';
// import { CollisionDialogModel } from '../../../../dialogs/collision-dialog/collision-dialog.model';
// import { MatDialog } from '@angular/material/dialog';
// import { CollisionDialogComponent } from '../../../../dialogs/collision-dialog/collision-dialog.component';
// import { selectAllTimetables } from '../../store/selectors/timetables.selectors';
// import { loadTimetables } from '../../store/actions/timetables.actions';
// import { selectAllTimetableEvents } from '../../store/selectors/timetables-events.selectors';
// import { loadTimetableEvents } from '../../store/actions/timetables-events.actions';
// import { timetableEventsActions, timetablesActions } from '../../store/actions';
// import { AbstractTableComponent } from '@core/generics/abstract-table.component';
// import { TimetableEventModel } from '../../model/timetable-event.model';
// import { TimetableEventFilterModel } from '../../model/timetable-event-filter.model';
// import { selectAllUsers } from 'src/app/routes/user/store/selectors/user.selectors';
// import { DOCUMENT } from '@angular/common';
// import { Observable } from 'rxjs';
// import { timetableEventsSelector, timetablesSelector } from '../../store/selectors';
// import {
// 	departmentActions,
// 	departmentApiActions,
// } from 'src/app/routes/department/store/actions';
// import { PaginatorFilter } from '@data/filter/paginator.filter';
// import { departmentSearchSelector } from 'src/app/routes/user/store/selectors';
// import {
// 	departmentSelectors,
// 	searchDepartmentSelectors,
// } from 'src/app/routes/department/store/selectors';
// import { getFaculties } from 'src/app/routes/department/store/reducers/department.reducer';
// import { Department } from '@elisa11/elisa-api-angular-client';
// @Component({
// 	selector: 'app-timetable',
// 	templateUrl: './timetable.component.html',
// 	styleUrls: ['./timetable.component.css'],
// 	changeDetection: ChangeDetectionStrategy.OnPush,
// })
// export class TimetableComponent
// 	extends AbstractTableComponent<TimetableEventModel>
// 	implements OnInit
// {
// 	@ViewChild('predmetyContainer') container: ElementRef;
// 	@ViewChildren('app-course-item') items: QueryList<ElementRef>;
// 	courses: EventData[];
// 	coursesData: EventData[]; // referencne data pre urcovanie typu, 'povodny stav'
// 	practices: EventData[];
// 	practicesData: EventData[]; // referencne data pre urcovanie typu, 'povodny stav'
// 	level: string;
// 	year: number;
// 	group: string;
// 	hours = [8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18];
// 	days = ['Po', 'Ut', 'St', 'Št', 'Pi'];
// 	_data: Array<EventData[]> = [];
// 	requirementTime: TimeDataUnit[][];
// 	cdkDropCourseLists: CdkDropList[];
// 	practicesID = 'practicesID';
// 	coursesID = 'coursesID';
// 	degrees = Object.keys(DegreeTypeEnum);
// 	// degrees: string[] = ['Bc', 'Ing', 'PhD'];
// 	years: number[] = [1, 2, 3];
// 	// groups: string[] = ['BIS', 'MSUS'];
// 	groups$ = this.store.select(selectAllGroups);
// 	// groups$ = this.store.select(selectSelectedTimetable);
// 	selectedDegree: string;
// 	selectedYear: number;
// 	selectedGroup: string;
// 	selectedTeacher: string;
// 	rooms: Room[] = [];
// 	selectedTimetable: Timetable = null;
// 	selectedEvents: Event;
// 	timetablesEventsList$: Observable<Event[]> = this.store.select(
// 		selectAllTimetableEvents
// 	);
// 	timetablesList$: Observable<Timetable[]> = this.store.select(selectAllTimetables);
// 	showCreateTC: boolean = false;
// 	nameOfNewTimetable: string = '';
// 	displayedColumns = ['id', 'rooms', 'groups', 'teachers'];
// 	@Input()
// 	isLoading!: boolean;
// 	eventFilterModel = {} as TimetableEventFilterModel;
// 	users = this.store.select(selectAllUsers);
// 	// events = this.store.select(selectAllEvents);
// 	// eventTest: EventData = this.mockService.getEventData();

// 	// departments$: Observable<Departments[]>;
// 	faculties$: Observable<Department[]>;
// 	deps;
// 	constructor(
// 		private renderer: Renderer2,
// 		private store: Store,
// 		private dialog: MatDialog,
// 		private actionSubject$: ActionsSubject
// 	) {
// 		super();
// 	}
// 	// mock constructor only
// 	// constructor(private mockService: MockService, private renderer: Renderer2,
// 	//             private store:Store, private dialog: MatDialog) {
// 	// 	this.courses = mockService.getCourses().map((value) => {
// 	// 		const data = value as EventData;
// 	// 		data.type = EventType.lecture;
// 	// 		if (data.id === 1 || data.id === 3) {
// 	// 			data.requestedTime = [
// 	// 				[
// 	// 					{
// 	// 						day: 0,
// 	// 						hour: 8,
// 	// 						typeId: 3,
// 	// 					},
// 	// 					{
// 	// 						day: 0,
// 	// 						hour: 9,
// 	// 						typeId: 1,
// 	// 					},
// 	// 					{
// 	// 						day: 0,
// 	// 						hour: 10,
// 	// 						typeId: 1,
// 	// 					},
// 	// 					{
// 	// 						day: 0,
// 	// 						hour: 11,
// 	// 						typeId: 1,
// 	// 					},
// 	// 					{
// 	// 						day: 0,
// 	// 						hour: 12,
// 	// 						typeId: 1,
// 	// 					},
// 	// 					{
// 	// 						day: 0,
// 	// 						hour: 13,
// 	// 						typeId: 3,
// 	// 					},
// 	// 					{
// 	// 						day: 0,
// 	// 						hour: 14,
// 	// 						typeId: 3,
// 	// 					},
// 	// 					{
// 	// 						day: 0,
// 	// 						hour: 15,
// 	// 						typeId: 3,
// 	// 					},
// 	// 					{
// 	// 						day: 0,
// 	// 						hour: 16,
// 	// 						typeId: 3,
// 	// 					},
// 	// 					{
// 	// 						day: 0,
// 	// 						hour: 17,
// 	// 						typeId: 3,
// 	// 					},
// 	// 					{
// 	// 						day: 0,
// 	// 						hour: 18,
// 	// 						typeId: 3,
// 	// 					},
// 	// 				],
// 	// 				[
// 	// 					{
// 	// 						day: 1,
// 	// 						hour: 8,
// 	// 						typeId: 3,
// 	// 					},
// 	// 					{
// 	// 						day: 1,
// 	// 						hour: 9,
// 	// 						typeId: 3,
// 	// 					},
// 	// 					{
// 	// 						day: 1,
// 	// 						hour: 10,
// 	// 						typeId: 3,
// 	// 					},
// 	// 					{
// 	// 						day: 1,
// 	// 						hour: 11,
// 	// 						typeId: 3,
// 	// 					},
// 	// 					{
// 	// 						day: 1,
// 	// 						hour: 12,
// 	// 						typeId: 3,
// 	// 					},
// 	// 					{
// 	// 						day: 1,
// 	// 						hour: 13,
// 	// 						typeId: 2,
// 	// 					},
// 	// 					{
// 	// 						day: 1,
// 	// 						hour: 14,
// 	// 						typeId: 2,
// 	// 					},
// 	// 					{
// 	// 						day: 1,
// 	// 						hour: 15,
// 	// 						typeId: 2,
// 	// 					},
// 	// 					{
// 	// 						day: 1,
// 	// 						hour: 16,
// 	// 						typeId: 2,
// 	// 					},
// 	// 					{
// 	// 						day: 1,
// 	// 						hour: 17,
// 	// 						typeId: 2,
// 	// 					},
// 	// 					{
// 	// 						day: 1,
// 	// 						hour: 18,
// 	// 						typeId: 2,
// 	// 					},
// 	// 				],
// 	// 				[
// 	// 					{
// 	// 						day: 2,
// 	// 						hour: 8,
// 	// 						typeId: 3,
// 	// 					},
// 	// 					{
// 	// 						day: 2,
// 	// 						hour: 9,
// 	// 						typeId: 3,
// 	// 					},
// 	// 					{
// 	// 						day: 2,
// 	// 						hour: 10,
// 	// 						typeId: 3,
// 	// 					},
// 	// 					{
// 	// 						day: 2,
// 	// 						hour: 11,
// 	// 						typeId: 3,
// 	// 					},
// 	// 					{
// 	// 						day: 2,
// 	// 						hour: 12,
// 	// 						typeId: 3,
// 	// 					},
// 	// 					{
// 	// 						day: 2,
// 	// 						hour: 13,
// 	// 						typeId: 2,
// 	// 					},
// 	// 					{
// 	// 						day: 2,
// 	// 						hour: 14,
// 	// 						typeId: 2,
// 	// 					},
// 	// 					{
// 	// 						day: 2,
// 	// 						hour: 15,
// 	// 						typeId: 2,
// 	// 					},
// 	// 					{
// 	// 						day: 2,
// 	// 						hour: 16,
// 	// 						typeId: 2,
// 	// 					},
// 	// 					{
// 	// 						day: 2,
// 	// 						hour: 17,
// 	// 						typeId: 2,
// 	// 					},
// 	// 					{
// 	// 						day: 2,
// 	// 						hour: 18,
// 	// 						typeId: 2,
// 	// 					},
// 	// 				],
// 	// 				[
// 	// 					{
// 	// 						day: 3,
// 	// 						hour: 8,
// 	// 						typeId: 1,
// 	// 					},
// 	// 					{
// 	// 						day: 3,
// 	// 						hour: 9,
// 	// 						typeId: 1,
// 	// 					},
// 	// 					{
// 	// 						day: 3,
// 	// 						hour: 10,
// 	// 						typeId: 1,
// 	// 					},
// 	// 					{
// 	// 						day: 3,
// 	// 						hour: 11,
// 	// 						typeId: 1,
// 	// 					},
// 	// 					{
// 	// 						day: 3,
// 	// 						hour: 12,
// 	// 						typeId: 1,
// 	// 					},
// 	// 					{
// 	// 						day: 3,
// 	// 						hour: 13,
// 	// 						typeId: 3,
// 	// 					},
// 	// 					{
// 	// 						day: 3,
// 	// 						hour: 14,
// 	// 						typeId: 3,
// 	// 					},
// 	// 					{
// 	// 						day: 3,
// 	// 						hour: 15,
// 	// 						typeId: 3,
// 	// 					},
// 	// 					{
// 	// 						day: 3,
// 	// 						hour: 16,
// 	// 						typeId: 3,
// 	// 					},
// 	// 					{
// 	// 						day: 3,
// 	// 						hour: 17,
// 	// 						typeId: 3,
// 	// 					},
// 	// 					{
// 	// 						day: 3,
// 	// 						hour: 18,
// 	// 						typeId: 3,
// 	// 					},
// 	// 				],
// 	// 				[
// 	// 					{
// 	// 						day: 4,
// 	// 						hour: 8,
// 	// 						typeId: 1,
// 	// 					},
// 	// 					{
// 	// 						day: 4,
// 	// 						hour: 9,
// 	// 						typeId: 1,
// 	// 					},
// 	// 					{
// 	// 						day: 4,
// 	// 						hour: 10,
// 	// 						typeId: 1,
// 	// 					},
// 	// 					{
// 	// 						day: 4,
// 	// 						hour: 11,
// 	// 						typeId: 1,
// 	// 					},
// 	// 					{
// 	// 						day: 4,
// 	// 						hour: 12,
// 	// 						typeId: 1,
// 	// 					},
// 	// 					{
// 	// 						day: 4,
// 	// 						hour: 13,
// 	// 						typeId: 3,
// 	// 					},
// 	// 					{
// 	// 						day: 4,
// 	// 						hour: 14,
// 	// 						typeId: 3,
// 	// 					},
// 	// 					{
// 	// 						day: 4,
// 	// 						hour: 15,
// 	// 						typeId: 3,
// 	// 					},
// 	// 					{
// 	// 						day: 4,
// 	// 						hour: 16,
// 	// 						typeId: 3,
// 	// 					},
// 	// 					{
// 	// 						day: 4,
// 	// 						hour: 17,
// 	// 						typeId: 3,
// 	// 					},
// 	// 					{
// 	// 						day: 4,
// 	// 						hour: 18,
// 	// 						typeId: 3,
// 	// 					},
// 	// 				],
// 	// 			] as unknown as TimeDataUnit[][];
// 	// 		}
// 	// 		return data;
// 	// 	});
// 	// 	this.coursesData = this.courses.slice();
// 	// 	this.rooms = mockService.getRooms();
// 	// 	this.practices = mockService.getPractices().map((value) => {
// 	// 		const data = value as EventData;
// 	// 		data.type = EventType.practice;
// 	// 		return data;
// 	// 	});
// 	// 	this.practicesData = this.practices.slice();
// 	// }

// 	@ViewChildren(CdkDropList)
// 	set cdkDropLists(value: QueryList<CdkDropList>) {
// 		this.cdkDropCourseLists = value.toArray();
// 	}
// 	/**
// 	 * Nutne pre size update course elementov
// 	 */
// 	@HostListener('window:resize', ['$event'])
// 	onResize(): void {
// 		if (this.items) {
// 			this.items.forEach((item) => {
// 				console.log('resize');
// 				this.renderer.setStyle(
// 					item.nativeElement,
// 					'width',
// 					this.getWidth(this.container.nativeElement.offsetWidth)
// 				);
// 			});
// 		}
// 	}

// 	ngOnInit(): void {
// 		this.store.dispatch(loadGroups());
// 		this.store.dispatch(loadTimetables());
// 		this.store.dispatch(loadTimetableEvents({ timetableId: 1 }));
// 		this.store.dispatch(loadTimetableEvents({ timetableId: 2 }));
// 		// this.store.dispatch(
// 		// 	departmentApiActions.searchDepartment({
// 		// 		query: { limit: 15, offset: 0 } as PaginatorFilter,
// 		// 	})
// 		// );
// 		this.getFaculties();

// 		// this.timetablesList$ = this.store.select(selectAllTimetables);

// 		// this.departments$ = this.store.select(selectAllDepartments);
// 		// this.store.select(selectAllDepartments).subscribe((data) => (this.deps = data));
// 		// this.store
// 		// 	.select(selectDepartmentState)
// 		// 	.subscribe((data) => (this.deps = data.entities));

// 		this.timetablesList$.subscribe((val) => console.log({ newVal: val }));
// 		for (const day of this.days) {
// 			this.fillDay(this._data, day);
// 		}
// 	}

// 	getFaculties() {
// 		console.log('Get Faculties');

// 		// this.store.select(departmentSelectors.selectFacultiesEmpty).subscribe((isEmpty) => {
// 		// 	console.log({ isEmpty: isEmpty });
// 		// 	if (isEmpty) {
// 		this.store.dispatch(departmentApiActions.getAllDepartments());
// 		// this.store.dispatch(
// 		// 	departmentApiActions.searchDepartment({
// 		// 		query: { limit: 15, offset: 0 } as PaginatorFilter,
// 		// 	})
// 		// );
// 		// 	}
// 		// });
// 		this.faculties$ = this.store.select(departmentSelectors.selectFaculties);
// 		this.store.select(departmentSelectors.selectFaculties).subscribe((res) => {
// 			this.deps = res;
// 		});
// 	}

// 	addTimetable(timetableName: string) {
// 		this.store.dispatch(timetablesActions.createTimetable({ name: timetableName }));
// 		this.nameOfNewTimetable = '';
// 	}

// 	fillDay(data: Array<CourseSerializerFull[]>, s: string): void {
// 		const day = this.hours.map(() => ({ name: '', code: '' } as CourseSerializerFull));

// 		data.push(day);
// 	}

// 	fillHour(day: CourseSerializerFull[], index: number): void {
// 		const item = { name: '', code: '' } as CourseSerializerFull;
// 		day.splice(index, 0, item);
// 	}

// 	isFillerBlock(course: CourseSerializerFull): boolean {
// 		return BasicUtils.isEmptyString(course.name);
// 	}

// 	removeItem(data: Array<CourseSerializerFull>, curIndex: number): void {
// 		// todo chybaju este podmienky... veci skacu po premiestneni
// 		if (data[curIndex + 1]) {
// 			// prezri element vyssie
// 			for (let i = curIndex + 1; i < data.length; i++) {
// 				if (this.isFillerBlock(data[i])) {
// 					data.splice(i, 1);
// 					break;
// 				}
// 			}
// 		} else {
// 			// prezri element nizsie lebo si na konci
// 			for (let i = curIndex - 1; i >= 0; i--) {
// 				if (this.isFillerBlock(data[i])) {
// 					data.splice(i, 1);
// 					break;
// 				}
// 			}
// 		}
// 	}

// 	drop(e: CdkDragDrop<EventData[]>, data: EventData[]): void {
// 		this.resetHighlighting();
// 		//TODO:TESTING DIALOG COLLISION////////
// 		const dialogCollision = this.collisionTrigger();
// 		dialogCollision.afterClosed();
// 		///////////////////////////////////////
// 		if (e.previousContainer === e.container) {
// 			moveItemInArray(e.container.data, e.previousIndex, e.currentIndex);
// 		} else {
// 			// todo chybaju este podmienky... veci skacu po premiestneni
// 			console.log('drop', e);
// 			const item = e.item.data as EventData;
// 			if (
// 				(e.container.id !== this.practicesID && e.container.id !== this.coursesID) ||
// 				(e.container.id === this.practicesID && this.isPractice(item)) ||
// 				(e.container.id === this.coursesID && this.isLesson(item))
// 			) {
// 				transferArrayItem(
// 					e.previousContainer.data,
// 					e.container.data,
// 					e.previousIndex,
// 					e.currentIndex
// 				);
// 				if (data.length > this.hours.length) {
// 					this.removeItem(data, e.currentIndex);
// 				}

// 				for (const day of this._data) {
// 					if (day.length < this.hours.length) {
// 						this.fillHour(
// 							day,
// 							day.findIndex((value) => this.isSame(value, e.item.data))
// 						);
// 					}
// 					if (day.length > this.hours.length) {
// 						this.removeItem(day, day.length);
// 					}
// 				}
// 			}
// 		}
// 	}

// 	isPractice(item: EventData): boolean {
// 		return this.practicesData.includes(item);
// 	}

// 	getWidth(offsetWidth: number): string {
// 		return `${offsetWidth / (this.hours.length + 1)}px`;
// 	}

// 	entered(enter: CdkDragEnter<EventData[]>): void {
// 		// console.log('enter', 'Calculate colision for', enter);
// 		console.log('colision time', enter.container.id, ' ', this.hours[enter.currentIndex]);
// 	}

// 	sorted(sort: CdkDragSortEvent<EventData[]>): void {
// 		// console.log('sorted', 'Calculate colision for', sort);
// 		// console.log('colision time', sort.container.id, ' ', this.hours[sort.currentIndex]);
// 		// todo call time request colision logic
// 		this.highlightRequirementTime(sort.item.data as unknown as EventData);
// 		// todo call lesson colision logic
// 	}

// 	exited(event: CdkDragExit<EventData[]>): void {
// 		console.log('exited', event);
// 		// todo call time request colision logic
// 		this.highlightRequirementTime(event.item.data as unknown as EventData);
// 	}

// 	getTimeUnit(i: number, j: number): TimeDataUnit {
// 		if (
// 			this.requirementTime &&
// 			this.requirementTime.length > i &&
// 			this.requirementTime[i].length > j
// 		) {
// 			return this.requirementTime[i][j];
// 		}
// 		return null;
// 	}

// 	private highlightRequirementTime(data: EventData): void {
// 		console.log('highlightRequirementTime item', data);
// 		this.requirementTime = data.requestedTime;
// 	}

// 	private resetHighlighting(): void {
// 		this.requirementTime = undefined;
// 	}

// 	private isSame(value: EventData, item: EventData): boolean {
// 		if (item) {
// 			return value.id === item.id;
// 		}
// 		return false;
// 	}

// 	private isLesson(item: EventData): boolean {
// 		return this.coursesData.includes(item);
// 	}

// 	//TODO: TESTING COLLISION TRIGGER
// 	private collisionTrigger() {
// 		return this.dialog.open(CollisionDialogComponent, {
// 			data: {
// 				ok_label: 'Dobre',
// 				no_label: 'Zrušiť',
// 				information: 'Čas: ',
// 				title: 'Kolízia',
// 				// information: "Čas: " + this.eventTest.collisions.map(collision => collision.courses.map((item) => item.time)),
// 				// title: "Názov kolízie: " + this.eventTest.collisions.map(collision => collision.courses.map((item) =>
// 				//   item.course.name)),
// 				// title: marker('dialog.collision.title'),
// 				// ok_label: marker('dialog.collision.ok_label'),
// 				// information: marker('dialog.collision.information'),
// 			} as CollisionDialogModel,
// 			disableClose: true,
// 		});
// 	}

// 	refreshTimetables() {
// 		this.store.dispatch(loadTimetables());
// 		if (this.selectedTimetable != null)
// 			this.store.dispatch(
// 				loadTimetableEvents({ timetableId: this.selectedTimetable.id })
// 			);
// 		this.selectedTimetable = null;
// 		this.selectedEvents = null;
// 	}

// 	timetableModelChange($event: Timetable) {
// 		if ($event != null) {
// 			this.store.dispatch(loadTimetableEvents({ timetableId: $event.id }));
// 		}
// 	}

// 	timetableEventModelChange($event: Event) {
// 		console.log($event);
// 	}

// 	clearTimetable($event: Timetable) {
// 		this.store.dispatch(timetablesActions.deleteTimetable({ id: $event.id }));
// 	}

// 	importTimetableEvents() {
// 		console.log('importTimetableEvents');
// 		this.store.dispatch(
// 			timetableEventsActions.importTimetableEvents({ timetable: this.selectedTimetable })
// 		);
// 	}
// }
