import { Comments, User } from '@elisa11/elisa-api-angular-client';

export class Reply implements Comments {
	commentBy: User;
	text: string;
	id: number;
	commentId: number;
	created_at: string;
	created_by: number;
}
