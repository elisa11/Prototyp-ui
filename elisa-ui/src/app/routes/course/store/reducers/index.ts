import * as fromCourse from './course.reducer';
import * as fromRoot from '@core/store/reducers';
import { Action, combineReducers } from '@ngrx/store';
import * as fromSearchCourse from './search-course.reducer';

export const courseFeatureKey = 'courses';

export interface CourseState extends fromRoot.AppState {
	[fromCourse.courseFeatureKey]: fromCourse.State;
	[fromSearchCourse.searchCourseFeatureKey]: fromSearchCourse.SearchState;
}

export function reducers(state: CourseState | undefined, action: Action) {
	return combineReducers({
		[fromCourse.courseFeatureKey]: fromCourse.reducer,
		[fromSearchCourse.searchCourseFeatureKey]: fromSearchCourse.reducer,
	})(state, action);
}
