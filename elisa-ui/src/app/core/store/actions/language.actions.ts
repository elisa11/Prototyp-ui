import { createAction, props } from '@ngrx/store';

export const loadLanguage = createAction(
	'[Core - Language] Load Language',
	props<{ lang: string }>()
);
