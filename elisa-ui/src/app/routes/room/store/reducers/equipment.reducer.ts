import { createReducer, on } from '@ngrx/store';
import { createEntityAdapter, EntityAdapter, EntityState } from '@ngrx/entity';
import { Equipment } from '@elisa11/elisa-api-angular-client';
import {
  equipmentApiActions,
} from '../actions';

export const equipmentFeatureKey = 'equipments';

export interface State extends EntityState<Equipment> {
  readonly selectedIds: number[];
  readonly count: number;
  readonly loading: boolean;
}

export const adapter: EntityAdapter<Equipment> = createEntityAdapter<Equipment>({
  selectId: (model) => model.id,
  sortComparer: false,
});

export const initialState = adapter.getInitialState({
  selectedIds: [],
  count: 0,
  loading: false
});

export const reducer = createReducer(
  initialState,
  on(equipmentApiActions.loadEquipments, (state) =>({
    ...state,
    loading: true,
  })),
  on(equipmentApiActions.loadEquipmentsSuccess, (state, {data}) =>
    adapter.upsertMany(data, {
      ...state,
      selectedIds: data.map((value) => value.id),
      count: data.length,
      loading: false,
    })
  ),
  on(equipmentApiActions.loadEquipmentsFailure, (state, { error }) => ({
    ...state,
    loading: false,
    error: error
  })),
);

export const getSelectedIds = (state: State) => state.selectedIds;
export const getLoading = (state: State) => state.loading;
export const getCount = (state: State) => state.count;
