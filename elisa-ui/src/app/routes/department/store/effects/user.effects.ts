import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { resetCachedData } from '@core/store/actions/settings.actions';
import { catchError, map, switchMap, tap } from 'rxjs/operators';
import { SnackEffects } from '@core/generics/snack.effects';
import { userApiActions, userDepartmentApiActions } from '../actions';
import { MatSnackBar } from '@angular/material/snack-bar';
import { of, zip } from 'rxjs';
import { NGXLogger } from 'ngx-logger';
import { Store } from '@ngrx/store';
import { UserCacheService } from '@core/service/cache/user-cache.service';
import { Users } from '../reducers/user.reducer';

const tag = 'UserEffects';

@Injectable()
export class UserEffects extends SnackEffects {
	resetData$ = createEffect(
		() =>
			this.actions$.pipe(
				ofType(resetCachedData),
				tap(() => this.service.clearCache())
			),
		{ dispatch: false }
	);

	loadUsers$ = createEffect(() =>
		this.actions$.pipe(
			ofType(userDepartmentApiActions.loadUserDepartmentsSuccess),
			tap((a) => this.logger.debug(tag, a)),
			map((action) => {
				return { ids: action.data.results.map((d) => d.user), depId: action.query.id };
			}),
			switchMap(({ ids, depId }) => {
				this.logger.debug(tag, 'user ids:', ids);

				if (ids === null || ids.length == 0) {
					return of(
						userApiActions.loadUsersSuccess({ data: { id: depId, data: [] } as Users })
					);
				}

				return zip(...ids.map((id) => this.service.getUser(id))).pipe(
					map((users) =>
						userApiActions.loadUsersSuccess({ data: { id: depId, data: users } as Users })
					),
					catchError((error) => of(userApiActions.loadUsersFailure({ error })))
				);
			})
		)
	);

	loadUsersFailure$ = createEffect(
		() =>
			this.actions$.pipe(
				ofType(userApiActions.loadUsersFailure),
				tap((err) => this.openSnackbar(err.error))
			),
		{ dispatch: false }
	);

	constructor(
		private actions$: Actions,
		private store: Store,
		private logger: NGXLogger,
		private service: UserCacheService,
		snack: MatSnackBar
	) {
		super(snack);
	}
}
