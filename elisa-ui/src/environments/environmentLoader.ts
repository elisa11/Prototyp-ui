import { ajax } from 'rxjs/ajax';

export const environmentLoader = () => {
	const url =
		window.location.protocol + '//' + window.location.host + '/' + 'api-gateway/config/';

	return ajax.getJSON(url);
};
