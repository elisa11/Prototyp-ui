import { Injectable } from '@angular/core';
import {
	ActivatedRouteSnapshot,
	CanActivate,
	Router,
	RouterStateSnapshot,
	UrlTree,
} from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from './auth.service';
import { map, take } from 'rxjs/operators';
import { NGXLogger } from 'ngx-logger';
import { Store } from '@ngrx/store';
import { authActions } from '../store/actions';
import { selectLoggedIn } from '../store/selectors/auth.selectors';

const tag = 'AuthGuard';

@Injectable({
	providedIn: 'root',
})
export class AuthGuard implements CanActivate {
	constructor(
		private authService: AuthService,
		private router: Router,
		private logger: NGXLogger,
		private store: Store
	) {}

	canActivate(
		route: ActivatedRouteSnapshot,
		state: RouterStateSnapshot
	): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
		return this.store.select(selectLoggedIn).pipe(
			map((authed) => {
				if (!authed) {
					this.logger.warn(tag, 'Not authenticated', state.url);
					this.store.dispatch(authActions.logout({ returnUrl: state.url }));
					return false;
				}
				return true;
			}),
			take(1)
		);
	}
}
