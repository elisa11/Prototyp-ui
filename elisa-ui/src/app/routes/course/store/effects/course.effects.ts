import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { MatSnackBar } from '@angular/material/snack-bar';
import { SnackEffects } from '@core/generics/snack.effects';
import { resetCachedData } from '@core/store/actions/settings.actions';
import {
	catchError,
	debounceTime,
	map,
	skip,
	switchMap,
	takeUntil,
	tap,
	withLatestFrom,
} from 'rxjs/operators';
import { asyncScheduler, EMPTY, of } from 'rxjs';
import { courseApiActions } from '../actions';
import { CoursesCacheService } from '../../services/courses-cache.service';
import { Store } from '@ngrx/store';
import { schemaSelectors } from '@core/store/selectors';
import { PaginatorFilter } from '@data/filter/paginator.filter';
import { deleteCourse, postCourseFailure, postCourseSuccess } from '../actions/course-api.actions';
import { UserApiActions } from '../../../user/store/actions';

@Injectable()
export class CourseEffects extends SnackEffects {
	resetData$ = createEffect(
		() =>
			this.actions$.pipe(
				ofType(resetCachedData),
				tap(() => this.service.clearCache())
			),
		{ dispatch: false }
	);

	search$ = createEffect(
		() =>
			({ debounce = 300, scheduler = asyncScheduler } = {}) =>
				this.actions$.pipe(
					ofType(courseApiActions.searchCourse),
					debounceTime(debounce, scheduler),
					withLatestFrom(this.store.select(schemaSelectors.selectScheme)),
					map((value) => {
						return { query: value[0].query, version: value[1].name };
					}),
					switchMap(({ query, version }) => {
						if (query === null) {
							return EMPTY;
						}

						const nextSearch$ = this.actions$.pipe(
							ofType(courseApiActions.searchCourse),
							skip(1)
						);

						return this.service.listCourses(query, version).pipe(
							takeUntil(nextSearch$),
							map((data) => courseApiActions.searchCourseSuccess({ data })),
							catchError((err) =>
								of(courseApiActions.searchCourseFailure({ error: err.message }))
							)
						);
					})
				)
	);
	searchFailed$ = createEffect(
		() =>
			this.actions$.pipe(
				ofType(courseApiActions.searchCourseFailure),
				tap((err) => this.openSnackbar(err.error))
			),
		{ dispatch: false }
	);

  postCourse$ = createEffect(()=>
      this.actions$.pipe(
        ofType(courseApiActions.postCourse),
        withLatestFrom(this.store.select(schemaSelectors.selectScheme)),
        map((value)=> {
            return {scheme: value[1].name, course: value[0].data}
          }
        ),
        switchMap((values)=>{
          return this.service.createCourse(values.scheme, values.course)
            .pipe(
              map((val) => courseApiActions.postCourseSuccess({data:val})),
              catchError((err) =>
                of(courseApiActions.postCourseFailure({error:err})))
            );
        })
      ),
  );

  postCourseSuccess = createEffect(() =>
    this.actions$.pipe(
      ofType(courseApiActions.postCourseSuccess),
      map(() => {
        this.openSnackbar("Predmet bol pridaný do zoznamu.");
        // this.openSnackbar(this.localeService.translate(marker('course.post.msg')))
        return courseApiActions.searchCourse({
          query: {} as PaginatorFilter});
      })
    )
  );

  postCourseFailure$ = createEffect(() =>
      this.actions$.pipe(
        ofType(courseApiActions.postCourseFailure),
        map(() =>
            this.openSnackbar("Nastala chyba pri vytváraní predmetu.")
          // this.openSnackbar(this.localeService.translate(marker('course.post.error')))
        )
      ),
    { dispatch: false }
  );

  putCourse$ = createEffect(()=>
    this.actions$.pipe(
      ofType(courseApiActions.putCourse),
      withLatestFrom(this.store.select(schemaSelectors.selectScheme)),
      map((value)=> {
          return {scheme: value[1].name, course: value[0].data}
        }
      ),
      switchMap((values)=>{
        return this.service.putCourse(values.scheme, values.course)
          .pipe(
            map((val) => courseApiActions.putCourseSuccess({data:val})),
            catchError((err) =>
              of(courseApiActions.putCourseFailure({error:err})))
          );
      })
    ),
  );

  putCourseSuccess = createEffect(() =>
    this.actions$.pipe(
      ofType(courseApiActions.putCourseSuccess),
      map(() => {
        this.openSnackbar("Predmet bol úspešne upravený.");
        // this.openSnackbar(this.localeService.translate(marker('course.post.msg')))
        return courseApiActions.searchCourse({
          query: {} as PaginatorFilter});
      })
    )
  );

  putCourseFailure$ = createEffect(() =>
      this.actions$.pipe(
        ofType(courseApiActions.putCourseFailure),
        map(() =>
            this.openSnackbar("Nastala chyba pri editovaní predmetu.")
          // this.openSnackbar(this.localeService.translate(marker('course.post.error')))
        )
      ),
    { dispatch: false }
  );

  deleteCourse = createEffect(() =>
    this.actions$.pipe(
      ofType(deleteCourse),
      withLatestFrom(this.store.select(schemaSelectors.selectScheme)),
      map((data) => {
        return { id: data[0].id, scheme: data[1].name };
      }),
      switchMap((data) => {
        return this.service.deleteCourse(data.scheme, data.id).pipe(
          map(()=> courseApiActions.deleteCourseDone({id:data.id})),
          catchError ((err) =>
            of(courseApiActions.deleteCourseFailed({error: err}))
          )
        );
      })
    ),
  );

  deleteCourseDone$ = createEffect(() =>
    this.actions$.pipe(
      ofType(courseApiActions.deleteCourseDone),
      map(() => {
        this.openSnackbar("Kurz bol vymazaný.");
        // this.openSnackbar(this.localeService.translate(marker('course.delete.msg')))
        return courseApiActions.searchCourse({
          query: {} as PaginatorFilter});
      })
    )
  );
  deleteCourseFailed$ = createEffect(() =>
      this.actions$.pipe(
        ofType(courseApiActions.deleteCourseFailed),
        map((err) =>
          this.openSnackbar("Nastala chyba pri vymazávaní kurzu.")
          // this.openSnackbar(this.localeService.translate(marker('course.delete.error')))
        )
      ),
    { dispatch: false }
  );
	constructor(
		private actions$: Actions,
		private service: CoursesCacheService,
		private store: Store,
		snack: MatSnackBar
	) {
		super(snack);
	}
}
