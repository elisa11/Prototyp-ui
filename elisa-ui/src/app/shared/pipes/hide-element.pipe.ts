import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
	name: 'hideElement',
	pure: false,
})
export class HideElementPipe implements PipeTransform {
	transform(value: boolean): string {
		return value ? 'none' : 'block';
	}
}
