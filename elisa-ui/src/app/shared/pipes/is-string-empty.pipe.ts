import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
	name: 'isStringEmpty',
})
export class IsStringEmptyPipe implements PipeTransform {
	transform(str: string): boolean {
		return !str || 0 === str.length;
	}
}
