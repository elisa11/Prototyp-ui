import { ChangeDetectionStrategy, Component } from '@angular/core';
import { AbstractSelectedPageComponent } from '@core/generics/abstract-selected-page.component';
import { Group } from '@elisa11/elisa-api-angular-client';
import { Store } from '@ngrx/store';
import { groupSelectors } from '../../store/selectors';

@Component({
	selector: 'app-selected-group-page',
	template: `
		<app-group-detail [data]="data$ | async" (onBack)="back($event)"></app-group-detail>
	`,
	changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SelectedGroupPageComponent extends AbstractSelectedPageComponent<Group> {
	constructor(private store: Store) {
		super();
		this.data$ = store.select(groupSelectors.selectSelectedGroup);
	}
}
