import * as fromUser from './user.reducer';
import * as fromRoot from '@core/store/reducers';
import { Action, combineReducers } from '@ngrx/store';
import * as fromSearch from './search.reducer';
import * as fromUserDepartment from './user-department.reducer';
import * as fromDepartment from './department.reducer';
import * as fromDepartmentSearch from './department-search.reducer';

export const usersFeatureKey = 'users';

export interface UsersState extends fromRoot.AppState {
	[fromUser.userFeatureKey]: fromUser.State;
	[fromSearch.searchFeatureKey]: fromSearch.State;
	[fromUserDepartment.userDepartmentFeatureKey]: fromUserDepartment.State;
	[fromDepartment.departmentFeatureKey]: fromDepartment.State;
	[fromDepartmentSearch.departmentSearchFeatureKey]: fromDepartmentSearch.State;
}

export function reducers(state: UsersState | undefined, action: Action) {
	return combineReducers({
		[fromUser.userFeatureKey]: fromUser.reducer,
		[fromSearch.searchFeatureKey]: fromSearch.reducer,
		[fromUserDepartment.userDepartmentFeatureKey]: fromUserDepartment.reducer,
		[fromDepartment.departmentFeatureKey]: fromDepartment.reducer,
		[fromDepartmentSearch.departmentSearchFeatureKey]: fromDepartmentSearch.reducer,
	})(state, action);
}
