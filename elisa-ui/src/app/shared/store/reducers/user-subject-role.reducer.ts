import { createReducer, on } from '@ngrx/store';
import { createEntityAdapter, EntityAdapter, EntityState } from '@ngrx/entity';
import { UserSubjectRole } from '@elisa11/elisa-api-angular-client';
import { userSubjectRolesActions } from '@shared/store/actions';

export const userSubjectRoleFeatureKey = 'userSubjectRole';

export interface State extends EntityState<UserSubjectRole> {
	count: 0;
	loading: false;
}

export const adapter: EntityAdapter<UserSubjectRole> =
	createEntityAdapter<UserSubjectRole>({
		selectId: (model) => model.id,
		sortComparer: false,
	});

export const initialState = adapter.getInitialState({
	count: 0,
	loading: false,
});

export const reducer = createReducer(
	initialState,
	on(userSubjectRolesActions.loadUserSubjectRoles, (state) => ({
		...state,
		loading: true,
	})),
	on(userSubjectRolesActions.loadUserSubjectRolesFailure, (state) => ({
		...state,
		loading: false,
	})),
	on(userSubjectRolesActions.loadUserSubjectRolesSuccess, (state, { data }) =>
		adapter.upsertMany(data.results, { ...state, count: data.count, loading: false })
	)
);

export const getCount = (state) => state.count;
export const getLoading = (state) => state.loading;
