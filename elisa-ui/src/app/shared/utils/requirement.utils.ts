import { RequirementModel } from '../../routes/requirement/model/requirement.model';
import { StateEnum } from '@elisa11/elisa-api-angular-client';
import { TimeDataUnit } from '../../routes/requirement/model/time-data-unit.model';
import { RoomModel } from '../../routes/room/model/room.model';
import { RequirementPartModel } from '../../routes/requirement/model/requirement-part.model';

export const RequirementUtils = {
	isRequirementOpen(r: RequirementModel): boolean {
		return r.state === StateEnum.Created || r.state === StateEnum.Edited;
	},
	isRequirementClosed(r: RequirementModel): boolean {
		return r.state === StateEnum.Approved || r.state === StateEnum.Rejected;
	},
	getPart(requirementParts: Array<RequirementPartModel>): {
		time: TimeDataUnit[][];
		room: RoomModel[];
	} {
		// todo
		return undefined;
	},
	setPart(data: { room: string; time: TimeDataUnit[][] }[]): Array<RequirementPartModel> {
		// todo
		return null;
	},
};
