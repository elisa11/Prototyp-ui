import { ChangeDetectionStrategy, Component } from '@angular/core';
import { combineLatest, Observable } from 'rxjs';
import { Department, User } from '@elisa11/elisa-api-angular-client';
import { Store } from '@ngrx/store';
import { map } from 'rxjs/operators';
import { DepartmentData } from '../../model/department-data.model';
import {
	selectError,
	selectLoading,
} from '../../store/selectors/department-search.selectors';
import { AbstractSelectedPageComponent } from '@core/generics/abstract-selected-page.component';
import { UserDepartmentApiActions } from '../../store/actions';
import {
	departmentSelector,
	userDepartmentSelector,
	userSelector,
} from '../../store/selectors';

@Component({
	selector: 'app-selected-user-page',
	template: `
		<app-user-detail
			[data]="data$ | async"
			[departmentData]="departments$ | async"
			[departmentDataError]="errorDepartments$ | async"
			[departmentDataLoading]="departmentsLoading$ | async"
			(onBack)="back($event)"
		></app-user-detail>
	`,
	changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SelectedUserPageComponent extends AbstractSelectedPageComponent<User> {
	departments$: Observable<DepartmentData[]>;
	departmentsLoading$: Observable<boolean>;
	errorDepartments$: Observable<string>;

	constructor(private store: Store) {
		super();
		store.dispatch(UserDepartmentApiActions.loadUserDepartments());
		this.data$ = store.select(userSelector.selectSelectedUser);
		this.errorDepartments$ = store.select(selectError);
		this.departmentsLoading$ = store.select(selectLoading);
		this.departments$ = combineLatest([
			store.select(departmentSelector.selectDepartmentByUserId),
			store.select(userDepartmentSelector.selectUserDepartmentByUserId),
		]).pipe(
			map((value) => {
				return { departments: value[0], userDepartments: value[1] };
			}),
			map((data) => {
				const deps = data.departments;
				const userDeps = data.userDepartments;
				console.log('user Department', data);
				return userDeps.map((userDep) => {
					const dep = deps.find((d) => d.id === userDep.department) ?? ({} as Department);
					return new DepartmentData(userDep, dep);
				});
			})
		);
	}
}
