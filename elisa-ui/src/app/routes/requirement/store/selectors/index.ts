import * as requirementSelectors from './requirement.selectors';
import * as searchRequirementSelectors from './search-requirement.selectors';
import * as courseSelectors from './course.selectors';
import * as userSelectors from './user.selectors';
import * as searchUserSelectors from './search-user.selectors';
import * as roomSelectors from './room.selectors';
import * as activitySelectors from './activity.selectors';
import * as subjectTeachersSelectors from './subject-teachers.selectors';
import * as groupSelectors from './group.selectors';
import * as searchGroupSelectors from './search-group.selectors';

export {
	requirementSelectors,
	searchRequirementSelectors,
	courseSelectors,
	userSelectors,
	roomSelectors,
	subjectTeachersSelectors,
	activitySelectors,
	searchUserSelectors,
	groupSelectors,
	searchGroupSelectors,
};
