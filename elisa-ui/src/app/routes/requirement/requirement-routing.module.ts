import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from '../../auth/services/auth.guard';
import { RequirementExistsGuard } from './guards';
import { ViewRequirementPageComponent } from './containers/view-requirement-page/view-requirement-page.component';
import { CollectionPageComponent } from './containers/collection-page/collection-page.component';
import { NewRequirementPageComponent } from './containers/new-requirement-page/new-requirement-page.component';

const routes: Routes = [
	{
		path: '',
		component: CollectionPageComponent,
		canActivate: [AuthGuard],
	},
	{
		path: 'form',
		component: NewRequirementPageComponent,
		canActivate: [AuthGuard],
	},
	{
		path: ':id',
		component: ViewRequirementPageComponent,
		canActivate: [AuthGuard, RequirementExistsGuard],
	},
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule],
})
export class RequirementRoutingModule {}
