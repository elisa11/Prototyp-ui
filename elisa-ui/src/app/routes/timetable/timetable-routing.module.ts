import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { TimetableComponent } from './components/timetable/timetable.component';
import { AuthGuard } from '../../auth/services/auth.guard';
import { CollectionPageComponent } from './containers/collection-page.component';

const routes: Routes = [
	{
		path: '',
		component: CollectionPageComponent,
		canActivate: [AuthGuard],
	},
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule],
})
export class TimetableRoutingModule {}
