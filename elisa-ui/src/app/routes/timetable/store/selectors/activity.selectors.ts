import { createSelector } from '@ngrx/store';
import * as fromActivity from '../reducers/activity.reducer';
import { Activity } from '@elisa11/elisa-api-angular-client';
import { selectTimetablesState } from './timetables.selectors';

export const selectActivityState = createSelector(
  selectTimetablesState,
  (s) => s[fromActivity.activityFeatureKey]
);

export const selectSelectedIds = createSelector(
  selectActivityState,
  fromActivity.getSelectedIds
);
export const selectTotalCount = createSelector(selectActivityState, fromActivity.getCount);
export const selectSearchLoading = createSelector(selectActivityState, fromActivity.getLoading);

export const { selectIds, selectEntities } =
  fromActivity.adapter.getSelectors(selectActivityState);

export const selectSelectedResults = createSelector(
  selectEntities,
  selectSelectedIds,
  (data, ids) => {
    return ids.map((id) => data[id]).filter((d): d is Activity => d != null);
  }
);
export const selectAllActivities = createSelector(  selectEntities,
  selectSelectedIds,
  (data, ids) => {
    return ids.map((id) => data[id]);
  }
)

// export const selectAllGroups = createSelector(selectGroupsState, (state: State)=> state.data)
