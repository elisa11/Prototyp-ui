import { createAction, props } from '@ngrx/store';
import { PaginatedSubjectTeachersList } from '@elisa11/elisa-api-angular-client';
import { PaginatorFilter } from '@data/filter/paginator.filter';

export const loadUserSubjects = createAction(
	'[Requirement - UserSubject/Api] Load UserSubjects',
	props<{ query: PaginatorFilter }>()
);

export const loadUserSubjectsSuccess = createAction(
	'[Requirement - UserSubject/Api] Load UserSubjects Success',
	props<{ data: PaginatedSubjectTeachersList }>()
);

export const loadUserSubjectsFailure = createAction(
	'[Requirement - UserSubject/Api] Load UserSubjects Failure',
	props<{ error: any }>()
);

export const loadUserSubjectsByCourse = createAction(
	'[Requirement - UserSubject/Api] Load UserSubjects By Course',
	props<{ cid: number }>()
);

export const resetUserSubjectsByCourse = createAction(
	'[Requirement - UserSubject/Api] Reset UserSubjects By Course'
);
