import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { SnackEffects } from '@core/generics/snack.effects';
import { Store } from '@ngrx/store';
import { MatSnackBar } from '@angular/material/snack-bar';
import { resetCachedData } from '@core/store/actions/settings.actions';
import { catchError, map, switchMap, tap, withLatestFrom } from 'rxjs/operators';
import { TypeCacheService } from '../../services/type-cache.service';
import { roomApiActions, typeApiActions } from '../actions';
import { of, zip } from 'rxjs';
import { schemaSelectors } from '@core/store/selectors';
import { userApiActions } from '../../../department/store/actions';

@Injectable()
export class TypeEffects extends SnackEffects {
	resetData$ = createEffect(
		() =>
			this.actions$.pipe(
				ofType(resetCachedData),
				tap(() => this.service.clearCache())
			),
		{ dispatch: false }
	);

	loadTypes$ = createEffect(() =>
		this.actions$.pipe(
			ofType(roomApiActions.searchRoomSuccess),
			map((action) => action.data.results.map((r) => r.room_type)),
			withLatestFrom(this.store.select(schemaSelectors.selectScheme)),
			map((value) => {
				return { ids: value[0], version: value[1].name };
			}),
			switchMap(({ ids, version }) => {
				if (ids === null || ids.length == 0) {
					return of(typeApiActions.loadRoomTypeSuccess({ data: [] }));
				}

				return zip(...ids.map((id) => this.service.getType(version, id))).pipe(
					map((data) => typeApiActions.loadRoomTypeSuccess({ data })),
					catchError((error) => of(typeApiActions.loadRoomTypeFailure({ error })))
				);
			})
		)
	);

	loadUsersFailure$ = createEffect(
		() =>
			this.actions$.pipe(
				ofType(userApiActions.loadUsersFailure),
				tap((err) => this.openSnackbar(err.error))
			),
		{ dispatch: false }
	);

	constructor(
		private actions$: Actions,
		private service: TypeCacheService,
		private store: Store,
		snack: MatSnackBar
	) {
		super(snack);
	}
}
