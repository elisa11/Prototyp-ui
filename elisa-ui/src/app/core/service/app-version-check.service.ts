import { Injectable, Optional, SkipSelf } from '@angular/core';
import { interval, Subscription, throwError } from 'rxjs';
import { HttpClient } from '@angular/common/http';

import PACKAGE from '../../../../package.json';
import { MatDialog } from '@angular/material/dialog';
import { marker } from '@biesbjerg/ngx-translate-extract-marker';
import { auditTime, first, startWith } from 'rxjs/operators';
import { Router } from '@angular/router';
import { NGXLogger } from 'ngx-logger';
import { ConfirmDialogComponent } from '../../dialogs/confirm-dialog/confirm-dialog.component';
import { ConfirmDialogModel } from '../../dialogs/confirm-dialog/confirm-dialog.model';

const tag = 'AppVersionCheckService';

@Injectable({
	providedIn: 'root',
})
export class AppVersionCheckService {
	private static initialVersion = '';
	private alive: boolean;
	private url: string;
	private subscription: Subscription;
	private dialogOpened = false;
	private timer = interval(5 * 60 * 1000).pipe(startWith(0));

	constructor(
		private http: HttpClient,
		private dialog: MatDialog,
		private router: Router,
		private logger: NGXLogger,
		@Optional()
		@SkipSelf()
		otherInstance: AppVersionCheckService
	) {
		if (otherInstance) {
			throwError('VersionCheckService should have only one instance.');
		}

		this.url = '/api-gateway/config/';
		const VERSION = PACKAGE.version;
		if (VERSION) {
			this.logger.debug(tag, 'App version: ', VERSION);
			AppVersionCheckService.initialVersion = VERSION;
			this.startTimer();
		}
	}

	get actual() {
		return AppVersionCheckService.initialVersion;
	}

	reloadCurrentRoute() {
		const currentUrl = this.router.url;
		this.router.navigateByUrl('/', { skipLocationChange: true }).then(() => {
			this.router.navigate([currentUrl]);
		});
	}

	private stopTimer() {
		this.alive = false;
		if (this.subscription) {
			this.subscription.unsubscribe();
			this.subscription = null;
		}
	}

	private startTimer() {
		if (!this.alive) {
			this.logger.debug(tag, 'Version check timer started');
			this.alive = true;
			this.subscription = this.timer.pipe(auditTime(500)).subscribe(
				() => {
					this.checkVersion();
				},
				(error) => this.logger.error(tag, 'Error starting timer: ', error)
			);
		}
	}

	private checkVersion() {
		this.http
			.get<any>(this.url)
			.pipe(first())
			.subscribe(
				(data: any) => {
					const version = data.properties.param.version;
					this.logger.debug(
						tag,
						`Initial version: ${AppVersionCheckService.initialVersion}; Actual version: ${version}`
					);

					if (
						data.properties.param.environment !== 'local' &&
						version !== AppVersionCheckService.initialVersion
					) {
						this.stopTimer();
						const dialogRef = this.dialog.open(ConfirmDialogComponent, {
							data: {
								title: marker('dialog.version-check.title'),
								question: marker('dialog.version-check.question'),
								noLabel: marker('dialog.version-check.no-label'),
								yesLabel: marker('dialog.version-check.yes-label'),
							} as ConfirmDialogModel,
							disableClose: true,
						});

						dialogRef
							.afterClosed()
							.pipe(first())
							.subscribe((result) => {
								if (result) {
									this.reloadCurrentRoute();
								} else {
									this.startTimer();
								}
								this.dialogOpened = false;
							});
					}
				},
				(error) => this.logger.error(tag, `Error getting version info: `, error)
			);
	}
}
