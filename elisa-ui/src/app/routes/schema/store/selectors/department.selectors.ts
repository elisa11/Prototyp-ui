import { createSelector } from '@ngrx/store';
import * as fromDepartment from '../reducers/department.reducer';
import { selectSchemasState } from './schema.selectors';

export const selectDepartmentEntitiesState = createSelector(
	selectSchemasState,
	(state) => state[fromDepartment.departmentFeatureKey]
);

export const {
	selectIds: selectDepartmentIds,
	selectEntities: selectDepartmentEntities,
	selectAll: selectAllDepartments,
} = fromDepartment.adapter.getSelectors(selectDepartmentEntitiesState);
