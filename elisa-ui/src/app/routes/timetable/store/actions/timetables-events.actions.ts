import { createAction, props } from '@ngrx/store';
import { Event, Timetable } from '@elisa11/elisa-api-angular-client';

// GET - Timetables/{parent_lookup_timetable}/events
export const loadTimetableEvents = createAction(
	'[Timetable - Timetable Events] Load Timetable Events',
	props<{
		roomId?: number;
		timetableId?: number;
		activityCategoryIdIn?: Array<number>;
		activityCourseDepartmentIdIn?: Array<number>;
		groupsIdIn?: Array<number>;
	}>()
);

export const loadTimetableEventsSuccess = createAction(
	'[Timetable - Timetable Events] Load Timetable Events Success',
	props<{ events: Event[] }>()
);

export const loadTimetableEventsFailure = createAction(
	'[Timetable - Timetable Events] Load Timetable Events Failure',
	props<{ error: any }>()
);

// POST - Timetables/{parent_lookup_timetable}/events/{id}/
export const postTimetableEvent = createAction(
	'[Timetable - Timetable Event] Post Timetable Event',
	props<{ timetable: Timetable; event: Event }>()
);

export const postTimetableEventSuccess = createAction(
	'[Timetable - Timetable Events] Post Timetable Event Success',
	props<{ event: Event }>()
);

export const postTimetableEventFailure = createAction(
	'[Timetable - Timetable Events] Post Timetable Event Failure',
	props<{ error: any }>()
);

// PUT - Timetables/{parent_lookup_timetable}/events/{id}/
export const putTimetableEvent = createAction(
	'[Timetable - Timetable Event] Put Timetable Event',
	props<{ event: Event }>()
);

export const putTimetableEventSuccess = createAction(
	'[Timetable - Timetable Events] Put Timetable Event Success',
	props<{ event: Event }>()
);

export const putTimetableEventFailure = createAction(
	'[Timetable - Timetable Events] Put Timetable Event Failure',
	props<{ error: any }>()
);

// DELETE - Timetables/{parent_lookup_timetable}/events/{id}/
export const deleteTimetableEvent = createAction(
	'[Timetable - Timetable Event] Delete Timetable Event',
	props<{ timetable: Timetable; event: Event }>()
);

export const deleteTimetableEventSuccess = createAction(
	'[Timetable - Timetable Events] Delete Timetable Event Success',
	props<{ event: Event }>()
);

export const deleteTimetableEventFailure = createAction(
	'[Timetable - Timetable Events] Delete Timetable Event Failure',
	props<{ error: any }>()
);

// POST - timetables/{id}/import_events
export const importTimetableEvents = createAction(
	'[Timetable - Timetable Events] Import Timetable Events',
	props<{ timetable: Timetable }>()
);

export const importTimetableEventsSuccess = createAction(
	'[Timetable - Timetable Events] Import Timetable Events Success',
	props<{ timetable: Timetable }>()
);

export const importTimetableEventsFailure = createAction(
	'[Timetable - Timetable Events] Import Timetable Events Failure',
	props<{ error: any }>()
);
