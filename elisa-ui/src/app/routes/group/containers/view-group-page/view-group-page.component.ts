import { ChangeDetectionStrategy, Component } from '@angular/core';
import { AbstractViewPageComponent } from '@core/generics/abstract-view-page.component';
import { ActivatedRoute, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { map } from 'rxjs/operators';
import { groupActions } from '../../store/actions';

@Component({
	template: `
		<app-selected-group-page (onBack)="back($event)"></app-selected-group-page>
	`,
	changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ViewGroupPageComponent extends AbstractViewPageComponent {
	constructor(router: Router, store: Store, route: ActivatedRoute) {
		super(router);
		this.actionsSubscription = route.params
			.pipe(map((params) => groupActions.selectGroup({ id: params.id })))
			.subscribe((action) => store.dispatch(action));
	}
}
