import { Component, Input } from '@angular/core';

@Component({
	selector: 'app-breadcrumbs',
	templateUrl: './breadcrumbs.component.html',
	styleUrls: ['./breadcrumbs.component.css'],
})
export class BreadcrumbsComponent {
	@Input() names: string[];
	@Input() links: string[] = [];

	constructor() {}

	buildPath(index: number): string[] {
		return ['/' + this.links[0]].concat(this.links.slice(1, index + 1));
	}
}
