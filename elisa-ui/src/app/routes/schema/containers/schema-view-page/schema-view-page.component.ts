import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { PaginatorFilter } from '@data/filter/paginator.filter';
import { CommonText } from '@data/common-text';
import { combineLatest, Observable } from 'rxjs';
import { VersionModel } from '../../model/version.model';
import { map } from 'rxjs/operators';
import {
	newSchemaActions,
	periodApiActions,
	schemaApiActions,
} from '../../store/actions';
import { PeriodModel } from '../../model/period.model';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {
	departmentSelector,
	newSchemaSelector,
	periodApiSelector,
	periodSelector,
	schemaApiSelector,
	schemaSelector,
} from '../../store/selectors';
import { Period, Version } from '@elisa11/elisa-api-angular-client';

@Component({
	template: `
		<div class="d-flex flex-row flex-wrap">
			<app-schema-table
				class="col-12 col-lg-6"
				[selected]="selectedParent$ | async"
				[isLoading]="schemaLoading$ | async"
				[pagination]="pagination"
				[resultsLength]="schemaTotalCount$ | async"
				[data]="schemas$ | async"
				(onFilterChange)="handleSchemaFilterChange($event)"
				(onSelectionChange)="handleParentSelection($event)"
				(onDelete)="deleteSchema($event)"
				(onManualImport)="importSchema($event)"
				(onUpdateVersion)="updateSchema($event)"
			></app-schema-table>

			<app-period-table
				class="col-12 col-lg-6"
				[selected]="selectedPeriod$ | async"
				[pagination]="pagination"
				[isLoading]="periodLoading$ | async"
				[resultsLength]="periodTotalCount$ | async"
				[data]="period$ | async"
				(onFilterChange)="handlePeriodFilterChange($event)"
				(onSelectionChange)="handlePeriodSelection($event)"
			>
			</app-period-table>

			<app-new-schema-form
				class="col-12 col-lg-6"
				[formGroup]="newVersionGroup"
				(onSubmit)="submitNewSchema($event)"
			></app-new-schema-form>
		</div>
	`,
	changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SchemaViewPageComponent implements OnInit {
	pagination = CommonText.pagination; // todo: settings selector
	schemaLoading$: Observable<boolean>;
	periodLoading$: Observable<boolean>;
	schemaTotalCount$: Observable<number>;
	periodTotalCount$: Observable<number>;
	selectedPeriod$: Observable<number>;
	selectedParent$: Observable<number>;
	schemas$: Observable<VersionModel[]>;
	period$: Observable<PeriodModel[]>;
	newVersionGroup: FormGroup;

	constructor(private store: Store, private fb: FormBuilder) {
		this.schemas$ = combineLatest([
			store.select(schemaApiSelector.selectSearchResults),
			store.select(periodSelector.selectPeriodEntities),
		]).pipe(
			map((value) => {
				return { versions: value[0], periods: value[1] };
			}),
			map((results) =>
				results.versions.map((res) => new VersionModel(res, results.periods[res.period]))
			)
		);
		this.schemaTotalCount$ = store.select(schemaSelector.selectSchemaCount);
		this.schemaLoading$ = store.select(schemaApiSelector.selectSearchLoading);

		this.newVersionGroup = this.fb.group({
			name: ['', [Validators.pattern(/^(?!pg_)\w+/), Validators.required]],
			period: ['', Validators.required],
			parent: [''],
		});

		this.period$ = combineLatest([
			store.select(periodSelector.selectSelectedPeriods),
			store.select(departmentSelector.selectDepartmentEntities),
		]).pipe(
			map((value) => {
				return {
					periods: value[0],
					departments: value[1],
				};
			}),
			map((results) =>
				results.periods.map((p) => new PeriodModel(p, results.departments[p.department]))
			)
		);
		this.periodTotalCount$ = store.select(periodSelector.selectPeriodCount);
		this.periodLoading$ = store.select(periodApiSelector.selectPeriodLoading);

		this.selectedParent$ = store.select(newSchemaSelector.selectParentId);
		this.selectedPeriod$ = store.select(newSchemaSelector.selectPeriodId);
	}

	ngOnInit(): void {
		this.store.dispatch(
			schemaApiActions.searchSchemas({
				query: {
					limit: this.pagination[0],
					offset: 0,
				} as PaginatorFilter,
			})
		);
		this.store.dispatch(
			periodApiActions.searchPeriods({
				query: {
					limit: this.pagination[0],
					offset: 0,
				} as PaginatorFilter,
			})
		);
	}

	handleSchemaFilterChange(filter: PaginatorFilter) {
		this.store.dispatch(schemaApiActions.searchSchemas({ query: filter }));
	}

	handlePeriodFilterChange(filter: PaginatorFilter) {
		this.store.dispatch(periodApiActions.searchPeriods({ query: filter }));
	}

	submitNewSchema(form: any) {
		this.store.dispatch(newSchemaActions.addNewSchema({ data: form }));
	}

	handlePeriodSelection(data: PeriodModel) {
		if (data) {
			this.store.dispatch(newSchemaActions.selectPeriod({ id: data.id }));
		}
		this.newVersionGroup.patchValue({ period: { ...data } as Period });
	}

	handleParentSelection(data: VersionModel) {
		if (data) {
			this.store.dispatch(newSchemaActions.selectParent({ id: data.id }));
		}
		this.newVersionGroup.patchValue({ parent: { ...data } as Version });
	}

	deleteSchema(version: VersionModel) {
		if (version) {
			this.store.dispatch(newSchemaActions.deleteSchema({ id: version.id }));
		}
	}

	importSchema(version: VersionModel) {
		if (version) {
			this.store.dispatch(newSchemaActions.importSchemaManual({ version }));
		}
	}

  updateSchema(version: VersionModel) {
    if (version){
      this.store.dispatch(newSchemaActions.updateSchema({version}));
    }
  }
}
