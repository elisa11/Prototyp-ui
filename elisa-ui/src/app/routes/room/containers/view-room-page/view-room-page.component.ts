import { ChangeDetectionStrategy, Component } from '@angular/core';
import { AbstractViewPageComponent } from '@core/generics/abstract-view-page.component';
import { ActivatedRoute, Router } from '@angular/router';
import { map } from 'rxjs/operators';
import { Store } from '@ngrx/store';
import { roomActions } from '../../store/actions';

@Component({
	template: ` <app-selected-room-page (onBack)="back($event)"></app-selected-room-page> `,
	changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ViewRoomPageComponent extends AbstractViewPageComponent {
	constructor(router: Router, store: Store, route: ActivatedRoute) {
		super(router);
		this.actionsSubscription = route.params
			.pipe(map((params) => roomActions.selectRoom({ id: params.id })))
			.subscribe((action) => store.dispatch(action));
	}
}
