import { Pipe, PipeTransform } from '@angular/core';
import { LocaleService } from '@core/service/locale/locale.service';

@Pipe({
	name: 'schemaTranslate',
	pure: false,
})
export class SchemaTranslatePipe implements PipeTransform {
	constructor(private localeService: LocaleService) {}

	transform(value: string): string {
		return value === 'schema.toolbar.label' ? this.localeService.translate(value) : value;
	}
}
