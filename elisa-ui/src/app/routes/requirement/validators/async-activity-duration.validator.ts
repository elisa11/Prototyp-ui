import { AbstractControl, AsyncValidator, ValidationErrors } from '@angular/forms';
import { combineLatest, Observable, of, pipe } from 'rxjs';
import { Store } from '@ngrx/store';
import { activitySelectors } from '../store/selectors';
import { debounceTime, first, map } from 'rxjs/operators';
import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class AsyncActivityDurationValidator implements AsyncValidator {
	constructor(private store: Store) {
		console.log('validator constr');
	}

	validate(
		control: AbstractControl
	): Promise<ValidationErrors | null> | Observable<ValidationErrors | null>;
	validate(control: AbstractControl): ValidationErrors | null;
	validate(
		control: AbstractControl
	):
		| Promise<ValidationErrors | null>
		| Observable<ValidationErrors | null>
		| ValidationErrors
		| null {
		const value = control.value;
		const validations: Observable<boolean>[] = [];
		const minDuration$: Observable<boolean> = this.store
			.select(activitySelectors.selectCourseResults)
			.pipe(
				first(),
				map((ais) => {
					console.log('value', value);
					const aisDuration: number =
						ais && ais.length > 0
							? ais
									.map((a) => a.duration)
									.reduce((accumulator, currentValue) => accumulator + currentValue)
							: 0;
					console.log('ais duration', aisDuration);
					const duration: number =
						value && value.length > 0
							? value
									.map((a) => a.duration)
									.reduce((accumulator, currentValue) => accumulator + currentValue)
							: 0;
					console.log('duration', duration);

					return duration < aisDuration;
				})
			);
		validations.push(minDuration$);

		return combineLatest(validations).pipe(
			debounceTime(500),
			map(([minDuration]) => {
				if (minDuration) {
					return of({
						minDuration: true,
					});
				}
				return of(null);
			})
		);
	}
}

export function validateActivityDuration(store: Store) {
	return (
		control: AbstractControl
	): Promise<ValidationErrors | null> | Observable<ValidationErrors | null> => {
		const value = control.value;
		return store.select(activitySelectors.selectCourseResults).pipe(
			first(),
			map((ais) => {
				console.log('value', value);
				const aisDuration: number =
					ais && ais.length > 0
						? ais
								.map((a) => a.duration)
								.reduce((accumulator, currentValue) => accumulator + currentValue)
						: 0;
				console.log('ais duration', aisDuration);
				const duration: number =
					value && value.length > 0
						? value
								.map((a) => a.duration)
								.reduce((accumulator, currentValue) => accumulator + currentValue)
						: 0;
				console.log('duration', duration);

				return duration < aisDuration;
			}),
			map((minDuration) => {
				console.log('min duration', minDuration);
				if (minDuration) {
					return {
						minDuration: true,
					};
				}
				return null;
			})
		);
	};
}
