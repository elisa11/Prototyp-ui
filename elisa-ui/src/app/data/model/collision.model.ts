import { CourseSerializerShort, Room } from '@elisa11/elisa-api-angular-client';

export class Collision {
  //TODO: Testovanie
	courses: { course: CourseSerializerShort; time: string }[];
	rooms: { room: Room; time: string };

	// TODO: zamyslieť sa, či boolean má význam
	// courses: { course: CourseSerializerShort; time: boolean[][] }[];
	// rooms: { room: Room; time: boolean[][]};
}
