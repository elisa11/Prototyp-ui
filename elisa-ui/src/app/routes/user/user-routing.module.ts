import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from '../../auth/services/auth.guard';
import { CollectionPageComponent } from './containers/collection-page/collection-page.component';
import { ViewUserPageComponent } from './containers/view-user-page/view-user-page.component';
import { UserExistsGuard } from './guards';
import { NewUserPageComponent } from './containers/new-user-page/new-user-page.component';

const routes: Routes = [
	{
		path: '',
		component: CollectionPageComponent,
		canActivate: [AuthGuard],
	},
  {
    path: 'form',
    component: NewUserPageComponent,
    canActivate: [AuthGuard],
  },
	{
		path: ':id',
		component: ViewUserPageComponent,
		canActivate: [UserExistsGuard, AuthGuard],
	},
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule],
})
export class UserRoutingModule {}
