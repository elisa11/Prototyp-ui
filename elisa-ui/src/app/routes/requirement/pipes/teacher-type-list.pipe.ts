import { Pipe, PipeTransform } from '@angular/core';
import { LocaleService } from '@core/service/locale/locale.service';
import { UserSubjectRole } from '@elisa11/elisa-api-angular-client';
import { marker } from '@biesbjerg/ngx-translate-extract-marker';

@Pipe({
	name: 'teacherTypeList',
})
export class TeacherTypeListPipe implements PipeTransform {
	constructor(private localeService: LocaleService) {}

	transform(teacherTypes: UserSubjectRole[]): string {
		return teacherTypes
			.map((teacherType) =>
				this.localeService.translate(marker(`user.type.${teacherType.name}`))
			)
			.join(', ');
	}
}
