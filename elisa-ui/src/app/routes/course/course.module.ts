import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CourseFormComponent } from './components/course-form/course-form.component';
import { CourseRoutingModule } from './course-routing.module';
import { CourseListComponent } from './components/course-list/course-list.component';
import { CourseDetailComponent } from './components/course-detail/course-detail.component';
import { SharedModule } from '@shared/shared.module';
import { TranslateModule } from '@ngx-translate/core';
import { MATERIAL_MODULES } from './index';
import { CollectionPageComponent } from './containers/collection-page/collection-page.component';
import { ViewCoursePageComponent } from './containers/view-course-page/view-course-page.component';
import { SelectedCoursePageComponent } from './containers/selected-course-page/selected-course-page.component';
import { NewCoursePageComponent } from './containers/new-course-page/new-course-page.component';
import { StoreModule } from '@ngrx/store';
import * as fromCourse from './store/reducers';
import { EffectsModule } from '@ngrx/effects';
import { CourseEffects } from './store/effects';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
	declarations: [
    CourseFormComponent,
    CourseListComponent,
    CourseDetailComponent,
    CollectionPageComponent,
    ViewCoursePageComponent,
    SelectedCoursePageComponent,
    NewCoursePageComponent,
	],
	imports: [
		CommonModule,
		CourseRoutingModule,
		SharedModule,
    ReactiveFormsModule,
    TranslateModule.forChild(),
		...MATERIAL_MODULES,
		StoreModule.forFeature(fromCourse.courseFeatureKey, fromCourse.reducers),
		EffectsModule.forFeature([CourseEffects]),
	],
})
export class CourseModule {}
