import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { SnackEffects } from '@core/generics/snack.effects';
import { resetCachedData } from '@core/store/actions/settings.actions';
import {
	catchError,
	debounceTime,
	filter,
	map,
	skip,
	switchMap,
	takeUntil,
	tap,
	withLatestFrom,
} from 'rxjs/operators';
import { Store } from '@ngrx/store';
import { NGXLogger } from 'ngx-logger';
import { LocaleService } from '@core/service/locale/locale.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import {
	PaginatedGroupList,
	PaginatedStudyPlanList,
	StudyPlansService,
} from '@elisa11/elisa-api-angular-client';
import { GroupsCacheService } from '../../../group/services/groups-cache.service';
import { groupApiActions, searchGroupActions, studyPlansApiActions } from '../actions';
import { schemaSelectors } from '@core/store/selectors';
import { PaginatorFilter } from '@data/filter/paginator.filter';
import { asyncScheduler, of, zip } from 'rxjs';
import { isNotNullOrUndefined } from 'codelyzer/util/isNotNullOrUndefined';
import { marker } from '@biesbjerg/ngx-translate-extract-marker';

const tag = 'GroupEffects';

@Injectable()
export class GroupEffects extends SnackEffects {
	resetData$ = createEffect(
		() =>
			this.actions$.pipe(
				ofType(resetCachedData),
				tap(() => this.service.clearCache())
			),
		{ dispatch: false }
	);
	searchGroups$ = createEffect(
		() =>
			({ debounce = 300, scheduler = asyncScheduler } = {}) =>
				this.actions$.pipe(
					ofType(searchGroupActions.searchGroups),
					debounceTime(debounce, scheduler),
					withLatestFrom(this.store.select(schemaSelectors.selectScheme)),
					map((value) => {
						return {
							search: value[0].search,
							version: value[1].name,
						};
					}),
					switchMap(({ search, version }) => {
						if (search === null || search === undefined || search.length === 0) {
							return of(
								searchGroupActions.searchGroupsSuccess({
									data: { count: 0, results: [] },
								})
							);
						}

						const nextSearch$ = this.actions$.pipe(
							ofType(searchGroupActions.searchGroups),
							skip(1)
						);
						const limit = 25;
						return this.service
							.listGroups({ search, limit } as PaginatorFilter, version)
							.pipe(
								takeUntil(nextSearch$),
								switchMap((value) => {
									const count = value.count;
									if (count > limit) {
										let req = Math.floor(count / limit) - 1;
										req = req + (count % limit > 0 ? 1 : 0);
										return zip(
											of(value),
											...[...Array(req).keys()].map((index) =>
												this.service.listGroups(
													{
														search,
														offset: (1 + index) * limit,
														limit,
													} as PaginatorFilter,
													version
												)
											)
										).pipe(
											map((data) => {
												return {
													count: data[0].count,
													results: data.flatMap((list) => list.results),
												} as PaginatedGroupList;
											})
										);
									} else {
										return of(value);
									}
								}),
								map((data) => searchGroupActions.searchGroupsSuccess({ data })),
								catchError((err) => {
									this.logger.error(tag, 'searchGroups$', err);
									return of(
										searchGroupActions.searchGroupsFailure({ error: err.message })
									);
								})
							);
					})
				)
	);
	searchFailed$ = createEffect(
		() =>
			this.actions$.pipe(
				ofType(searchGroupActions.searchGroupsFailure),
				tap((err) =>
					this.openSnackbar(
						this.localeService.translate(marker('requirement.course.error'))
					)
				)
			),
		{ dispatch: false }
	);
	loadStudyPlan$ = createEffect(() =>
		this.actions$.pipe(
			ofType(studyPlansApiActions.loadStudyPlan),
			filter(({ cid }) => isNotNullOrUndefined(cid)),
			withLatestFrom(this.store.select(schemaSelectors.selectScheme)),
			map((value) => {
				return { cid: value[0].cid, scheme: value[1].name };
			}),
			switchMap(({ cid, scheme }) => {
				this.logger.debug(tag, 'studyPlansService.studyPlansList id', cid);
				const limit = 25;
				return this.studyPlansService
					.studyPlansList(scheme, cid, undefined, limit, 0)
					.pipe(
						switchMap((value) => {
							const count = value.count;
							if (count > limit) {
								let req = Math.floor(count / limit) - 1;
								req = req + (count % limit > 0 ? 1 : 0);
								return zip(
									of(value),
									...[...Array(req).keys()].map((index) =>
										this.studyPlansService.studyPlansList(
											scheme,
											cid,
											undefined,
											limit,
											(1 + index) * limit
										)
									)
								).pipe(
									map((data) => {
										const results = data.flatMap((list) => list.results);
										this.logger.debug(
											tag,
											'studyPlansService.studyPlansList results',
											results
										);
										return {
											count: data[0].count,
											results,
										} as PaginatedStudyPlanList;
									})
								);
							} else {
								return of(value);
							}
						})
					);
			}),
			map((data) => groupApiActions.loadGroupBySP({ ids: data.results.map((d) => d.id) }))
		)
	);
	loadGroupsByCourse$ = createEffect(() =>
		this.actions$.pipe(
			ofType(groupApiActions.loadGroupBySP),
			withLatestFrom(this.store.select(schemaSelectors.selectScheme)),
			map((value) => {
				return { ids: value[0].ids, scheme: value[1].name };
			}),
			switchMap(({ ids, scheme }) => {
				const limit = 25;
				this.logger.debug(tag, 'loadGroupsByCourse$ outer switch ids', ids);
				return zip(
					...ids.map((id) => {
						return this.service
							.listGroups({ limit, offset: 0 } as PaginatorFilter, scheme, undefined, id)
							.pipe(
								map((value) => {
									return { id, list: value };
								}),
								catchError((error) => {
									this.logger.error(tag, 'loadGroupsByCourse$', error);
									return of({ id: undefined, list: undefined } as {
										id: number;
										list: PaginatedGroupList;
									});
								})
							);
					})
				).pipe(
					switchMap((res) => {
						this.logger.debug(tag, 'loadGroupsByCourse$ inner switch', res);
						return zip(
							...res.map(({ id, list }) => {
								const count = list.count;
								if (count > limit) {
									let req = Math.floor(count / limit) - 1;
									req = req + (count % limit > 0 ? 1 : 0);
									return zip(
										of(list),
										...[...Array(req).keys()].map((index) =>
											this.service.listGroups(
												{
													offset: (1 + index) * limit,
													limit,
												} as PaginatorFilter,
												scheme,
												undefined,
												id
											)
										)
									).pipe(
										map((data) => {
											return {
												count: data[0].count,
												results: data.flatMap((d) => d.results),
											} as PaginatedGroupList;
										})
									);
								} else {
									this.logger.debug(tag, 'loadGroupsByCourse$ inner switch else', list);
									return of(list);
								}
							})
						).pipe(
							map((data) => {
								this.logger.debug(
									tag,
									'loadGroupsByCourse$ inner switch map zipped',
									data
								);
								return {
									count: data[0].count,
									results: data.flatMap((list) => list.results),
								} as PaginatedGroupList;
							})
						);
					})
				);
			}),
			map((data) => groupApiActions.loadGroupSuccess({ data })),
			catchError((error) => {
				this.logger.error(tag, 'loadGroupsByCourse$', error);
				return of(groupApiActions.loadGroupFailure({ error }));
			})
		)
	);

	constructor(
		private actions$: Actions,
		private store: Store,
		private logger: NGXLogger,
		private localeService: LocaleService,
		private service: GroupsCacheService,
		private studyPlansService: StudyPlansService,
		snack: MatSnackBar
	) {
		super(snack);
	}
}
