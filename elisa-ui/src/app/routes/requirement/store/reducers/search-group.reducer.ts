import { createReducer, on } from '@ngrx/store';
import { searchGroupActions } from '../actions';

export const searchGroupFeatureKey = 'searchGroup';

export interface State {
	readonly ids: number[];
	readonly loading: boolean;
	readonly error: string;
	readonly query: string;
}

export const initialState: State = {
	ids: [],
	loading: false,
	error: '',
	query: '',
};
export const reducer = createReducer(
	initialState,
	on(searchGroupActions.searchGroups, (state, { search }) => ({
		...state,
		query: search,
		loading: true,
	})),
	on(searchGroupActions.searchGroupsSuccess, (state, { data }) => ({
		...state,
		ids: data.results.map((x) => x.id),
		loading: false,
	})),
	on(searchGroupActions.searchGroupsFailure, (state, { error }) => ({
		...state,
		error: error,
		loading: false,
	}))
);

export const getIds = (state: State) => state.ids;

export const getQuery = (state: State) => state.query;

export const getLoading = (state: State) => state.loading;

export const getError = (state: State) => state.error;
