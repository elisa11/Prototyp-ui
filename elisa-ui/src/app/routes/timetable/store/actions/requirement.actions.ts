import { createAction, props } from '@ngrx/store';
import { PaginatedRequirementList, Requirement } from '@elisa11/elisa-api-angular-client';

export const loadRequirements = createAction(
	'[Timetable - Requirement] Load Requirements'
);

export const loadRequirementsSuccess = createAction(
	'[Timetable - Requirement] Load Requirements Success',
	props<{ data: PaginatedRequirementList }>()
);

export const loadRequirementsFailure = createAction(
	'[Timetable - Requirement] Load Requirements Failure',
	props<{ error: any }>()
);

export const requirementSelected = createAction(
	'[Requirement - Requirement] Requirement Selected',
	props<{ requirement: Requirement }>()
);

export const setSelectedRequirementByCourse = createAction(
	'[Requirement - Requirement] Requirement Selected By Course',
	props<{ courseId: number }>()
);
