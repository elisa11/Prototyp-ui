import { createAction, props } from '@ngrx/store';
import { PaginatedGroupList } from '@elisa11/elisa-api-angular-client';

export const loadGroup = createAction(
	'[Requirement - Group/Api] Load Group',
	props<{ search: string }>()
);

export const loadGroupBySP = createAction(
	'[Requirement - Group/Api] Load Group By Study Plans',
	props<{ ids: number[] }>()
);
export const resetGroupBySP = createAction(
	'[Requirement - Group/Api] Reset Group By Study Plans'
);

export const loadGroupSuccess = createAction(
	'[Requirement - Group/Api] Load Group Success',
	props<{ data: PaginatedGroupList }>()
);

export const loadGroupFailure = createAction(
	'[Requirement - Group/Api] Load Group Failure',
	props<{ error: any }>()
);
