import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import {
	AbstractControl,
	FormBuilder,
	FormGroup,
	FormGroupDirective,
	Validators,
} from '@angular/forms';
import { ActivityCategory, Room, RoomCategory } from '@elisa11/elisa-api-angular-client';
import { debounceTime, takeUntil } from 'rxjs/operators';
import { Subject, Subscription } from 'rxjs';

@Component({
	selector: 'app-room-selection',
	templateUrl: './room-selection.component.html',
})
export class RoomSelectionComponent implements OnInit, OnDestroy {
	@Input()
	readonly!: boolean;
	@Input()
	filteredRooms: Room[];
	@Input() label: ActivityCategory;
	@Input()
	loading: boolean;
	@Input()
	roomTypes: RoomCategory[];
	formGroup: FormGroup;
	controls: { [p: string]: AbstractControl };
	private roomSub: Subscription;
	private typeSub: Subscription;
	private minSub: Subscription;
	private maxSub: Subscription;
	private unsubscribe$ = new Subject();
	@Output() private onFilterChange = new EventEmitter<{
		room: string;
		capacity: { min: number; max: number };
		type: RoomCategory[];
	}>();

	constructor(private fb: FormBuilder) {
		this.formGroup = fb.group({
			room: [undefined, Validators.required],
			type: [[]],
			min: [undefined],
			max: [undefined],
		});

		this.controls = this.formGroup.controls;
	}

	get formData() {
		return this.formGroup.value;
	}

	roomDisplay = (data: Room) => {
		return data && data.name ? data.name : '';
	};

	ngOnInit(): void {
		this.roomSub = this.controls.room.valueChanges
			.pipe(debounceTime(250), takeUntil(this.unsubscribe$))
			.subscribe((room) => {
				this.onFilterChange.emit({
					room,
					capacity: { max: this.formData.max, min: this.formData.min },
					type: this.formData.type,
				});
			});
		this.typeSub = this.controls.type.valueChanges
			.pipe(debounceTime(250), takeUntil(this.unsubscribe$))
			.subscribe((type) => {
				this.onFilterChange.emit({
					room: this.formData.room,
					capacity: { max: this.formData.max, min: this.formData.min },
					type,
				});
			});
		this.minSub = this.controls.min.valueChanges
			.pipe(debounceTime(250), takeUntil(this.unsubscribe$))
			.subscribe((min) => {
				this.onFilterChange.emit({
					room: this.formData.room,
					capacity: { max: this.formData.max, min: min },
					type: this.formData.type,
				});
			});
		this.maxSub = this.controls.max.valueChanges
			.pipe(debounceTime(250), takeUntil(this.unsubscribe$))
			.subscribe((max) => {
				this.onFilterChange.emit({
					room: this.formData.room,
					capacity: { max: max, min: this.formData.min },
					type: this.formData.type,
				});
			});
	}

	ngOnDestroy(): void {
		this.unsubscribe$.next(this);
		this.unsubscribe$.complete();
	}

	resetForm(form: FormGroupDirective) {
		form.resetForm();
	}
}
