package sk.stuba.fei.elisa.elisauiapi.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.Map;

@Getter
@Setter
@Configuration()
@ConfigurationProperties("config")
public class PublicConfig {

    private Map<String, Object> service;
    private Map<String, Object> log;
    private Map<String, Object> param;

}
