import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { DepartmentsCacheService } from '@core/service/cache/departments-cache.service';
import { catchError, map, switchMap, tap, withLatestFrom } from 'rxjs/operators';
import {
	loadDepartmentsFailure,
	loadDepartmentsSuccess,
} from '../actions/department-api.actions';
import { Store } from '@ngrx/store';
import { of } from 'rxjs';
import { NGXLogger } from 'ngx-logger';
import { loadUserDepartmentsSuccess } from '../actions/user-department-api.actions';
import { Departments } from '../reducers/department.reducer';
import { MatSnackBar } from '@angular/material/snack-bar';
import { SnackEffects } from '@core/generics/snack.effects';
import { schemaSelectors } from '@core/store/selectors';

const tag = 'DepartmentEffects';

@Injectable()
export class DepartmentEffects extends SnackEffects {
	loadDepartments$ = createEffect(() =>
		this.actions$.pipe(
			ofType(loadUserDepartmentsSuccess),
			withLatestFrom(this.store.select(schemaSelectors.selectScheme)),
			map((value) => {
				return { data: value[0].data, version: value[1].name };
			}),
			switchMap(({ data, version }) => {
				this.logger.debug(tag, 'loadDepartments$', data);

				if (data && data.id && data.data && data.data.length <= 0) {
					return of(
						loadDepartmentsSuccess({
							departments: { data: [], id: data.id } as Departments,
						})
					);
				}

				return this.service.getDepartmentByUser(version, data.id).pipe(
					map((response) => {
						return { data: response, id: data.id } as Departments;
					}),
					map((departments) => loadDepartmentsSuccess({ departments })),
					catchError((error) => {
						this.logger.error(tag, 'loadDepartments$', error);
						return of(loadDepartmentsFailure({ error }));
					})
				);
			}),
			catchError((error) => {
				this.logger.error(tag, 'loadDepartments$', error);
				return of(loadDepartmentsFailure({ error }));
			})
		)
	);

	loadDepartmentsSuccess$ = createEffect(
		() =>
			this.actions$.pipe(
				ofType(loadDepartmentsSuccess),
				tap((x) => this.logger.debug(tag, 'loadDepartmentsSuccess$', x))
			),
		{ dispatch: false }
	);

	loadDepartmentsFailure$ = createEffect(
		() =>
			this.actions$.pipe(
				ofType(loadDepartmentsFailure),
				tap((err) => this.openSnackbar(err.error))
			),
		{ dispatch: false }
	);

	constructor(
		private actions$: Actions,
		private store: Store,
		private service: DepartmentsCacheService,
		private logger: NGXLogger,
		snack: MatSnackBar
	) {
		super(snack);
	}
}
