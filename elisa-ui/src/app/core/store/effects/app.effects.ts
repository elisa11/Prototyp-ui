import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { map } from 'rxjs/operators';
import { appActions, layoutActions } from '@core/store/actions';
import { authActions } from '../../../auth/store/actions';

@Injectable()
export class AppEffects {
	authInit$ = createEffect(() =>
		this.actions$.pipe(
			ofType(appActions.initApp),
			map((action) => authActions.authInit())
		)
	);
	loadVersion$ = createEffect(() =>
		this.actions$.pipe(
			ofType(appActions.initApp),
			map((action) => layoutActions.loadVersion())
		)
	);

	constructor(private actions$: Actions) {}
}
