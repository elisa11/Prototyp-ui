import { Injectable } from '@angular/core';
import {
	ActivatedRouteSnapshot,
	CanActivate,
	CanLoad,
	Route,
	RouterStateSnapshot,
	UrlSegment,
	UrlTree,
} from '@angular/router';
import { combineLatest, Observable } from 'rxjs';
import { AuthService } from './auth.service';
import { NGXLogger } from 'ngx-logger';
import { Role } from '../store/model/Roles.model';
import { Store } from '@ngrx/store';
import { selectRoleIds } from '../store/selectors/role.selectors';
import { map } from 'rxjs/operators';
import { authRedirect } from '../store/actions/auth.actions';
import { selectLoggedIn } from '../store/selectors/auth.selectors';

const tag = 'RoleGuard';

@Injectable({
	providedIn: 'root',
})
export class RoleGuard implements CanActivate, CanLoad {
	constructor(
		private logger: NGXLogger,
		private authService: AuthService,
		private store: Store
	) {}

	canActivate(
		route: ActivatedRouteSnapshot,
		state: RouterStateSnapshot
	): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
		const expectedRoles = route.data.roles as Role[];
		return combineLatest([
			this.store.select(selectRoleIds),
			this.store.select(selectLoggedIn),
		]).pipe(
			map(([roles, loggedIn]) => {
				return { roles, loggedIn };
			}),
			map((data) => {
				const roles = data.roles;
				const isAuthenticated = data.loggedIn;
				const isAuthorized = expectedRoles.some((role) => roles.includes(role.id));

				if (!isAuthenticated) {
					return false;
				}

				if (isAuthorized) {
					return true;
				} else {
					this.logger.warn(tag, 'Not authorized', state.url);
					this.store.dispatch(
						authRedirect({ url: 'auth/authorize', returnUrl: state.url })
					);
					return false;
				}
			})
		);
	}

	canLoad(
		route: Route,
		segments: UrlSegment[]
	): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
		const expectedRoles = route.data.roles as Role[];

		return combineLatest([
			this.store.select(selectRoleIds),
			this.store.select(selectLoggedIn),
		]).pipe(
			map(([roles, loggedIn]) => {
				return { roles, loggedIn };
			}),
			map((data) => {
				const roles = data.roles;
				const isAuthenticated = data.loggedIn;
				const isAuthorized = expectedRoles.some((role) => roles.includes(role.id));
				const path = segments.map((seg) => seg.path).join('/');

				if (!isAuthenticated) {
					return false;
				}

				if (isAuthorized) {
					return true;
				} else {
					this.logger.warn(tag, 'Not authorized', path);
					this.store.dispatch(authRedirect({ url: 'auth/authorize', returnUrl: path }));
					return false;
				}
			})
		);
	}
}
