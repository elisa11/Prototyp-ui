import { AfterViewChecked, Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CommonText } from '@data/common-text';
import { marker } from '@biesbjerg/ngx-translate-extract-marker';
import { Subscription, timer } from 'rxjs';
import { LocaleService } from '@core/service/locale/locale.service';
import { NGXLogger } from 'ngx-logger';

@Component({
	selector: 'app-authorize',
	templateUrl: './authorize.component.html',
})
export class AuthorizeComponent implements OnInit, AfterViewChecked, OnDestroy {
	link: string;
	msg: string = marker('error.authorize.regular');
	subscription: Subscription;
	private url: string;

	constructor(
		private router: Router,
		private route: ActivatedRoute,
		private localeService: LocaleService,
		private logger: NGXLogger
	) {}

	ngAfterViewChecked(): void {
		const source = timer(4500);
		this.subscription = source.subscribe(() =>
			this.router.navigate(['/login'], { queryParams: { returnUrl: this.url } })
		);
	}

	ngOnDestroy(): void {
		if (this.subscription) {
			this.subscription.unsubscribe();
		}
	}

	ngOnInit(): void {
		this.url = (this.route.snapshot.queryParams.link as string) ?? '/';
		this.logger.debug('AuthorizeComponent', this.url);
		if (this.url.length >= 3) {
			this.url = this.url.slice(0, 3);
			this.link = this.localeService.translate(
				CommonText.navLinks.find((value) => value.link.startsWith(this.url)).label
			);
		} else {
			this.url = '/';
			this.msg = marker('error.authorize.home');
			this.link = this.localeService.translate(marker('error.link.home'));
		}
	}
}
