import { Provider } from '@angular/core';
import { ErrorStateMatcher, ShowOnDirtyErrorStateMatcher } from '@angular/material/core';

export const ErrorStateMatcherProvider: Provider = {
	provide: ErrorStateMatcher,
	useClass: ShowOnDirtyErrorStateMatcher,
};
