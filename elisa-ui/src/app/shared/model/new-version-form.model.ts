import { Period } from '@elisa11/elisa-api-angular-client';

export interface NewVersionFormModel {
	name: string;
	period: Period;
	parent?: Period;
}
