import { createSelector } from '@ngrx/store';
import { selectTimetablesState } from './timetables.selectors';
import * as fromTimetablesEvents from '../reducers/timetables-events.reducer';
import { Event } from '@elisa11/elisa-api-angular-client';

export const selectTimetablesEventState = createSelector(
	selectTimetablesState,
	(s) => s[fromTimetablesEvents.timetablesFeatureKey]
);

export const selectSelectedIds = createSelector(
	selectTimetablesEventState,
	fromTimetablesEvents.getSelectedIds
);
export const selectTotalCount = createSelector(
	selectTimetablesEventState,
	fromTimetablesEvents.getCount
);
export const selectSearchLoading = createSelector(
	selectTimetablesEventState,
	fromTimetablesEvents.getLoading
);

export const { selectIds, selectEntities } = fromTimetablesEvents.adapter.getSelectors(
	selectTimetablesEventState
);

export const selectSelectedResults = createSelector(
	selectEntities,
	selectSelectedIds,
	(data, ids) => {
		return ids.map((id) => data[id]).filter((d): d is Event => d != null);
	}
);
export const selectAllTimetableEvents = createSelector(
	selectEntities,
	selectSelectedIds,
	(data, ids) => {
		return ids.map((id) => data[id]);
	}
);
export const selectTimetableEventsLoading = createSelector(
	selectTimetablesEventState,
	fromTimetablesEvents.getLoading
);

export const selectSearchTimetableEvents = createSelector(
	selectEntities,
	selectSelectedIds,
	(data, ids) => {
		return ids.map((id) => data[id]).filter((e): e is Event => e != null);
	}
);
