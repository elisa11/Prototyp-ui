import { createSelector } from '@ngrx/store';
import { selectCourseEntities, selectCoursesState } from './course.selectors';
import {
	getError,
	getIds,
	getLoading,
	getQuery,
	searchCourseFeatureKey,
} from '../reducers/search-course.reducer';
import { CourseSerializerFull } from '@elisa11/elisa-api-angular-client';

export const selectSearchCourseState = createSelector(
	selectCoursesState,
	(state) => state[searchCourseFeatureKey]
);

export const selectSearchIds = createSelector(selectSearchCourseState, getIds);
export const selectSearchQuery = createSelector(selectSearchCourseState, getQuery);
export const selectSearchLoading = createSelector(selectSearchCourseState, getLoading);
export const selectSearchError = createSelector(selectSearchCourseState, getError);

export const selectSearchResults = createSelector(
	selectCourseEntities,
	selectSearchIds,
	(data, searchIds) => {
		return searchIds
			.map((id) => data[id])
			.filter((entity): entity is CourseSerializerFull => entity != null);
	}
);
