import { MatInputModule } from '@angular/material/input';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatCardModule } from '@angular/material/card';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';

export const MATERIAL_MODULES = [
	MatInputModule,
	MatTooltipModule,
	MatButtonModule,
	MatIconModule,
	MatCardModule,
	MatProgressSpinnerModule,
];
