import { createFeatureSelector, createSelector } from '@ngrx/store';
import * as fromCourse from '../reducers/course.reducer';
import { courseFeatureKey, CourseState } from '../reducers';
import { CourseSerializerFull } from '@elisa11/elisa-api-angular-client';

export const selectCoursesState = createFeatureSelector<CourseState>(courseFeatureKey);

export const selectCourseEntitiesState = createSelector(
	selectCoursesState,
	(state) => state[fromCourse.courseFeatureKey]
);

export const selectSelectedCourseId = createSelector(
	selectCourseEntitiesState,
	fromCourse.getSelectId
);

export const selectCourseCount = createSelector(
	selectCourseEntitiesState,
	fromCourse.getCount
);

export const { selectIds: selectCourseIds, selectEntities: selectCourseEntities } =
	fromCourse.adapter.getSelectors(selectCourseEntitiesState);

export const selectSelectedCourse = createSelector(
	selectCourseEntities,
	selectSelectedCourseId,
	(entities, selectedId) => {
		return selectedId && entities ? entities[selectedId] : ({} as CourseSerializerFull);
	}
);
