import { createFeatureSelector } from '@ngrx/store';
import { sharedFeatureKey, SharedState } from '@shared/store/reducers';

export const selectSharedState = createFeatureSelector<SharedState>(sharedFeatureKey);
