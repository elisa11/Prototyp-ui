import { Component, Input, OnInit, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, FormGroupDirective, Validators } from '@angular/forms';
import { RoomEquipmentModel } from '../../model/room-equipment.model';
import { Paths } from '@shared/store/reducers/form.reducer';
import { Store } from '@ngrx/store';
import { formActions } from '@shared/store/actions';
import { Equipment } from '@elisa11/elisa-api-angular-client';
import { EquipmentModel } from '../../model/equipment.model';

@Component({
  selector: 'app-equipment-selection',
  templateUrl: './equipment-selection.component.html',
})
export class EquipmentSelectionComponent implements OnInit {
  @Input() readOnly: boolean = false;
  formGroup: FormGroup;
  path = Paths.split;
  private equipmentModels: EquipmentModel[] = [];
  facilities: EquipmentModel[] = [];
  private firstImport = true;
  private onChange = new EventEmitter<EquipmentModel[]>();

  constructor(private fb: FormBuilder, private store: Store) {
    this.formGroup = fb.group({
      countF: ['', Validators.compose([Validators.required, Validators.min(1)])],
      facility: ['', Validators.required],
    });
  }

  get equipments(){
    return this.equipmentModels;
  }

  @Input() set equipments(data: EquipmentModel[]) {
    if (this.firstImport) {
      this.equipmentModels = data;
      const selectedFacilities = new Set<EquipmentModel>(data);
      this.facilities = this.facilities.filter((x) => !selectedFacilities.has(x));
      this.onChange.emit(this.equipments);
    }
  }

  ngOnInit(): void {
    this.firstImport = true;
  }

  remove(data: EquipmentModel): void {
    this.facilities = this.facilities.filter((d)=> d.id !== data.id)
    const eq ={
      id: data.id,
      name: data.name
    } as EquipmentModel;
    this.equipments.push(eq);
    this.onChange.emit(this.equipments);
    this.formGroup.reset();
  }

  resetForm(form: FormGroupDirective) {
    form.resetForm();
  }

  add() {
    const formData = this.formGroup.value;
    const facility = formData.facility;
    const countFacility = formData.countF;
    this.facilities.push({
      id: facility.id,
      name:facility.name,
      count: countFacility
    } as EquipmentModel)
    this.equipments = this.equipments.filter((x) => x.id !==facility.id)
    this.onChange.emit(this.equipments);
    this.store.dispatch(formActions.submitFormSuccess({ path: this.path }))
  }
}
