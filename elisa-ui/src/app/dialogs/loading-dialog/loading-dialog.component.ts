import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';

@Component({
	template: ` <div fxLayout="column" fxLayoutAlign="space-between center">
		<mat-spinner></mat-spinner>
		<span class="py-2">{{ data.text }}</span>
	</div>`,
})
export class LoadingDialogComponent {
	constructor(
		private dialogRef: MatDialogRef<LoadingDialogComponent>,
		@Inject(MAT_DIALOG_DATA) public data: { text: string }
	) {}
}
