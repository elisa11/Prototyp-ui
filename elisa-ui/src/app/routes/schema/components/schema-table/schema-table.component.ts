import { Component, EventEmitter, Input, Output } from '@angular/core';
import { VersionModel } from '../../model/version.model';
import { Subscription } from 'rxjs';
import { SelectionModel } from '@angular/cdk/collections';
import { AbstractTableComponent } from '@core/generics/abstract-table.component';
import { ConfirmDialogComponent } from '../../../../dialogs/confirm-dialog/confirm-dialog.component';
import { ConfirmDialogModel } from '../../../../dialogs/confirm-dialog/confirm-dialog.model';
import { deleteSchema } from '../../store/actions/new-schema.actions';
import { MatDialog } from '@angular/material/dialog';
import { Store } from '@ngrx/store';

@Component({
	selector: 'app-schema-table',
	templateUrl: './schema-table.component.html',
})
export class SchemaTableComponent extends AbstractTableComponent<VersionModel> {
	displayedColumns = [
		'select',
		'id',
		'name',
		'status',
		'parent_schema',
		'last_updated',
		'period',
		'btn',
	];

	@Output()
	onSelectionChange: EventEmitter<VersionModel> = new EventEmitter<VersionModel>();
	selection: SelectionModel<VersionModel>;
	@Input()
	private selected: number;
	private selection$: Subscription;
	@Output()
	private onDelete = new EventEmitter<VersionModel>();
	@Output()
	private onManualImport = new EventEmitter<VersionModel>();
  @Output()
  private onUpdateVersion = new EventEmitter<VersionModel>();

	constructor(private dialog: MatDialog,
              private store: Store) {
		super();
		const initialSelection = [];
		const allowMultiSelect = false;
		this.selection = new SelectionModel<VersionModel>(allowMultiSelect, initialSelection);
		this.selection$ = this.selection.changed.subscribe((data) =>
			this.onSelectionChange.emit(data.added[0])
		);
	}

	@Input()
	set data(data: VersionModel[]) {
		this.dataSource.data = data;
		if (this.selected && this.selection) {
			this.selection.select(data.find((value) => value.id === this.selected));
		}
	}
  ngOnInit() { }

  ngOnDestroy(): void {
		if (this.selection$) {
			this.selection$.unsubscribe();
			this.selection$ = null;
		}
	}

	refreshTable(){
    this.refresh();
  }

  //TODO: nahradiť setTimeout niečím lepším, aby sa urobil automatický reload po vymazaní
	deleteSchema(row: VersionModel) {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      data: {
        title: "Vymazanie verzie rozvrhu",
        // title: marker('dialog.import.title'),
        question: "Chcete vymazať verziu rozvrhu?",
        // question: marker('dialog.import.question'),
        noLabel: "Nie",
        // noLabel: marker('dialog.import.no-label'),
        yesLabel: "Áno",
        // yesLabel: marker('dialog.import.yes-label'),
      } as ConfirmDialogModel,
      disableClose: true,
    });
    dialogRef.afterClosed().subscribe(dialogRes =>{
      if (dialogRes){
        this.store.dispatch(deleteSchema({ id: row.id }))
        setTimeout(() =>{this.refreshTable();}, 3000)
      }
    })
	}

	manualImport(row: VersionModel) {
		this.onManualImport.emit(row);
	}

  updateForVersion(row: VersionModel) {
    this.onUpdateVersion.emit(row);
  }
}
