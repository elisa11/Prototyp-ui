import { createAction, props } from '@ngrx/store';
import { PaginatedUserList } from '@elisa11/elisa-api-angular-client';

export const searchUsers = createAction(
	'[Requirement - User] Search Users',
	props<{ search: string }>()
);

export const searchUsersSuccess = createAction(
	'[Requirement - User] Search Users Success',
	props<{ data: PaginatedUserList }>()
);

export const searchUsersFailure = createAction(
	'[Requirement - User] Search Users Failure',
	props<{ error: any }>()
);
