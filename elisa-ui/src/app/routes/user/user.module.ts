import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UserRoutingModule } from './user-routing.module';
import { UserListComponent } from './components/user-list/user-list.component';
import { UserDetailComponent } from './components/user-detail/user-detail.component';
import { SharedModule } from '@shared/shared.module';
import { TranslateModule } from '@ngx-translate/core';
import { MATERIAL_MODULES } from './index';
import { StoreModule } from '@ngrx/store';
import * as fromUser from './store/reducers';
import { SelectedUserPageComponent } from './containers/selected-user-page/selected-user-page.component';
import { ViewUserPageComponent } from './containers/view-user-page/view-user-page.component';
import { EffectsModule } from '@ngrx/effects';
import { DepartmentEffects, UserDepartmentEffects, UserEffects } from './store/effects';
import { CollectionPageComponent } from './containers/collection-page/collection-page.component';
import { ReactiveFormsModule } from '@angular/forms';
import { NewUserPageComponent } from './containers/new-user-page/new-user-page.component';
import { UserFormComponent } from './components/user-form/user-form.component';
import { RoleSelectionComponent } from './components/role-selection/role-selection.component';

@NgModule({
	declarations: [
	  UserFormComponent,
		UserListComponent,
		UserDetailComponent,
		SelectedUserPageComponent,
		ViewUserPageComponent,
		CollectionPageComponent,
    NewUserPageComponent,
    RoleSelectionComponent
	],
	imports: [
		CommonModule,
		SharedModule,
    UserRoutingModule,
    ReactiveFormsModule,
    TranslateModule.forChild(),
    ...MATERIAL_MODULES,
		StoreModule.forFeature(fromUser.usersFeatureKey, fromUser.reducers),
		EffectsModule.forFeature([UserEffects, UserDepartmentEffects, DepartmentEffects]),
	],
})
export class UserModule {}
