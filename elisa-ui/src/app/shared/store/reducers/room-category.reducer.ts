import { createReducer, on } from '@ngrx/store';
import { createEntityAdapter, EntityAdapter, EntityState } from '@ngrx/entity';
import { RoomCategory } from '@elisa11/elisa-api-angular-client';
import { roomCategoryActions } from '@shared/store/actions';

export const roomTypesFeatureKey = 'roomTypes';

export interface State extends EntityState<RoomCategory> {
	count: 0;
	loading: false;
}

export const adapter: EntityAdapter<RoomCategory> = createEntityAdapter<RoomCategory>({
	selectId: (model: RoomCategory) => model.id,
	sortComparer: false,
});

export const initialState = adapter.getInitialState({
	count: 0,
	loading: false,
});

export const reducer = createReducer(
	initialState,
	on(roomCategoryActions.loadRoomCategories, (state) => ({ ...state, loading: true })),
	on(roomCategoryActions.loadRoomCategoriesFailure, (state) => ({
		...state,
		loading: false,
	})),
	on(roomCategoryActions.loadRoomCategoriesSuccess, (state, { data }) =>
		adapter.upsertMany(data.results, { ...state, count: data.count, loading: false })
	)
);

export const getCount = (state) => state.count;
export const getLoading = (state) => state.loading;
