import { Injectable } from '@angular/core';
import {
	HttpErrorResponse,
	HttpEvent,
	HttpHandler,
	HttpInterceptor,
	HttpRequest,
} from '@angular/common/http';
import { combineLatest, Observable, throwError } from 'rxjs';
import { NGXLogger } from 'ngx-logger';
import { environment } from '@environment/environment';
import { catchError, exhaustMap, map, switchMap } from 'rxjs/operators';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { authRedirect } from '../../auth/store/actions/auth.actions';
import { selectLoggedIn, selectToken } from '../../auth/store/selectors/auth.selectors';
import { refreshToken } from '../../auth/store/actions/auth-api.actions';

const tag = 'JwtInterceptor';

@Injectable()
export class JwtInterceptor implements HttpInterceptor {
	constructor(private router: Router, private logger: NGXLogger, private store: Store) {}

	intercept(
		request: HttpRequest<unknown>,
		next: HttpHandler
	): Observable<HttpEvent<unknown>> {
		if (request.url.startsWith(environment.service.elisa.url)) {
			if (request.url.endsWith('/logout') || request.url.endsWith('/login')) {
				return next.handle(request);
			}

			return combineLatest([
				this.store.select(selectToken),
				this.store.select(selectLoggedIn),
			]).pipe(
				map(([token, authenticated]) => {
					return { token, authenticated };
				}),
				exhaustMap((data) => {
					const token = data.token;
					const authenticated = data.authenticated;

					if (authenticated) {
						const req = this.setAuthenticationToken(request, token);

						if (req.url.endsWith('/token/refresh/')) {
							return next.handle(req).pipe(
								catchError((error: HttpErrorResponse) => {
									if (error.status === 401) {
										this.logger.error(tag, 'token refresh failed');
										this.store.dispatch(
											authRedirect({
												url: 'auth/login',
												returnUrl: this.router.routerState.snapshot.url,
											})
										);
										return throwError(error);
									} else {
										this.logger.error(tag, 'Not security error', error);
										return throwError(error);
									}
								})
							);
						}

						return next.handle(req).pipe(
							catchError((error, caught) => {
								if (error instanceof HttpErrorResponse) {
									if (
										error.status &&
										error.status === 401 &&
										error.error &&
										error.error.code === 'token_not_valid'
									) {
										this.logger.error(tag, 'No valid token error', error);

										return this.ifTokenExpired().pipe(
											switchMap((newToken) => {
												return next.handle(
													this.setAuthenticationToken(request, newToken)
												);
											})
										);
									} else {
										return throwError(error);
									}
								}
								return caught;
							})
						);
					}

					return next.handle(request);
				})
			);
		}

		return next.handle(request);
	}

	private ifTokenExpired() {
		this.logger.warn(tag, 'token expired');
		selectToken.release();
		this.store.dispatch(refreshToken());
		return this.store.select(selectToken);
	}

	private setAuthenticationToken(
		request: HttpRequest<unknown>,
		token: string
	): HttpRequest<unknown> {
		return request.clone({
			setHeaders: {
				Authorization: `Bearer ${token}`,
			},
		});
	}
}
