import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { resetCachedData } from '@core/store/actions/settings.actions';
import { catchError, map, switchMap, tap, withLatestFrom } from 'rxjs/operators';
import { of } from 'rxjs';
import { Store } from '@ngrx/store';
import { selectSelectedUserId } from '../selectors/user-department.selectors';
import {
	loadUserDepartments,
	loadUserDepartmentsFailure,
	loadUserDepartmentsSuccess,
} from '../actions/user-department-api.actions';
import { MatSnackBar } from '@angular/material/snack-bar';
import { SnackEffects } from '@core/generics/snack.effects';
import { UserDepartmentsCacheService } from '@core/service/cache/user-departments-cache.service';
import { LocaleService } from '@core/service/locale/locale.service';
import { NGXLogger } from 'ngx-logger';

const tag = 'UserDepartmentEffects';

@Injectable()
export class UserDepartmentEffects extends SnackEffects {
	resetData$ = createEffect(
		() =>
			this.actions$.pipe(
				ofType(resetCachedData),
				tap(() => this.service.clearCache())
			),
		{ dispatch: false }
	);
	// todo filter
	loadUserDepartments$ = createEffect(() =>
		this.actions$.pipe(
			ofType(loadUserDepartments),
			withLatestFrom(this.store.select(selectSelectedUserId)),
			switchMap((data) =>
				this.service.getUserDepartments(data[1], undefined).pipe(
					map((list) =>
						loadUserDepartmentsSuccess({ data: { data: list.results, id: data[1] } })
					),
					catchError((error) => {
						this.logger.error(tag, '', error);
						return of(loadUserDepartmentsFailure({ error }));
					})
				)
			)
		)
	);

	loadUserDepartmentsFailure$ = createEffect(
		() =>
			this.actions$.pipe(
				ofType(loadUserDepartmentsFailure),
				tap((err) => this.openSnackbar(err.error))
			),
		{ dispatch: false }
	);

	constructor(
		private actions$: Actions,
		private store: Store,
		private service: UserDepartmentsCacheService,
		private localeService: LocaleService,
		private logger: NGXLogger,
		snack: MatSnackBar
	) {
		super(snack);
	}
}
