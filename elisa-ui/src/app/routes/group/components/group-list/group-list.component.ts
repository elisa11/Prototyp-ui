import { Component } from '@angular/core';

import { Group } from '@elisa11/elisa-api-angular-client';
import { AbstractTableComponent } from '@core/generics/abstract-table.component';

@Component({
	selector: 'app-group-list',
	templateUrl: './group-list.component.html',
})
export class GroupListComponent extends AbstractTableComponent<Group> {
	displayedColumns = ['id', 'abbr', 'name'];

	constructor() {
		super();
	}
}
