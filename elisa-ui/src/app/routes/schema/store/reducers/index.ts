import * as fromSchema from '../reducers/schema.reducer';
import { Action, combineReducers } from '@ngrx/store';
import * as fromSearchSchema from './search-schema.reducer';
import * as fromPeriod from './period.reducer';
import * as fromSearchPeriod from './search-period.reducer';
import * as fromDepartment from './department.reducer';
import * as fromRoot from '@core/store/reducers';
import * as fromFilterParents from './new-schema.reducer';

export const schemaFeatureKey = 'schema';

export interface SchemaState extends fromRoot.AppState {
	[fromSchema.schemaFeatureKey]: fromSchema.State;
	[fromSearchSchema.searchSchemaFeatureKey]: fromSearchSchema.State;
	[fromPeriod.periodFeatureKey]: fromPeriod.State;
	[fromSearchPeriod.searchPeriodFeatureKey]: fromSearchPeriod.State;
	[fromDepartment.departmentFeatureKey]: fromDepartment.State;
	[fromFilterParents.newSchemaFeatureKey]: fromFilterParents.State;
}

export function reducers(state: SchemaState | undefined, action: Action) {
	return combineReducers({
		[fromPeriod.periodFeatureKey]: fromPeriod.reducer,
		[fromSearchPeriod.searchPeriodFeatureKey]: fromSearchPeriod.reducer,
		[fromSchema.schemaFeatureKey]: fromSchema.reducer,
		[fromSearchSchema.searchSchemaFeatureKey]: fromSearchSchema.reducer,
		[fromDepartment.departmentFeatureKey]: fromDepartment.reducer,
		[fromFilterParents.newSchemaFeatureKey]: fromFilterParents.reducer,
	})(state, action);
}
