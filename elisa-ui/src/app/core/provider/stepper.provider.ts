import { Provider } from '@angular/core';
import { STEPPER_GLOBAL_OPTIONS } from '@angular/cdk/stepper';

export const StepperProvider: Provider = {
	provide: STEPPER_GLOBAL_OPTIONS,
	useValue: { showError: true },
};
