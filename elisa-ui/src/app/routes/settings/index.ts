import { MatExpansionModule } from '@angular/material/expansion';
import { MatSelectModule } from '@angular/material/select';
import { MatInputModule } from '@angular/material/input';

export const MATERIAL_MODULES = [MatExpansionModule, MatSelectModule, MatInputModule];
