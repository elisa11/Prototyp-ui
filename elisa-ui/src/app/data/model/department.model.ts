import { Department as DepartmentDto } from '@elisa11/elisa-api-angular-client';

export class Department {
	// todo delete
	id?: number;
	name: string;
	abbr: string;
	parent?: number;

	public constructor(init?: Partial<DepartmentDto>) {
		Object.assign(this, init);
	}
}
