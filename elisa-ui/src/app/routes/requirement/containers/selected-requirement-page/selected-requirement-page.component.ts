import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { AbstractSelectedPageComponent } from '@core/generics/abstract-selected-page.component';
import { Store } from '@ngrx/store';
import {
  activitySelectors,
  groupSelectors,
  requirementSelectors,
  roomSelectors,
  subjectTeachersSelectors,
  userSelectors
} from '../../store/selectors';
import { RequirementModel } from '../../model/requirement.model';
import { map, tap } from 'rxjs/operators';
import { combineLatest, Observable } from 'rxjs';
import { authSelectors } from '../../../../auth/store/selectors';
import { CommentModel } from '@data/model/comment.model';
import { commentsApiActions, subjectTeachersApiActions } from '../../store/actions';
import { PaginatorFilter } from '@data/filter/paginator.filter';
import {
  activityCategorySelectors,
  studyFormSelectors,
  userSubjectRoleSelectors
} from '@shared/store/selectors';
import { ActivityModel } from '../../model/activity.model';
import { SubjectTeacherModel } from '../../model/subject-teacher.model';
import { isNotNullOrUndefined } from 'codelyzer/util/isNotNullOrUndefined';

@Component({
  selector: 'app-selected-requirement-page',
  template: `
    <app-requirement-detail
      [data]='data$ | async'
      [loggedUserId]='userId | async'
      [pagination]='pagination'
      [activityData]='activityData$ | async'
      (onBack)='back($event)'
      (onNewComment)='onNewComment($event)'
      (onDeleteComment)='onDeleteComment($event)'
      (onEditComment)='onEditComment($event)'
      (onSubjectTeachersFilterChange)='onSubjectTeacherFilterChange($event)'
    ></app-requirement-detail>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SelectedRequirementPageComponent extends AbstractSelectedPageComponent<RequirementModel> {
  userId: Observable<number>;
  @Input()
  rid: number;
  subjectTeachersData$: Observable<SubjectTeacherModel[]>;
  activityData$: Observable<ActivityModel[]>;

  constructor(private store: Store) {
    super();

    this.activityData$ = combineLatest([
      store.select(activitySelectors.selectCourseResults),
      store.select(activityCategorySelectors.selectAll),
      store.select(studyFormSelectors.selectAll)
    ]).pipe(
      map((value) => {
        return { activities: value[0], categories: value[1], forms: value[2] };
      }),
      map((data) =>
        data.activities.map(
          (a) =>
            new ActivityModel(
              a,
              data.categories.find((c) => c.id === a.category),
              data.forms.find((f) => f.id === a.form)
            )
        )
      )
    );

    this.data$ = combineLatest([
      store.select(requirementSelectors.selectSelectedRequirement),
      store.select(userSelectors.selectSelected),
      store.select(activityCategorySelectors.selectAll),
      store.select(roomSelectors.selectSearchResults),
      store.select(groupSelectors.selectSelectedResults),
      store.select(userSubjectRoleSelectors.selectAll)
    ]).pipe(
      map((value) => {
        return {
          requirement: value[0],
          users: value[1],
          category: value[2],
          rooms: value[3],
          groups: value[4],
          roles: value[5]
        };
      }),
      map(({ requirement, users, category, rooms, roles, groups }) => {
        // parse teachers + roles
        const parsedTeacherData = requirement.requirement_teachers.map((d) => {
          return {
            user: users.find((u) => u.id === d.teacher),
            roles: requirement.requirement_teachers
              .filter((fd) => fd.teacher === d.teacher)
              .map((fd) => roles.find((role) => role.id === fd.role))
          } as SubjectTeacherModel;
        });
        const teacherData: SubjectTeacherModel[] = [];
        parsedTeacherData.forEach((d) => {
          if (
            !isNotNullOrUndefined(
              teacherData.find((nd) => nd.user && d.user && nd.user.id === d.user.id)
            )
          ) {
            teacherData.push(d);
          }
        });
        console.log('teacher data', teacherData);
        return new RequirementModel(
          requirement,
          users,
          category,
          teacherData,
          rooms,
          groups
        );
      }),
      tap((x) => console.log('data', x))
    );

    this.userId = store
      .select(authSelectors.selectPayload)
      .pipe(map((value) => value.user_id));
  }

  onNewComment(data: CommentModel) {
    this.store.dispatch(commentsApiActions.addComment({ data }));
  }

  onDeleteComment(cid: number) {
    this.store.dispatch(commentsApiActions.deleteComment({ cid }));
  }

  onEditComment(data: CommentModel) {
    this.store.dispatch(commentsApiActions.editComment({ data }));
  }

  onSubjectTeacherFilterChange(data: PaginatorFilter) {
    this.store.dispatch(subjectTeachersApiActions.loadUserSubjects({ query: data }));
  }
}
