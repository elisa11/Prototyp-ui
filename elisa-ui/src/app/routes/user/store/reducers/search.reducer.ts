import { createReducer, on } from '@ngrx/store';
import { UserApiActions } from '../actions';
import { PaginatorFilter } from '@data/filter/paginator.filter';

export const searchFeatureKey = 'search';

export interface State {
	readonly ids: number[];
	readonly loading: boolean;
	readonly error: string;
	readonly query: PaginatorFilter;
}

export const initialState: State = {
	ids: [],
	loading: false,
	error: '',
	query: {} as PaginatorFilter,
};

export const reducer = createReducer(
	initialState,
	on(UserApiActions.searchUsers, (state, { query }) => {
		return query === null
			? {
					ids: [],
					loading: false,
					error: '',
					query,
			  }
			: {
					...state,
					loading: true,
					error: '',
					query,
			  };
	}),
	on(UserApiActions.searchUsersSuccess, (state, { users }) => ({
		ids: users.results.map((user) => user.id),
		loading: false,
		error: '',
		query: state.query,
	})),
	on(UserApiActions.searchUsersFailure, (state, { errorMsg }) => ({
		...state,
		loading: false,
		error: errorMsg,
	}))
);

export const getIds = (state: State) => state.ids;

export const getQuery = (state: State) => state.query;

export const getLoading = (state: State) => state.loading;

export const getError = (state: State) => state.error;
