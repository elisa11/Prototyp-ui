import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { SnackEffects } from '@core/generics/snack.effects';
import { RoomsCacheService } from '../../../room/services/rooms-cache.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { NGXLogger } from 'ngx-logger';
import { roomActions } from '../actions';
import { asyncScheduler, of, zip } from 'rxjs';
import {
	catchError,
	debounceTime,
	map,
	skip,
	switchMap,
	switchMapTo,
	takeUntil,
	tap,
	withLatestFrom,
} from 'rxjs/operators';
import { schemaSelectors } from '@core/store/selectors';
import { Store } from '@ngrx/store';
import { PaginatorFilter } from '@data/filter/paginator.filter';
import { PaginatedRoomList } from '@elisa11/elisa-api-angular-client';
import { resetCachedData } from '@core/store/actions/settings.actions';

const tag = 'RoomEffects';

@Injectable()
export class RoomEffects extends SnackEffects {
	resetData$ = createEffect(
		() =>
			this.actions$.pipe(
				ofType(resetCachedData),
				tap(() => this.service.clearCache())
			),
		{ dispatch: false }
	);

	searchRoom$ = createEffect(() =>
		this.actions$.pipe(
			ofType(roomActions.loadRooms),
			withLatestFrom(this.store.select(schemaSelectors.selectScheme)),
			map((value) => {
				return {
					search: value[0].search,
					version: value[1].name,
				};
			}),
			switchMap(({ search, version }) => {
				const limit = 25;
				return this.service
					.listRooms(version, undefined, { search, limit } as PaginatorFilter)
					.pipe(
						map((data) => roomActions.loadRoomsSuccess({ data })),
						catchError((err) => {
							this.logger.error(tag, 'searchRoom$', err);
							return of(roomActions.loadRoomsFailure({ error: err.message }));
						})
					);
			}),
			catchError((err) => {
				this.logger.error(tag, 'searchRoom$', err);
				return of(roomActions.loadRoomsFailure({ error: err.message }));
			})
		)
	);

	constructor(
		private actions$: Actions,
		private store: Store,
		private logger: NGXLogger,
		private service: RoomsCacheService,
		snack: MatSnackBar
	) {
		super(snack);
	}
}
