import { createAction, props } from '@ngrx/store';
import { Department } from '@elisa11/elisa-api-angular-client';

export const loadDepartments = createAction(
	'[Schema - Department/Api] Load Departments',
	props<{ ids: number[] }>()
);

export const loadDepartmentsSuccess = createAction(
	'[Schema - Department/Api] Load Departments Success',
	props<{ data: Department[] }>()
);

export const loadDepartmentsFailure = createAction(
	'[Schema - Department/Api] Load Departments Failure',
	props<{ error: any }>()
);
