import * as fromGroup from './group.reducer';
import * as fromRoot from '@core/store/reducers';
import { Action, combineReducers } from '@ngrx/store';
import * as fromSearchGroup from './search-group.reducer';

export const groupsFeatureKey = 'groups';

export interface GroupState extends fromRoot.AppState {
	[fromGroup.groupFeatureKey]: fromGroup.State;
	[fromSearchGroup.searchGroupFeatureKey]: fromSearchGroup.State;
}

export function reducers(state: GroupState | undefined, action: Action) {
	return combineReducers({
		[fromGroup.groupFeatureKey]: fromGroup.reducer,
		[fromSearchGroup.searchGroupFeatureKey]: fromSearchGroup.reducer,
	})(state, action);
}
