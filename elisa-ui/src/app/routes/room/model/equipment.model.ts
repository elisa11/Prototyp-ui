import { Equipment } from '@elisa11/elisa-api-angular-client';

export class EquipmentModel{
  readonly id: number;
  name: string;
  count: number;

  constructor(unit: Equipment){
    Object.assign(this,unit);
  }
}

