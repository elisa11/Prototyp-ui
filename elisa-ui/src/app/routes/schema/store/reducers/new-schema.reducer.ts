import { createReducer, on } from '@ngrx/store';
import { newSchemaActions } from '../actions';

export const newSchemaFeatureKey = 'newSchema';

export interface State {
	readonly periodId: number;
	readonly parentId: number;
	readonly importLoading: boolean;
}

export const initialState: State = {
	periodId: undefined,
	parentId: undefined,
	importLoading: false,
};

export const reducer = createReducer(
	initialState,
	on(newSchemaActions.importSchema, (state: State) => ({
		...state,
		importLoading: true,
	})),
	on(newSchemaActions.selectParent, (state: State, { id }) => ({
		...state,
		parentId: id,
	})),
	on(newSchemaActions.selectPeriod, (state: State, { id }) => ({
		...state,
		periodId: id,
	})),
	on(newSchemaActions.importSchemaDone, () => initialState),
	on(newSchemaActions.importSchemaManual, (state) => ({
		...state,
		importLoading: true,
	})),
	on(newSchemaActions.importSchemaManualDone, (state) => ({
		...state,
		importLoading: false,
	})),
  on(newSchemaActions.updateSchema, (state) => ({
    ...state,
    importLoading: true,
  })),
  on(newSchemaActions.updateSchemaDone, (state) => ({
    ...state,
    importLoading: false,
  }))

);

export const getPeriodId = (state: State) => state.periodId;
export const getParentId = (state: State) => state.parentId;
export const getImportLoading = (state: State) => state.importLoading;
