import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { Settings } from '@data/settings.data';
import { NGXLogger } from 'ngx-logger';

const tag = 'SettingsService';

@Injectable({
	providedIn: 'root',
})
export class SettingsService {
	private readonly settingsLocalKey = 'settings_local';
	private settingsSubject: BehaviorSubject<Settings>;

	constructor(private logger: NGXLogger) {
		this.settingsSubject = new BehaviorSubject<Settings>(this.defaultSettings);
	}

	get settings(): Observable<Settings> {
		return this.settingsSubject.asObservable();
	}

	get defaultSettings(): Settings {
		return new Settings(true, 'sk', 1);
	}

	/**
	 * Nacitanie z localStorage, ak nie su ziadne uloz default
	 */
	reload(): void {
		const storedSettings = JSON.parse(
			localStorage.getItem(this.settingsLocalKey)
		) as Settings;
		this.logger.debug(tag, 'Settings loaded from local storage', storedSettings);
		if (storedSettings) {
			this.settingsSubject.next(storedSettings);
		} else {
			this.save(this.defaultSettings);
			this.reload();
		}
	}

	/**
	 * Nacitanie z DB
	 */
	fetch(userId: number): void {
		this.logger.debug(tag, 'setting from DB storage', userId);
		// todo endpoint pre ukladanie nastaveni
	}

	/**
	 * Ulozenie do DB a localStorage
	 *
	 * @param settings nastavenia
	 */
	save(settings: Settings): void {
		localStorage.setItem(this.settingsLocalKey, JSON.stringify(settings));
		this.logger.debug(tag, 'Settings saved to local storage');

		// todo endpoint pre ukladanie nastaveni
		this.logger.debug(tag, 'Settings saved to db storage');
	}
}
