import { CourseSerializerFull } from '@elisa11/elisa-api-angular-client';
import { ActivityModel } from '../../routes/requirement/model/activity.model';
import { SubjectTeacherModel } from '../../routes/requirement/model/subject-teacher.model';

export interface RequirementFormModel {
	teachers: SubjectTeacherModel[];
	course: CourseSerializerFull;
	split: ActivityModel[];
	message: string;
	groupsNum: number;
	studentsNum: number;
	studentsNumExtra: number;
	maxParallel: number;
	oddWeek: boolean;
	afterLecture: boolean;
	maxSeries: number;
}
