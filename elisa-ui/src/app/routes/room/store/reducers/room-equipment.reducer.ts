import { createReducer, on } from '@ngrx/store';
import { createEntityAdapter, EntityAdapter, EntityState } from '@ngrx/entity';
import { RoomEquipment } from '@elisa11/elisa-api-angular-client';
import { roomEquipmentApiActions, roomActions } from '../actions';

export const roomEquipmentFeatureKey = 'roomEquipment';
export type RoomEquipmentMap = { id: number; data: RoomEquipment[] };

export interface State extends EntityState<RoomEquipmentMap> {
	readonly selectedId: number;
	readonly total: number;
}

export const adapter: EntityAdapter<RoomEquipmentMap> =
	createEntityAdapter<RoomEquipmentMap>({
		selectId: (model: RoomEquipmentMap) => model.id,
		sortComparer: false,
	});
export const initialState: State = adapter.getInitialState({
	selectedId: null,
	total: 0,
});

export const reducer = createReducer(
	initialState,
	on(roomActions.selectRoom, (state: State, { id }) => ({
		...state,
		selectedId: id,
	})),
	on(roomEquipmentApiActions.loadRoomEquipmentSuccess, (state, { id, data }) =>
		adapter.upsertOne({ id, data: data.results } as RoomEquipmentMap, {
			...state,
			total: data.count,
		})
	)
);

export const getSelectedId = (state: State) => state.selectedId;
export const getTotal = (state: State) => state.total;
