import { createReducer, on } from '@ngrx/store';
import { createEntityAdapter, EntityAdapter, EntityState } from '@ngrx/entity';
import { Department } from '@elisa11/elisa-api-angular-client';
import { departmentActions, departmentApiActions } from '../actions';

export const departmentFeatureKey = 'department';

export interface State extends EntityState<Department> {
	readonly selectedId: number;
	readonly count: number;
}

export const adapter: EntityAdapter<Department> = createEntityAdapter<Department>({
	selectId: (model: Department) => model.id,
	sortComparer: false,
});

export const initialState: State = adapter.getInitialState({
	selectedId: null,
	count: 0,
});

export const reducer = createReducer(
	initialState,
	on(departmentApiActions.searchDepartmentSuccess, (state: State, { data }) =>
		adapter.upsertMany(data.results, { ...state, count: data.count })
	),
	on(departmentActions.selectDepartment, (state, { id }) => ({
		...state,
		selectedId: id,
	})),
	on(departmentActions.loadDepartment, (state, { department }) =>
		adapter.upsertOne(department, state)
	)
);
export const getSelectId = (state: State) => state.selectedId;
export const getCount = (state: State) => state.count;
