import { Pipe, PipeTransform } from '@angular/core';
import { EventData } from '@data/model/event.model';

@Pipe({
	name: 'collisionColor',
})
export class CollisionColorPipe implements PipeTransform {
	transform(event: EventData): string {
		const n = event.collisions ? event.collisions.length : 0;
		return n === 0 ? 'green' : 'red';
	}
}
