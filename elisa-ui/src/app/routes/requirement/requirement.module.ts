import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RequirementRoutingModule } from './requirement-routing.module';
import { RequirementListComponent } from './components/requirement-list/requirement-list.component';
import { CommentComponent } from './components/comment/comment.component';
import { CommentListComponent } from './components/comment/comment-list/comment-list.component';
import { TeacherSelectComponent } from './components/teacher-select/teacher-select.component';
import { RequirementFormComponent } from './components/requirement-form/requirement-form.component';
import { SharedModule } from '@shared/shared.module';
import { TranslateModule } from '@ngx-translate/core';
import { NgxMaskModule } from 'ngx-mask';
import { MATERIAL_MODULES, REQ_PIPES } from './index';
import { ReactiveFormsModule } from '@angular/forms';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { EffectsModule } from '@ngrx/effects';
import {
	ActivityEffects,
	CommentsEffects,
	CourseEffects,
	GroupEffects,
	RequirementEffects,
	RoomEffects,
	SubjectTeachersEffects,
	UserEffects,
} from './store/effects';
import { CollectionPageComponent } from './containers/collection-page/collection-page.component';
import { ViewRequirementPageComponent } from './containers/view-requirement-page/view-requirement-page.component';
import { SelectedRequirementPageComponent } from './containers/selected-requirement-page/selected-requirement-page.component';
import { StoreModule } from '@ngrx/store';
import * as fromReq from './store/reducers';
import { NewRequirementPageComponent } from './containers/new-requirement-page/new-requirement-page.component';
import { RoomSelectionComponent } from './components/room-selection/room-selection.component';
import { RequirementDetailComponent } from './components/requirement-detail/requirement-detail.component';
import { ScrollingModule } from '@angular/cdk/scrolling';
import { RoomDetailComponent } from './components/room-detail/room-detail.component';
import { TeacherTableComponent } from './components/teacher-detail/teacher-table.component';
import { ActivitySelectionComponent } from './components/activity-selection/activity-selection.component';
import { GroupSelectComponent } from './components/group-select/group-select.component';
import { ViewGroupPipe } from './pipes/view-group.pipe';
import { TimePickerComponent } from './components/time-picker/time-picker.component';
import { MomentModule } from 'ngx-moment';
import { TeacherFilterPipe } from './pipes/teacher-filter.pipe';
import { RequirementActivitiesPipe } from './pipes/requirement-activities.pipe';

@NgModule({
	declarations: [
		RequirementListComponent,
		CommentComponent,
		CommentListComponent,
		RequirementFormComponent,
		TeacherSelectComponent,
		TimePickerComponent,
		REQ_PIPES,
		CollectionPageComponent,
		ViewRequirementPageComponent,
		SelectedRequirementPageComponent,
		NewRequirementPageComponent,
		RoomSelectionComponent,
		RequirementDetailComponent,
		RoomDetailComponent,
		TeacherTableComponent,
		ActivitySelectionComponent,
		GroupSelectComponent,
		ViewGroupPipe,
		TeacherFilterPipe,
		RequirementActivitiesPipe,
	],
	imports: [
		CommonModule,
		RequirementRoutingModule,
		TranslateModule.forChild(),
		NgxMaskModule.forChild(),
		SharedModule,
		ReactiveFormsModule,
		...MATERIAL_MODULES,
		DragDropModule,
		StoreModule.forFeature(fromReq.requirementFeatureKey, fromReq.reducers),
		EffectsModule.forFeature([
			RequirementEffects,
			CourseEffects,
			RoomEffects,
			GroupEffects,
			CommentsEffects,
			UserEffects,
			SubjectTeachersEffects,
			ActivityEffects,
		]),
		ScrollingModule,
		MomentModule,
	],
})
export class RequirementModule {}
