import {
	Comments,
	CourseSerializerFull,
	Department,
	Group,
	Requirement,
	RequirementActivities,
	RequirementPart,
	RequirementTeachers,
	StateEnum,
	User,
} from '@elisa11/elisa-api-angular-client';
import { TeacherType } from '@data/model/teacher-type.model';

export class RequirementData implements Requirement {
	readonly course: CourseSerializerFull;
	readonly created_at: string;
	readonly created_by: User;
	readonly department: Department;
	exercise_after_lecture: boolean;
	exercise_odd_week: boolean;
	groups_count: number;
	readonly id: number;
	max_serial_courses: number;
	readonly requirement_activities: Array<RequirementActivities>;
	readonly requirement_comments: Array<Comments>;
	readonly requirement_parts: Array<RequirementPart>;
	readonly requirement_teachers: Array<RequirementTeachers>;
	state: StateEnum;
	students_count: number;
	students_count_extra: number;
	readonly updated_at: string;
	max_parallel_courses?: number;
	// state: StateEnum;
	// groups_count: number;
	// students_count: number;
	// max_serial_courses: number;
	// exercise_odd_week: boolean;
	// exercise_after_lecture: boolean;
	// created_by: User;
	// created_at: string;
	// updated_at: string;
	// requirement_parts: RequirementPart[];
	// requirement_comments: Comments[];
	// course: CourseSerializerFull;
	// department: Department;
	// split: string;
	// id: number; // todo: chyba v clientovi
	// updatedAt: Date; // todo: chyba v clientovi
	// teachers: { teacher: User; teacherTypes: TeacherType[] }[]; // todo: volat courses/id/teachers
	// groups: Group[]; // todo: chyba v oracle a inteligentnej STU
	// studentsCountExtra: number; // todo: doplnit do klienta, lebo pocet studentoov moze byt 20 + 4
}
