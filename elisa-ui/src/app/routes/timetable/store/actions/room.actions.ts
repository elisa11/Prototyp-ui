import { createAction, props } from '@ngrx/store';
import { PaginatedRoomList, Room } from '@elisa11/elisa-api-angular-client';

export const loadRooms = createAction(
	'[Timetable - Room] Load Rooms',
	props<{ search: string }>()
);

export const loadRoomsSuccess = createAction(
	'[Timetable - Room] Load Rooms Success',
	props<{ data: PaginatedRoomList }>()
);

export const loadRoomsFailure = createAction(
	'[Timetable - Room] Load Rooms Failure',
	props<{ error: any }>()
);

export const roomSelected = createAction(
	'[Room - Room] Room Selected',
	props<{ Room: Room }>()
);
