import { Pipe, PipeTransform } from '@angular/core';
import { CommonText } from '@data/common-text';
import { StateEnum } from '@elisa11/elisa-api-angular-client';

@Pipe({
	name: 'status',
})
export class StatusPipe implements PipeTransform {
	transform(status: StateEnum, ...args: unknown[]): string {
		return CommonText.StatusValues.find((value) => value.value === status).name ?? '';
	}
}
