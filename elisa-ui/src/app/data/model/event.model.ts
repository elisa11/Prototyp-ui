import { Collision } from './collision.model';
import { GroupData } from './groupData';
import { isNotNullOrUndefined } from 'codelyzer/util/isNotNullOrUndefined';
import {
	CourseSerializerFull,
	DepartmentSerializerShort,
	Event,
	Room,
	User,
} from '@elisa11/elisa-api-angular-client';
import { EventType } from '../common-text';
import { TimeDataUnit } from '../../routes/requirement/model/time-data-unit.model';

export class EventData implements CourseSerializerFull {
	id: number;
	type: EventType;
	teacher: User; // todo zmenit na array, nema cviko aj viacero ucitelov? Alebo drzat ucitela v skupine
	room: Room;

	// TODO: Otázniky zrušiť pri group, startTime,
  //  endTime a requestedTime, je to len kvôli testovaniu
	group?: GroupData;
	day: string;
	startTime?: Date;
	endTime?: Date;
	collisions: Collision[];
	requestedTime?: TimeDataUnit[][];

	code?: string;
	completion?: string;
	credits?: number;
	department: DepartmentSerializerShort;
	name: string;
	timetable: number; // id rozvrhu
	period?: number;

	public constructor(init?: Event) {
		Object.assign(this, init);
		if (isNotNullOrUndefined(init.day)) {
			this.day = init.day;
		}
	}
}
