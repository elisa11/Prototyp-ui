import { createAction, props } from '@ngrx/store';
import { PaginatedRequirementList } from '@elisa11/elisa-api-angular-client';
import { PaginatorFilter } from '@data/filter/paginator.filter';
import { RequirementFilter } from '../../model/requirement-filter.model';
import { RequirementModel } from '../../model/requirement.model';

export const searchRequirement = createAction(
	'[Requirement - Requirement/Api] Search Requirements',
	props<{ query: PaginatorFilter; filter: RequirementFilter }>()
);

export const searchRequirementSuccess = createAction(
	'[Requirement - Requirement/Api] Search Requirements Success',
	props<{ data: PaginatedRequirementList }>()
);

export const searchRequirementFailure = createAction(
	'[Requirement - Requirement/Api] Search Requirements Failure',
	props<{ error: any }>()
);

export const postRequirement = createAction(
	'[Requirement - Requirement/Api] Post Requirement',
	props<{ data: RequirementModel }>()
);

export const postRequirementSuccess = createAction(
	'[Requirement - Requirement/Api] Post Requirement Success'
);

export const postRequirementFailure = createAction(
	'[Requirement - Requirement/Api] Post Requirement Failure',
	props<{ error: any }>()
);

export const putRequirement = createAction(
	'[Requirement - Requirement/Api] Put Requirement',
	props<{ data: RequirementModel }>()
);

export const putRequirementSuccess = createAction(
	'[Requirement - Requirement/Api] Put Requirement Success'
);

export const putRequirementFailure = createAction(
	'[Requirement - Requirement/Api] Put Requirement Failure',
	props<{ error: any }>()
);

export const deleteRequirement = createAction(
	'[Requirement - Requirement/Api] Delete Requirement',
	props<{ id: number }>()
);

export const deleteRequirementDone = createAction(
	'[Requirement - Requirement/Api] Delete Requirement Done',
	props<{ id: number }>()
);

export const deleteRequirementFailed = createAction(
	'[Requirement - Requirement/Api] Delete Requirement Failed',
	props<{ error: any }>()
);
