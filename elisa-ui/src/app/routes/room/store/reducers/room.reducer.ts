import { createReducer, on } from '@ngrx/store';
import { Room } from '@elisa11/elisa-api-angular-client';
import { createEntityAdapter, EntityAdapter, EntityState } from '@ngrx/entity';
import { roomActions, roomApiActions } from '../actions';

export const roomFeatureKey = 'room';

export interface State extends EntityState<Room> {
	readonly selectedId: number;
	readonly count: number;
}

export const adapter: EntityAdapter<Room> = createEntityAdapter<Room>({
	selectId: (model: Room) => model.id,
	sortComparer: false,
});
export const initialState: State = adapter.getInitialState({
	selectedId: null,
	count: 0,
});

export const reducer = createReducer(
	initialState,
	on(roomApiActions.searchRoomSuccess, (state: State, { data }) =>
		adapter.upsertMany(data.results, {
			...state,
			count: data.count,
		})
	),
	on(roomActions.loadRoom, (state, { data }) => adapter.upsertOne(data, state)),
	on(roomActions.selectRoom, (state, { id }) => ({ ...state, selectedId: id })),
  on(roomApiActions.deleteRoomDone, (state, { id }) =>
    adapter.removeOne(id, state))
);

export const getSelectId = (state: State) => state.selectedId;
export const getCount = (state: State) => state.count;
