import { createSelector } from '@ngrx/store';
import { selectDepartmentState } from './department.selectors';
import * as fromUser from '../reducers/user.reducer';

export const selectDepartmentUserState = createSelector(
	selectDepartmentState,
	(s) => s[fromUser.userFeatureKey]
);
export const selectSelectedId = createSelector(
	selectDepartmentUserState,
	fromUser.getSelectedDepartmentId
);

export const { selectIds: selectIds, selectEntities: selectEntities } =
	fromUser.adapter.getSelectors(selectDepartmentUserState);

export const selectUsersByDepartmentId = createSelector(
	selectEntities,
	selectSelectedId,
	(entities, id) => {
		return id && entities[id] ? entities[id].data : [];
	}
);
