import { createReducer, on } from '@ngrx/store';
import { loadRoles, notAuthorized } from '../actions/role.actions';

export const roleFeatureKey = 'role';

export interface State {
	readonly rolesId: number[];
}

export const initialState: State = { rolesId: [] };

export const reducer = createReducer(
	initialState,
	on(loadRoles, (state, payload) => ({ ...state, rolesId: payload.rolesId })),
	on(notAuthorized, (state) => initialState)
);

export const getRoles = (state: State) => state.rolesId;
